function createNQPosFiles(dest,animalID,sessionNum, dioFramePulseChannelName)
%createNQPosFiles(dest,animalID,sessionNum)
%createNQPosFiles(dest,animalID,sessionNum, dioFramePulseChannelName)
%
%This function extracts position tracking information and saves data in the NeuroQuery format.
%It is assumed that there is at least one *.videoPositionTracking file in the current directory. 

%
%The function also tries to separate the data by epochs.  This 
%information is acquired from any .trodesComments files found in the 
%current directory, or by lookinbg for gaps in recording data if there is a '*.time' folder in the current directory
%conaining time information about the session (from
%extractTimeBinaryFile.m).
%
%
%dest -- the directory where the processed files should be saved for the
%animal
%animalID -- a string identifying the animal's id (appended to the
%beginning of the files).
%sessionNum -- the session number (in chronological order for the animal)
%dioFramePulseChannelName (optional) -- the name (ID string) of the digital input channel pulses 
%when each frame occured.  Frame times are recalulated from these pulses.NOT WORKING YET.


tFiles = dir(['*.videoPositionTracking']);
currDir = pwd;
sessionString = getTwoDigitNumber(sessionNum);


if (nargin > 3)
    %A digital channel was given containing frame pulses
    DIOdir = dir('*.DIO');
    if  (length(DIOdir) ~= 1)
        error('One and only one .DIO directory must exist in this folder. Run extractDioBinaryFiles.m');
    end
    [p,n,e,v] = fileparts(DIOdir(1).name);
    cd(DIOdir(1).name);
    targetFile = [n,'.dio_', dioFramePulseChannelName,'.dat'];
    targetFileCheck = dir(targetFile);
    if (isempty(targetFileCheck))
        cd(currDir);
        error([targetFileCheck, ' does not exist in ', p]);
    end
    pulseData = readTrodesExtractedDataFile(targetFile);
    pulseTimes = pulseData.fields(1).data(find(pulseData.fields(2).data == 1));
    if ((length(pulseTimes)>1)&& (pulseData.fields(2).data(1) == 1)) 
        pulseTimes = pulseTimes(2:end);
    end
    disp([num2str(length(pulseTimes)), ' pulses found.']);
    
    cd(currDir);
end 
    

%Get the epoch boundaries.  This assumes that there is at least one
%*.trodesComments file in the current directory with "epoch start" and "epoch end" indicators 
[epochList, fileOffsets] = getEpochs(1);  %assumes that there is at least a 1-second gap in data between epochs if no .trodesComments file is found
%epochs = readTrodesTaskFile();


posMatrix = [];
allTimeStamps = [];
%There may be more than one file in the current directory.  If so, process
%them all, then sort by time.
for fileInd = 1:length(tFiles)
    disp(['Reading file: ',tFiles(fileInd).name]);
    offset = 0;
    tmpFileName = tFiles(fileInd).name;
    dotLoc = strfind(tmpFileName,'.');
    baseName = tmpFileName(1:dotLoc-1);
    tmpPosData = readTrodesExtractedDataFile(tmpFileName);
    
    tmpTimeStamps = readCameraModuleTimeStamps([baseName,'.videoTimeStamps']);
    if isfield(tmpPosData,'clockrate')
        clockrate = tmpPosData.clockrate;
    else
        clockrate = 30000;
    end

    
    for offsetCheck = 1:length(fileOffsets)
        
        if isequal(fileOffsets(offsetCheck).file, [baseName,'.trodesComments']);           
            offset = fileOffsets(offsetCheck).offset;
            disp(['Using offset: ', num2str(offset)]);
        end
    end
    
    tmpTimeStamps = tmpTimeStamps + offset;
    allTimeStamps = [allTimeStamps; tmpTimeStamps];
    
    %Create a matrix to hold the position data from the file
    tmpPosMatrix = zeros(length(tmpPosData.fields(1).data),5);
    tmpPosMatrix(:,1) = double(tmpPosData.fields(1).data + offset)/clockrate;
    for f = 2:length(tmpPosData.fields)
        tmpPosMatrix(:,f) = double(tmpPosData.fields(f).data);
    end
    posMatrix = [posMatrix; tmpPosMatrix];
    
end

%Sort the values by the timestamps.
posMatrix = sortrows(posMatrix,1);

%sort the timestamps
allTimeStamps = sort(allTimeStamps);
disp([num2str(length(allTimeStamps)), ' total frames found.']);

fieldNames = {'time','x1','y1','x2','y2'};

rawpos{sessionNum} = [];
%check if epochs are defined, if so separate by epoch
if (~isempty(epochList))
    for e = 1:size(epochList,1)
        rawpos{sessionNum} = [];
        rawpos{sessionNum}{e} = [];
        epochPosMatrix = posMatrix(find((posMatrix(:,1) >= epochList(e,1)) & (posMatrix(:,1) < epochList(e,2))),:);
        
        for i=1:5
            rawpos{sessionNum}{e} = setfield(rawpos{sessionNum}{e},fieldNames{i},epochPosMatrix(:,i));
        end
        epochString = getTwoDigitNumber(e);
        cd(dest)
        save([animalID,'rawpos',sessionString,'-',epochString], 'rawpos');
        cd(currDir);
    end    
else
    for i=1:5
        rawpos{sessionNum} = setfield(rawpos{sessionNum},fieldNames{i},posMatrix(:,i));
    end
    cd(dest)
    save([animalID,'rawpos',sessionString], 'rawpos');
    cd(currDir);
end


%---------------------------------------------------

function numString = getTwoDigitNumber(input)
    
if (input < 10)
    numString = ['0',num2str(input)];
else
    numString = num2str(input);
end


    
