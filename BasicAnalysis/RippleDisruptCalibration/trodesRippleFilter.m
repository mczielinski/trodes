function out = trodesRippleFilter(filename, channels, downSampledRate, skipTime, varargin)
%out = trodesRippleFilter(filename,channels,downSampledRate)
%out = trodesRippleFilter(filename,channels,downSampledRate,skipTime)
%out = trodesRippleFilter(filename,channels,downSampledRate,skipTime,OPTIONS)
%Reads in the designated channels from a Trodes .rec file
%filename -- the name of the .rec file, i.e., 'myrecording.rec'
%channels -- a list of channels to extract from the file, where each entry
%row lists the nTrode ID, follwed by the channel number (1-based)
%downSampledRate -- rate to downsample data to before filtering
%skipTime (default 0) -- skips reading in the time of each sample
%out -- a structure containing the broadband traces of the channels,timestamps, and some configuration info
%OPTIONS
%configFileName -- if the configuration settings at not at the top of the
%file, you can designate another file which has the config settings.

if (nargin < 4)
    skipTime = 0;
end
contData = readTrodesFileContinuous(filename, channels, skipTime, varargin);
assert(mod(contData.samplingRate, downSampledRate) == 0, ...
        sprintf(['New downsampling %d rate must be divisible by ', ...
        'the data sampling rate %d'], downSampledRate, contData.samplingRate));

% Downsample data and put into a new structure
samplingRateScale = contData.samplingRate / downSampledRate;
downSampledData.channelData = downsample(contData.channelData, samplingRateScale);
downSampledData.timestamps = downsample(contData.timestamps, samplingRateScale);
downSampledData.samplingRate = downSampledRate;

% Copy data information into new structure
downSampledData.headerSize = contData.headerSize;
downSampledData.numChannels = contData.numChannels;

% Ripple filter
[rfilt_b, rfilt_a] = butter(4, [150,250]/(downSampledRate/2));
downSampledData.rippleFilterData = filter(rfilt_b, rfilt_a, downSampledData.channelData);


out = downSampledData;