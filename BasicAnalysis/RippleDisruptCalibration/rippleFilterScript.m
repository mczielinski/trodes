rec_filename = '20151125_templeton_300uV_stim1m.rec';
directory = '/opt/data36/daliu/trodes/templeton/';

%channels = [2,1;2,2;2,3;2,4]
channels = [1,1];
%channels = [1,1;1,2;1,3;1,4;2,1;2,2;2,3;2,4]

rippleFiltData4 = trodesRippleFilter(strcat(directory, rec_filename), channels, 1500, 4);
rippleFiltData8 = trodesRippleFilter(strcat(directory, rec_filename), channels, 1500, 8);

dio = readTrodesFileDigitalChannels(strcat(directory, rec_filename));

%%
plot(rippleFiltData4.timestamps, rippleFiltData4.channelData, 'r');
hold on;
plot(rippleFiltData4.timestamps, rippleFiltData4.rippleFilterData, 'b');
plot(rippleFiltData8.timestamps, rippleFiltData8.rippleFilterData, 'c');
hold off;
