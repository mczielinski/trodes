
#CONFIG += debug
#CONFIG += console
CONFIG += warn_on
QT += opengl widgets xml multimedia multimediawidgets
UI_DIR = ./ui
MOC_DIR = ./moc
OBJECTS_DIR = ./obj

#The TRODES_CODE define is necessary to allow code to be excluded for modules
DEFINES += TRODES_CODE

INCLUDEPATH += src-main
INCLUDEPATH += src-display
INCLUDEPATH += src-config
INCLUDEPATH += src-threads

#stuff specific for mac
macx {
INCLUDEPATH += Libraries/Mac
HEADERS    += Libraries/Mac/WinTypes.h
HEADERS    += Libraries/Mac/ftd2xx.h
LIBS       += /usr/local/lib/libftd2xx.dylib
ICON        = src-main/trodesIcon.icns
OTHER_FILES += \
    src-main/Info.plist
QMAKE_INFO_PLIST += src-main/Info.plist
}


unix:!macx {
INCLUDEPATH += Libraries/Linux
INCLUDEPATH += /usr/local/include  #assuming ftdi lib is in /usr/local/lib
LIBS += -L/usr/local/lib -lftd2xx #assuming ftdi lib is in /usr/local/lib

HEADERS    += Libraries/Linux/WinTypes.h
HEADERS    += Libraries/Linux/ftd2xx.h
}

win32 {
RC_ICONS += trodesIcon.ico
INCLUDEPATH += Libraries/Windows
HEADERS    += ftd2xx.h
#LIBS      += -L./Libraries/Windows/ -lftd2xx
LIBS       += -L$$quote($$PWD/Libraries/Windows) -lftd2xx
}


HEADERS   += src-main/sharedVariables.h \
    src-config/configuration.h \
    src-threads/usbdaqThread.h \
    src-threads/audioThread.h \
    src-main/iirFilter.h \
    src-threads/triggerThread.h \
    src-main/globalObjects.h\
    src-main/mainWindow.h\
    src-threads/streamProcessorThread.h \
    src-threads/recordThread.h \
    src-display/streamDisplay.h \
    src-main/cocoaInitializer.h \
    src-threads/simulateDataThread.h \
    src-main/sourceController.h \
    src-threads/fileSourceThread.h \
    src-display/spikeDisplay.h \
    src-display/dialogs.h \
    src-main/networkMessage.h \
    src-main/trodesSocket.h\
    src-main/customApplication.h \
    src-main/trodesSocketDefines.h \
    src-threads/ethernetSourceThread.h \
    src-main/abstractTrodesSource.h \
    src-threads/simulateSpikesThread.h \
    src-threads/spikeDetectorThread.h \
    src-display/rfDisplay.h




SOURCES   += \
    src-threads/usbdaqThread.cpp \
    src-threads/audioThread.cpp \
    src-main/iirFilter.cpp \
    src-threads/triggerThread.cpp \
    src-config/configuration.cpp\
    src-main/main.cpp\
    src-main/mainWindow.cpp\
    src-threads/streamProcessorThread.cpp \
    src-threads/recordThread.cpp \
    src-display/streamDisplay.cpp \
    src-threads/simulateDataThread.cpp \
    src-main/sourceController.cpp \
    src-threads/fileSourceThread.cpp \
    src-display/spikeDisplay.cpp \
    src-display/dialogs.cpp \
    src-main/trodesSocket.cpp \
    src-main/customApplication.cpp \
    src-threads/ethernetSourceThread.cpp \
    src-main/abstractTrodesSource.cpp \
    src-threads/simulateSpikesThread.cpp \
    src-threads/spikeDetectorThread.cpp \
    src-display/rfDisplay.cpp



OTHER_FILES += \
    src-main/cocoaInitializer.mm


#-------------------------------------------------
# Settings required to use the Intan/OpenEphys interface
#CONFIG += RHYTHMINTERFACE

RHYTHMINTERFACE {
DEFINES += RHYTHM

SOURCES +=     src-threads/rhythmThread.cpp \
    src-threads/rhythm-api/okFrontPanelDLL.cpp \
    src-threads/rhythm-api/rhd2000datablock.cpp \
    src-threads/rhythm-api/rhd2000evalboard.cpp \
    src-threads/rhythm-api/rhd2000registers.cpp
HEADERS +=     src-threads/rhythmThread.h \
    src-threads/rhythm-api/okFrontPanelDLL.h \
    src-threads/rhythm-api/rhd2000datablock.h \
    src-threads/rhythm-api/rhd2000evalboard.h \
    src-threads/rhythm-api/rhd2000registers.h

unix:!macx {
LIBS       += -ldl # needed for dynamic linking to FrontPanel library
}

macx {
copy_opalkelly_lib.commands = "cp $$PWD/Libraries/Mac/libokFrontPanel.dylib $$OUT_PWD/Trodes.app/Contents/MacOS/; \
cp $$PWD/../Resources/Rhythm/main.bit $$OUT_PWD/Trodes.app/Contents/MacOS/"
}
win32 {
copy_opalkelly_lib.commands = "copy $$PWD/Libraries/Windows/okFrontPanel.dll $$OUT_PWD/"
}
unix:!macx {
copy_opalkelly_lib.commands = "cp $$PWD/Libraries/Linux/libokFrontPanel.so $$OUT_PWD/"
}
first.depends = $(first) copy_opalkelly_lib
export(first.depends)
export(copy_opalkelly_lib.commands)
QMAKE_EXTRA_TARGETS += first copy_opalkelly_lib
}
#-------------------------------------------------


