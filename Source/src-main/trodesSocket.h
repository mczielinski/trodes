/*cont
   Trodes is a free, open-source neuroscience data collection and experimental control toolbox

   Copyright (C) 2012 Mattias Karlsson

   This program is free software: you can redistribute it and/or modify
                               it under the terms of the GNU General Public License as published by
                               the Free Software Foundation, either version 3 of the License, or
                               (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifndef TRODESSOCKET_H
#define TRODESSOCKET_H

#include <QtGui>
#include <QtCore>
#include <QObject>
#include <QtNetwork>
#include <QVector>
#include <QImage>
#include <QDialog>
#include <QLabel>
#include <QListWidget>
#include <QSpinBox>
#include <QPushButton>
#include <QGridLayout>
#include <QtXml>
#include "configuration.h"
#include "trodesSocketDefines.h"

/* The following external objects are necessary for parsing the trodes configuration file */
extern GlobalConfiguration *globalConf;
extern HardwareConfiguration *hardwareConf;
extern NTrodeTable *nTrodeTable;
extern streamConfiguration *streamConf;
extern SpikeConfiguration *spikeConf;
extern headerDisplayConfiguration *headerConf;
extern ModuleConfiguration *moduleConf;
extern NetworkConfiguration *networkConf;

//extern int sourceSamplingRate;



class TrodesDataStream : public QDataStream  {
public:
    TrodesDataStream();
    TrodesDataStream(QIODevice *d);
    TrodesDataStream(QByteArray *a, QIODevice::OpenMode mode);
    TrodesDataStream(const QByteArray & a);
};



class DataTypeSpec {
public:
    DataTypeSpec();
    ~DataTypeSpec();


    bool dataTypeSelected();

    // Note that if you add dataTypes to list below, you also need to modify the two DataStream functions above.

    qint8 moduleID;                      // the id of the module sending this structure
    QString hostName;                  // the name of the host on which this is running
    quint16 hostPort;                  // the tcp port from which this module will send data (if relevant)
    qint16  socketType;                 // TCPIP, UDP or LOCAL
    qint16 dataType;                   // a number representing one or  more dataTypes bit-wise or-ed together

    QList<int>              contNTrodeIndexList; // the list of continuous NTrodes available
    QList<int>              spikeNTrodeIndexList; // the list of spike data NTrodes available

    // For UDP sockets, each datatype will have a single socket or a list of sockets.
    // These are only set if dataSocketType is set to UDP in the network section of the configuration file
    QList<quint16>              contNTrodeUDPPortList; // the UDP ports for each continuous NTrode
    QList<quint16>              spikeNTrodeUDPPortList; // the UDP ports for each spike NTrode
    quint16                     digitalIOUDPPort;
    quint16                     analogIOUDPPort;
    quint16                     positionUDPPort;
};

// Define the DataStream << and >> operator to allow us to stream a DataTypeSpec
TrodesDataStream& operator<<(TrodesDataStream& dataStream, const DataTypeSpec& dataTypeSpec);

TrodesDataStream& operator>>(TrodesDataStream& dataStream, DataTypeSpec& dataTypeSpec);


class TrodesMessage : public QObject {
    Q_OBJECT

public:
    TrodesMessage(QObject *parent = 0);
    TrodesMessage(quint8 mType, QByteArray m, QObject *parent);
    ~TrodesMessage();

    qint8 messageType;
    QByteArray message;
};


class DigitalIOSpinBox : public QSpinBox {
    Q_OBJECT

public:
    DigitalIOSpinBox(QWidget *parent = 0, bool input = true);
    ~DigitalIOSpinBox();

public slots:
    void      updateValue(int newPort);

protected:
    const bool input;
    int currentIndex;
    void stepBy(int steps);

};


class NTrodeSelectDialog : public QDialog {
    Q_OBJECT

public:
    explicit NTrodeSelectDialog(const QString &title, QWidget *parent = 0);
    ~NTrodeSelectDialog();
    int loadFromXML(QDomNode &nt);
    void saveToXML(QDomDocument &doc, QDomElement &rootNode, bool continuousData);
    int numberSelected();

private:
    QListWidget *nTrodeSelector;
    QLabel *nSelected;
    QPushButton *done;

private slots:
    void updateSelected(QListWidgetItem *item);
    void updateSelected(void);
    void updateNTrodeList(void);
    void updateNTrodeList(int nTrode, int chan);


signals:
    void nTrodeSelected(qint16 nTrode, bool selected);
};


/*
   TrodesSocketMessageHandler is used by both server and a client objects to
   act as the inferface between the available QT signals and slots and the lower-lever
   socket code.

 */

class TrodesSocketMessageHandler : public QObject {
    Q_OBJECT

public:
    TrodesSocketMessageHandler(QObject *parent = 0);
    ~TrodesSocketMessageHandler();
    void setSocket(QTcpSocket* tcpSocketIn);
    void setSocket(int socketDescriptor);
    bool isConnected();
    bool isDedicatedLine();
    void setDedicatedLine();
    void setDedicatedLine(quint8 dataType);
    bool isModuleDataStreamingOn();
    QString getModuleName();
    bool    hasName();
    void setDecimation(int dec);
    void setSourceSamplingRate(int rate);
    int getDecimation();
    int getNTrodeId();
    int getNTrodeIndex();
    int getNTrodeChan();
    void setSocketType(int sType);
    int getSocketType(void);
    void setRemoteHost(QHostAddress rhost);
    QHostAddress getRemoteHost();
    void setRemotePort(quint16 port);
    quint16 getRemotePort();

    QTcpSocket*     tcpSocket;
    QUdpSocket*     udpSocket;

    QElapsedTimer testTimer;



private slots:

    void disconnectHandler();
    void errorHandler(QAbstractSocket::SocketError socketError);

public slots:

    void readMessage();

    //Slots relating to sending messages

    void sendMessage(quint8 dataType, int message);
    void sendMessage(quint8 dataType, QByteArray message);
    void sendMessage(quint8 dataType); //when there is no message beyond the data type to send
    void sendMessage(TrodesMessage *trodesMessage);
    void sendMessage(quint8 messageType, const char *message, quint32 messageSize);


    void resetSocketForNewThread(TrodesSocketMessageHandler *);
    void resetSocketForNewThread();

    void setModuleID(qint8 ID);
    qint8 getModuleID();
    void setDataType(quint8 dataType, qint16 userData); //used to change the socket to a dedicated data line
    quint8 getDataType(void);
    void sendStartAquisition();
    void sendStopAquisition();
    void setModuleTimePtr(const uint32_t *t);
    void sendTimeRequest();
    void sendCurrentTime(uint32_t);
    void sendTimeRateRequest();
    void sendTimeRate();
    void sendCurrentStateRequest();
    void sendOpenFile(QString);
    void sendCloseFile();
    void sendStartSave();
    void sendStopSave();
    void sendSourceConnect(QString);
    void sendModuleName(QString);
    void sendModuleInstance(int16_t instanceNum);
    void sendCameraImage(const QImage &newImage, int cameraNum = 0); //we pass the images by reference
    void sendContinuousDataPoint(uint32_t t, int16_t dataPoint);
    void sendSpikeData(uint32_t t, int nPoints, int16_t *waveform);
    void sendDigitalIOData(uint32_t t, int port, char input, char value);
    void sendAnimalPosition(uint32_t t, int16_t x, int16_t y, uint8_t cameraNum);

    void sendEvent(uint32_t t, QString eventName); //Can be sent from a module to server AND from server to modules
    void sendEvent(uint32_t t, QString eventName, QString moduleName); //Can be sent from a module to server AND from server to modules

    void sendNewEventNameRequest(QString eventName); //From a module to server
    void sendEventRemoveRequest(QString eventName); //From a module to server
    void sendEventSubscribe(QString eventName, QString moduleName);
    void sendEventUnSubscribe(QString eventName, QString moduleName);
    //void sendEventNameRemoved(QString eventName, QString moduleNam); //From server to modules
    //void sendNewEventCreated(QString eventName); //From server to modules
    void sendEventList(QStringList eventList);
    void requestEventList();
    void sendSubscriptionList(QStringList subsList);
    void requestSubscriptionList();

    void sendSettleCommand();


    void setNTrodeId(int trodeId);
    void setNTrodeIndex(int index);
    void setNTrodeChan(int trodeChan);
    void turnOnDataStreaming();
    void turnOffDataStreaming();

    void readData(quint8 *dataType, uint32_t *dataSize, char *data);
    void sendQuit();

    void sendStateScriptEvent(QString eventString);
    void closeConnection();
//    void sendConfigRequest();

signals:

    //Signals relating to received message content
    void setDataTypeReceived(TrodesSocketMessageHandler *messageHandler, quint8 datatype, qint16 userData);
    void dataProvidedReceived(DataTypeSpec *dp);
    void sendAllDataAvailableReceived(TrodesSocketMessageHandler *messageHandler);
    void messageReceived(quint8, QByteArray);
    void startAquisitionEventReceived();
    void stopAquisitionEventReceived();
    void currentTimeReceived(quint32);
    void timeRateReceived(quint32);
    void openFileEventReceived(QString fileName);
    void sourceConnectEventRecieved(QString source);
    void closeFileEventReceived();
    void startSaveEventReceived();
    void stopSaveEventReceived();
    void moduleIDRequestReceived();

    //For event system
    void eventNameAddReqest(QString eventName, QString moduleName, qint8 modID); //From module to server
    void eventNameRemoveRequest(QString eventName, QString moduleName, qint8 modID); //From module to server
    void eventUpdatesSubscribe(QString eventName, QString moduleName, qint8 requestingModuleID); //From module to server
    void eventUpdatesUnSubscribe(QString eventName, QString moduleName, qint8 requestingModuleID); //From module to server
    void eventListReceived(QStringList list); //From server to module
    void eventListReqested(qint8 modID); //From module to server
    void eventOccurred(uint32_t t, QString eventName, QString moduleName); //Both directions
    void eventSubscriptionListRequested(qint8 modID);
    void eventSubscriptionListReceived(QStringList subList);

    //Settle command
    void settleCommandTriggered();


    void moduleDataStreamOn(bool);

    void cameraImageRecieved(QImage newImage, int cameraNum);
    void finished();

    void moduleIDReceived(qint8);
    void dataAvailableReceived(DataTypeSpec *);
    void sendDataAvailableReceived(TrodesSocketMessageHandler *);
    void configurationReceived(QString);

    void startDataServerReceived(quint16);
    void dataServerStarted();

    void setDecimationReceived(quint16);
    void moduleDataChanUpdated(int, int);

    void stateScriptCommandReceived(QString);
    void stateSciptEventReceived(QString);
    void nameReceived(TrodesSocketMessageHandler *handler, QString name);
    void instanceReveivedFromServer(int instance);


    void socketDisconnected();
    void moduleDisconnected(qint8 ID);
    void socketErrorHappened(QString errorMessage);

    void quitCommandReceived();

    void currentStateRequested(TrodesSocketMessageHandler *);

private:
    qint8 moduleID;           //The number for the module using this message handler
    quint32 inputSize;
    const uint32_t *moduleTime; //a constant pointer to the module's clock


    bool _hasModulename;
    QString _moduleName;

    // definitions used if this is a data connection
    quint8 dataType;
    bool dataTypeSpecific;
    bool moduleDataStreaming;
    int nTrodeId;
    int nTrodeIndex;
    int nTrodeChan;              // used if this is continuous data
    quint16 decimation;          // decimation of 10 means send every 10th point
    int sourceSampRate;
    int socketType;              // TCPIP, UDP or LOCAL
    QHostAddress    remoteHost;
    quint16         remotePort;

    void copyMessageHandlerVar(TrodesSocketMessageHandler *);
};


/*
   TrodesClient objects are used to connect to an already-available TrodesServer object.

 */

class TrodesClient : public TrodesSocketMessageHandler {
    Q_OBJECT

public:
    TrodesClient(QObject *parent = 0);
    TrodesClient(int socketType, QObject *parent = 0);// for UDP sockets
    QStringList findLocalTrodesServers();
    QString getCurrentAddress();
    quint16 getCurrentPort();

    //QTcpSocket* tcpSocket;


private slots:

public slots:
    //Slots relating to socket operation
    void setAddress(QString addressIn); //note that address can be an IP address string or a hostname
    void setPort(quint16 portIn);
    void connectToHost();
    void disconnectFromHost();

signals:
    //signals relating to socket operation
    void connected();
    void disconnected();

private:
    QString address;
    quint16 port;
};


class TrodesServer : public QTcpServer {
    Q_OBJECT

public:
    TrodesServer(QObject *parent = 0);
    QList<QString> findAvailableAddresses();
    ~TrodesServer();

    QString getCurrentAddress();
    quint16 getCurrentPort();

    quint8 nConnections();

    QList<TrodesSocketMessageHandler*> messageHandlers;
    void setModuleTimePtr(const uint32_t *t);
    void setModuleTimeRate(int rate);



private slots:


    void newConnectionRequest();
    void newLocalConnectionRequest();

    void sendModuleID(TrodesSocketMessageHandler *messageHandler, qint8 moduleID);
    void clientDisconnectHandler();
    void removeHandlerThread();
    void setDataTypeForConnection(TrodesSocketMessageHandler *messageHandler, quint8 dataType, qint16 userData);

    void nameReceivedFromModule(TrodesSocketMessageHandler* messageHandler, QString moduleName);

    void tsAddDataAvailable(DataTypeSpec*);
    void tsSendAllDataAvailableToModule(TrodesSocketMessageHandler *messageHandler);
    void sendDataAvailableToModules(DataTypeSpec *dataAvailable);
    void sendModuleDisconnected(qint8 ID);
    void sendStateScript(QString script);

    void sendCurrentStateToModule(TrodesSocketMessageHandler *messageHandler);

    void relaySettleComand();

    void sendEventListToModules();
    void moduleRequstedEventList(qint8 modID);
    void moduleRequestedSubscriptionList(qint8 modID);
    void removeEventTypeFromList(QString eventName, QString moduleName, qint8 modID);
    void addEventTypeToList(QString eventName, QString moduleName, qint8 modID);
    void eventOccurred(uint32_t t, QString eventName, QString moduleName);
    void addListenerToEvent(QString eventName, QString moduleNameOfEvent, qint8 listenerModuleID);
    void removeListenerFromEvent(QString eventName, QString moduleNameOfEvent, qint8 listenerModuleID);

public slots:

    //Slots relating to socket operation
    void setAddress(QString addressIn);
    void startServer(QString identifier, quint16 port = 0); // for the main Trodes server
    void startLocalServer(QString identifier, quint16 port = 0); // for other servers

    //Slots relating to outgoing messages to messaging clients
    void sendMessageToModules(TrodesMessage *trodesMessage);
    void sendMessageToModule(TrodesSocketMessageHandler *messageHandler, TrodesMessage *trodesMessage);
    void sendMessageToModule(qint8 modID, TrodesMessage *trodesMessage);
    void setNamedModuleMessageHandler(TrodesSocketMessageHandler *messageHandler, QString name);
    void sendFileOpened(QString filename);
    void sendSourceConnect(QString source);
    void sendFileClose();
    void sendStartRecord();
    void sendStopRecord();

    void deleteServer();

    void addEventTypeToList(QString eventName);
    void removeEventTypeFromList(QString eventName);
    void eventOccurred(uint32_t t, QString eventName);


    //void disconnectFromHost();

signals:
    // ------- These signals are never emitted - are they needed?
    void modulesReady();
    void moduleConnected();
    void dataServerStarted(); // a signal the modules use to trigger a message send to Trodes
    void newModuleDataRequest(quint8);  // signals that a module has sent a data request; the request is sent out the all the threads and the other modules
    void moduleDataServerStarted(); // the signal trodes uses to terminate a wait loop. We might want to modify this to transmit the module ID
    void newSpikeDataClient(qint8); // the argument is the module number that this will connect to
    void newContDataClient(qint8); // the argument is the module number that this will connect to
    void newHeaderDataClient(qint8); // the argument is the module number that this will connect to

    // ======== Signals relating to socket operation
    void socketErrorHappened(QString errorMessage);
    void moduleIDSent();
    //void clientConnected(TrodesSocketMessageHandler*);
    void clientConnected(); // Only used with local server?!
    void clientDisconnected();

    //void newModuleDataRequest(TrodesSocketMessageHandler*);

    void nameReceived(TrodesSocketMessageHandler *messageHandler, QString name);
    void quitReceived();
    void finished();

    // ========= Signals specific for messaging server
    void dataAvailableReceived(DataTypeSpec *dp);
    void doSendAllDataAvailable(TrodesSocketMessageHandler *messageHandler);  // these are named "do.." because they carry forward signals of similar names from a TrodesMessageHandler
    void doAddDataAvailable(DataTypeSpec *dataAvailable);
    void doRemoveDataAvailable(qint8 ID);

    // ========= Signals relating to new dedicated data line sockets
    void newDataHandler(TrodesSocketMessageHandler*, qint16 userData);
    void moduleDataStreamOn(bool);

    // ========== Signals routed to open messageHandlers
    void signal_send_openfile(QString filename);
    void signal_source_connect(QString source);
    void signal_file_close();
    void signal_start_record();
    void signal_stop_record();

    void trodesServerError(QString);
    void restartCrashedModule(QString modName);

    //===========Signals routed back to Trodes from messageHandlers
    void settleCommandTriggered();

private:

    QString address;
    quint16 port;
    const uint32_t               *moduleTime; //a constant pointer to the module's clock
    QList<QThread*>               messageHandlerThreads;
    qint8 moduleCounter;
    TrodesSocketMessageHandler    *stateScriptMessageHandler;  // a pointer to the stateScript Handler for convenience
    int sourceTimeRate;

    bool state_fileOpen;
    bool state_recording;
    QString state_source;
    QString state_fileName;

    QStringList eventNames;
    QStringList eventOwners;
    QList<qint8> eventModIDs;
    QList<QList<qint8> > eventListenerIDs;

    struct eventDescription {
        QString name;
        int moduleID;
    };

    struct moduleIDNameCombo {
        qint8 moduleID;
        int moduleInstance;
        QString moduleName;
    };

    //Keeps track of all the module names
    QList<moduleIDNameCombo> moduleNames;


};

// Class for direct communication to ECU via MCU.  Used currently only for function go commands to trigger functions
// immediately
class TrodesECUClient : public QObject {
    Q_OBJECT

public:
    TrodesECUClient(QObject *parent = 0);
    ~TrodesECUClient();

    QUdpSocket *udpSocket;
    QHostAddress hardwareAddress;

public slots:
    void sendFunctionGoCommand(qint16 functionNum);
};


// Class for UDP server or client, currently used only for data if dataSocketType is specified as udp in the configuration file
class TrodesUDPSocket: public QObject {
    Q_OBJECT

public:
    TrodesUDPSocket(QString hostAddress = "", QObject *parent = 0);
    ~TrodesUDPSocket();
    QUdpSocket *socket;
    TrodesSocketMessageHandler *newDataHandler(int dataType);
    QString getCurrentAddress();
    quint16 getCurrentPort();
 };



//We will use this to store all module network connections in modules and to store information about the data each module provides in trodes.
class TrodesModuleNetwork : public QObject {
    Q_OBJECT

public:
    TrodesModuleNetwork(QObject *parent = 0);
    ~TrodesModuleNetwork();
    TrodesServer *tcpServer;  // used only on trodes; this is the main trodes server that sends and receives messages from the modules
    TrodesClient  *trodesClient;
    TrodesServer *dataServer;
    TrodesUDPSocket *udpDataServer;
    TrodesECUClient *ecuClient;
    QList<TrodesClient *> dataClient;

    QString moduleName;   // If this is not empty the moduleName will be sent to trodes.
    DataTypeSpec dataProvided;   // the data that this module provides to other modules
    QList <DataTypeSpec> dataAvailable;   // the data available from other modules and trodes
    quint8 dataNeeded;     // the data type(s) this module needs.  This makes it possible for this object to automatically start
                           // the clients necessary to get the data.
    bool useQTSocketsForData;   // true if we want to use the dataClient QT sockets that are part of this structure to get data; defaults to true
    bool trodesClientConnect();
    bool trodesClientConnect(bool sepThread);
    bool trodesClientConnect(QString hostAddress, quint16 hostPort, bool sepThread);
    bool trodesECUClientConnect();
    int  dataSocketType; // the type of sockets to use for data

    QString trodesServerHost;
    quint16 trodesServerPort;
    qint8 moduleID;  // the ID number of this module
    bool dataServerStarted;


public slots:
    void setTcpMessageClientConnected();
    void setModuleID(qint8 ID);
    void addDataAvailable(DataTypeSpec *dp);
    void sendAllDataAvailableToModule(TrodesSocketMessageHandler *messageHandler);
    void removeDataAvailable(qint8 ID);
    void sendStateScript(QString *script);
    void disconnectClient();
    void sendTimeRateRequest();
    void sendModuleName(QString name);
    void sendCurrentStateRequest();



signals:
    void stateScriptFunctionRange(int, int);
    void dataServerCreated();  // modules can now safely connect slots to dataServer
    void startDataClient(DataTypeSpec *DataTypeSpec, int currentDataType);
    void moduleDataChanUpdated(int, int);
    void quitReceived();
    void messageForModule(TrodesSocketMessageHandler *, TrodesMessage *);
    void dataClientStarted();

    void sig_SetDataType(quint8, qint16);
    void sig_disconnectClient();

    void sig_sendMessage(quint8 dataType, int message);
    void sig_sendMessage(quint8 dataType, QByteArray message);
    void sig_sendMessage(quint8 dataType);  //when there is no message beyond the data type to send
    void sig_sendMessage(TrodesMessage *trodesMessage);
    void sig_sendMessage(quint8 messageType, const char *message, quint32 messageSize);
    void sig_sendTimeRateRequest();
    void sig_sendModuleName(QString);
    void sig_sendCurrentStateRequest();


private:
    bool separateThread;
};

#endif // TRODESSOCKET_H
