/*
Trodes is a free, open-source neuroscience data collection and experimental control toolbox

Copyright (C) 2012 Mattias Karlsson

This program is free software: you can redistribute it and/or modify
                               it under the terms of the GNU General Public License as published by
                               the Free Software Foundation, either version 3 of the License, or
                               (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef IIRFILTER_H
#define IIRFILTER_H

#include <QObject>
#include "stdint.h"
#include <QHash>


class ButterworthFilter
{
public:
    ButterworthFilter();
    ~ButterworthFilter();
    int16_t addValue(int16_t value);
    void calcCoef();
    void setFilterRange(int lower, int upper);
    void setSamplingRate(int rate);
    void resetHistory();

private:

    QHash <QString, QHash<int, double> > denominatorHash;
    QHash <QString, double> gainHash;

    int16_t currentValue;
    double inputFreqHz;
    int16_t filterInput_section1[5];
    int64_t filterOutput_section1[5];
    int16_t* outputBuffer;
    int16_t actualOutputValue;

    int64_t ACoef_bandpass[3];  //integer form of the coefficients
    int64_t BCoef_bandpass[3];

    double acof_bandpass[3];  //floating point form

    int32_t bcof_bandpass[3];
    double sf_bandpass;

    int32_t AScaleFactor_bandpass;
    int32_t BScaleFactor_bandpass;
    int32_t DCGain_bandpass;

    int outputBufferSize;
    int outputBufferWritePos;
    quint64 dataWritten;
    int lowerCutoffHz;
    int upperCutoffHz;

    bool isLowPass;


};


class BesselFilter
{
public:
    BesselFilter();
    ~BesselFilter();
    int16_t addValue(int16_t value);
    void calcCoef();
    void setFilterRange(int lower, int upper);
    void setSamplingRate(int rate);
    void resetHistory();

private:

    QHash <QString, QHash<int, double> > aCoefHash;

    QHash <QString, QVector<double> > aCoefHashVect;
    QHash <QString, double> gainHash;

    double inputFreqHz;
    int16_t* outputBuffer;
    int16_t actualOutputValue;

    int outputBufferSize;
    int outputBufferWritePos;
    quint64 dataWritten;
    int lowerCutoffHz;
    int upperCutoffHz;

    double xv[3];
    double yv[5];

    double bcoef[3];
    double acoef[4];
    double gain;

    bool isLowPass;

};

#endif // IIRFILTER_H
