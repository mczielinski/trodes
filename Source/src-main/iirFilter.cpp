/*
Trodes is a free, open-source neuroscience data collection and experimental control toolbox

Copyright (C) 2012 Mattias Karlsson

This program is free software: you can redistribute it and/or modify
                               it under the terms of the GNU General Public License as published by
                               the Free Software Foundation, either version 3 of the License, or
                               (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include "iirFilter.h"
//#include "globalObjects.h"
//#include <stdlib.h>
//#include <stdio.h>
#include <math.h>


#include <QtCore/qmath.h>
#include <QDebug>



//This object is used to filter the data stream


ButterworthFilter::ButterworthFilter()
    : currentValue(0)
    ,outputBufferSize(1024)
    ,outputBufferWritePos(0)
    ,dataWritten(0)
    ,isLowPass(false) {

    //These are the filter coefficients, calculated with Matlab
    //Direct form II, 2nd-order sections, Butterworth bandpass IIR.

    //25kHz sampling rate
    denominatorHash["25000_600_6000"][0] = -0.960539130195126267253158403036650270224;
    denominatorHash["25000_600_6000"][1] = 0.107222236273887383539715756342047825456;

    denominatorHash["25000_500_6000"][0] = -0.972411397490032469725917962932726368308;
    denominatorHash["25000_500_6000"][1] = 0.09452783117928216516379080758269992657;

    denominatorHash["25000_400_6000"][0] = -0.984255491306317398247927030752180144191;
    denominatorHash["25000_400_6000"][1] =  0.081863550537037976795318172662518918514;

    denominatorHash["25000_300_6000"][0] = -0.996075240190273603069215369032463058829;
    denominatorHash["25000_300_6000"][1] =  0.069225300679420315175249811545654665679;

    denominatorHash["25000_200_6000"][0] = -1.007874441613059390832063400011975318193;
    denominatorHash["25000_200_6000"][1] =  0.056609021165738071523776397953042760491;

    denominatorHash["25000_600_5000"][0] = -1.108065141846663825830887617485132068396;
    denominatorHash["25000_600_5000"][1] = 0.236758585155025763624436763166158925742;

    denominatorHash["25000_500_5000"][0] = -1.116560674454310175818250172596890479326;
    denominatorHash["25000_500_5000"][1] =  0.223526482897149159079219771228963509202;

    denominatorHash["25000_400_5000"][0] = -1.12500861176226241155973184504546225071;
    denominatorHash["25000_400_5000"][1] = 0.210368512056528150777623409339867066592;

    denominatorHash["25000_300_5000"][0] = -1.133411999091010757823028143320698291063;
    denominatorHash["25000_300_5000"][1] = 0.197279929435297152195971648325212299824;

    denominatorHash["25000_200_5000"][0] = -1.141773821722097403963402939552906900644;
    denominatorHash["25000_200_5000"][1] = 0.184256085348443843230370475794188678265;

    gainHash["25000_600_6000"] = 0.446388881863056308230142121828976087272;
    gainHash["25000_500_6000"] = 0.452736084410358896601422884486964903772;
    gainHash["25000_400_6000"] = 0.459068224731481011602340913668740540743;
    gainHash["25000_300_6000"] = 0.465387349660289884045738517670542933047;
    gainHash["25000_200_6000"] = 0.471695489417130964238111801023478619754;
    gainHash["25000_600_5000"] = 0.381620707422487104309993810602463781834;
    gainHash["25000_500_5000"] = 0.388236758551425420460390114385518245399;
    gainHash["25000_400_5000"] = 0.394815743971735910733400487515609711409;
    gainHash["25000_300_5000"] = 0.401360035282351368390862944579566828907;
    gainHash["25000_200_5000"] = 0.407871957325778078384814762102905660868;


    //30kHz sampling rate
    denominatorHash["30000_600_6000"][0] = -1.116560674454310175818250172596890479326;
    denominatorHash["30000_600_6000"][1] =  0.223526482897149159079219771228963509202;

    denominatorHash["30000_500_6000"][0] = -1.123603797434477247207951222662813961506;
    denominatorHash["30000_500_6000"][1] =  0.212556561670022015064773768244776874781;

    denominatorHash["30000_400_6000"][0] = -1.130615634516592082903230220836121588945;
    denominatorHash["30000_400_6000"][1] =  0.201635369371541806060577073367312550545;

    denominatorHash["30000_300_6000"][0] = -1.137597921636290587699136267474386841059;
    denominatorHash["30000_300_6000"][1] =  0.190760202218566710108405004575615748763;

    denominatorHash["30000_200_6000"][0] = -1.14455236721697728441426988865714520216;
    denominatorHash["30000_200_6000"][1] =  0.17992839927925943777076156493421876803;

    denominatorHash["30000_600_5000"][0] = -1.242844246084267023633174176211468875408;
    denominatorHash["30000_600_5000"][1] =  0.336537181372023208592025866892072372139;

    denominatorHash["30000_500_5000"][0] = -1.247096540773272144164707242453005164862;
    denominatorHash["30000_500_5000"][1] =  0.324919696232906340949142531826510094106;

    denominatorHash["30000_400_5000"][0] = -1.251319995238413751437178689229767769575;
    denominatorHash["30000_400_5000"][1] =  0.313381004050685307849022365189739502966;

    denominatorHash["30000_300_5000"][0] = -1.255515818994959120757926029909867793322;
    denominatorHash["30000_300_5000"][1] =  0.301917800368199318317152801682823337615;

    denominatorHash["30000_200_5000"][0] = -1.259685193738915076622220112767536193132;
    denominatorHash["30000_200_5000"][1] =  0.290526856731916527376569092666613869369;

    gainHash["30000_600_6000"] = 0.388236758551425420460390114385518245399;
    gainHash["30000_500_6000"] = 0.39372171916498899246761311587761156261;
    gainHash["30000_400_6000"] = 0.399182315314229096969711463316343724728;
    gainHash["30000_300_6000"] = 0.4046198988907167004569487289700191468;
    gainHash["30000_200_6000"] = 0.41003580036037029499240702534734737128;
    gainHash["30000_600_5000"] = 0.33173140931398836794841145092505030334;
    gainHash["30000_500_5000"] = 0.337540151883546857281004349715658463538;
    gainHash["30000_400_5000"] = 0.343309497974657318319913201776216737926;
    gainHash["30000_300_5000"] = 0.349041099815900313085847983529674820602;
    gainHash["30000_200_5000"] = 0.354736571634041708556139838037779554725 ;

    //inputFreqHz = hardwareConf->sourceSamplingRate;
    for (int n = 0; n < 5; n++) {
        filterInput_section1[n] = 0;
        filterOutput_section1[n] = 0;
    }
    outputBuffer = new int16_t[outputBufferSize];
    setFilterRange(600,6000);

}

ButterworthFilter::~ButterworthFilter() {
    delete outputBuffer;
}

void ButterworthFilter::setSamplingRate(int rate) {
    inputFreqHz = rate;
}

void ButterworthFilter::resetHistory() {
    for (int n = 0; n < 5; n++) {
        filterInput_section1[n] = 0;
        filterOutput_section1[n] = 0;
    }
}

int16_t ButterworthFilter::addValue(int16_t value) {
    //This function takes in a new raw value, adds it to the filter history,
    //and returns the filtered value.


    for (int n = 4; n > 0; n--) {
        filterInput_section1[n] = filterInput_section1[n-1];
        filterOutput_section1[n] = filterOutput_section1[n-1];
    }

    filterInput_section1[0] = value;
    filterOutput_section1[0] = BCoef_bandpass[0] * filterInput_section1[0];

    for (int n = 1; n < 3; n++) {
        filterOutput_section1[0] += (BCoef_bandpass[n] * filterInput_section1[n]) - (ACoef_bandpass[n-1] * filterOutput_section1[n]);
    }

    filterOutput_section1[0]  = (filterOutput_section1[0] >> AScaleFactor_bandpass); //divide by the int16 used for fixed-point conversion scaling
    actualOutputValue = filterOutput_section1[0] >> DCGain_bandpass;

    outputBuffer[outputBufferWritePos] = actualOutputValue;
    outputBufferWritePos = (outputBufferWritePos+1) % outputBufferSize;
    dataWritten++;

    return actualOutputValue;

}

void ButterworthFilter::setFilterRange(int lower, int upper) {
    upperCutoffHz = upper;
    lowerCutoffHz = lower;
    calcCoef();
}

void ButterworthFilter::calcCoef() {

    double maxACoef;
    double maxACoef1;
    double maxACoef2;
    double maxBCoef;

    int64_t tmpAScale;
    int64_t tmpBScale;


    //Set the coefficients based on the hash key
    QString filterSettingString;

    filterSettingString.append(QString("%1_").arg(inputFreqHz));
    filterSettingString.append(QString("%1_").arg(lowerCutoffHz));
    filterSettingString.append(QString("%1").arg(upperCutoffHz));

    acof_bandpass[0] = denominatorHash.value(filterSettingString).value(0);
    acof_bandpass[1] = denominatorHash.value(filterSettingString).value(1);
    sf_bandpass = gainHash.value(filterSettingString);

    bcof_bandpass[0] = 1;
    bcof_bandpass[1] = 0;
    bcof_bandpass[2] = -1;

    maxBCoef = ((double) qMax(abs(bcof_bandpass[0]),abs(bcof_bandpass[1])))*sf_bandpass;
    maxACoef1 =  acof_bandpass[0];
    if (maxACoef1 < 0) {
        maxACoef1 *= -1;
    }
    maxACoef2 =  acof_bandpass[1];
    if (maxACoef2 < 0) {
        maxACoef2 *= -1;
    }

    if (maxACoef1 > maxACoef2) {
        maxACoef =  maxACoef1;
    } else{
        maxACoef =  maxACoef2;
    }

    //Find the nearest power of 2 scaling factor that makes the
    //maximum coefficient values come close, but not over,
    //2^25. This is for floating point to fixed point conversion.
    tmpAScale = pow(2,25)/maxACoef;
    tmpBScale = pow(2,25)/maxBCoef;

    for (int i = 0;i<32;i++) {
        if (pow(2,i) <= tmpAScale) {
            AScaleFactor_bandpass = i;
        }
        else {
            break;
        }
    }
    for (int i = 0;i<32;i++) {
        if (pow(2,i) <= tmpBScale) {
            BScaleFactor_bandpass = i;
        }
        else {
            break;
        }
    }
    DCGain_bandpass = BScaleFactor_bandpass-AScaleFactor_bandpass;

    BCoef_bandpass[0] = (int64_t)((( (double) bcof_bandpass[0] * sf_bandpass )) * pow(2,BScaleFactor_bandpass));
    BCoef_bandpass[1] = (int64_t)((( (double) bcof_bandpass[1] * sf_bandpass )) * pow(2,BScaleFactor_bandpass));
    BCoef_bandpass[2] = (int64_t)((( (double) bcof_bandpass[2] * sf_bandpass )) * pow(2,BScaleFactor_bandpass));
    ACoef_bandpass[0] = (int64_t)(acof_bandpass[0] * pow(2,AScaleFactor_bandpass));
    ACoef_bandpass[1] = (int64_t)(acof_bandpass[1] * pow(2,AScaleFactor_bandpass));

}

//----------------------------------------------------------------------------

BesselFilter::BesselFilter()
    :outputBufferSize(1024)
    ,outputBufferWritePos(0)
    ,dataWritten(0)
    ,isLowPass(false){

    //Coefficients were obtained from http://www-users.cs.york.ac.uk/~fisher/mkfilter
    //Bessel bandpass, designed with matched z-transform

    //Low pass filters are 2nd order, Band pass filters are 2nd order

    //25kHz sampling rate
    aCoefHashVect["25000_600_6000"] << -5.02815933e-02	 << 3.48684444e-01	<< -1.38919690e+00	<< 2.07810697e+00;
    gainHash["25000_600_6000"] = 1.36751963;

    aCoefHashVect["25000_500_6000"] << -4.75730559e-02	 << 3.38729900e-01	<< -1.40193286e+00	<< 2.10211093e+00;
    gainHash["25000_500_6000"] = 1.33245599;

    aCoefHashVect["25000_400_6000"] << -4.50104204e-02	 << 3.28850323e-01	<< -1.41513805e+00	<< 2.12584358e+00;
    gainHash["25000_400_6000"] = 1.29875076;

    aCoefHashVect["25000_300_6000"] << -4.25858273e-02	 << 3.19055281e-01	<< -1.42879574e+00	<< 2.14930825e+00;
    gainHash["25000_300_6000"] = 1.26628842;

    aCoefHashVect["25000_200_6000"] << -4.02918406e-02	 << 3.09353642e-01	<< -1.44288954e+00	<< 2.17250821e+00;
    gainHash["25000_200_6000"] = 1.23496107;

    aCoefHashVect["25000_600_5000"] << -8.74759847e-02	 << 5.69019625e-01	<< -1.74058481e+00	<< 2.24784666e+00;
    gainHash["25000_600_5000"] = 1.60543552;

    aCoefHashVect["25000_500_5000"] << -8.27638832e-02	 << 5.52744730e-01	<< -1.74282375e+00	<< 2.26521197e+00;
    gainHash["25000_500_5000"] = 1.55430544;

    aCoefHashVect["25000_400_5000"] << -7.83056102e-02	 << 5.36691196e-01	<< -1.74557249e+00	<< 2.28239258e+00;
    gainHash["25000_400_5000"] = 1.50567341;

    aCoefHashVect["25000_300_5000"] << -7.40874927e-02	 << 5.20865749e-01	<< -1.74881700e+00	<< 2.29939113e+00;
    gainHash["25000_300_5000"] = 1.45929276;

    aCoefHashVect["25000_200_5000"] << -7.00965940e-02	 << 5.05274487e-01	<< -1.75254342e+00	<< 2.31621017e+00;
    gainHash["25000_200_5000"] = 1.41493722;

    aCoefHashVect["25000_0_100"] << -9.46132627e-01	 << 1.94513845e+00;
    gainHash["25000_0_100"] = 1.00586139e+03;

    aCoefHashVect["25000_0_200"] << -8.95166947e-01	 << 1.89129835e+00;
    gainHash["25000_0_200"] = 2.58491728e+02;

    aCoefHashVect["25000_0_300"] << -8.46946655e-01	<< 1.83847820e+00;
    gainHash["25000_0_300"] = 1.18085246e+02;

    aCoefHashVect["25000_0_400"] << -8.01323864e-01	 << 1.78667556e+00;
    gainHash["25000_0_400"] = 6.82672909e+01;

    aCoefHashVect["25000_0_500"] << -7.58158652e-01	 << 1.73588713e+00;
    gainHash["25000_0_500"] = 4.49003913e+01;

    //30kHz sampling rate
    aCoefHashVect["30000_600_6000"] << -8.27638832e-02 << 5.52744730e-01 << -1.74282375e+00	 << 2.26521197e+00;
    gainHash["30000_600_6000"] = 1.55430544;

    aCoefHashVect["30000_500_6000"] << -7.90316183e-02	<<  5.39351119e-01	<< -1.74507956e+00	<<  2.27954186e+00;
    gainHash["30000_500_6000"] = 1.51361600;

    aCoefHashVect["30000_400_6000"] << -7.54676612e-02	<<  5.26115166e-01	<< -1.74768127e+00	<<  2.29374502e+00;
    gainHash["30000_400_6000"] = 1.47451718;

    aCoefHashVect["30000_300_6000"] << -7.20644219e-02	<< 5.13040488e-01	<< -1.75062083e+00	<<  2.30782293e+00;
    gainHash["30000_300_6000"] = 1.43687527;

    aCoefHashVect["30000_200_6000"] << -6.88146528e-02	<< 5.00130406e-01	<< -1.75389027e+00	<<  2.32177706e+00;
    gainHash["30000_200_6000"] = 1.40056555;

    aCoefHashVect["30000_600_5000"] << -1.31292736e-01	 << 8.11261017e-01	<< -2.13366443e+00	<< 2.44719019e+00;
    gainHash["30000_600_5000"] = 1.89565447;

    aCoefHashVect["30000_500_5000"] << -1.25372047e-01	 << 7.91119608e-01	<< -2.12566535e+00	 << 2.45547481e+00;
    gainHash["30000_500_5000"] = 1.83293842;

    aCoefHashVect["30000_400_5000"] << -1.19718353e-01	 << 7.71300258e-01	<< -2.11808631e+00	 << 2.46370798e+00;
    gainHash["30000_400_5000"] = 1.77333585;

    aCoefHashVect["30000_300_5000"] << -1.14319614e-01	 << 7.51802234e-01	<< -2.11092035e+00	 << 2.47189069e+00;
    gainHash["30000_300_5000"] = 1.71654394;

    aCoefHashVect["30000_200_5000"] << -1.09164333e-01	 << 7.32624654e-01	<< -2.10416047e+00	 << 2.48002389e+00;
    gainHash["30000_200_5000"] = 1.66228545;

    aCoefHashVect["30000_0_100"] << -9.54904667e-01	<< 1.95421109e+00;
    gainHash["30000_0_100"] = 1.44179090e+03;

    aCoefHashVect["30000_0_200"] << -9.11842923e-01	<< 1.90913163e+0;
    gainHash["30000_0_200"] = 3.68827887e+02;

    aCoefHashVect["30000_0_300"] << -8.70723063e-01	 << 1.86476091e+00;
    gainHash["30000_0_300"] = 1.67724692e+02;

    aCoefHashVect["30000_0_400"] << -8.31457516e-01	 << 1.821097740e+00;
    gainHash["30000_0_400"] = 9.65271770e+01;

    aCoefHashVect["30000_0_500"] << -7.93962663e-01	 << 1.778140493e+00;
    gainHash["30000_0_500"] = 6.32024557e+01;

    aCoefHashVect["1000_0_100"] << -2.50495817e-01 << 9.22123218e-01;
    gainHash["1000_0_300"] = 3.04532109e+00;

    aCoefHashVect["1000_0_200"] << -6.27481541e-02 << 3.49319595e-01;
    gainHash["1000_0_300"] = 1.40168204e+00;

    aCoefHashVect["1000_0_300"] << -1.57181501e-02 << 9.11277009e-02;
    gainHash["1000_0_300"] = 1.081559950e+00;


    //inputFreqHz = hardwareConf->sourceSamplingRate;
    outputBuffer = new int16_t[outputBufferSize];
    setSamplingRate(30000);
    setFilterRange(600,6000);

    //Initialize history to 0
    for (int i=0;i<3;i++) {
        xv[i] = 0;
    }
    for (int i=0;i<5;i++) {
        yv[i] = 0;
    }

}

BesselFilter::~BesselFilter() {
    delete outputBuffer;
}

void BesselFilter::setSamplingRate(int rate) {
    inputFreqHz = rate;
}

void BesselFilter::resetHistory() {
    //Initialize history to 0
    for (int i=0;i<3;i++) {
        xv[i] = 0;
    }
    for (int i=0;i<5;i++) {
        yv[i] = 0;
    }
}

int16_t BesselFilter::addValue(int16_t value) {
    //This function takes in a new raw value, adds it to the filter history,
    //and returns the filtered value.

    if (isLowPass) {
        //2nd order lowpass has two poles
        xv[0] = (double) value / gain;
                yv[0] = yv[1]; yv[1] = yv[2];
                yv[2] =   bcoef[0]*xv[0]
                             + ( acoef[0] * yv[0]) + (  acoef[1] * yv[1]);
                actualOutputValue = (int16_t) yv[2];

    } else {
        //2nd order bandpass has 4 poles
        xv[0] = xv[1]; xv[1] = xv[2];
        xv[2] = (double) value / gain;
        yv[0] = yv[1]; yv[1] = yv[2]; yv[2] = yv[3]; yv[3] = yv[4];
        yv[4] =   (bcoef[0]*xv[0] + bcoef[1]*xv[1] + bcoef[2]*xv[2])
                         + ( acoef[0] * yv[0]) + (  acoef[1] * yv[1])
                         + ( acoef[2] * yv[2]) + (  acoef[3] * yv[3]);
        actualOutputValue = (int16_t) yv[4];
    }

    outputBuffer[outputBufferWritePos] = actualOutputValue;
    outputBufferWritePos = (outputBufferWritePos+1) % outputBufferSize;
    dataWritten++;

    return actualOutputValue;

}

void BesselFilter::setFilterRange(int lower, int upper) {
    upperCutoffHz = upper;
    lowerCutoffHz = lower;
    calcCoef();
    //TODO error checking in case filter range is invalid
}

void BesselFilter::calcCoef() {


    if (lowerCutoffHz ==0) {
        isLowPass = true;
    } else {
        isLowPass = false;
    }
    //Set the coefficients based on the hash key
    QString filterSettingString;

    filterSettingString.append(QString("%1_").arg(inputFreqHz));
    filterSettingString.append(QString("%1_").arg(lowerCutoffHz));
    filterSettingString.append(QString("%1").arg(upperCutoffHz));

    if (!aCoefHashVect.contains(filterSettingString)) {
        qDebug() << "ERROR: BesselFilter does not have filters for " << filterSettingString;
    }
    for (int i=0; i < aCoefHashVect.value(filterSettingString).length(); i++) {

        acoef[i] = aCoefHashVect.value(filterSettingString).value(i);
    }

    /*
    acoef[0] = aCoefHashVect.value(filterSettingString).value(0);
    acoef[1] = aCoefHashVect.value(filterSettingString).value(1);
    acoef[2] = aCoefHashVect.value(filterSettingString).value(2);
    acoef[3] = aCoefHashVect.value(filterSettingString).value(3);
    */

    gain = gainHash.value(filterSettingString);

    //qDebug() << "New filter values: " << acoef[0] << acoef[1] << acoef[2] << acoef[3];


    bcoef[0] = 1.0;
    bcoef[1] = -2.0;
    bcoef[2] = 1.0;


}


