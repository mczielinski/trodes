/*
Trodes is a free, open-source neuroscience data collection and experimental control toolbox

Copyright (C) 2012 Mattias Karlsson

This program is free software: you can redistribute it and/or modify
                               it under the terms of the GNU General Public License as published by
                               the Free Software Foundation, either version 3 of the License, or
                               (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef CONFIGURATION_H
#define CONFIGURATION_H

#include <QObject>
//#include <QWidget>
#include <QColor>
#include <QDomNode>
#include <QXmlStreamWriter>
//#include "sharedVariables.h"

#ifdef TRODES_CODE
#include "iirFilter.h"
#endif

//This this the value that defines the uV step per LSB
#define AD_CONVERSION_FACTOR 12780  //uV =  Input value*(12780/65535)
//#define AD_CONVERSION_FACTOR 12500

//#define DIGITALTYPE 0
//#define INT16TYPE   1

#define MAX_HARDWARE_CHANNELS 3084  // just a guess; this will have to be changed if we ever go to higher channel counts
#define MAX_CHAN_PER_NTRODE 1024
#define POINTSINWAVEFORM 40
#define MAX_SPIKE_POINTS (POINTSINWAVEFORM * MAX_CHAN_PER_NTRODE)


class GlobalConfiguration : public QObject
{
    Q_OBJECT
public:
    GlobalConfiguration(QObject *parent);
    int loadFromXML(QDomNode &globalConfNode);
    void saveToXML(QDomDocument &doc, QDomElement &rootNode);

    QString filePrefix;
    QString filePath;

    bool saveDisplayedChanOnly;  // save only the displayed data (note that all Digital IO channels are always saved
    bool realTimeMode;

};

struct DeviceChannel {
    QString idString;
    enum {DIGITALTYPE, INT16TYPE} dataType;
    int startByte;
    int digitalBit;
    int port;
    int interleavedDataIDByte;
    int interleavedDataIDBit;
    bool input;
};

struct DeviceInfo {
    QString name;
    int packetOrderPreference;
    int numBytes;
    bool available;
    QList<DeviceChannel> channels;
    int byteOffset;
};

class HardwareConfiguration : public QObject
{
    Q_OBJECT
public:
    HardwareConfiguration(QObject *parent);
    //~HardwareConfiguration();
    int loadFromXML(QDomNode &moduleConfNode);
    void saveToXML(QDomDocument &doc, QDomElement &rootNode);
    int NCHAN;  // Number of configured hardware channels
    int sourceSamplingRate; // sampling rate of Intan chips
    int headerSize; // number of bytes in header for this hardware
    bool ECUConnected;
    bool headerSizeManuallyDefined;
    QList<DeviceInfo> devices;

};



// The module configuration specifies which modules are launched and which need to exchange data
class SingleModuleConf
{
public:
     QString    moduleName; // the program name with path (if not located in the same dir as Trodes)
     int        sendTrodesConfig; //if 1, the path to the config file is sent in the arguments
     int        sendNetworkInfo;//if 1, the address and port for the trodes server are sent
     QString    hostName; // the name of the host on which this module is run;
     QStringList moduleArguments; //the command line argument list for the module
};


class ModuleConfiguration : public QObject
{
  Q_OBJECT
public:
    ModuleConfiguration(QObject *parent);
    ~ModuleConfiguration();
    int loadFromXML(QDomNode &moduleConfNode);
    void saveToXML(QDomDocument &doc, QDomElement &rootNode);
    bool modulesDefined;
    qint8 myID;  // The number for this program (-1 for trodes, 0 - n for modules)
    QString trodesConfigFileName;
    QList<SingleModuleConf> singleModuleConf;

    bool       modulePresent(QString modName);
    int        findModule(QString modName);


};

class NetworkConfiguration : public QObject
{
    Q_OBJECT
public:
    NetworkConfiguration();
    ~NetworkConfiguration();
    int loadFromXML(QDomNode &moduleConfNode);
    void saveToXML(QDomDocument &doc, QDomElement &rootNode);

    QString trodesHost;  // the hostname of the machine running trodes
    quint16 trodesPort;  // the port number for the trodes server that all modules connect to

    QString hardwareAddress;  // the broadcast address for the network that the hardware is attached to
    quint16 hardwarePort;  // the port number to communicate with the hardware.  This is fixed.
    quint16 ecuDirectPort;  // the port number to communicate to the ECU via the MCU.  This is fixed


    qint32  dataSocketType; // TCPIP, UDP or LOCAL
    qint8   myModuleID;  // the ID for this module
    bool    networkConfigFound;

    qint8 numModulesConnected;

};


class NTrode
{
public:
   int number;
   QList<int> hw_chan;
};

class NTrodeTable : public QObject
{
  Q_OBJECT
public:
  QList<NTrode *> ntrodes;
  NTrodeTable() {}

};


class streamConfiguration : public QObject
{
  Q_OBJECT
public:
  int nColumns;
  double tLength;
  double FS;
  QColor backgroundColor;

  // These three lists are each the length of the total number of channels in the config file
  // (i.e., Sum-over-ntrodes of numChannels-per-nTrode)
  QList<int> trodeIndexLookup; // This list is the nTrode index by channel
  QList <int> trodeNumberLookup; // This list is the nTrode ID by channel
  QList<int> trodeChannelLookup; // This list is the channelNumber by channel (i.e., which channel of ntrode)
  QList<int> trodeIndexLookupByHWChan;
  QList<int> trodeChannelLookupByHWChan;

  int nChanConfigured; // the total number of configured channels;

  //bool saveOnlyDisplayedChannels;
  bool *saveHWChan;  // true for hardware channels that are displayed

#ifdef TRODES_CODE
  // We only include the filters if we are compiling this within TRODES.
  //ButterworthFilter* dataFilters;
  BesselFilter *dataFilters;

#endif


  streamConfiguration();
  ~streamConfiguration();

  int loadFromXML(QDomNode &eegDispConfNode);
  void saveToXML(QDomDocument &doc, QDomElement &rootNode);
  void setChanToSave();
  void listChanToSave(); // for debugging


public slots:
  void setTLength(double newTLength);
  void setBackgroundColor(QColor c);

signals:
  void updatedTLength(double);
  void updatedBackgroundColor(QColor c);
};


//The header portion of each packet can contain extra information, such as digital inputs and DC-coupled analog inputs
class headerChannel
{
public:
   QString idString; //what is displayed (doesn't have to be a number)
   int dataType;
   bool input; // true for inputs, false for outputs
   bool storeStateChanges;
   int port; // the statescript port number
   int startByte;
   int digitalBit;
   int interleavedDataIDByte;
   int interleavedDataIDBit;
   QColor color;
   int maxDisp;
   QString deviceName;
};

class headerDisplayConfiguration : public QObject {
    Q_OBJECT
public:
    headerDisplayConfiguration(QObject *parent);
    ~headerDisplayConfiguration();
    int loadFromXML(QDomNode &headerConfNode);
    void saveToXML(QDomDocument &doc, QDomElement &rootNode);
    QList<headerChannel> headerChannels;

    int maxDigitalPort(bool input);
    int minDigitalPort(bool input);
    bool digitalPortValid(int port, bool input);
    bool digitalIDValid(QString ID, bool input);

    QStringList digInIDList;
    QList<int> digInPortList;
    QStringList digOutIDList;
    QList<int> digOutPortList;

};


class SingleSpikeTrodeConf
{
public:
  int nTrodeId; // arbitrary ID
  int refNTrode; // default -1 = ground
  int refChan; // default 0
  int lowFilter;
  int highFilter;
  int moduleDataChan;
  int moduleDataHighFilter;
  bool refOn;
  bool filterOn;
  bool moduleDataOn;

  QColor color;
  QList<int> maxDisp;
  QList<int> thresh;
  QList<int> hw_chan;
  QList<int> unconverted_hw_chan;  //hw channel actually come in interleaved, so we need to convert from the non-interleaved numbers
  QList<int> thresh_rangeconvert;
  QList<int> streamingChannelLookup;
  QList<bool> triggerOn;
};


class SpikeConfiguration : public QObject
{
  Q_OBJECT
public:
  QList<SingleSpikeTrodeConf *> ntrodes;

  SpikeConfiguration(QObject *parent, int numberOfDisplayedNTrodes) : QObject(parent) {
    for (int i = 0; i < numberOfDisplayedNTrodes; i++) {
      ntrodes.append(new SingleSpikeTrodeConf);
      ntrodes[i]->nTrodeId = i;
      ntrodes[i]->refNTrode = 0;
      ntrodes[i]->refChan = 0;
      ntrodes[i]->color = QColor("#0000FF");
      ntrodes[i]->lowFilter = 300;
      ntrodes[i]->highFilter = 6000;
      ntrodes[i]->moduleDataChan = 0;
      ntrodes[i]->moduleDataHighFilter = 200;
      ntrodes[i]->refOn = true;
      ntrodes[i]->filterOn = true;
      ntrodes[i]->moduleDataOn = true;
    }
  }
  //SpikeConfiguration(QObject *parent, QDomDocument &doc);
  ~SpikeConfiguration();

  int loadFromXML(QDomNode &spikeConfNode);
  void saveToXML(QDomDocument &doc, QDomElement &rootNode);
  int convertHWchan(int);
  SingleSpikeTrodeConf* operator[](int i) { return ntrodes.at(i); }

signals:

  void updatedModuleData(void);
  void updatedRef(void);
  void updatedMaxDisplay(void);
  void newMaxDisplay(int nTrode, int newMaxVal);
  void newThreshold(int nTrode, int newThresh);
  void newTriggerMode(int hwChannel, bool triggerOn);
  void newThreshold(int nTrode, int chan, int newThresh);
  void newTriggerMode(int nTrode, int chan, bool triggerOn);
  void updatedTraceColor(void);
  void updatedFilter(void);
  void updatedThresh(void);
  void changeAllMaxDisp(int);
  void changeAllThresh(int);

public slots:

#ifdef TRODES_CODE
  void setModuleDataSwitch(int nTrode, bool on);
  void setRefSwitch(int nTrode, bool on);
  void setFilterSwitch(int nTrode, bool on);
  void setModuleDataChan(int nTrode, int newChan);
  void setMaxDisp(int nTrode, int chan, int newMaxDisp);
  void setThresh(int nTrode, int chan, int newThresh);
  void setMaxDisp(int nTrode, int newMaxDisp);
  void setThresh(int nTrode,  int newThresh);
  void setTriggerMode(int nTrode, int chan, bool newTriggerMode);
  void setReference(int nTrode, int newRefNTrode, int newRefNTrodeChan);
  void setColor(int nTrode, QColor newColor);
  void setLowFilter(int nTrode, int cutoff);
  void setHighFilter(int nTrode, int cutoff);
  void setModuleDataHighFilter(int nTrode, int cutoff);
#endif
};




int nsParseTrodesConfig(QString configFileName);
bool writeTrodesConfig(QString configFileName);




#endif // CONFIGURATION_H
