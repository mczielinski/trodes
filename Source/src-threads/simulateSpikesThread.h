/*
Trodes is a free, open-source neuroscience data collection and experimental control toolbox

Copyright (C) 2012 Mattias Karlsson

This program is free software: you can redistribute it and/or modify
                               it under the terms of the GNU General Public License as published by
                               the Free Software Foundation, either version 3 of the License, or
                               (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef SIMULATESPIKESTHREAD_H
#define SIMULATESPIKESTHREAD_H



#include <QtGui>
#include <QGLWidget>
#include <QThread>
#include <QTimer>
#include <QElapsedTimer>
#include "configuration.h"
#include "sharedVariables.h"
#include "abstractTrodesSource.h"

class simulateSpikesRuntime: public AbstractSourceRuntime
{
    Q_OBJECT
public:
  explicit simulateSpikesRuntime(QObject *parent = 0);
  ~simulateSpikesRuntime();
  //void run();
  //bool quitNow;
  //bool aquiring;
  double waveCarrierFrequency;
  double waveFrequency;
  double waveAmplitude;
  int threshold;
  void setCarrierFrequency(double cf);




private:

  //QTimer *pullTimer;
  //QTime  *timeKeeper;
  QElapsedTimer stopWatch;
  qint64 nSecElapsed;
  int currentSampleNum;
  void generateData();
  double  sourceData[5000];
  int   waveRes;
  float samplesPerCycle;
  int currentCyclePosition;
  float cyclePositionCarryOver;

  int currentCarrierCyclePosition;
  double carrierFreqStepSize;

  bool insertingSpike;
  int spikeIdx;
  int16_t spikeWaveform[60] = {0, -3, -11, -25, -39, -54, -45, 25, 208, 520,
  817, 901, 759, 503, 228, -17, -219, -374, -490, -573, -624, -649, -654, -643,
  -624, -591, -546, -493, -436, -377, -316, -255, -193, -135, -83, -36, 7, 44,
  73, 96, 116, 135, 151, 162, 166, 165, 158, 145, 129, 109, 90, 72, 57, 44, 34,
  26, 18, 9, 2, 0};

public slots:
  void Run(void);
  //void endThread(void);


signals:

private slots:
  void pullTimerExpired();

};


class simulateSpikesInterface : public AbstractTrodesSource {
  Q_OBJECT

public:
  simulateSpikesInterface(QObject *parent);
  ~simulateSpikesInterface(void);
  int state;

private:
  simulateSpikesRuntime* acquisitionThread;

public slots:

  void InitInterface(void);
  void StartAcquisition(void);
  void StopAcquisition(void);
  void CloseInterface(void);

  void setCarrierFrequency(double);
  void setFrequency(int);
  void setAmplitude(int);
  void setThreshold(int);
  double getCarrierFrequency(void);
  int  getFrequency(void);
  int  getAmplitude(void);
  int  getThreshold(void);

  private slots:

};

#endif // SIMULATESPIKESTHREAD_H
