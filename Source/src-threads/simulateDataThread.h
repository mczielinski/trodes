/*
Trodes is a free, open-source neuroscience data collection and experimental control toolbox

Copyright (C) 2012 Mattias Karlsson

This program is free software: you can redistribute it and/or modify
                               it under the terms of the GNU General Public License as published by
                               the Free Software Foundation, either version 3 of the License, or
                               (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef SIMULATEDATATHREAD_H
#define SIMULATEDATATHREAD_H



#include <QtGui>
#include <QGLWidget>
#include <QThread>
#include <QTimer>
#include <QElapsedTimer>
#include "configuration.h"
#include "sharedVariables.h"
#include "abstractTrodesSource.h"

class simulateDataRuntime: public AbstractSourceRuntime
{
    Q_OBJECT
public:
  explicit simulateDataRuntime(QObject *parent = 0);
  ~simulateDataRuntime();
  //void run();
  //bool quitNow;
  //bool aquiring;
  double waveCarrierFrequency;
  double waveFrequency;
  double waveAmplitude;
  int threshold;
  bool loopFinished;
  void setCarrierFrequency(double cf);




private:

  //QTimer *pullTimer;
  //QTime  *timeKeeper;
  QElapsedTimer stopWatch;
  qint64 nSecElapsed;
  int currentSampleNum;
  void generateData();
  double  sourceData[5000];
  int   waveRes;
  float samplesPerCycle;
  int currentCyclePosition;
  float cyclePositionCarryOver;

  int currentCarrierCyclePosition;
  double carrierFreqStepSize;



public slots:
  void Run(void);
  //void endThread(void);


signals:

private slots:
  void pullTimerExpired();

};


class simulateDataInterface : public AbstractTrodesSource {
  Q_OBJECT

public:
  simulateDataInterface(QObject *parent);
  ~simulateDataInterface(void);
  int state;

private:
  simulateDataRuntime* acquisitionThread;

public slots:

  void InitInterface(void);
  void StartAcquisition(void);
  void StopAcquisition(void);
  void CloseInterface(void);
  void SendSettleCommand(void);

  void setCarrierFrequency(double);
  void setFrequency(int);
  void setAmplitude(int);
  void setThreshold(int);
  double getCarrierFrequency(void);
  int  getFrequency(void);
  int  getAmplitude(void);
  int  getThreshold(void);

  private slots:

};

#endif // SIMULATEDATATHREAD_H
