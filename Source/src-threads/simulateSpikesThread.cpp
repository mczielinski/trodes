/*
Trodes is a free, open-source neuroscience data collection and experimental control toolbox

Copyright (C) 2012 Mattias Karlsson

This program is free software: you can redistribute it and/or modify
                               it under the terms of the GNU General Public License as published by
                               the Free Software Foundation, either version 3 of the License, or
                               (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include "simulateSpikesThread.h"
#include "globalObjects.h"

simulateSpikesRuntime::simulateSpikesRuntime(QObject *parent):
    waveCarrierFrequency(0.0)
    , waveFrequency(30.0)
    , waveAmplitude(50.0)
    , threshold(0)
    , nSecElapsed(0)
    , currentSampleNum(0)
    , waveRes(5000)
    , currentCyclePosition(0)
    , cyclePositionCarryOver(0)
    , currentCarrierCyclePosition(0)
    , spikeIdx(0), insertingSpike(false)
{

    generateData();

}

simulateSpikesRuntime::~simulateSpikesRuntime() {

}

void simulateSpikesRuntime::Run() {

  static int cycle = 0;
    //int PACKET_SIZE = 2*(hardwareConf->NCHAN) + 4 + 1 + 1;

    quitNow = false;
    aquiring = true;
    //timeKeeper->start();
    stopWatch.start();


    //Some variables for implementing a 'brake' for the loop
    QElapsedTimer loopTimer;
    qint64 optimalLoopTime_nS = (qint64)(1/((double) hardwareConf->sourceSamplingRate)*1000000000);
    qint64 loopTimeSurplus_nS;

    qDebug() << "Signal generator loop running....";
    qint64 samplesWritten = 0;
    int16_t currentValue;
    int16_t currentValue_withSpike;
    float cycleTime;
    float cycleStepsPerSample;
    float nSecPerSample;

    double carrierFreqStepsDue = 0.0;
    carrierFreqStepSize = waveCarrierFrequency *((double) waveRes / (double) hardwareConf->sourceSamplingRate);

    double convertFactor = (double) 65536/ (double) AD_CONVERSION_FACTOR;

    while (quitNow != true) {

        if (!aquiring) {
            stopWatch.restart();
            samplesWritten = 0;
            //currentTimeStamp = 0;
            QThread::msleep(100);
        } else {
            loopTimer.restart();
            samplesPerCycle = (hardwareConf->sourceSamplingRate/waveFrequency);
            cycleStepsPerSample = waveRes/samplesPerCycle;

            nSecPerSample = 1000.0/hardwareConf->sourceSamplingRate;
            nSecElapsed = stopWatch.elapsed();

            int samplesDue = floor(nSecElapsed/nSecPerSample)-samplesWritten;

            while (samplesDue > 0) {
                cycleTime =  (float) currentTimeStamp/ (float) hardwareConf->sourceSamplingRate;

                if (waveCarrierFrequency == 0) {
                    //No carrier wave
                    currentValue = (int16_t)(sourceData[currentCyclePosition]*waveAmplitude*convertFactor);
                } else {

                    //Calulate the current position in the carrier frequency
                    carrierFreqStepsDue = carrierFreqStepsDue+carrierFreqStepSize;
                    currentCarrierCyclePosition = (currentCarrierCyclePosition+ (int)carrierFreqStepsDue)%waveRes;
                    //the current output value is multipleied by the carrier vaue
                    currentValue = (int16_t)(sourceData[currentCyclePosition]*waveAmplitude*((sourceData[currentCarrierCyclePosition]+1.0)/2)*convertFactor);
                    carrierFreqStepsDue = carrierFreqStepsDue - floor(carrierFreqStepsDue);

                }


                if (qAbs(currentValue) < threshold) {
                    currentValue = 0;
                }

                if (insertingSpike) {
                  currentValue_withSpike = currentValue - spikeWaveform[spikeIdx++]; //
                  if (spikeIdx >= 60) {
                    spikeIdx = 0;
                    insertingSpike = false;
                  }
                }
                else if (currentTimeStamp % (hardwareConf->sourceSamplingRate/4) == 0) {
                  currentValue_withSpike = currentValue;
                  insertingSpike = true;
                  cycle = (cycle + 1) % 2;
                }

                //header
                for (int headerNum = 0; headerNum < hardwareConf->headerSize; headerNum++) {
                    if (headerNum==0) {
                        rawData.digitalInfo[(rawData.writeIdx*hardwareConf->headerSize)+headerNum] = currentValue; //Decomposes the waveform value into bits
                    } else {
                        rawData.digitalInfo[(rawData.writeIdx*hardwareConf->headerSize)+headerNum] = currentValue; //Write waveform value for the other slots
                    }
                }

                rawData.timestamps[rawData.writeIdx] = currentTimeStamp;
                currentTimeStamp++;

                for (int sampNum = 0; sampNum < hardwareConf->NCHAN; sampNum++) {
                  if ((sampNum % 2) == cycle)
                    rawData.data[(rawData.writeIdx*hardwareConf->NCHAN)+sampNum] = currentValue_withSpike;
                  else
                    rawData.data[(rawData.writeIdx*hardwareConf->NCHAN)+sampNum] = currentValue;
                }

                samplesDue--;
                samplesWritten++;

                //Advance the write markers and release a semaphore
                rawData.writeIdx = (rawData.writeIdx + 1) % EEG_BUFFER_SIZE;

                for (int a = 0; a < rawDataAvailable.length(); a++) {
                    rawDataAvailable[a]->release(1);
                }

                rawDataWritten++;


                //Calculate the next position in the sine wave
                cyclePositionCarryOver = cyclePositionCarryOver + cycleStepsPerSample;
                currentCyclePosition = (currentCyclePosition+(int)cyclePositionCarryOver) % waveRes;
                cyclePositionCarryOver = cyclePositionCarryOver - floor(cyclePositionCarryOver);


            }


            //A brake to make sure that the loop doesn't run faster than is has to
            loopTimeSurplus_nS = optimalLoopTime_nS-loopTimer.nsecsElapsed();

            if (loopTimeSurplus_nS > 1000) {
                QThread::usleep(5*loopTimeSurplus_nS/1000);
            }

        }

    }

}


/*
void simulateSpikesRuntime::endThread() {
    quit();
}*/


void simulateSpikesRuntime::generateData() {

   //int waveResolution = 5000;

    for (int sampleIndex = 0; sampleIndex < waveRes; sampleIndex++) {
        //sourceData[sampleIndex] = (int16_t) (qSin(2 * M_PI * (sampleIndex/waveRes)) * (65536/12500) );
        sourceData[sampleIndex] = (double) ((qSin(2 * M_PI * ((float) sampleIndex/ (float) waveRes)) ));
    }

}

void simulateSpikesRuntime::setCarrierFrequency(double cf) {
    waveCarrierFrequency = cf;
    carrierFreqStepSize = waveCarrierFrequency *((double) waveRes / (double) hardwareConf->sourceSamplingRate);

}

void simulateSpikesRuntime::pullTimerExpired() {

}





simulateSpikesInterface::simulateSpikesInterface(QObject *) {
  state = SOURCE_STATE_NOT_CONNECTED;
  acquisitionThread = NULL;

}

simulateSpikesInterface::~simulateSpikesInterface() {


    if (acquisitionThread != NULL) {
        delete(acquisitionThread);
    }
}

void simulateSpikesInterface::InitInterface() {
  //initialization went ok, so start the runtime thread
  acquisitionThread = new simulateSpikesRuntime(NULL);
  setUpThread(acquisitionThread);

  state = SOURCE_STATE_INITIALIZED;
  emit stateChanged(SOURCE_STATE_INITIALIZED);

}

void simulateSpikesInterface::StartAcquisition(void) {

  rawData.writeIdx = 0; // location where we're currently writing

  acquisitionThread->aquiring = true;

  emit startRuntime();
  qDebug() << "Told runtime to start";

  emit acquisitionStarted();
  state = SOURCE_STATE_RUNNING;
  emit stateChanged(SOURCE_STATE_RUNNING);
}


void simulateSpikesInterface::StopAcquisition(void) {


  emit acquisitionStopped();
  state = SOURCE_STATE_INITIALIZED;
  emit stateChanged(SOURCE_STATE_INITIALIZED);
  acquisitionThread->aquiring = false;
}

void simulateSpikesInterface::CloseInterface(void) {
  if (state == SOURCE_STATE_RUNNING)
    StopAcquisition();

  if (state == SOURCE_STATE_INITIALIZED) {

    //if the runtime thread is running, kill it
    acquisitionThread->quitNow = true;

    /*
    if (acquisitionThread != NULL) {
        acquisitionThread->quitNow = true;
        acquisitionThread->endThread();
        acquisitionThread->wait(); //block until the thread has fully terminated
    }*/

    //close device here


    qDebug() << "Closed device.";
    emit stateChanged(SOURCE_STATE_NOT_CONNECTED);

  }
}


double simulateSpikesInterface::getCarrierFrequency() {
    return (double) acquisitionThread->waveCarrierFrequency;
}

int simulateSpikesInterface::getFrequency() {
    return (int) acquisitionThread->waveFrequency;
}

int simulateSpikesInterface::getAmplitude() {
    return (int) acquisitionThread->waveAmplitude;
}

int simulateSpikesInterface::getThreshold() {
    return acquisitionThread->threshold;
}

void simulateSpikesInterface::setCarrierFrequency(double freqIn) {
    acquisitionThread->setCarrierFrequency(freqIn);
    //acquisitionThread->waveCarrierFrequency = freqIn;

}

void simulateSpikesInterface::setFrequency(int freqIn) {
    acquisitionThread->waveFrequency = (double) freqIn;
}

void simulateSpikesInterface::setAmplitude(int ampIn) {

    acquisitionThread->waveAmplitude = (double) ampIn;

}

void simulateSpikesInterface::setThreshold(int threshIn) {
    acquisitionThread->threshold = threshIn;
}
/*
void simulateSpikesRuntime::StopAcquisition() {
    pullTimer->stop();
    quit();
}*/

