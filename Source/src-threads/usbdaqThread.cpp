/*
Trodes is a free, open-source neuroscience data collection and experimental control toolbox

Copyright (C) 2012 Mattias Karlsson

This program is free software: you can redistribute it and/or modify
                               it under the terms of the GNU General Public License as published by
                               the Free Software Foundation, either version 3 of the License, or
                               (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "usbdaqThread.h"
#include "globalObjects.h"


FT_STATUS res;
FT_HANDLE ftdi;
char const *desc_old = "Rat Backpack 2 A";
char const *desc = "Spikegadgets MCU A";
char const *desc2 = " A";
//char const *desc2 = "";

USBDAQRuntime::USBDAQRuntime(QObject *parent) {


}


void USBDAQRuntime::Run() {

  DWORD BytesReceived;
  //int PACKET_SIZE = 2*(hardwareConf->NCHAN) + 4 + 1 + 1;
  PACKET_SIZE = 2*(hardwareConf->NCHAN) + 4 + (2*hardwareConf->headerSize); //Packet size in bytes
  //unsigned char buffer[PACKET_SIZE];
  //buffer = new unsigned char[PACKET_SIZE];
  buffer.resize(PACKET_SIZE);

  unsigned char *RxBuffer;
  int remainingSamples = 0;
  int leftInBuffer;
  double dTimestamp = 0.0;
  int tic = 0;
  int tempMax;
  int maxAvailable = 0;
  int numConsecErrors = 0;
  quitNow = false;
  aquiring = false;
  qDebug() << "USB handle events loop running....";

  while (quitNow != true) {



      if (badFrameAlignment && tempSyncByteLocation > 0) {
          //We have lost packet alignment, but we have a candidate sync byte location. We dump
          //everything up to that point in the current packet.
          res = FT_Read(ftdi,buffer.data(),tempSyncByteLocation,&BytesReceived);
      }

      //Read in data from FTDI chip.  This function does not return until PACKET_SIZE
      //number of bytes have been read into the buffer.
      res = FT_Read(ftdi,buffer.data(),PACKET_SIZE,&BytesReceived);
      if (BytesReceived != (DWORD) PACKET_SIZE) {
          //if (aquiring) {
              qDebug() << "Error in USB acquisition";
              numConsecErrors++;
              if (numConsecErrors > 2000) {

                  //Here we should display a message that something has gone wrong,
                  //but perhaps not stop data acquisition altogether?

                  //quitNow = true;
                  emit failure();
              }
              break;
          //}
      } else {
          //Process the received data packet.
          //The packet contains a 16-bit header for binary info,
          //a 32-bit time stamp, and hardwareConf->NCHAN 16-bit samples.

          leftInBuffer = PACKET_SIZE;
          RxBuffer = buffer.data();

          //The first byte is always 0x55.  Check to make sure we have
          //the correct frame alignment
          if (!checkFrameAlignment(RxBuffer)) {
              //We don't have alignment!  So now we drop everything
              //and try to find it.
              numConsecErrors++;
              if (numConsecErrors > 200) {
                  //if we have had many bad alignments in a row,
                  //consider it a fatal error and stop

                  quitNow = true;
                  emit failure();
              }
              //We dump the rest of the processing for this packet
              continue;
          }
          numConsecErrors = 0; //Packet looks good

          //Read header info
          memcpy(&(rawData.digitalInfo[rawData.writeIdx*hardwareConf->headerSize]), \
                 RxBuffer, hardwareConf->headerSize*sizeof(uint16_t));

          RxBuffer += (2*hardwareConf->headerSize);
          leftInBuffer -= (2*hardwareConf->headerSize);

          //Process time stamp
          uint32_t* dataPtr = (uint32_t *)(RxBuffer);
          currentTimeStamp = *dataPtr;
          checkForCorrectTimeSequence();
          rawData.timestamps[rawData.writeIdx] = *dataPtr;
          RxBuffer += 4;
          leftInBuffer -= 4;
          dTimestamp += 1.0;
          rawData.dTime[rawData.writeIdx] = dTimestamp;

          //Process the channels in the packet
          remainingSamples = hardwareConf->NCHAN;
          memcpy(&(rawData.data[rawData.writeIdx*hardwareConf->NCHAN]), \
                 RxBuffer, remainingSamples*sizeof(uint16_t));
          RxBuffer += remainingSamples * 2;
          leftInBuffer -= remainingSamples * 2;
          remainingSamples = 0;


          //Advance the write markers and release a semaphore
          rawData.writeIdx = (rawData.writeIdx + 1) % EEG_BUFFER_SIZE;

          for (int a = 0; a < rawDataAvailable.length(); a++) {
              rawDataAvailable[a]->release(1);
          }
          rawDataWritten++;

          if ((tempMax = rawDataAvailable[0]->available()) > maxAvailable)
              maxAvailable = tempMax;

          if ((++tic % 10000) == 0) {
              maxAvailable = 0;
          }

      //}
      }
  }

  emit finished();


}


USBDAQInterface::USBDAQInterface(QObject *) {
  state = SOURCE_STATE_NOT_CONNECTED;
  usbDataProcessor = NULL;


  //usbDataProcessor = new USBDAQRuntime(NULL);
  //usbDataProcessor->connect(this, SIGNAL(startRuntime()), SLOT(Run()));

}

USBDAQInterface::~USBDAQInterface() {

    /*
    if (usbDataProcessor != NULL) {
        delete(usbDataProcessor);
    }*/
}

void USBDAQInterface::InitInterface() {

    FT_STATUS	ftStatus;
    DWORD libraryVersion = 0;

    ftStatus = FT_GetLibraryVersion(&libraryVersion);
    if (ftStatus == FT_OK)
    {
        qDebug() << "Library version: " << (unsigned int)libraryVersion;
    }
    else
    {
        qDebug() << "Error reading library version.";

    }






    // Setup FTDI FT2232H interface
    connectErrorThrown = false;
#if defined (__linux) || defined (__APPLE__)
  // need to manually set VID and PID on linux
  res = FT_SetVIDPID(VENDOR, DEVICE);
  if (res != FT_OK) {
    qDebug() << "SetVIDPID failed";
    return;
  }
#endif



  //Display all detected devices
  FT_DEVICE_LIST_INFO_NODE *devInfo;
 // int iNumDevices = 0;
  DWORD numDevs = 0;


/*
  ftStatus = FT_CreateDeviceInfoList(&numDevs);
  qDebug() << "Number of FTDI devices: " << numDevs;


  if((ftStatus == FT_OK) && (numDevs > 0))
  {
      //devInfo = (FT_DEVICE_LIST_INFO_NODE*)malloc(sizeof(FT_DEVICE_LIST_INFO_NODE)*numDevs);
      devInfo = new FT_DEVICE_LIST_INFO_NODE;

      ftStatus = FT_GetDeviceInfoList(devInfo,&numDevs);
      if (ftStatus == FT_OK) {
          {
              for(long unsigned int i = 0; i < numDevs; i++) {
                  qDebug() << "Description: " << (const char*)devInfo[i].Description;
                  qDebug() << "Serial: " << devInfo[i].SerialNumber;
              }
              //delete devInfo;
          }
      }
  }
*/

  //res = FT_Open(0, &ftdi);
  res = FT_OpenEx((void*)desc, FT_OPEN_BY_DESCRIPTION, &ftdi);
  if (res != FT_OK) {
    //try the old USB label
    res = FT_OpenEx((void*)desc_old, FT_OPEN_BY_DESCRIPTION, &ftdi);
    if (res != FT_OK) {
        res = FT_OpenEx((void*)desc2, FT_OPEN_BY_DESCRIPTION, &ftdi);
        if (res != FT_OK) {
            qDebug() << "Open FTDI failed";
            connectErrorThrown = true;
            emit stateChanged(SOURCE_STATE_CONNECTERROR);
            return;
        }
    }
  }

  res = FT_ResetDevice(ftdi);
  res |= FT_SetUSBParameters(ftdi, 65536, 65536);	//Set USB request transfer size
  res |= FT_SetFlowControl(ftdi,FT_FLOW_RTS_CTS,0,0);
  res |= FT_SetChars(ftdi, false, 0, false, 0);	 //Disable event and error characters
  res |= FT_SetBitMode(ftdi, 0xff, 0x40);
  res |= FT_Purge(ftdi, FT_PURGE_RX | FT_PURGE_TX);
  res |= FT_SetLatencyTimer(ftdi, 64);
  res |= FT_SetTimeouts(ftdi, 1000, 1000);
  if (res != FT_OK) {
    qDebug() << "Error initializing device";
    connectErrorThrown = true;
    emit stateChanged(SOURCE_STATE_CONNECTERROR);
    return;
  }
    
  //initialization went ok, so start the runtime thread
  usbDataProcessor = new USBDAQRuntime(NULL);
  setUpThread(usbDataProcessor);

  /*
  workerThread = new QThread();
  usbDataProcessor->moveToThread(workerThread);

  //connect(workerThread, SIGNAL(started()), usbDataProcessor, SLOT(Run()));
  usbDataProcessor->connect(this, SIGNAL(startRuntime()), SLOT(Run()));
  connect(usbDataProcessor, SIGNAL(finished()), workerThread, SLOT(quit()));
  connect(usbDataProcessor, SIGNAL(finished()), usbDataProcessor, SLOT(deleteLater()));
  connect(workerThread, SIGNAL(finished()), workerThread, SLOT(deleteLater()));
  workerThread->start();
  */


  state = SOURCE_STATE_INITIALIZED;

  emit stateChanged(SOURCE_STATE_INITIALIZED);

}

void USBDAQInterface::StartAcquisition(void) {
  static int runtimeStarted  = 0;
  unsigned char TxBuffer[256]; // Contains data to write to device
  
  rawData.writeIdx = 0; // location where we're currently writing

  // Send "start data capture" command
  TxBuffer[0] = 0x62; // stop
  TxBuffer[1] = 0x63; // load channel configuration
  switch (hardwareConf->NCHAN) {
    case 0:
      TxBuffer[2] = 0x00; // only card 0 enabled = 32 channels
      break;
    case 32:
      TxBuffer[2] = 0x01; // only card 0 enabled = 32 channels
      break;
    case 64:
      TxBuffer[2] = 0x03; // card 0 and 1 enabled = 64 channels
      break;
    case 96:
      TxBuffer[2] = 0x07; // card 0,1,and 2 enabled = 96 channels
      break;
    case 128:
      TxBuffer[2] = 0x0F; // card 0,1,2, and 3 enabled = 128 channels
      break;
    case 160:
      TxBuffer[2] = 0x1F; // card 0,1,2,3, and 4 enabled = 160 channels
      break;
    default:
      TxBuffer[2] = 0x01; // default mode: only card 0 enabled = 32 channels
      break;

  }

  //TxBuffer[3] = 0x61; // 0x61 = start data capture
  TxBuffer[3] = startCommandValue;
  //nsUSBRuntime->aquiring = true;
  usbDataProcessor->aquiring = true;

  // send start capture command
  

  DWORD BytesWritten;
  res = FT_Write(ftdi, TxBuffer, 4, &BytesWritten);
  if (res != FT_OK) {
    qDebug() << "Error writing";
    return;
  }

  if (runtimeStarted == 0) {
    emit startRuntime();
    qDebug() << "Told runtime to start";
  }

  emit acquisitionStarted();
  state = SOURCE_STATE_RUNNING;
  emit stateChanged(SOURCE_STATE_RUNNING);
}


void USBDAQInterface::StopAcquisition(void) {
  unsigned char TxBuffer[256]; // Contains data to write to device
  
  TxBuffer[0] = 0x62; // 0x62 = stop data capture

  // send stop capture command
  DWORD BytesWritten;
  res = FT_Write(ftdi, TxBuffer, 1, &BytesWritten);
  if (res != FT_OK) {
    qDebug() << "Error stopping";
  } else {
    qDebug() << "Stopped acquisition.";
  }

  //New section-----------------

  usbDataProcessor->quitNow = true;
  QThread::msleep(100);

  res = FT_Close(ftdi);
  if (res != FT_OK) {
    qDebug() << "Error closing";
  }

  if (!connectErrorThrown) {
    InitInterface();
  }

 //----------------------------

  emit acquisitionStopped();
  state = SOURCE_STATE_INITIALIZED;
  emit stateChanged(SOURCE_STATE_INITIALIZED);
  //nsUSBRuntime->aquiring = false;
  usbDataProcessor->aquiring = false;
}

void USBDAQInterface::SendSettleCommand() {

    unsigned char TxBuffer[256]; // Contains data to write to device

    TxBuffer[0] = 0x66; // 0x66 = settle command
    //TxBuffer[0] = 0x65; // 0x65 = init SD card for writing
    // send stop capture command
    DWORD BytesWritten;
    res = FT_Write(ftdi, TxBuffer, 1, &BytesWritten);
    if (res != FT_OK) {
      qDebug() << "Error sending settle command.";
    } else {
      qDebug() << "Settle command sent.";
    }

}

void USBDAQInterface::SendSDCardUnlock() {
    unsigned char TxBuffer[256]; // Contains data to write to device

    TxBuffer[0] = 0x65; // 0x65 = unlock SD card for writing
    // send stop capture command
    DWORD BytesWritten;
    res = FT_Write(ftdi, TxBuffer, 1, &BytesWritten);
    if (res != FT_OK) {
      qDebug() << "Error sending unlock command.";
    } else {
      qDebug() << "SD card unlock command sent.";
    }
}

void USBDAQInterface::ConnectToSDCard() {
    unsigned char TxBuffer[256]; // Contains data to write to device

    TxBuffer[0] = 0x67; // 0x67 = ping the MCU for the card
    // send stop capture command
    DWORD BytesWritten;
    res = FT_Write(ftdi, TxBuffer, 1, &BytesWritten);
    if (res != FT_OK) {
      qDebug() << "Error sending SD card Connect command.";
    } else {
      qDebug() << "SD card connect command sent.";
    }

    emit SDCardStatus(false,0,false,false);
}

void USBDAQInterface::ReconfigureSDCard(int numChannels) {

    //unsigned char TxBuffer[256]; // Contains data to write to device
    //QByteArray temp(TxBuffer, 256);
    //QDataStream msg(&temp, QIODevice::ReadWrite);


    QByteArray datagram;
    datagram.resize(3);
    QDataStream msg(&datagram, QIODevice::ReadWrite);
    msg.setByteOrder(QDataStream::LittleEndian);
    msg << 0x68; // 0x68 = ping the MCU for the card
    msg << (uint16_t)numChannels;

    DWORD BytesWritten;
    res = FT_Write(ftdi, datagram.data(), 3, &BytesWritten);
    //res = FT_Write(ftdi, TxBuffer, 3, &BytesWritten);
    if (res != FT_OK) {
      qDebug() << "Error sending SD card reconfigure command.";
    } else {
      qDebug() << "SD card reconfigure command sent.";
    }
}

void USBDAQInterface::CloseInterface(void) {

  if (state == SOURCE_STATE_INITIALIZED || connectErrorThrown) {

    if (usbDataProcessor != NULL) {
        usbDataProcessor->quitNow = true;
    }

    //if the runtime thread is running, kill it
    /*
    if (usbDataProcessor != NULL) {
        usbDataProcessor->quitNow = true;
        usbDataProcessor->endThread();
        usbDataProcessor->wait(); //block until the thread has fully terminated
    }*/
    res = FT_Close(ftdi);
    if (res != FT_OK) {
      qDebug() << "Error closing";
    }

    qDebug() << "Closed FT2232H device.";
    emit stateChanged(SOURCE_STATE_NOT_CONNECTED);

  } else if (state == SOURCE_STATE_RUNNING) {
      StopAcquisition();
  }
}

