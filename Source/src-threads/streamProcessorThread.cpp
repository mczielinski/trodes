/*
   Trodes is a free, open-source neuroscience data collection and experimental control toolbox

   Copyright (C) 2012 Mattias Karlsson

   This program is free software: you can redistribute it and/or modify
                               it under the terms of the GNU General Public License as published by
                               the Free Software Foundation, either version 3 of the License, or
                               (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "streamProcessorThread.h"
#include "globalObjects.h"
#include "trodesSocket.h"
#include "time.h"


StreamProcessorManager::StreamProcessorManager(QWidget *parent) :
    QObject(parent)
{
    //neuralDataMinMax.resize(streamConf->trodeIndexLookup.length()); //total number of defined channels
    neuralDataMinMax.resize(hardwareConf->NCHAN); //some might be dead.
    gotSourceFailSignal = false;
    PSTHAuxTriggerChannel = -1;
    PSTHAuxTriggerState = true;

    int totalChannels = 0;
    int streamChannels = 0; // the number of channels in the current streamProcessor

    for (int n = 0; n < spikeConf->ntrodes.length(); n++)
        spikeDetectors.append(NULL); // initialize with NULL

    QList<int> nTrodeList;
    for (int trodeCount = 0; trodeCount < spikeConf->ntrodes.length(); trodeCount++) {


#ifndef NEW_SPIKE_DETECTOR
        //Create one thread for each nTrode to detect spike events
        createNewTriggerThread(trodeCount);
        if (trodeCount == 0) {
            //We only listed to the buffer overrun signal form the first trigger thread.
            connect(nTrodeTriggerProcessors[0], SIGNAL(bufferOverrun()), this, SIGNAL(bufferOverrun()));
        }
#endif

        /* The goal of the code below is to create threads with NUMCHANNELSPERSTREAMPROCESSOR channels or fewer each.
         * This is only problematic if a single nTrodes has more than NUMCHANNELSPERSTREAMPROCESSOR channels, in which case it will
         * have it's own thread */

        if ((streamChannels + spikeConf->ntrodes[trodeCount]->maxDisp.length()) > NUMCHANNELSPERSTREAMPROCESSOR) {
            if (spikeConf->ntrodes[trodeCount]->maxDisp.length() > NUMCHANNELSPERSTREAMPROCESSOR){
                // issue a warning
                qDebug() << "WARNING: nTrode index" << trodeCount << "has more than" << NUMCHANNELSPERSTREAMPROCESSOR << "channels; Creating dedicated thread";
            }
            if (streamChannels != 0) {
                // We have a set of channels from a previous iteration of the loop, so we create a thread for them and then move on
                createNewProcessorThread(nTrodeList);
                qDebug() << "Starting new streamProcessorThread for nTrodes" << nTrodeList;
#ifdef NEW_SPIKE_DETECTOR
                createNewSpikeManagerThread(nTrodeList);
#endif
                nTrodeList.clear();
                streamChannels = 0;
            }
        }
        // add the current nTrode to the list
        nTrodeList.push_back(trodeCount);
        streamChannels += spikeConf->ntrodes[trodeCount]->maxDisp.length();
    }
    //Create a processor for the last group
    int numHeaderChannels = headerConf->headerChannels.length(); //total number of header channels (aux. analog and digital channels)

    if ((nTrodeList.length() > 0) && (numHeaderChannels == 0)) {
        createNewProcessorThread(nTrodeList);
        qDebug() << "Starting new streamProcessorThread for nTrodes" << nTrodeList;
    } else if (numHeaderChannels > 0) {
        qDebug() << "Starting new streamProcessorThread for just auxilliary channels";
        QList<int> headerChanList;
        auxDataMinMax.resize(numHeaderChannels);
        for (int headerChan = 0; headerChan < numHeaderChannels; headerChan++) {
            headerChanList.append(headerChan);            
            DigitalStateChangeInfo dsci;
            dioStateChanges.append(dsci);
        }
        createNewProcessorThread(nTrodeList,headerChanList);
    }

    if (nTrodeList.length() > 0) {
#ifdef NEW_SPIKE_DETECTOR
        createNewSpikeManagerThread(nTrodeList);
#endif
    }   
}

StreamProcessorManager::~StreamProcessorManager()
{
}

void StreamProcessorManager::sourceFail() {
    if (!gotSourceFailSignal) {
        gotSourceFailSignal = true;
        emit sourceFail_Sig();
    }
}

void StreamProcessorManager::digitalStateChanged(int headerChannelInd, quint32 t, bool state) {


    dioStateChanges[headerChannelInd].timeStamps.push_back(t);
    dioStateChanges[headerChannelInd].states.push_back(state);

    if (dioStateChanges[headerChannelInd].timeStamps.length() > NUMDIGSTATECHANGESTOKEEP) {
        dioStateChanges[headerChannelInd].timeStamps.pop_front();
        dioStateChanges[headerChannelInd].states.pop_front();
    }
}

void StreamProcessorManager::clearAllDigitalStateChanges() {
    for (int i=0; i<dioStateChanges.length(); i++) {
        dioStateChanges[i].states.clear();
        dioStateChanges[i].timeStamps.clear();
    }
}

void StreamProcessorManager::createNewProcessorThread(QList<int> nTrodeList)
{
    //Creates a new thread and streamProcessor object

    int groupNumber = streamProcessors.length();

    processorThreads.push_back(new QThread);
    processorThreads.last()->setObjectName("Trodes-Processor");
    //processorThreads.last()->setPriority(QThread::HighestPriority);
    streamProcessors.push_back(new StreamProcessor(0, groupNumber, nTrodeList, this));
    connect(streamProcessors.last(), SIGNAL(addDataProvided(DataTypeSpec*)), this, SIGNAL(addDataProvided(DataTypeSpec*)));
    connect(streamProcessors.last(), SIGNAL(sourceFail()),this,SLOT(sourceFail()));
    streamProcessors.last()->moveToThread(processorThreads.last());
    connect(processorThreads.last(), SIGNAL(started()), streamProcessors.last(), SLOT(setUp()));
    connect(streamProcessors.last(), SIGNAL(bufferOverrun()), this, SIGNAL(bufferOverrun()));
    connect(streamProcessors.last(), SIGNAL(digitalStateChanged(int,uint32_t,bool)), this, SLOT(digitalStateChanged(int,quint32,bool)));

    connect(this, SIGNAL(startAllProcessorLoops()), streamProcessors.last(), SLOT(runLoop()));

    //connect(streamProcessors.last(), SIGNAL(), processorThreads.last(), SLOT(quit()), Qt::DirectConnection);
    processorThreads.last()->start();
}

void StreamProcessorManager::createNewProcessorThread(QList<int> nTrodeList, QList<int> auxChanList)
{
    //Creates a new thread and streamProcessor object

    int groupNumber = streamProcessors.length();

    processorThreads.push_back(new QThread);
    processorThreads.last()->setObjectName("Trodes-Processor");
    //processorThreads.last()->setPriority(QThread::HighestPriority);
    streamProcessors.push_back(new StreamProcessor(0, groupNumber, nTrodeList, this));
    streamProcessors.last()->addAuxChannels(auxChanList);
    connect(streamProcessors.last(), SIGNAL(addDataProvided(DataTypeSpec*)), this, SIGNAL(addDataProvided(DataTypeSpec*)));
    connect(streamProcessors.last(), SIGNAL(sourceFail()),this,SLOT(sourceFail()));
    streamProcessors.last()->moveToThread(processorThreads.last());
    connect(processorThreads.last(), SIGNAL(started()), streamProcessors.last(), SLOT(setUp()));
    connect(streamProcessors.last(), SIGNAL(bufferOverrun()), this, SIGNAL(bufferOverrun()));
    connect(streamProcessors.last(), SIGNAL(digitalStateChanged(int,quint32,bool)), this, SLOT(digitalStateChanged(int,quint32,bool)));
    connect(this, SIGNAL(startAllProcessorLoops()), streamProcessors.last(), SLOT(runLoop()));

    //connect(streamProcessors.last(), SIGNAL(), processorThreads.last(), SLOT(quit()), Qt::DirectConnection);
    processorThreads.last()->start();
}

#ifndef NEW_SPIKE_DETECTOR
void StreamProcessorManager::createNewTriggerThread(int nTrodeNum)
{
    triggerThreads.push_back(new QThread);
    triggerThreads.last()->setObjectName("Trodes-Triggers");
    nTrodeTriggerProcessors.push_back(new Trigger(0, nTrodeNum, spikeConf->ntrodes[nTrodeNum]->hw_chan.length()));
    nTrodeTriggerProcessors.last()->moveToThread(triggerThreads.last());
    connect(triggerThreads.last(), SIGNAL(started()), nTrodeTriggerProcessors.last(), SLOT(setUp()));
    connect(this, SIGNAL(startAllProcessorLoops()), nTrodeTriggerProcessors.last(), SLOT(runLoop()));
    triggerThreads.last()->start();
}
#else
void StreamProcessorManager::createNewSpikeManagerThread(QList<int> nTrodeList)
{
    spikeDetectorThreads.push_back(new QThread);
    spikeDetectorThreads.last()->setObjectName("Trodes-Detector");
    spikeDetectorManagers.push_back(new SpikeDetectorManager(0, nTrodeList, &spikeDetectors)); // spikeDetectors is going to be populated
    connect(spikeDetectorManagers.last(), SIGNAL(addDataProvided(DataTypeSpec*)), this, SIGNAL(addDataProvided(DataTypeSpec*)));
    spikeDetectorManagers.last()->moveToThread(spikeDetectorThreads.last());
    connect(spikeDetectorThreads.last(), SIGNAL(started()), spikeDetectorManagers.last(), SLOT(setupAndRun()));
    connect(spikeConf, SIGNAL(newThreshold(int, int)), spikeDetectorManagers.last(), SLOT(updateSpikeThreshold(int, int)));

    //connect(spikeConf, SIGNAL(newThreshold(int, int, int)), spikeDetectorManagers.last(), SLOT(updateSpikeThreshold(int, int, int)));
    connect(spikeConf, SIGNAL(newTriggerMode(int, int, bool)), spikeDetectorManagers.last(), SLOT(updateSpikeMode(int, int, bool)));
    spikeDetectorThreads.last()->start();
}
#endif


void StreamProcessorManager::startAcquisition()
{
    gotSourceFailSignal = false;
    emit startAllProcessorLoops(); //sends signal to all processor threads to start their loops

}

void StreamProcessorManager::stopAcquisition()
{
#ifndef NEW_SPIKE_DETECTOR
    for (int i = 0; i < nTrodeTriggerProcessors.length(); i++) {
        nTrodeTriggerProcessors[i]->quitNow = true;
    }

#endif
    for (int i = 0; i < streamProcessors.length(); i++) {
        streamProcessors[i]->quitNow = 1;
    }
}

void StreamProcessorManager::updateTriggerThresh(int nTrodeIdx, int channel, int newThresh)
{
#ifndef NEW_SPIKE_DETECTOR
    //newThresh is unused, but the connecting signal has it as an ouptput
    nTrodeTriggerProcessors[nTrodeIdx]->setThresh(channel, spikeConf->ntrodes[nTrodeIdx]->thresh_rangeconvert[channel]);
#endif
}

void StreamProcessorManager::updateTriggerMode(int nTrodeIdx, int channel, bool triggerOn)
{
#ifndef NEW_SPIKE_DETECTOR
    nTrodeTriggerProcessors[nTrodeIdx]->setTriggerMode(channel, triggerOn);
#endif
}

void StreamProcessorManager::removeAllProcessors()
{
    stopAcquisition();

    for (int i = 0; i < streamProcessors.length(); i++) {
        processorThreads[i]->quit();
        processorThreads[i]->deleteLater();
        streamProcessors[i]->deleteLater();
    }

#ifndef NEW_SPIKE_DETECTOR
    for (int i = 0; i < nTrodeTriggerProcessors.length(); i++) {
        triggerThreads[i]->quit();

        triggerThreads[i]->deleteLater();
        nTrodeTriggerProcessors[i]->deleteLater();
    }
#else
    for (int i = 0; i < spikeDetectorManagers.length(); i++) {
        spikeDetectorManagers[i]->stopRunning();
    }

    //This is a bit of a hack, and will cause a crash upon shutdown if any of the StreamProcessors have
    //not finished running.  Ideally, the StreamProcessor threads would send a signal when they are done that
    //is used to delete the spike detection threads, but because there are multiple StreamProcessors and multiple
    //spikeDetectorThreads, this is complicated to set up.  So, for now, we sleep for long enough to ensure that
    //the StreamProcessors have all shut down.
    QThread::msleep(100);
    for (int i = 0; i < spikeDetectorThreads.length(); i++) {
        spikeDetectorThreads[i]->quit();
        spikeDetectorThreads[i]->deleteLater();
        spikeDetectorManagers[i]->deleteLater();

    }

#endif
}

QVector<uint32_t> StreamProcessorManager::getDigitalEventTimes() {

    QVector<uint32_t> outputTimes;
    if (PSTHAuxTriggerChannel < dioStateChanges.length() && PSTHAuxTriggerChannel > -1) {
        for (int i=0; i < dioStateChanges[PSTHAuxTriggerChannel].timeStamps.length(); i++) {
            if (dioStateChanges[PSTHAuxTriggerChannel].states[i] == PSTHAuxTriggerState) {
                outputTimes.push_back(dioStateChanges[PSTHAuxTriggerChannel].timeStamps[i]);
            }
        }
    }
    return outputTimes;

}

QString StreamProcessorManager::getPSTHTriggerID()
{
    if (headerConf != NULL && PSTHAuxTriggerChannel > -1) {
        return headerConf->headerChannels[PSTHAuxTriggerChannel].idString;

    } else {
        return "";
    }
}

bool StreamProcessorManager::getPSTHTriggerState() {
    return PSTHAuxTriggerState;
}

void StreamProcessorManager::updateDataLength(double tlength)
{
    for (int i = 0; i < streamProcessors.length(); i++) {
        streamProcessors[i]->newDataLength = hardwareConf->sourceSamplingRate * tlength;
        //When this flag is set to true, the thread's loop will update the trace length and reset the flag to false
        streamProcessors[i]->updateDataLengthFlag = true;
    }
}

void StreamProcessorManager::updateChannels()
{
    for (int i = 0; i < streamProcessors.length(); i++) {
        streamProcessors[i]->updateChannelsFlag = true;
    }
}

void StreamProcessorManager::createSpikeLogs(QString dataDir)
{
//  for (int i = 0; i < nTrodeTriggerProcessors.length(); i++) {
//    nTrodeTriggerProcessors[i]->createLogFile(dataDir);
//  }
}

void StreamProcessorManager::setPSTHTrigger(int headerChannel, bool state)
{
    qDebug() << "PSTH trigger set";
    PSTHAuxTriggerChannel = headerChannel;
    PSTHAuxTriggerState = state;
}

//This object is used to pull out raw data for a set of channels, filter the signals, and
//calculate what should be displayed on the stream display. It runs inside a seperate thread.

StreamProcessor::StreamProcessor(QObject*, int groupNumber, QList <int> nTList, StreamProcessorManager* managerPtr) :
    groupNum(groupNumber),
    nTrodeList(nTList),
    streamManager(managerPtr)
{
    dataLength = hardwareConf->sourceSamplingRate * streamConf->tLength;
    isLooping = false;
    nAuxChan = 0;
    hasDigIO = false;
    hasAuxAnalog = false;
    isSetup = false;

    nChan = 0;
    for (int trode = 0; trode < nTList.length(); trode++) {
        nChan = nChan + spikeConf->ntrodes[trode]->hw_chan.length();
        nTrodeIdList.push_back(spikeConf->ntrodes[nTrodeList[trode]]->nTrodeId);
    }

}

StreamProcessor::~StreamProcessor()
{
    if (dataHandler.length() > 0) {
        foreach(TrodesSocketMessageHandler * mh, dataHandler)
        {
            if (mh != NULL) {
                mh->closeConnection();
                mh->deleteLater();
            }
        }
    }
    if (digitalIOHandler != NULL) {
        if (digitalIOHandler != NULL) {
            digitalIOHandler->closeConnection();
            digitalIOHandler->deleteLater();
        }
    }
    if (analogIOHandler != NULL) {
        if (analogIOHandler != NULL) {
            analogIOHandler->closeConnection();
            analogIOHandler->deleteLater();
        }
    }


    for (int i = 0; i < nTrodeList.length(); i++) {
        delete [] dataPoints[i];
    }
    delete [] dataPoints;

}

void StreamProcessor::addAuxChannels(QList<int> auxList) {
    //Can only add aux channels before setUp() has been executed
    if (!isSetup) {
        for (int i=0; i<auxList.length(); i++) {
            auxChannelList.append(auxList[i]);
        }
        //auxChannelList = auxList;
        nAuxChan = auxList.length(); // For header group, this is just the length of header channels

        for (int i=0; i < nAuxChan; i++) {
            if (headerConf->headerChannels[i].dataType == DeviceChannel::DIGITALTYPE) {
                hasDigIO = true;
            } else if (headerConf->headerChannels[i].dataType == DeviceChannel::INT16TYPE) {
                hasAuxAnalog = true;
            }
        }
    }
}

void StreamProcessor::setUp() {

    //dataFilters = new BesselFilter[nChan];

    //Calculate how many data points go into one display pixel on the stream plot
    raw_increment.resize(EEG_TIME_POINTS);
    for (int j = 0; j < EEG_TIME_POINTS; j++) {
        raw_increment[j] = (int)((double)(j + 1) * (double)dataLength / (double)EEG_TIME_POINTS) -
                           (int)((double)j * (double)dataLength / (double)EEG_TIME_POINTS);
    }

    //Create a new semaphore for this group of channels.
    //if this is not the first time a workspace has been loaded, then there will already be semaphores in place.
    //Only create more if they are needed.
    rawDataAvailableMutex.lock();
    while (groupNum >= rawDataAvailable.length()) {
        rawDataAvailable.append(new QSemaphore);
    }
    rawDataAvailableMutex.unlock();


    for (int trode = 0; trode < nTrodeList.length(); trode++) {
        filtersOn.push_back(spikeConf->ntrodes[nTrodeList.at(trode)]->filterOn);
        triggered.push_back(false);
    }


    dataPoints = new int16_t*[nTrodeList.length()];
    rawDataAvailableIdx = groupNum;
    for (int nt = 0; nt < nTrodeList.length(); nt++) {
        //Get channel-specific info from the loaded configuration file


        // if we haven't added it already, add this to the data available index list
        if (dataProvided.contNTrodeIndexList.indexOf(nTrodeList.at(nt)) == -1) {
            dataProvided.contNTrodeIndexList.push_back(nTrodeList.at(nt));
        }

        //dataMinMax is what keeps the currently plotted data (for streaming page)
        //For each horizontal pixel, it keeps the maximum and minimum values that occured
        //in the data stream for that channel during that time window. Minimum and maximum
        //values alternate (which is why the array is twice as long as EEE_TIME_POINTS).

        for (int c = 0; c < spikeConf->ntrodes[nTrodeList.at(nt)]->hw_chan.length(); c++) {
            int ch = spikeConf->ntrodes[nTrodeList.at(nt)]->hw_chan[c];
            streamManager->neuralDataMinMax[ch].resize((int)EEG_TIME_POINTS * 2);
            for (int j = 0; j < EEG_TIME_POINTS; j++) {
                streamManager->neuralDataMinMax[ch][2 * j].x = (double)j * (double)dataLength / (double)EEG_TIME_POINTS;
                streamManager->neuralDataMinMax[ch][2 * j].y = 0;
                streamManager->neuralDataMinMax[ch][(2 * j) + 1].x = (double)j * (double)dataLength / (double)EEG_TIME_POINTS;
                streamManager->neuralDataMinMax[ch][(2 * j + 1)].y = 0;
            }
        }

        dataPoints[nt] = new int16_t[spikeConf->ntrodes[nTrodeList.at(nt)]->hw_chan.length()];
    }

    for (int auxCh = 0; auxCh < auxChannelList.length(); auxCh++) {
        //This is not for neural channels (digital inputs, aux analog inputs,...)
        int hdrChan = auxChannelList.at(auxCh);
        if ((headerConf->headerChannels[hdrChan].dataType == DeviceChannel::INT16TYPE) &&
                    (headerConf->headerChannels[hdrChan].interleavedDataIDByte != -1)) {
            interleavedAuxChannelStates[headerConf->headerChannels[hdrChan].idString] = 0;
        }
        streamManager->auxDataMinMax[hdrChan].resize((int)EEG_TIME_POINTS * 2);
        for (int j = 0; j < EEG_TIME_POINTS; j++) {
            streamManager->auxDataMinMax[hdrChan][2 * j].x = (double)j * (double)dataLength / (double)EEG_TIME_POINTS;
            streamManager->auxDataMinMax[hdrChan][2 * j].y = 0;
            streamManager->auxDataMinMax[hdrChan][(2 * j) + 1].x = (double)j * (double)dataLength / (double)EEG_TIME_POINTS;
            streamManager->auxDataMinMax[hdrChan][(2 * j + 1)].y = 0;
        }
    }

    rawIdx = 0;
    dataIdx = 0;


    dataProvided.moduleID = TRODES_ID;
    dataProvided.socketType = networkConf->dataSocketType;
    qDebug() << "dataSocketType" << networkConf->dataSocketType;

    // we need to create a server or servers to send out data. The type of server is specified in the network configuration it it was defined
    if (networkConf->dataSocketType == TRODESSOCKETTYPE_TCPIP) {
        dataServer = new TrodesServer();
        if (networkConf->networkConfigFound) {
            //if the config file designates the address , use it
            dataServer->setAddress(networkConf->trodesHost);
        }
        dataServer->startLocalServer("streamManager");

        connect(dataServer, SIGNAL(newDataHandler(TrodesSocketMessageHandler*, qint16)), this, SLOT(newDataHandler(TrodesSocketMessageHandler*, qint16)));
        // Fill out the rest of the dataProvided structure for this streamManager
        dataProvided.hostName = dataServer->getCurrentAddress();
        dataProvided.hostPort = dataServer->getCurrentPort();
        qDebug() << "dataProvided port" << dataProvided.hostPort;
    }
    else if (networkConf->dataSocketType == TRODESSOCKETTYPE_UDP) {
        dataProvided.hostName = networkConf->trodesHost;
    }
    // otherwise we set up the sockets individually as we go through the dataTypes below

    // set the dataType
    qDebug() << "StreamProcessor started server on port" << dataProvided.hostPort << "nTrodes indeces" << dataProvided.contNTrodeIndexList;

    dataProvided.dataType = 0;
    if (hasDigIO) {
        dataProvided.dataType |= TRODESDATATYPE_DIGITALIO;
        if (networkConf->dataSocketType == TRODESSOCKETTYPE_UDP) {
            // create a message handler for this socket. For TCPIP this will all happen as the client connects
            // create a udp server for this nTrode
            TrodesUDPSocket *trodesUDPSocket = new TrodesUDPSocket("", this);
            digitalIOHandler = trodesUDPSocket->newDataHandler(TRODESDATATYPE_DIGITALIO);
            // set the UDP port in dataProvided
            dataProvided.digitalIOUDPPort = trodesUDPSocket->getCurrentPort();
        }
    }
    if (hasAuxAnalog) {
        dataProvided.dataType |= TRODESDATATYPE_ANALOGIO;
        if (networkConf->dataSocketType == TRODESSOCKETTYPE_UDP) {
            TrodesUDPSocket *trodesUDPSocket = new TrodesUDPSocket("", this);
            analogIOHandler = trodesUDPSocket->newDataHandler(TRODESDATATYPE_ANALOGIO);
            // set the UDP port in dataProvided
            dataProvided.analogIOUDPPort = trodesUDPSocket->getCurrentPort();
        }
    }
    if (nChan > 0) {
        dataProvided.dataType |= TRODESDATATYPE_CONTINUOUS;
        if (networkConf->dataSocketType == TRODESSOCKETTYPE_UDP) {
            for (int trode = 0; trode < nTrodeList.length(); trode++) {
                // create a udp server for this nTrode
                TrodesUDPSocket *trodesUDPSocket = new TrodesUDPSocket("", this);
                TrodesSocketMessageHandler *newHandler = trodesUDPSocket->newDataHandler(TRODESDATATYPE_CONTINUOUS);
                // for continuous data we can now just call newDataHandler to add it to the list
                newDataHandler(newHandler, nTrodeList.at(trode));
                dataProvided.contNTrodeUDPPortList.append(trodesUDPSocket->getCurrentPort());
            }
        }
    }

    // add that dataProvided structure to the main list
    emit addDataProvided(&dataProvided);

    quitNow = 0;
    updateChannelsFlag = false;
    updateDataLengthFlag = false;

    if (networkConf->dataSocketType == TRODESSOCKETTYPE_TCPIP) {
        // initialize the handlers; they will be set up when clients connect
        digitalIOHandler = NULL;
        analogIOHandler = NULL;
    }


    // if this handles digital IO data, allocate space for a variable that will hold the current digital IO state    
    if (hasDigIO || hasAuxAnalog) {

        digStates.resize(headerConf->headerChannels.length());
        // fill the array with false to initialize
        digStates.fill(false);

        /*
        digInState = new bool[headerConf->maxDigitalPort(true)];
        digOutState = new bool[headerConf->maxDigitalPort(false)];

        // fill the arrays with zeros to initialize
        memset(digInState, 0, headerConf->maxDigitalPort(true) * sizeof(bool));
        memset(digOutState, 0, headerConf->maxDigitalPort(false) * sizeof(bool));*/
    }


    connect(spikeConf, SIGNAL(updatedModuleData()), this, SLOT(updateModuleDataChan()));

    isSetup = true;
}


void StreamProcessor::updateDataLength(void)
{
    // Note that this is safe only because we've chosen to have a constant number of points for
    // the time base of the display. We would need to be more careful if raw_increment was going
    // to change size, in order to not have thread clashes....
    qDebug() << "Old dataLength " << dataLength;
    qDebug() << "New dataLength " << newDataLength;

    dataLength = newDataLength;
    raw_increment.resize(dataLength);

    for (int j = 0; j < EEG_TIME_POINTS; j++) {
        //qDebug() << j;
        raw_increment[j] = (int)((double)(j + 1) * (double)dataLength / (double)EEG_TIME_POINTS) -
                           (int)((double)j * (double)dataLength / (double)EEG_TIME_POINTS);

        //There is really no need to update the xaxis on the traces, so for now we skip this step
        //This means that the xlabels are fixed to whatever values they had in the constructor,
        //and as long as we do not update them in the stream display widget, everything should work fine.
    }
    dataIdx = 0;
    qDebug() << "Set data length";
}



void StreamProcessor::updateChannels(void)
{
    // Keeping track of filters and references is important because if we just
    // refer to the global values, they may change unexpectedly. BUT, if we
    // restrict these things to be changed only when data is NOT being recorded
    // the small glitches shouldn't matter.
    qDebug() << "updating channels";
    for (int trode = 0; trode < nTrodeList.length(); trode++) {
        filtersOn[trode] = spikeConf->ntrodes[nTrodeList.at(trode)]->filterOn;
    }
}


void StreamProcessor::updateModuleDataChan()
{

    for (int i = 0; i < dataHandler.length(); i++) {
        int tempIndex = dataHandler[i]->getNTrodeIndex();
        dataHandler[i]->setNTrodeChan(spikeConf->ntrodes[tempIndex]->moduleDataChan);
        int highFilterVal = spikeConf->ntrodes[tempIndex]->moduleDataHighFilter;
        filtersForContinuousSendChannels[i]->setFilterRange(0, highFilterVal);
    }
}



void StreamProcessor::newDataHandler(TrodesSocketMessageHandler *messageHandler, qint16 requestedNTrodeIndex)
{
    // check to the requested datatype
    if (messageHandler->getDataType() == TRODESDATATYPE_CONTINUOUS) {
        //qDebug() << "StreamProcessor geting new data hander for nTrodeIndex " << requestedNTrodeIndex << "local nTrodeIndeces" << nTrodeList << "port" << dataServer->getCurrentPort();

        // set the information for this handler
        dataHandler.push_back(messageHandler);
        // check to see if the requested NTrode is available, and put out an error if not
        if (nTrodeIdList[nTrodeList.indexOf(requestedNTrodeIndex)] == -1) {
            qDebug() << "Error: StreamProcessor got request for non-existent NTrodeIndex" << requestedNTrodeIndex;
            return;
        }
        dataHandler.last()->setNTrodeId(nTrodeIdList[nTrodeList.indexOf(requestedNTrodeIndex)]);
        dataHandler.last()->setNTrodeIndex(requestedNTrodeIndex);

        int tmpNTrodeIndex = dataHandler.last()->getNTrodeIndex();
        dataHandler.last()->setNTrodeChan(spikeConf->ntrodes[tmpNTrodeIndex]->moduleDataChan);


        filtersForContinuousSendChannels.push_back(new BesselFilter());

        //Not sure if we want the full sampling rate here...
        filtersForContinuousSendChannels.last()->setSamplingRate(hardwareConf->sourceSamplingRate);

        int highFilterVal = spikeConf->ntrodes[tmpNTrodeIndex]->moduleDataHighFilter;
        filtersForContinuousSendChannels.last()->setFilterRange(0, highFilterVal);
        //updateFiltersForContinuousSend();
        //qDebug() << "StreamProcessor got new data hander #" << dataHandler.length() << "nTrodeIndex" << requestedNTrodeIndex << "highFilter" << highFilterVal;
    }
    else if (messageHandler->getDataType() == TRODESDATATYPE_DIGITALIO) {
        digitalIOHandler = messageHandler;
    }
    else if (messageHandler->getDataType() == TRODESDATATYPE_ANALOGIO) {
        analogIOHandler = messageHandler;
    }
}

void StreamProcessor::runLoop()
{
    //This is where the raw data is filtered, and where the streaming display is calculated.
    //In each display bin, we draw a vertical line connecting the minumim and maximum collected values
    //in that time bin.
    //When the 'stream from source' menu is chosen, this function is executed via an emitted signal
    //from the source controller

    int samplesToCopy = 0;

    quitNow = 0;
    isLooping = true;
    bool exitLoop = false;

    int tmpDataPoint = 0;
    int tmpMaxPoint = 0;
    int tmpMinPoint = 0;
    //int dInd;
    int rdInd;

    bool newDigIOState;
    bool digInput;
    int port;



    char* startBytePtr; //for processing header channels
    uint8_t* interleavedDataIDBytePtr; //for processing header channels that are interleaved

    rawIdx = 0;
    dataIdx = 0;
    streamDataRead = 0;

    numLoopsWithNoData = 0;

    int streamIdx = 0;
    int hw_chan = 0;
    int stream_inc = 0;

    uint32_t timestamp;

    qDebug() << "Starting stream processor";

    QThread::usleep(100); //This appears to solve a race condition that sometimes occurs.  TODO: a real solution

    while (!exitLoop) {
        if (rawDataAvailable[rawDataAvailableIdx]->tryAcquire(1, 100)) {
            samplesToCopy = rawDataAvailable[rawDataAvailableIdx]->available();
            if (samplesToCopy > 15000) {
                emit bufferOverrun();
            }
            if (!rawDataAvailable[rawDataAvailableIdx]->tryAcquire(samplesToCopy))
                qDebug() << "Error acquiring available samples, group " << groupNum;

            samplesToCopy += 1; // we acquired one at the beginning

            for (int s = 0; s < samplesToCopy; s++) {
                // -----------------------------------------------------------------
                // LOAD NEW DATA STRUCTURE
                rdInd = rawIdx * hardwareConf->NCHAN;

                timestamp = rawData.timestamps[rawIdx];
                streamIdx = 0;


                //First we process the neural channels
                for (int n = 0; n < nTrodeList.length(); n++) {
                    int nt = nTrodeList.at(n); // the nTrodeList may not be identical to all the nTrodes

                    //If the digital reference is on, subtract the ref value
                    for (int c = 0; c < spikeConf->ntrodes[nt]->hw_chan.length(); c++) {

                        hw_chan = spikeConf->ntrodes[nt]->hw_chan[c];

                        tmpDataPoint = rawData.data[rdInd + hw_chan];
                        if (tmpDataPoint == -32768) {
                            //This is a NAN, replace with 0
                            tmpDataPoint = 0;
                        } else if (spikeConf->ntrodes[nt]->refOn) {
                           //tmpDataPoint = rawData.data[rdInd + hw_chan]-rawData.data[rdInd + spikeConf->ntrodes[spikeConf->ntrodes[nt]->refNTrode]->hw_chan[spikeConf->ntrodes[nt]->refChan]];
                           tmpDataPoint = tmpDataPoint-rawData.data[rdInd + spikeConf->ntrodes[spikeConf->ntrodes[nt]->refNTrode]->hw_chan[spikeConf->ntrodes[nt]->refChan]];

                        }
                        //else {
                        //    tmpDataPoint = rawData.data[rdInd + spikeConf->ntrodes[nt]->hw_chan[c]];

                        //}


                        //Extracellular recordings, so we reverse polarity to make spikes go upward
                        tmpDataPoint *= -1;



                        //send data to module if enabled

                        int nTrodeId = spikeConf->ntrodes.at(nt)->nTrodeId;
                        for (int d = 0; d < dataHandler.length(); d++) {
                            if ((dataHandler[d]->getNTrodeId() == nTrodeId) && (dataHandler[d]->getNTrodeChan() == c) &&
                                    dataHandler[d]->isModuleDataStreamingOn()) {
                                int16_t sendDataPoint = filtersForContinuousSendChannels[d]->addValue(tmpDataPoint);
                                if ((timestamp % dataHandler[d]->getDecimation()) == 0) {
                                    if (timestamp == 0) {
                                        qDebug() << "Stream processor sent a 0 timestamp" << ". Current thread: " << QThread::currentThreadId();
                                    }
                                    dataHandler[d]->sendContinuousDataPoint(timestamp, sendDataPoint);
                                }
                            }
                        }

                        //Run the signal through the filter (this is done below if the stream display is not
                        //linked to the filters
                        if (filtersOn[n] && linkStreamToFilters) {
                            tmpDataPoint = streamConf->dataFilters[hw_chan].addValue(tmpDataPoint);
                            //tmpDataPoint = dataFilters[currentChannel].addValue(tmpDataPoint);
                        }

                        //If the signals are huge, clip them in the display.  TODO:  use the max display setting for this channel as the clip.


                        tmpMaxPoint = qMin(tmpDataPoint, 5000);
                        tmpMinPoint = qMax(tmpDataPoint, -5000);



                        //Calulate the current max and min values in the current display bin
                        if (stream_inc > 0) {
                            //Old bin, so compare to existing value
                            streamManager->neuralDataMinMax[hw_chan][dataIdx * 2].y =
                                    qMax((GLfloat)tmpMaxPoint, streamManager->neuralDataMinMax[hw_chan][dataIdx * 2].y);
                            streamManager->neuralDataMinMax[hw_chan][(dataIdx * 2) + 1].y =
                                    qMin((GLfloat)tmpMinPoint, streamManager->neuralDataMinMax[hw_chan][(dataIdx * 2) + 1].y);
                        }
                        else {
                            //New bin, so reset the value
                            streamManager->neuralDataMinMax[hw_chan][dataIdx * 2].y = tmpMaxPoint;
                            streamManager->neuralDataMinMax[hw_chan][(dataIdx * 2) + 1].y = tmpMinPoint;
                        }




                        //Filter here if stream display is not linked to filters
                        if (filtersOn[n] && !linkStreamToFilters) {
                            tmpDataPoint = streamConf->dataFilters[hw_chan].addValue(tmpDataPoint);
                            //tmpDataPoint = dataFilters[currentChannel].addValue(tmpDataPoint);
                        }

                        // Send data to spike detector
#ifndef NEW_SPIKE_DETECTOR
                        streamManager->nTrodeTriggerProcessors[nt]->addValue(c, tmpDataPoint, .timestamp);
#else
                        dataPoints[n][c] = tmpDataPoint;
                        //streamManager->spikeDetectors[nt]->newChannelData(c, tmpDataPoint, rawDataSample.timestamp);
#endif


                        streamIdx++; // this keeps track of which row in the

                    }
#ifdef NEW_SPIKE_DETECTOR
                    streamManager->spikeDetectors[nt]->newData(dataPoints[n], timestamp);
#endif

                }



                //Now we process the auxilliary channels (digial I/O, analog I/O), if any

                for (int h = 0; h < auxChannelList.length(); h++) {
                    int hch = auxChannelList[h];



                    startBytePtr = ((char*)(rawData.digitalInfo + (rawIdx * hardwareConf->headerSize))) + headerConf->headerChannels[h].startByte;
                    if (headerConf->headerChannels[hch].dataType == DeviceChannel::DIGITALTYPE) {
                        /*
                        rawDataSample.headerData[h] = (int)((*startBytePtr & (1 << headerConf->headerChannels[h].digitalBit)) >>
                                                            headerConf->headerChannels[h].digitalBit);
                        */

                        tmpDataPoint = (int)((*startBytePtr & (1 << headerConf->headerChannels[hch].digitalBit)) >>
                                             headerConf->headerChannels[hch].digitalBit);

                    }
                    else if (headerConf->headerChannels[hch].dataType == DeviceChannel::INT16TYPE) {
                        // TO DO: add analogIO output
                        if (headerConf->headerChannels[hch].interleavedDataIDByte != -1) {
                            interleavedDataIDBytePtr = ((uint8_t*)(rawData.digitalInfo + (rawIdx * hardwareConf->headerSize))) + headerConf->headerChannels[hch].interleavedDataIDByte;

                            if (*interleavedDataIDBytePtr & (1 << headerConf->headerChannels[hch].interleavedDataIDBit)) {
                                //This interleaved data point belongs to this channel, so update the channel. Otherwise, no update occurs.
                                //qDebug() << headerConf->headerChannels[h].interleavedDataIDBit;
                                tmpDataPoint = *((int16_t*)(startBytePtr)); //change to 16-bit pointer, then dereference
                                interleavedAuxChannelStates[headerConf->headerChannels[hch].idString] = tmpDataPoint;
                            } else {
                                //Use the last data point received
                                tmpDataPoint = interleavedAuxChannelStates[headerConf->headerChannels[hch].idString];
                            }
                        } else {

                            tmpDataPoint = *((int16_t*)(startBytePtr)); //change to 16-bit pointer, then dereference
                        }
                    }


                    if (headerConf->headerChannels[hch].dataType == DeviceChannel::DIGITALTYPE) {
                        newDigIOState = false;

                        port = headerConf->headerChannels[hch].port;

                        // check if this is an input, and if so, if the state of the port has changed
                        if ((bool)tmpDataPoint != digStates[hch]) {
                            newDigIOState = true;
                            digStates[hch] = (bool)tmpDataPoint;

                            //Send the change event info to the StreamProcessorManager, which
                            //keeps a record of all the events.
                            //We should have a gate for this to allow users to exclude
                            //channels that are changing often

                            if (headerConf->headerChannels[hch].storeStateChanges) {
                                emit digitalStateChanged(hch,timestamp,digStates[hch]);
                            }
                        }

                        /*
                        if ((headerConf->headerChannels[hch].input) && ((bool)tmpDataPoint != digInState[port])) {
                            newDigIOState = true;
                            digInput = true;
                            digInState[port] = (bool)tmpDataPoint;

                            //emit digitalStateChanged(hch,timestamp,digInState[port]);
                        }
                        // if not, check if this is an output, and if so, if the state of the port has changed
                        else if ((!headerConf->headerChannels[hch].input) && ((bool)tmpDataPoint != digOutState[port])) {
                            newDigIOState = true;
                            digInput = false;
                            digOutState[port] = (bool)tmpDataPoint;
                            //emit digitalStateChanged(hch,timestamp,digOutState[port]);
                        }*/

                        // check to see if we are could send out data
                        if (digitalIOHandler != NULL) {                           
                            // if the state changed and we're supposed to stream data, send out the new port status.
                            if ((newDigIOState) && (digitalIOHandler->isModuleDataStreamingOn())) {

                                digitalIOHandler->sendDigitalIOData(timestamp, port,
                                                                    (char)headerConf->headerChannels[hch].input, (char)tmpDataPoint);
                            }
                        }

                    }

                    //If the signals are huge, clip them in the display.  TODO:  use the max display setting for this channel as the clip.

                    //tmpMaxPoint = qMin(tmpDataPoint, 2);
                    //tmpMinPoint = qMax(tmpDataPoint, -1);

                    //Calulate the current max and min values in the current display bin
                    if (stream_inc > 0) {
                        //Old bin, so compare to existing value
                        streamManager->auxDataMinMax[hch][dataIdx * 2].y =
                                qMax((GLfloat)tmpDataPoint, streamManager->auxDataMinMax[hch][dataIdx * 2].y);
                        streamManager->auxDataMinMax[hch][(dataIdx * 2) + 1].y =
                                qMin((GLfloat)tmpDataPoint, streamManager->auxDataMinMax[hch][(dataIdx * 2) + 1].y);
                    }
                    else {
                        //New bin, so reset the value
                        streamManager->auxDataMinMax[hch][dataIdx * 2].y = tmpDataPoint;
                        streamManager->auxDataMinMax[hch][(dataIdx * 2) + 1].y = tmpDataPoint;
                    }
                }


                rawIdx = (rawIdx + 1) % EEG_BUFFER_SIZE;
                if (++stream_inc >= raw_increment[dataIdx]) {
                    dataIdx = (dataIdx + 1) % EEG_TIME_POINTS;
                    stream_inc = 0;
                }
            }
        } else if (quitNow == 1) {
            exitLoop = true;
            isLooping = false;
        } else {
            //qDebug() << "No data!";
            numLoopsWithNoData++;
            if (numLoopsWithNoData > 20000) {
                numLoopsWithNoData = 0;
                qDebug() << "No data!";
                //emit sourceFail();
            }
        }


        if (updateChannelsFlag.testAndSetAcquire(true, false)) {
            updateChannels();
        }
        if (updateDataLengthFlag.testAndSetAcquire(true, false)) {
            updateDataLength();
        }
        // check to see if data are available on any of the dataHandlers
        for (int i = 0; i < dataHandler.length(); i++) {
            if (((dataHandler[i]->getSocketType() == TRODESSOCKETTYPE_TCPIP) &&
                    (dataHandler[i]->tcpSocket->waitForReadyRead(0))) ||
               ((dataHandler[i]->getSocketType() == TRODESSOCKETTYPE_UDP) &&
                   (dataHandler[i]->udpSocket->hasPendingDatagrams()))) {
                //qDebug() << "reading message on dataHandler" << i;
                dataHandler[i]->readMessage();
            }


        }

        if (digitalIOHandler != NULL) {
            if (((digitalIOHandler->getSocketType() == TRODESSOCKETTYPE_TCPIP) &&
                    (digitalIOHandler->tcpSocket->waitForReadyRead(0))) ||
                ((digitalIOHandler->getSocketType() == TRODESSOCKETTYPE_UDP) &&
                    (digitalIOHandler->udpSocket->hasPendingDatagrams()))) {
                qDebug() << "reading message on digitalIOHandler";
                digitalIOHandler->readMessage();
            }
        }
        // TODO: Analog data
    }


    //Don't use: this version causes up to 20x increase in CPU usage during multithreading!!!

    /*
    RawDataSample rawDataSample(0, 0);
    while (!exitLoop) {
        if (rawDataAvailable[rawDataAvailableIdx]->tryAcquire(1, 100)) {
            samplesToCopy = rawDataAvailable[rawDataAvailableIdx]->available();
            if (samplesToCopy > 15000) {
                emit bufferOverrun();
            }
            if (!rawDataAvailable[rawDataAvailableIdx]->tryAcquire(samplesToCopy))
                qDebug() << "Error acquiring available samples, group " << groupNum;

            samplesToCopy += 1; // we acquired one at the beginning

            for (int s = 0; s < samplesToCopy; s++) {
                // -----------------------------------------------------------------
                // LOAD NEW DATA STRUCTURE
                rdInd = rawIdx * hardwareConf->NCHAN;



                for (int n = 0; n < spikeConf->ntrodes.length(); n++) {

                    for (int c = 0; c < spikeConf->ntrodes.at(n)->hw_chan.length(); c++) {
                        rawDataSample.neuralData[n][c] = rawData.data[rdInd + spikeConf->ntrodes[n]->hw_chan[c]];
                    }
                }

                for (int h = 0; h < headerConf->headerChannels.length(); h++) {
                    startBytePtr = ((char*)(rawData.digitalInfo + (rawIdx * hardwareConf->headerSize))) + headerConf->headerChannels[h].startByte;
                    if (headerConf->headerChannels[h].dataType == DeviceChannel::DIGITALTYPE) {
                        rawDataSample.headerData[h] = (int)((*startBytePtr & (1 << headerConf->headerChannels[h].digitalBit)) >>
                                                            headerConf->headerChannels[h].digitalBit);

                    }
                    else if (headerConf->headerChannels[h].dataType == DeviceChannel::INT16TYPE) {
                        // TO DO: add analogIO output
                        if (headerConf->headerChannels[h].interleavedDataIDByte != -1) {
                            interleavedDataIDBytePtr = ((uint8_t*)(rawData.digitalInfo + (rawIdx * hardwareConf->headerSize))) + headerConf->headerChannels[h].interleavedDataIDByte;

                            if (*interleavedDataIDBytePtr & (1 << headerConf->headerChannels[h].interleavedDataIDBit)) {
                                //This interleaved data point belongs to this channel, so update the channel. Otherwise, no update occurs.
                                //qDebug() << headerConf->headerChannels[h].interleavedDataIDBit;
                                rawDataSample.headerData[h] = *((int16_t*)(startBytePtr)); //change to 16-bit pointer, then dereference
                            }
                        } else {

                            rawDataSample.headerData[h] = *((int16_t*)(startBytePtr)); //change to 16-bit pointer, then dereference
                        }
                    }
                }
                rawDataSample.timestamp = rawData.timestamps[rawIdx];
                rawDataSample.dTime = rawData.timestamps[rawIdx];
                // -----------------------------------------------------------------


                streamIdx = 0;


                //First we process the neural channels
                for (int n = 0; n < nTrodeList.length(); n++) {
                    int nt = nTrodeList.at(n); // the nTrodeList may not be identical to all the nTrodes

                    //If the digital reference is on, subtract the ref value
                    for (int c = 0; c < spikeConf->ntrodes[nt]->hw_chan.length(); c++) {


                        if (spikeConf->ntrodes[nt]->refOn) {
                            tmpDataPoint = (int)(rawDataSample.neuralData[nt][c] -
                                                 rawDataSample.neuralData[spikeConf->ntrodes[nt]->refNTrode][spikeConf->ntrodes[nt]->refChan]);

                        }
                        else {

                            tmpDataPoint = rawDataSample.neuralData[nt][c];
                        }


                        //Extracellular recordings, so we reverse polarity to make spikes go upward
                        tmpDataPoint *= -1;

                        hw_chan = spikeConf->ntrodes[nt]->hw_chan[c];

                        //send data to module if enabled

                        int nTrodeId = spikeConf->ntrodes.at(nt)->nTrodeId;
                        for (int d = 0; d < dataHandler.length(); d++) {
                            if ((dataHandler[d]->getNTrodeId() == nTrodeId) && (dataHandler[d]->getNTrodeChan() == c) &&
                                    dataHandler[d]->isModuleDataStreamingOn()) {
                                int16_t sendDataPoint = filtersForContinuousSendChannels[d]->addValue(tmpDataPoint);
                                if ((rawDataSample.timestamp % dataHandler[d]->getDecimation()) == 0) {
                                    if (rawDataSample.timestamp == 0) {
                                        qDebug() << "Stream processor sent a 0 timestamp" << ". Current thread: " << QThread::currentThreadId();
                                    }
                                    dataHandler[d]->sendContinuousDataPoint(rawDataSample.timestamp, sendDataPoint);
                                }
                            }
                        }

                        //Run the signal through the filter (this is done below if the stream display is not
                        //linked to the filters
                        if (filtersOn[n] && linkStreamToFilters) {
                            tmpDataPoint = streamConf->dataFilters[hw_chan].addValue(tmpDataPoint);
                            //tmpDataPoint = dataFilters[currentChannel].addValue(tmpDataPoint);
                        }

                        //If the signals are huge, clip them in the display.  TODO:  use the max display setting for this channel as the clip.


                        tmpMaxPoint = qMin(tmpDataPoint, 5000);
                        tmpMinPoint = qMax(tmpDataPoint, -5000);



                        //Calulate the current max and min values in the current display bin
                        if (stream_inc > 0) {
                            //Old bin, so compare to existing value
                            streamManager->neuralDataMinMax[hw_chan][dataIdx * 2].y =
                                    qMax((GLfloat)tmpMaxPoint, streamManager->neuralDataMinMax[hw_chan][dataIdx * 2].y);
                            streamManager->neuralDataMinMax[hw_chan][(dataIdx * 2) + 1].y =
                                    qMin((GLfloat)tmpMinPoint, streamManager->neuralDataMinMax[hw_chan][(dataIdx * 2) + 1].y);
                        }
                        else {
                            //New bin, so reset the value
                            streamManager->neuralDataMinMax[hw_chan][dataIdx * 2].y = tmpMaxPoint;
                            streamManager->neuralDataMinMax[hw_chan][(dataIdx * 2) + 1].y = tmpMinPoint;
                        }




                        //Filter here if stream display is not linked to filters
                        if (filtersOn[n] && !linkStreamToFilters) {
                            tmpDataPoint = streamConf->dataFilters[hw_chan].addValue(tmpDataPoint);
                            //tmpDataPoint = dataFilters[currentChannel].addValue(tmpDataPoint);
                        }

                        // Send data to spike detector
#ifndef NEW_SPIKE_DETECTOR
                        streamManager->nTrodeTriggerProcessors[nt]->addValue(c, tmpDataPoint, rawDataSample.timestamp);
#else
                        dataPoints[n][c] = tmpDataPoint;
                        //streamManager->spikeDetectors[nt]->newChannelData(c, tmpDataPoint, rawDataSample.timestamp);
#endif


                        streamIdx++; // this keeps track of which row in the

                    }
#ifdef NEW_SPIKE_DETECTOR
                    streamManager->spikeDetectors[nt]->newData(dataPoints[n], rawDataSample.timestamp);
#endif

                }




                //Now we process the auxilliary channels (digial I/O, analog I/O), if any
                for (int h = 0; h < auxChannelList.length(); h++) {
                    int hch = auxChannelList[h];

                    tmpDataPoint = rawDataSample.headerData[hch];
                    if (headerConf->headerChannels[hch].dataType == DeviceChannel::DIGITALTYPE) {
                        newDigIOState = false;
                        // check to see if we are could send out data
                        if (digitalIOHandler != NULL) {
                            port = headerConf->headerChannels[hch].port;
                            // check if this is an input, and if so, if the state of the port has changed
                            if ((headerConf->headerChannels[hch].input) && ((bool)tmpDataPoint != digInState[port])) {
                                newDigIOState = true;
                                digInput = true;
                                digInState[port] = (bool)tmpDataPoint;
                            }
                            // if not, check if this is an output, and if so, if the state of the port has changed
                            else if ((!headerConf->headerChannels[hch].input) && ((bool)tmpDataPoint != digOutState[port])) {
                                newDigIOState = true;
                                digInput = false;
                                digOutState[port] = (bool)tmpDataPoint;
                            }
                            // if the state changed and we're supposed to stream data, send out the new port status.
                            if ((newDigIOState) && (digitalIOHandler->isModuleDataStreamingOn())) {
                                digitalIOHandler->sendDigitalIOData(rawDataSample.timestamp, port,
                                                                    (char)digInput, (char)tmpDataPoint);
                            }
                        }
                    }

                    //If the signals are huge, clip them in the display.  TODO:  use the max display setting for this channel as the clip.

                    //tmpMaxPoint = qMin(tmpDataPoint, 2);
                    //tmpMinPoint = qMax(tmpDataPoint, -1);

                    //Calulate the current max and min values in the current display bin
                    if (stream_inc > 0) {
                        //Old bin, so compare to existing value
                        streamManager->auxDataMinMax[hch][dataIdx * 2].y =
                                qMax((GLfloat)tmpDataPoint, streamManager->auxDataMinMax[hch][dataIdx * 2].y);
                        streamManager->auxDataMinMax[hch][(dataIdx * 2) + 1].y =
                                qMin((GLfloat)tmpDataPoint, streamManager->auxDataMinMax[hch][(dataIdx * 2) + 1].y);
                    }
                    else {
                        //New bin, so reset the value
                        streamManager->auxDataMinMax[hch][dataIdx * 2].y = tmpDataPoint;
                        streamManager->auxDataMinMax[hch][(dataIdx * 2) + 1].y = tmpDataPoint;
                    }
                }

                rawIdx = (rawIdx + 1) % EEG_BUFFER_SIZE;
                if (++stream_inc >= raw_increment[dataIdx]) {
                    dataIdx = (dataIdx + 1) % EEG_TIME_POINTS;
                    stream_inc = 0;
                }
            }
        } else if (quitNow == 1) {
            exitLoop = true;
            isLooping = false;
        } else {
            //qDebug() << "No data!";
            numLoopsWithNoData++;
            if (numLoopsWithNoData > 20000) {
                numLoopsWithNoData = 0;
                qDebug() << "No data!";
                //emit sourceFail();
            }
        }


        if (updateChannelsFlag.testAndSetAcquire(true, false)) {
            updateChannels();
        }
        if (updateDataLengthFlag.testAndSetAcquire(true, false)) {
            updateDataLength();
        }
        // check to see if data are available on any of the dataHandlers
        for (int i = 0; i < dataHandler.length(); i++) {
            if (dataHandler[i]->tcpSocket->waitForReadyRead(0)) {
                qDebug() << "reading message on dataHandler" << i;
                dataHandler[i]->readMessage();
            }
        }

        if (digitalIOHandler != NULL) {
            if (digitalIOHandler->tcpSocket->waitForReadyRead(0)) {
                digitalIOHandler->readMessage();
            }
        }
    }

    */
    qDebug() << "Stream processor loop ended.";
}
