/*
Trodes is a free, open-source neuroscience data collection and experimental control toolbox

Copyright (C) 2015 Mattias Karlsson, Caleb Kemere

This program is free software: you can redistribute it and/or modify
                               it under the terms of the GNU General Public License as published by
                               the Free Software Foundation, either version 3 of the License, or
                               (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef RHYTHMTHREAD_H
#define RHYTHMTHREAD_H


#include <QThread>
#include <QVector>
#include "abstractTrodesSource.h"
#include "rhythm-api/okFrontPanelDLL.h"
#include "rhythm-api/rhd2000datablock.h"
#include "rhythm-api/rhd2000evalboard.h"
#include "rhythm-api/rhd2000registers.h"

class RhythmRuntime : public AbstractSourceRuntime {
  Q_OBJECT
public:
  RhythmRuntime(QObject *parent);
  QVector<unsigned char> buffer;

  enum initializationError {
      INITIALIZATION_SUCCESS,
      FRONTPANEL_LIB_LOAD_ERROR,
      XEM6010_NOT_FOUND,
      BITFILE_NOT_FOUND,
      BITFILE_UPLOAD_ERROR,
      OTHER_ERROR
  };

public:
  int stopNow;

public slots:
  void Run(void);
  void scanPorts(void);
  int initializeBoard(void);
  void closeBoard(void);

signals:
  void stopped(void);

private:
  Rhd2000EvalBoard *rhythmInterfaceBoard;


  bool deviceFound, boardInitialized;
  double cableLengthPortA, cableLengthPortB, cableLengthPortC, cableLengthPortD;

  double boardSampleRate;
  int numUsbBlocksToRead;

  QVector<int> chipId;
  //QVector<bool> auxDigOutEnabled;
  queue<Rhd2000DataBlock> dataQueue;

  int deviceId(Rhd2000DataBlock* dataBlock, int stream, int& register59Value);
  void changeSampleRate(int sampleRateIndex);

};

class RhythmInterface : public AbstractTrodesSource {
  Q_OBJECT

public:
  RhythmInterface(QObject *parent);
  ~RhythmInterface(void);
  int state;

private:
  //struct libusb_transfer **transfers;
  //int n_transfers;
  RhythmRuntime *rhythmDataProcessor;
  QThread *workerThread;

public slots:
  void InitInterface(void);
  void StartAcquisition(void);
  void StopAcquisition(void);
  void CloseInterface(void);

};



#endif // RHYTHMTHREAD_H
