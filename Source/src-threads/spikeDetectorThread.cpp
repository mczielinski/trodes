/*
   Trodes is a free, open-source neuroscience data collection and experimental control toolbox

   Copyright (C) 2015 Mattias Karlsson

   This program is free software: you can redistribute it and/or modify
                               it under the terms of the GNU General Public License as published by
                               the Free Software Foundation, either version 3 of the License, or
                               (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "spikeDetectorThread.h"
//#include "globalObjects.h"
//#include "trodesSocket.h"

SpikeDetectorManager::SpikeDetectorManager(QObject *parent, QList<int> ntList, QList<ThresholdSpikeDetector *> *spikeDetectorList) :
    QObject(parent),
    streamManagerSpikeDetectorList(spikeDetectorList),
    nTrodeList(ntList)
{
    // nTrodeList specifies which NTrodes this SpikeDetectorThread is responsible for

    for (int n = 0; n < spikeConf->ntrodes.length(); n++)
        nTrodeLookup.append(-1); // start with minus 1's

    for (int n = 0; n < spikeConf->ntrodes.length(); n++) {
        for (int j = 0; j < nTrodeList.length(); j++) {
            if (nTrodeList.at(j) == n) {
                nTrodeLookup[n] = j;
                break;
            }
        }
    }
}

SpikeDetectorManager::~SpikeDetectorManager()
{
}

void SpikeDetectorManager::setupAndRun() // Runs after object is sent to its own thread
{
    // Create an ThresholdSpikeDetector for each ntrode. This is the object
    // that contains the waveform data and also has the spike detection
    // functionality.
    dataProvided.moduleID = TRODES_ID;
    dataProvided.dataType = TRODESDATATYPE_SPIKES;
    dataProvided.socketType = networkConf->dataSocketType;

    if (networkConf->dataSocketType == TRODESSOCKETTYPE_TCPIP) {
        // we need to create a tcpServer to send out data
            dataServer = new TrodesServer();
        if (networkConf->networkConfigFound) {
            //if the config file designates the address , use it
            dataServer->setAddress(networkConf->trodesHost);
        }
        dataServer->startLocalServer("spikeDetectorManager");
        // Fill out the rest of the dataProvided structure for this streamManager
        dataProvided.hostName = dataServer->getCurrentAddress();
        dataProvided.hostPort = dataServer->getCurrentPort();
    }
    else if (networkConf->dataSocketType == TRODESSOCKETTYPE_UDP) {
        dataProvided.hostName = networkConf->trodesHost;
    }

    for (int nt = 0; nt < nTrodeList.length(); nt++) {
        managedSpikeDetectors.append(new ThresholdSpikeDetector(this, nTrodeList.at(nt)));
        // each time a new data handler is created we send it to all the spike detectors but only the one with
        // the matching nTrode saves it for data sending
        if (networkConf->dataSocketType == TRODESSOCKETTYPE_TCPIP) {
            connect(dataServer, SIGNAL(newDataHandler(TrodesSocketMessageHandler*, qint16)),
                    managedSpikeDetectors.last(), SLOT(newDataHandler(TrodesSocketMessageHandler*,qint16)));
        }
        else if (networkConf->dataSocketType == TRODESSOCKETTYPE_UDP) {
            // create a UDP server for this nTrode
            TrodesUDPSocket *trodesUDPSocket = new TrodesUDPSocket("", this);
            TrodesSocketMessageHandler *newHandler = trodesUDPSocket->newDataHandler(TRODESDATATYPE_SPIKES);
            // we can now just call newDataHandler to add it to the list
            newDataHandler(newHandler, nTrodeList.at(nt));
            dataProvided.spikeNTrodeUDPPortList.append(trodesUDPSocket->getCurrentPort());
        }

        if (streamManagerSpikeDetectorList->at(nTrodeList.at(nt)) == NULL) {
            streamManagerSpikeDetectorList->replace(nTrodeList.at(nt), managedSpikeDetectors.last());
            // add this nTrode index to the dataProvided list
            dataProvided.spikeNTrodeIndexList.append(nTrodeList.at(nt));
        }
        else {
            qDebug() << "Multiple spike detectors generated for one nTrode?!";
        }
    }

    // add that dataProvided structure to the main list
    emit addDataProvided(&dataProvided);

    //QTimer *latencyReporting = new QTimer(this);
    //connect(latencyReporting, SIGNAL(timeout()), managedSpikeDetectors[0], SLOT(reportLatency()));
    //latencyReporting->start(1000);

    // Infinite loop here will block thread event loop.
    // So we'll process spikes, check for events, and then sleep for a little bit.
    // The length of the sleep will affect the latency of spike reporting, but also reduce load.

    quitNow = false;
    while (!quitNow) {
        for (int nt = 0; nt < nTrodeList.length(); nt++)
            managedSpikeDetectors[nt]->processNewData();
        this->thread()->eventDispatcher()->processEvents(QEventLoop::AllEvents);
        if (globalConf->realTimeMode) {
            QThread::usleep(1000); //is using less than 1ms useful here? Big hit on CPU.
        } else {
            QThread::usleep(10000);
        }
    }
}

void SpikeDetectorManager::stopRunning() {
    quitNow = true;
}


void SpikeDetectorManager::updateSpikeThreshold(int nTrode, int newThresh)
{
    if (nTrodeLookup.at(nTrode) != -1)
        managedSpikeDetectors.at(nTrodeLookup.at(nTrode))->newSpikeThreshold(spikeConf->ntrodes[nTrode]->thresh_rangeconvert[0]);

}

void SpikeDetectorManager::updateSpikeMode(int nTrode, int channel, bool triggerOn)
{
    if (nTrodeLookup.at(nTrode) != -1)
        managedSpikeDetectors.at(nTrodeLookup.at(nTrode))->newTriggerOn(channel, triggerOn);
}


ThresholdSpikeDetector::ThresholdSpikeDetector(QObject *parent, int nt) :
    GenericSpikeDetector(parent, nt)
{
    dataBuffer = new int16_t *[BUFFERSIZE];
    thresholds.resize(numChannels);
    triggersOn.resize(numChannels);
    isLocked.resize(numChannels);

    timeBuffer = new uint32_t[BUFFERSIZE];
    for (int c = 0; c < numChannels; c++) {
        thresholds[c] = spikeConf->ntrodes[nTrode]->thresh_rangeconvert[c];
        isLocked[c] = false;
    }
    for (int i = 0; i < BUFFERSIZE; i++) {
        dataBuffer[i] = new int16_t[numChannels];
        for (int c = 0; c < numChannels; c++)
            dataBuffer[i][c] = 0;
    }

    waveForms.resize(numChannels);
    peaks.resize(numChannels * 2); //trough info will be stored too
    for (int i = 0; i < numChannels; i++) {
        waveForms[i].resize(POINTSINWAVEFORM);
        peaks[i] = 0;
        peaks[i + numChannels] = 0;
        for (int j = 0; j < POINTSINWAVEFORM; j++) {
            waveForms[i][j].x = j;
            waveForms[i][j].y = 0;
        }
    }

    snipStartPtr = 0;
    tPtr = snipStartPtr + POINTSINWAVEFORM;
    last_tPtr = tPtr;
    threshPtr = snipStartPtr + POINTSTOREWIND;

    largest_latency = 0;

    // allocate space for an array for all of the points of a spike
    nSpikePoints = numChannels * POINTSINWAVEFORM;
    waveformData = new int16_t[nSpikePoints];
}

ThresholdSpikeDetector::~ThresholdSpikeDetector()
{
    for (int i = 0; i < BUFFERSIZE; i++) {
        delete [] dataBuffer[i];
    }
    delete [] timeBuffer;
    delete [] dataBuffer;
}

void ThresholdSpikeDetector::newDataHandler(TrodesSocketMessageHandler *messageHandler, qint16 nTrodeIndex)
{
    if (nTrode == nTrodeIndex) {
        // add the data handler to the local list
        dataHandler.append(messageHandler);
        dataHandler.last()->setNTrodeIndex(nTrode);
    }
}


void ThresholdSpikeDetector::newData(const int16_t *newData, const uint32_t time)
{
    // Copy newly filtered spike data into the dataBuffer
    // This function is actually directly called in streamProcessor. Thus, it executes
    // in the streamProcessor thread. Try to make it as low impact as possible!
    //
    // There are two versions of this function. This one, which processes a chunk of data
    // at a time, and the next one, which processes a point at a time.

    tPtr = (tPtr + 1) % BUFFERSIZE;
    timeBuffer[tPtr] = time;
    memcpy(dataBuffer[tPtr], newData, numChannels * sizeof(int16_t));

//  for (int c = 0; c < numChannels; c++)
//    dataBuffer[tPtr][c] = newData[c];
}

void ThresholdSpikeDetector::newChannelData(int channel, int16_t newData, uint32_t time)
{
    // Copy newly filtered spike data into the dataBuffer
    // This function is actually directly called in streamProcessor. Thus, it executes
    // in the streamProcessor thread. Try to make it as low impact as possible!
    //
    // There are two versions of this function. This one, which processes a sample of data
    // at a time, and the previous one, which processes a full nTrode's worth at a time.
    // We only need one timestamp for each data point, so we'll update if it hasn't changed


    if (timeBuffer[tPtr] != time) {
        tPtr = (tPtr + 1) % BUFFERSIZE;
        timeBuffer[tPtr] = time;
    }
    dataBuffer[tPtr][channel] = newData;
}

void ThresholdSpikeDetector::newSpikeThreshold(int newThresh)
{
    for (int i=0; i<thresholds.length();i++) {
        thresholds[i] = newThresh;
    }
}

void ThresholdSpikeDetector::processNewData()
{
    // This is the actual function which detects spikes.
    // Originally, we designed it to be called for each new value, but this ends up hitting us with function
    // call overhead. So we've modified it so that each time it's called, it tries to catch up with the most
    // recent data sample, indexed by tPtr. We have to be cognizant that new data could be added at any point
    // during the function execution, so we are careful about getting a snapshot of the state at the beginning.
    //
    // The system "latency" is going to be how many samples behind we are when we call the function. We track
    // a worst case and a running average (implemented as an IIR filter).

    // Detection operation:
    // check threshold at data[newest - post-threshold length]
    // check that not locked out because a spike has already been detected
    //   (lockout ends when all channels go below their thresholds)
    //   if triggered
    //     compute current snippet peak
    //     emit snippet and peak

    int16_t datum;
    int16_t *waveformPtr;
    char *charPtr;

    int new_tPtr;

    new_tPtr = tPtr; // ASSUMING THIS IS ATOMIC/SAFE (should be fine for an int). Snapshot the tPtr; it might change later)

    int samples_behind = new_tPtr - last_tPtr;
    if (samples_behind < 0) // account for circular buffer
        samples_behind += BUFFERSIZE;
    last_tPtr = new_tPtr; // update last_tPtr

    // track latency
    largest_latency = qMax(largest_latency, samples_behind);
    average_latency = 0.9 * average_latency + 0.1 * samples_behind;

    for (int k = 0; k < samples_behind; k++) {
        snipStartPtr = (snipStartPtr + 1) % BUFFERSIZE;
        threshPtr = (threshPtr + 1) % BUFFERSIZE;

        spikeDetected = false; spikeVetoed = false;
        for (int c = 0; c < numChannels; c++) {
            if (triggersOn[c]) {
                if (!isLocked[c] && (dataBuffer[threshPtr][c] >= thresholds[c]))
                    spikeDetected = true; // if we weren't locked out and are over threshold, propose spike detection
                if (isLocked[c] && (dataBuffer[threshPtr][c] >= thresholds[c]))
                    spikeVetoed = true; // if we were locked out and still over threshold, veto spike detection
                isLocked[c] = (dataBuffer[threshPtr][c] >= thresholds[c]);
            }
        }
        if (spikeDetected & !spikeVetoed) { // a spike was detected and not vetoed
            // Initialize peaks
            for (int c = 0; c < numChannels; c++) {
                peaks[c] = -1000; // should make this max int
                peaks[c + numChannels] = 1000; // trough
            }

            if (writeSpikesToDisk) {
                //Write the time stamp of the spike
                charPtr = (char*)(&(timeBuffer[threshPtr]));
                outStream->writeRawData(charPtr, sizeof(uint32_t));
            }

            // Copy waveform into snippet
            waveformPtr = waveformData;
            for (int i = 0; i < POINTSINWAVEFORM; i++) { // LOOP ORDER IS IMPORTANT FOR DATAHANDLER STREAM!!!
                for (int c = 0; c < numChannels; c++, waveformPtr++) {
                    *waveformPtr = datum = dataBuffer[(snipStartPtr + i) % BUFFERSIZE][c]; // should efficentize this with a memcpy?
                    waveForms[c][i].y = datum; // implicit cast to GLFloat

                    // find waveform min and max
                    if (datum > peaks[c])
                        peaks[c] = datum;
                    if (datum < peaks[c + numChannels])
                        peaks[c + numChannels] = datum;

                    if (writeSpikesToDisk) {
                        charPtr = (char*)(&datum);
                        outStream->writeRawData(charPtr, 2);
                    }
                }
            }

            // go through the list of dataHandlers and send the spikeData out when appropriate
            for (int i = 0; i < dataHandler.length(); i++) {
                if (dataHandler.at(i)->isModuleDataStreamingOn()) {
                    dataHandler.at(i)->sendSpikeData(timeBuffer[threshPtr], nSpikePoints, waveformData);
                }
            }

            //Emit the waveform data for all channels
            //peaks contains the peak amplitude for all channels, followed by the minimum amplitude for all channels
            //Normally, sending a pointer to another thread to data that is going to be overwritten by
            //this thread during the next event is a bad idea.  However, this particular signal/slot is connected via
            //a direct connection instead of a queued connection, which means that this thread will do the work
            //of copying the data before it moves on.
            emit spikeDetectionEvent(nTrode, waveForms.constData(), peaks.constData(), timeBuffer[threshPtr]);
        }
    }
}

void ThresholdSpikeDetector::clearHistory() {
    for (int i = 0; i < BUFFERSIZE; i++) {
        for (int c = 0; c < numChannels; c++)
            dataBuffer[i][c] = 0;
    }
    snipStartPtr = 0;
    tPtr = snipStartPtr + POINTSINWAVEFORM;
    last_tPtr = tPtr;
    threshPtr = snipStartPtr + POINTSTOREWIND;
}

int ThresholdSpikeDetector::reportLatency()
{
    qDebug() << "NTrode[" << nTrode << "]: Average latency: " << average_latency << ". Largest latency: " << largest_latency << " samples.";
}

GenericSpikeDetector::GenericSpikeDetector(QObject *parent, int nt) :
    QObject(parent),
    nTrode(nt)
{
    numChannels = spikeConf->ntrodes[nTrode]->hw_chan.length();
    triggersOn.resize(numChannels);
    for (int c = 0; c < numChannels; c++) {
        triggersOn[c] = spikeConf->ntrodes[nTrode]->triggerOn[c];
    }

    logFile = NULL;
    writeSpikesToDisk = false;
}

GenericSpikeDetector::~GenericSpikeDetector()
{
}


void GenericSpikeDetector::newTriggerOn(int channel, bool triggerOn)
{
    triggersOn[channel] = triggerOn;
}

void GenericSpikeDetector::createLogFile(QString path)
{
    QString fileName = path + QDir::toNativeSeparators("/") + QString("nTrode%1").arg(nTrode + 1) + ".spikes";

    logFile->setFileName(fileName);
    if (logFile->exists()) {
        logFile->remove();
    }
    writeTrodesConfig(fileName); //write the current configuration and settings to disk

    //append recorded data after the config info
    if (!logFile->open(QIODevice::Append)) {
        qDebug() << "Error opening file";
    }
    outStream = new TrodesDataStream(logFile); //link outStream to the file
    writeSpikesToDisk = true;
}

void GenericSpikeDetector::closeLogFile()
{
    if ((logFile->isOpen())) {
        writeSpikesToDisk = false;
        QThread::msleep(10);
        delete outStream;
        logFile->close();
    }
}

