/*
Trodes is a free, open-source neuroscience data collection and experimental control toolbox

Copyright (C) 2012 Mattias Karlsson

This program is free software: you can redistribute it and/or modify
                               it under the terms of the GNU General Public License as published by
                               the Free Software Foundation, either version 3 of the License, or
                               (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include "fileSourceThread.h"
#include "globalObjects.h"

#include <QFileDialog>

fileSourceRuntime::fileSourceRuntime(QObject *parent):
    waitForThreadsFlag(false)
    , mSecElapsed(0)
    , currentSampleNum(0)
    , currentTimestamp(0) {
    loopFinished = true;

}

fileSourceRuntime::~fileSourceRuntime() {

}

void fileSourceRuntime::Run() {


    calculateChannelInfo();

    //char buffer[PACKET_SIZE];
    buffer.resize(PACKET_SIZE);
    char *RxBuffer;
    int remainingSamples = 0;
    int leftInBuffer;
    double dTimestamp = 0.0;

    //Some variables for implementing a 'brake' for the loop
    QElapsedTimer loopTimer;
    qint64 optimalLoopTime_nS = (qint64)(1/((double) hardwareConf->sourceSamplingRate*filePlaybackSpeed)*1000000000);
    qint64 loopTimeSurplus_nS;

    //qint64 samplesRead = 0;

    quitNow = false;
    aquiring = true;

    //timeKeeper->start();
    stopWatch.start();

    qDebug() << "File playback loop running....";
    qint64 samplesWritten = 0;
    //float cycleStepsPerSample;
    float mSecPerSample;
    bool atBeginning = true;
    loopFinished = false;

    while (quitNow != true) {

        if (!aquiring) {
            stopWatch.restart();
            samplesWritten = 0;
            QThread::msleep(100);

            if (jumpToBeginning) { //reset back to the beginning of the file
                stopWatch.restart();
                samplesWritten = 0;
                currentTimeStamp = 0;
                if (!atBeginning) {
                    file->seek(fileDataPos); //return to the begining of the file
                    atBeginning = true;
                    playbackFileCurrentLocation = fileDataPos;
                    jumpToBeginning = false;
                }
            }
        } else {
            if (waitForThreadsFlag) {
                QThread::msleep(250);


                if (filePlaybackSpeed > 1) {
                    filePlaybackSpeed--;
                    qDebug() << "Reducing processing speed to " << filePlaybackSpeed;
                }
                //TODO: we need a lock here
                waitForThreadsFlag = false;
            }
            loopTimer.restart();
            atBeginning = false;
            //samplesPerCycle = (hardwareConf->sourceSamplingRate/waveFrequency);
            //cycleStepsPerSample = waveRes/samplesPerCycle;

            //float nSecPerSample = 1000000000.0/hardwareConf->sourceSamplingRate;
            mSecPerSample = 1000.0/(hardwareConf->sourceSamplingRate*filePlaybackSpeed);

            //mSecPerSample = 1000.0/100000;


            //nSecElapsed = nSecElapsed + stopWatch.nsecsElapsed();
            //nSecElapsed = stopWatch.nsecsElapsed();
            mSecElapsed = stopWatch.elapsed();

            //stopWatch.restart();
            int samplesDue = floor(mSecElapsed/mSecPerSample)-samplesWritten;

            while (samplesDue > 0) {
                //int16_t currentValue = (int16_t) ((float) sourceData[currentCyclePosition] * (2*(float) waveAmplitude/12500.0));
                if (!(file->read(buffer.data(),PACKET_SIZE) == PACKET_SIZE)) {
                    //could not read a full packet.  Probably end of file.
                    qDebug() << "End of file reached";
                    //quitNow = true;
                    if (exportMode){
                        QThread::msleep(1000); // pause to let the rest of the threads finish processing
                    }
                    aquiring = false;
                    emit endOfFileReached();
                    break;
                }


                playbackFileCurrentLocation = file->pos();

                leftInBuffer = PACKET_SIZE;
                RxBuffer = buffer.data();

                //Read header info
                memcpy(&(rawData.digitalInfo[rawData.writeIdx*hardwareConf->headerSize]), \
                       RxBuffer, hardwareConf->headerSize*sizeof(uint16_t));
                RxBuffer += (2*hardwareConf->headerSize);
                leftInBuffer -= (2*hardwareConf->headerSize);


                //Process time stamp
                uint32_t* dataPtr = (uint32_t *)(RxBuffer);
                currentTimeStamp = *dataPtr;
                rawData.timestamps[rawData.writeIdx] = *dataPtr;
                RxBuffer += 4;
                leftInBuffer -= 4;
                dTimestamp += 1.0;
                rawData.dTime[rawData.writeIdx] = dTimestamp;

                int16_t *rawDataPtr = rawData.data + rawData.writeIdx*hardwareConf->NCHAN;
                int16_t *RxBufferPtr;
                // copy the data points to the appropriate elements of the rawData.data buffer
                if (!globalConf->saveDisplayedChanOnly) {
                    //The entire harware channel list was saved
                    RxBufferPtr = (int16_t *) RxBuffer;
                    remainingSamples = hardwareConf->NCHAN;
                    memcpy(&(rawData.data[rawData.writeIdx*hardwareConf->NCHAN]), \
                           RxBuffer, remainingSamples*sizeof(uint16_t));
                } else {
                     //Some channels were possibly excluded from the file
                    for (int i = 0; i < numChannelsInFile; i++) {
                        RxBufferPtr = (int16_t *)(buffer.data()+channelPacketLocations_input[i]);
                        //RxBufferPtr = (int16_t *) RxBuffer + channelPacketLocations_input[i];
                        rawDataPtr[channelPacketLocations_output[i]] = *RxBufferPtr;

                    }
                }

                //Process the data in the packet
                /*
                if (!globalConf->saveDisplayedChanOnly) {
                    int16_t RxBufferPtr = (int16_t *) RxBuffer
                    remainingSamples = hardwareConf->NCHAN;
                    memcpy(&(rawData.data[rawData.writeIdx*hardwareConf->NCHAN]), \
                           RxBuffer, remainingSamples*sizeof(uint16_t));
                }
                else {
                    int16_t *rawDataPtr = rawData.data + rawData.writeIdx*hardwareConf->NCHAN;
                    int16_t *RxBufferPtr;
                    // copy the data points to the appropriate elements of the rawData.data buffer
                    for (int i = 0; i < numChannelsInFile; i++) {
                        RxBufferPtr = (int16_t *) RxBuffer + channelPacketLocations_input[i];
                        rawDataPtr[channelPacketLocations_output[i]] = *RxBufferPtr;

                    }
                }*/

                //RxBuffer += remainingSamples * 2;
                //leftInBuffer -= remainingSamples * 2;
                //remainingSamples = 0;

                //Advance the write markers and release a semaphore
                rawData.writeIdx = (rawData.writeIdx + 1) % EEG_BUFFER_SIZE;

                for (int a = 0; a < rawDataAvailable.length(); a++) {
                    rawDataAvailable[a]->release(1);
                }

                rawDataWritten++;
                samplesWritten++;
                samplesDue--;


            }
            //QThread::usleep(1000);

            loopTimeSurplus_nS = optimalLoopTime_nS-loopTimer.nsecsElapsed();
            if (loopTimeSurplus_nS > 1000) {
                QThread::usleep(5*loopTimeSurplus_nS/1000);
            }
        }

    }

    file->close();

    delete file;
    qDebug() << "File read loop finished.";
    loopFinished = true;
    emit finished();

}

void fileSourceRuntime::calculateChannelInfo() {


        int packetHeaderSize = (2*hardwareConf->headerSize)+4; //Aux info plus 4-byte timestamp
        int packetTimeLocation = 2*hardwareConf->headerSize;


        QList<int> sortedChannelList;
        //Gather all HW channels
        for (int n = 0; n < spikeConf->ntrodes.length(); n++) {
            for (int c = 0; c < spikeConf->ntrodes[n]->hw_chan.length(); c++) {
                int tempHWRead = spikeConf->ntrodes[n]->hw_chan[c];
                sortedChannelList.push_back(tempHWRead);
            }
        }
        //Sort the channels
        std::sort(sortedChannelList.begin(),sortedChannelList.end());

        //Remember how many channels are actually saved in the file
        //(may be different from how many channels came from recording hardware)

        if (globalConf->saveDisplayedChanOnly) {
            numChannelsInFile = sortedChannelList.length();
        } else {
            numChannelsInFile = hardwareConf->NCHAN;
        }

        //filePacketSize = packetHeaderSize+(2*numChannelsInFile);

        //Then we find any unsaved channels, and from that determine the new packet locations
        //of each channel

        int lastChannel = -1;
        int unusedChannelsSoFar = 0;
        for (int i=0;i<sortedChannelList.length();i++) {
            if ((sortedChannelList[i]-lastChannel > 1) && (globalConf->saveDisplayedChanOnly)){
                unusedChannelsSoFar = unusedChannelsSoFar + (sortedChannelList[i]-lastChannel-1);
            }
            channelPacketLocations_input.push_back((2*hardwareConf->headerSize)+4+(2*(sortedChannelList[i]-unusedChannelsSoFar)));
            channelPacketLocations_output.push_back(sortedChannelList[i]);
            //qDebug() << sortedChannelList[i] << channelPacketLocations_input.last() << channelPacketLocations_output.last();
            lastChannel = sortedChannelList[i];
        }
        //numChannelsInFile = numChannelsInFile-unusedChannelsSoFar;


        if (globalConf->saveDisplayedChanOnly) {
            PACKET_SIZE = 2*(numChannelsInFile) + 4 + (2*hardwareConf->headerSize);
        } else {
            PACKET_SIZE = 2*(hardwareConf->NCHAN) + 4 + (2*hardwareConf->headerSize);
        }

}


/*
void fileSourceRuntime::endThread() {
    quit();
}*/


fileSourceInterface::fileSourceInterface(QObject*) {
  state = SOURCE_STATE_NOT_CONNECTED;
  acquisitionThread = NULL;
  filePaused = false;

}

fileSourceInterface::~fileSourceInterface() {

    //if (acquisitionThread != NULL) {
    //    delete(acquisitionThread);
    //}
}

void fileSourceInterface::InitInterface() {

  //open device here
    //qDebug() << "in file source, init";
    //streamConf->listChanToSave();
  QFile* filePtr = new QFile;
  filePtr->setFileName(playbackFile);

  //open file
  if (!filePtr->open(QIODevice::ReadOnly)) {
      delete filePtr;
      return;
  }

  if (!filePtr->seek(fileDataPos)) { //skip past the config header
      delete filePtr;
      return;
  }

  //initialization went ok, so start the runtime thread
  acquisitionThread = new fileSourceRuntime(NULL);
  acquisitionThread->file = filePtr;
  //acquisitionThread->connect(this, SIGNAL(startRuntime()), SLOT(Run()));
  connect(acquisitionThread,SIGNAL(endOfFileReached()),this,SLOT(StopAcquisition()));
  setUpThread(acquisitionThread);

  filePaused = false;
  playbackFileSize = filePtr->size();
  playbackFileCurrentLocation = fileDataPos;

  filePlaybackSpeed = 1; //Normal speed

  state = SOURCE_STATE_INITIALIZED;
  emit stateChanged(SOURCE_STATE_INITIALIZED);

}

void fileSourceInterface::StartAcquisition(void) {

    //qDebug() << "in file source, start acq";
    //streamConf->listChanToSave();

    //if we were paused we don't need to start any new stream threads
    if (!filePaused) {

        rawData.writeIdx = 0; // location where we're currently writing
        emit acquisitionStarted(); //tell stream thread to start

        if (acquisitionThread->loopFinished) {
            emit startRuntime();
            qDebug() << "Told runtime to start";
        }
    }
    acquisitionThread->aquiring = true;
    filePaused = false;


    //emit acquisitionStarted();
    state = SOURCE_STATE_RUNNING;
    emit stateChanged(SOURCE_STATE_RUNNING);

}

void fileSourceInterface::PauseAcquisition() {

    filePaused = true;
    //state = SOURCE_STATE_INITIALIZED;
    //emit stateChanged(SOURCE_STATE_INITIALIZED);
    acquisitionThread->aquiring = false;
    emit acquisitionPaused();
}

void fileSourceInterface::StopAcquisition(void) {


  emit acquisitionStopped();
  state = SOURCE_STATE_INITIALIZED;
  emit stateChanged(SOURCE_STATE_INITIALIZED);
  acquisitionThread->jumpToBeginning = true;
  acquisitionThread->aquiring = false;
  filePaused = false;

}

void fileSourceInterface::CloseInterface(void) {
  if (state == SOURCE_STATE_RUNNING)
    StopAcquisition();

  if (state == SOURCE_STATE_INITIALIZED) {

    //if the runtime thread is running, kill it

    /*
    if (acquisitionThread != NULL) {
        acquisitionThread->quitNow = true;
        acquisitionThread->endThread();
        acquisitionThread->wait(); //block until the thread has fully terminated
    }*/

    //close device

    qDebug() << "Sending quit signal";
    if (acquisitionThread != NULL) {
        acquisitionThread->quitNow = true;

    }

    while (!acquisitionThread->loopFinished) {
        QThread::msleep(10);
    }

    //acquisitionThread->file->close();
    //delete acquisitionThread->file;
    playbackFileOpen= false;



    qDebug() << "Closed device.";

    emit stateChanged(SOURCE_STATE_NOT_CONNECTED);

  }
}

void fileSourceInterface::waitForThreads(void) {
    //TODO: we need a lock here
    acquisitionThread->waitForThreadsFlag = true;
}





