/*
Trodes is a free, open-source neuroscience data collection and experimental control toolbox

Copyright (C) 2012 Mattias Karlsson

This program is free software: you can redistribute it and/or modify
                               it under the terms of the GNU General Public License as published by
                               the Free Software Foundation, either version 3 of the License, or
                               (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef SOUNDDIALOG_H
#define SOUNDDIALOG_H

#include <QDialog>
#include <QSlider>
#include <QLabel>
#include <QtWidgets>

//-----------------------------------------

class TrodesButton : public QPushButton {

Q_OBJECT

public:
    TrodesButton(QWidget *parent = 0);
    void setRedDown(bool yes);

};
//--------------------------------------

class ExportDialog : public QWidget {


Q_OBJECT

public:
    ExportDialog(QWidget *parent = 0);

    QGroupBox     *spikeBox;
    QComboBox     *triggerSelector;
    QComboBox     *noiseRemoveSelector;
    QGroupBox     *ModuleDataBox;
    QComboBox     *ModuleDataChannelSelector;
    QComboBox     *ModuleDataFilterSelector;

    QLabel        *triggerModeLabel;
    QLabel        *noiseLabel;
    QLabel        *ModuleDataChannelLabel;
    QLabel        *ModuleDataFilterLabel;

    QPushButton   *cancelButton;
    QPushButton   *exportButton;

    QProgressBar  *progressBar;
    QTimer        *progressCheckTimer;


private:

signals:

    void startExport(bool spikesOn, bool ModuleDataon, int triggerSetting, int noiseSetting, int ModuleDataChannelSetting, int ModuleDataFilterSetting);
    void exportCancelled();
    void closing();

private slots:

    void exportButtonBushed();
    void cancelButtonPushed();
    void timerExpired();
};


class soundDialog : public QWidget
{

Q_OBJECT

public:
    soundDialog(int currentGain, int currentThresh, QWidget *parent = 0);
    QSlider *gainSlider;
    QSlider *threshSlider;
    QLabel *gainDisplay;
    QLabel *threshDisplay;
    QLabel *gainTitle;
    QLabel *threshTitle;

private:

signals:

};


class waveformGeneratorDialog : public QWidget
{

Q_OBJECT

public:
    waveformGeneratorDialog(double currentCarrierFreq, int currentFreq, int currentAmp, int currentThresh, QWidget *parent = 0);
 //   waveformGeneratorDialog(int currentFreq, int currentAmp, QWidget *parent = 0);

    QDoubleSpinBox *carrierFreqSpinBox;
    QSlider *freqSlider;
    QSlider *ampSlider;
    QSlider *threshSlider;
    QLabel *freqDisplay;
    QLabel *ampDisplay;
    QLabel *threshDisplay;
    QLabel *carrierFreqTitle;
    QLabel *freqTitle;
    QLabel *ampTitle;
    QLabel *threshTitle;

protected:
    void closeEvent(QCloseEvent* event);

private:

signals:
    void windowClosed(void);
};


class spikeGeneratorDialog : public QWidget
{

Q_OBJECT

public:
    spikeGeneratorDialog(double currentCarrierFreq, int currentFreq, int currentAmp, int currentThresh, QWidget *parent = 0);
 //   waveformGeneratorDialog(int currentFreq, int currentAmp, QWidget *parent = 0);

    QDoubleSpinBox *carrierFreqSpinBox;
    QSlider *freqSlider;
    QSlider *ampSlider;
    QSlider *threshSlider;
    QLabel *freqDisplay;
    QLabel *ampDisplay;
    QLabel *threshDisplay;
    QLabel *carrierFreqTitle;
    QLabel *freqTitle;
    QLabel *ampTitle;
    QLabel *threshTitle;

protected:
    void closeEvent(QCloseEvent* event);

private:

signals:
    void windowClosed(void);
};


//-------------------------------------------------

class CommentDialog : public QWidget {

Q_OBJECT

public:
    CommentDialog(QString fileName, QWidget *parent = 0);


private:

    QLineEdit* newCommentEdit;

    QPushButton* saveButton;

    QLabel* commentLabel;
    //QLabel* lastComment;
    QTextEdit* lastComment;

    QString fileName;

    void    getHistory();
    void    saveLine();


private slots:

    void saveCurrentComment();
    void somethingEntered();


public slots:


protected:
    void closeEvent(QCloseEvent* event);

signals:

    void windowOpenState(bool);
    void windowClosed();

};

struct HeadstageSettings {
    bool autoSettleOn;
    int percentChannelsForSettle;
    int threshForSettle;
};

class HeadstageSettingsDialog : public QWidget
{

Q_OBJECT

public:
    HeadstageSettingsDialog(HeadstageSettings settings, QWidget *parent = 0);
 //   waveformGeneratorDialog(int currentFreq, int currentAmp, QWidget *parent = 0);


    QGroupBox       *autoSettleBox;
    QSlider         *percentChannelsSlider;
    QSlider         *threshSlider;
    QLabel          *percentIndicator;
    QLabel          *threshIndicator;

    QLabel          *percentTitle;
    QLabel          *threshTitle;

    TrodesButton    *okButton;
    TrodesButton    *cancelButton;

protected:
    void closeEvent(QCloseEvent* event);
    void resizeEvent(QResizeEvent* event);

private:

    HeadstageSettings currentSettings;
    bool settingsChanged;

private slots:

    void percentSliderChanged(int value);
    void threshSliderChanged(int value);
    void autoSettleOnToggled(bool on);
    void okButtonPressed();
    void cancelButtonPressed();

signals:
    void windowClosed(void);
    void newSettings(HeadstageSettings settings);
};



//-----------------------------------
class RasterPlot : public QWidget {

    Q_OBJECT

public:
    RasterPlot(QWidget *parent);

    void addRaster(const QVector<qreal> &times);
    void setRasters(const QVector<QVector<qreal> > &times);
    void clearRasters();
    void setXRange(qreal minR, qreal maxR);
    void setXLabel(QString l);
    void setYLabel(QString l);

protected:

    void resizeEvent(QResizeEvent *event);
    void paintEvent(QPaintEvent *event);

private:


    qreal maxX;
    qreal minX;
    QVector<QVector<qreal> > eventTimes;
    QVector<qreal> xTickLabels;
    QVector<qreal> yTickLabels;
    QString xLabel;
    QString yLabel;


public slots:

private slots:

signals:

};
//-----------------------------------
class HistogramPlot : public QWidget {

    Q_OBJECT

public:
    HistogramPlot(QWidget *parent);

    void setXRange(qreal minR, qreal maxR);
    void setXLabel(QString l);
    void setYLabel(QString l);
    void setData(QVector<qreal> values);

protected:

    void resizeEvent(QResizeEvent *event);
    void paintEvent(QPaintEvent *event);
    //void paint()

    /*
    void mousePressEvent(QMouseEvent *event);
    void mouseDoubleClickEvent(QMouseEvent *event);
    void leaveEvent(QEvent *event);
    void mouseMoveEvent(QMouseEvent *event);
    void keyPressEvent(QKeyEvent *event);
    void wheelEvent(QWheelEvent *);
    */

private:


    qreal maxX;
    qreal minX;
    qreal maxY;
    QVector<qreal> barValues;
    QVector<qreal> xTickLabels;
    QVector<qreal> yTickLabels;
    QString xLabel;
    QString yLabel;


public slots:

private slots:

signals:

};

class PSTHDialog : public QWidget {

Q_OBJECT

public:
    PSTHDialog(QWidget *parent = 0);
    void plot(const QVector<uint32_t> &trialTimes, const QVector<uint32_t> &eventTimes);


private:

    QLabel   *rangeLabel;
    QLabel   *binsLabel;
    QSpinBox *rangeSpinBox;
    QSpinBox *numBinsSpinBox;

    HistogramPlot *window;
    RasterPlot *rasterWindow;
    qreal binSize;
    int numBins;
    qreal absTimeRange;
    QVector<int> counts;
    int numTrials;
    QVector<uint32_t> trialTimes;
    QVector<uint32_t> eventTimes;

    void calcDisplay();


private slots:
    void setNumBins(int nBins);
    void setRange(int msecRange);



public slots:


protected:
    //void closeEvent(QCloseEvent* event);
    void resizeEvent(QResizeEvent *event);

signals:

    void windowOpenState(bool);
    void windowClosed();

};

#endif // SOUNDDIALOG_H
