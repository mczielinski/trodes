/*
Trodes is a free, open-source neuroscience data collection and experimental control toolbox

Copyright (C) 2012 Mattias Karlsson

This program is free software: you can redistribute it and/or modify
                               it under the terms of the GNU General Public License as published by
                               the Free Software Foundation, either version 3 of the License, or
                               (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef DISPLAY_H
#define DISPLAY_H

#include <QtGui>
//#include <QGLWidget>
#include "configuration.h"
#include "iirFilter.h"
#include "dialogs.h"
#include "streamProcessorThread.h"
#include "sharedVariables.h"
#include <QGridLayout>
#include <QtWidgets>

#define INCLUDETOOL_ID 1
#define EDITTOOL_ID 2
#define PANTOOL_ID 3

#define NUMCLUSTERS 32

struct ClusterPolygonSet {
    int xAxis;
    int yAxis;
    QVector<QPoint> points;
    int bitInd;
    int clusterInd;
};

class RubberBandPolygonNode : public QObject, public QGraphicsRectItem {
    Q_OBJECT

public:

    RubberBandPolygonNode(int nodeNum, QGraphicsItem *parent = 0);
    void setColor(QColor c);

private:
    int nodeNum;
    bool dragging;
    QColor color;


protected:
    void mouseMoveEvent(QGraphicsSceneMouseEvent* event);
    void mouseReleaseEvent(QGraphicsSceneMouseEvent *event);
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget);

signals:
    void nodeMoved(int nodeNum);
    void nodeMoveFinished();


};



class RubberBandPolygon : public QObject, public QGraphicsPolygonItem {

Q_OBJECT

public:
    RubberBandPolygon(QGraphicsItem *parent = 0);
    ~RubberBandPolygon();
    struct Axes {
        int x;
        int y;
    };

    void addPoint(QPointF p);
    void setCurrentAmpWindow(QRectF win);
    void moveLastPoint(QPointF);
    void removeLastPoint();
    bool isIncludeType();
    bool isExcludeType();
    bool isZoneType();
    void setIncludeType();
    void setExcludeType();
    void setZoneType();
    void calculateIncludedPoints(bool* inside, int imageWidth, int imageHeight);
    void setAxes(int x, int y);
    Axes getAxes();
    int getClusterNum();
    void setClusterNum(int cNum);
    void setPolyBitInd(int bitInd);
    int getPolyBitInd();
    void setColor(QColor c);
    QVector<QPoint> getPoints();


private:
    QVector<RubberBandPolygonNode*> nodes;
    QVector<QPointF> points; //points in pixel space
    QVector<QPointF> relativePoints; // points are mapped between 0 and 1 in x,y coordinates
    QVector<QPointF> amplitudeSpacePoints; // points are mapped in amplitude space
    QColor color;
    int type;
    int polyBitInd;
    int clusterNum;
    bool dragging;
    QRectF ampWindow;
    int xAxis;
    int yAxis;

    //QGraphicsPolygonItem* poly;

protected:
    void mouseMoveEvent(QGraphicsSceneMouseEvent* event);
    void mousePressEvent(QGraphicsSceneMouseEvent *event);
    void mouseReleaseEvent(QGraphicsSceneMouseEvent *event);
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget);

private slots:
    void childNodeMoved(int nodeNum);
    void childNodeMoved();

public slots:
    void updateSize();
    void highlight();
    void removeHighlight();


signals:
    void hasHighlight();
    //void shapeChanged();
    void shapeChanged(int clustNum, int polygonBitIndex);
    void rightClicked(const QPoint& pos);


};

class ScatterPlotMipmap {

public:
    ScatterPlotMipmap(QRectF space, int numXPixels, int numYPixels);
    void addPoint(qreal x, qreal y, int colorIndex);
    void setPoints(int startInd, int endInd, const QVector<int>* inputData, const QVector<quint8> &clusterIDs);

    void setWindows(QRectF sourceWindow, QRect destWindow);
    void setDestWindow(QRect destWindow);
    void createImages();
    void deleteImages();
    void clearData();
    void setColorTable(const QList<QColor> &colors);

    QImage getCurrentImage();
    QImage getCurrentImage(QRect outputWindow);
    QPair<int,int> axes;


private:
    QVector<QImage*> imageVector; //Contains all of the mipmap images
    QRectF scatterSpace; //The full logical space of the data
    QRectF currentSpaceWindow;  //The current view window of the logical space
    QRect  sourceImageWindow; //The current pixel-based window of the current mipmap level image
    QRect  destWindow; //The current pixel window of the destination

    bool initialized;

    int xPixels;
    int yPixels;
    int currentImageIndex;

    QImage currentImage;
    QVector<QRgb> colorTable;

    QImage deAlias(const QImage& orig, QRect window);

};

class CustomScene : public QGraphicsScene {

    Q_OBJECT

public:

    CustomScene(QWidget *parent, int bufferSizeInput);

    QVector<vertex2d> displayData;

    int bufferSize;
    int currentXdisplay;
    int currentYdisplay;
    bool replotScatter;
    bool initialized;

    void setDataPoint(const QVector<int>* inputData, int currentBufferIndex);
    void setBackgroundPic(QImage *pic);

private:

    bool plotNewPoints;
    bool displayMulipleProjections;
    QImage *backgroundPic;

protected:
    void mousePressEvent(QGraphicsSceneMouseEvent *event);
    void drawBackground(QPainter *painter, const QRectF &rect);

    //void resizeGL(int w, int h);
    //void initializeGL();
    //void paintEvent(QPaintEvent *event);

public slots:

    //void updateAxes();
    void setChannels(int channelX, int channelY, const QVector<int> *inputData);
    //void setMaxDisplay(int channel, int maxDisplayVal);
    //void setMaxDisplay(int maxDisplayVal);
    void setMultipleProjections(bool multiple);

signals:
    void emptySpaceClicked();

};



class ScatterPlotWindow : public QGraphicsView {

    Q_OBJECT

public:
    ScatterPlotWindow(QWidget *parent, int bufferSizeInput);
    //VideoDisplayWindow *dispWin; //shows the video frames
    void addIncludePolygon();
    void addExcludePolygon();
    void addZonePolygon();

    void setDisplayedNTrode(int nTrodeInd, QVector<int> minDisplay, QVector<int> maxDisplay, QList<ScatterPlotMipmap*> mmptrs);
    void setDataPoint(const QVector<int>* inputData, int currentBufferIndex, int clustID);
    void setClusterColors(const QList<QColor> colors);
    void setCurrentCluster(int clust);
    void refresh();




    QList<ClusterPolygonSet> getPolygonsForCluster(int clusterInd);
    CustomScene *scene;

protected:
    void resizeEvent(QResizeEvent *event);
    void mousePressEvent(QMouseEvent *event);
    void mouseDoubleClickEvent(QMouseEvent *event);
    void leaveEvent(QEvent *event);
    void mouseMoveEvent(QMouseEvent *event);
    void keyPressEvent(QKeyEvent *event);
    void wheelEvent(QWheelEvent *);

private:

    QVector<QVector<RubberBandPolygon*> > polygons;
    QVector<QVector<bool> > polygonsTaken;
    void setMipMapPtrs(QList<ScatterPlotMipmap*> mmPtrs);
    void showPolygonContextMenu(const QPoint& pos, int nTrode, int polygonNum);

    QList<QColor> clusterColors;
    int currentNTrode;
    int currentCluster;
    int currentXdisplay;
    int currentYdisplay;
    int currentMipmapIndex;

    bool currentlyDrawing;
    int currentlySelectedPolygon;
    int currentTool;
    bool displayMultipleProjections;
    int bufferSize;

    int currentMouseXPanel;
    int currentMouseYPanel;
    QPair<int,int> currentMouseChannels;
    int currentPairIndex;


    QVector<int>  maxAmplitude;
    QVector<int>  minAmplitude;
    QList<ScatterPlotMipmap*> mipMaps;
    //QList<ScatterPlotMipmap*> mipMaps_lowRange;  //hi-res, with a low maximum
    double lowRangeMipmapMax;


    int maxDispVal_uV;
    QImage currentBackgroundPic;


public slots:
    void polygonHighlighted();
    void polygonRightClicked(const QPoint& pos);
    void spaceClicked();
    void setTool(int toolNum);
    void setNoHighlight();
    void setMaxDisplay(int channel, int maxDisplayVal);
    void setMaxDisplay(int maxDisplayVal);
    void setDisplayWindow(QVector<int> minDisplay, QVector<int> maxDisplay);
    void setChannels(int channelX, int channelY, const QVector<int> *inputData, const QVector<quint8> &clustID);
    void setData(const QVector<int> *inputData, const QVector<quint8> &clustID);
    void setMultipleProjections(bool multiple);
    void updateBackgroundPlot();
    void clearData();



private slots:

    void calculatePointsInsidePolygon(int clustInd, int polygonBitIndex);

signals:
    void newIncludeCalculation(QVector<bool>);
    void userInput1(QPoint loc);
    void userInput2(QPoint loc);
    void newClusterCalculationNeeded(int clusterInd, ClusterPolygonSet p, int polyBitIndex);
    void polygonDeleted(int clusterInd, int polyBitIndex);
    void zoom(int amount);
    void lockSpikeSorting();
    void unlockSpikeSorting();
    void mouseOverChannels(int xChan, int YChan);
    void channelPairSelected(int pairIndex);
    void PSTHRequested(int clustInd);
    //void newIncludeCalculation(PixIncludeArray);
};




class GraphicsWindow : public QGraphicsView {

    Q_OBJECT

public:
    GraphicsWindow(QWidget *parent, int bufferSizeInput, int numChannels);
    //VideoDisplayWindow *dispWin; //shows the video frames
    void addIncludePolygon();
    void addExcludePolygon();
    void addZonePolygon();

    void setWindowActive(bool active);
    void setDataPoint(const QVector<int>* inputData, int currentBufferIndex, int clustID);
    void setClusterColors(const QList<QColor> colors);
    void setCurrentCluster(int clust);
    void refresh();

    QList<ClusterPolygonSet> getPolygonsForCluster(int clusterInd);

    CustomScene *scene;

protected:
    void resizeEvent(QResizeEvent *event);
    void mousePressEvent(QMouseEvent *event);
    void mouseDoubleClickEvent(QMouseEvent *event);
    void mouseMoveEvent(QMouseEvent *event);
    void keyPressEvent(QKeyEvent *event);
    void wheelEvent(QWheelEvent *);

private:

    QVector<RubberBandPolygon*> polygons;
    QVector<bool> polygonsTaken;

    QList<QColor> clusterColors;
    int currentCluster;
    int currentXdisplay;
    int currentYdisplay;
    int currentMipmapIndex;

    bool currentlyDrawing;
    int currentlySelectedPolygon;
    int currentTool;
    bool displayMultipleProjections;
    int bufferSize;
    bool isShown;

    QVector<int> maxAmplitude;
    QVector<int> minAmplitude;

    QList<ScatterPlotMipmap*> mipMaps;
    //QList<ScatterPlotMipmap*> mipMaps_lowRange;  //hi-res, with a low maximum
    double lowRangeMipmapMax;


    int maxDispVal_uV;
    QImage currentBackgroundPic;


public slots:
    void polygonHighlighted();
    void spaceClicked();
    void setTool(int toolNum);
    void setNoHighlight();
    void setMaxDisplay(int channel, int maxDisplayVal);
    void setMaxDisplay(int maxDisplayVal);
    void setChannels(int channelX, int channelY, const QVector<int> *inputData, const QVector<quint8> &clustID);
    void setData(const QVector<int> *inputData, const QVector<quint8> &clustID);
    void setMultipleProjections(bool multiple);
    void updateBackgroundPlot();
    void clearData();



private slots:
    void calculateConsideredPixels();
    void calculatePointsInsidePolygon(int clustInd, int polygonBitIndex);

signals:
    void newIncludeCalculation(QVector<bool>);
    void userInput1(QPoint loc);
    void userInput2(QPoint loc);
    void newClusterCalculationNeeded(int clusterInd, int polyBitIndex);
    void polygonDeleted(int clusterInd, int polyBitIndex);
    void zoom(int amount);
    void lockSpikeSorting();
    void unlockSpikeSorting();
    //void newIncludeCalculation(PixIncludeArray);
};


//ScopeWindow plots the spike waveform for one channel.
class ScopeWindow : public QWidget
{
  Q_OBJECT

public:
  ScopeWindow(QWidget *parent);
  ~ScopeWindow();
  int                   nTrodeNum;
  QVector<QPolygon> waveFormData;
  QColor traceColor;
  int thresh;
  int maxAmplitude;
  void plotData(const QVector<vertex2d> *waveForm);
  //void setWaveformData(const QVector<vertex2d> *waveForm, int channel);

private:

  int beforeTriggerLength;
  int afterTriggerLength;
  bool highlight;
  int highlightChannel;
  int numChannels;
  int pairChannel1;
  int pairChannel2;

  //QVector<vertex2d> *waveForm;

protected:

  void paintEvent(QPaintEvent *event);
  void mousePressEvent(QMouseEvent *);
  void wheelEvent(QWheelEvent *);

public slots:

  void setMaxDisplay(int maxDisplayVal);
  void setThresh(int threshVal);
  void setHighlight(int channel);
  void setNoHighlight();
  void setNTrode(int nTrodeInd);
  void setCurrentDisplayPair(int ch1, int ch2);


signals:
  void clicked(int channel);
  void zoom(int amount);

};



// TriggerScopeDisplayWidget displays spike waveform for all channels in an nTrode
class TriggerScopeDisplayWidget : public QFrame
{
  Q_OBJECT

public:
  TriggerScopeDisplayWidget(QWidget *parent);

public:



  //QList<QPushButton*> audioButton;
  //QList<QLineEdit*>   maxDispEdit;
  //QList<QLineEdit*>  threshEdit;
  QSpinBox*             maxDispControl;
  QSpinBox*             threshControl;


  //QList<QSpinBox*>   maxDispSpin;
  //QList<QSpinBox*>  threshSpin;
  QList<QPushButton*>  triggerSwitches;
  QLabel*               maxLabel;
  QLabel*               minLabel;
  QLabel*               zeroLabel;


  //the list of ScopeWindow objects does the actual waveform display
  ScopeWindow* scopeWindows;

  //QPushButton  *linkMaxDisplaySettingsButton;
  //QPushButton  *linkThreshSettingsButton;

  bool linkMaxDisplaySettings;
  bool linkThreshSettings;
  bool slaveMode; //if true, does not update the config objects when settings change.


  int nTrodeNumber;

private:
  QGridLayout            *scopeLayout;
  QGridLayout            *triggerButtonLayout;

  int                   threshold;
  int                   maxDisplay;
  int                   numberOfChannels;

  //void setThreshhold_display(int newThresh);
  //void setMaxDisplay_display(int newMaxDisplay);

  bool emitThreshSignal;
  bool emitMaxDispSignal;

  int pairChannel1;
  int pairChannel2;

signals:

  void audioChanged(int channel);
  void setAudioChannel(int HWchannel);
  void updateAudioSettings();
  //void newThreshold(int nTrode, int channel, int newThresh);
  void newMaxDisplaySignal(int newMaxDisp);
  //void changeAllMaxDisp(int newMaxDisp);
  //void changeAllThresh(int newThresh);
  void zoom(int amount);

public slots:
  void setDisplayedNTrode(int nTrodeInd);
  void setTraceColor(QColor color);
  void setMaxDisplay(int newMax);
  void setThreshold(int newThresh);
  //void updateTriggerMode(int channel);
  void triggerButtonToggled();

  void updateThreshhold(void);
  void updateMaxDisplay(void);
  //void updateThreshhold(int channel);
  //void updateMaxDisplay(int channel);
  void updateAudio(int channel);
  void updateAudioHighlight(int channel);
  void turnOffAudio();
   void setCurrentDisplayPair(int ch1, int ch2);


};

//TriggerScopeSettingsWidget contains the settings controls for each nTrode
class TriggerScopeSettingsWidget : public QWidget
{
  Q_OBJECT

public:
  TriggerScopeSettingsWidget(QWidget *parent, int trodeNum);
  ~TriggerScopeSettingsWidget();

public:

  int nTrodeNumber;
  QList<QGridLayout*>  widgetLayouts;
  QList<QLabel*> labels;

  QCheckBox     *linkCheckBox;
  QGroupBox     *refBox;
  QGroupBox     *filterBox;
  QGroupBox     *ModuleDataFilterBox;
  QComboBox     *RefTrodeBox;
  QComboBox     *RefChannelBox;
  QComboBox     *lowFilterCutoffBox;
  QComboBox     *highFilterCutoffBox;
  QComboBox     *ModuleDataHighFilterCutoffBox;
  QComboBox     *ModuleDataChannelBox;


private:


signals:
    void updateAudioSettings();
    void changeAllRefs(int nTrode, int channel);
    void changeAllFilters(int low, int high);
    void toggleAllRefs(bool on);
    void toggleAllFilters(bool on);
    void toggleLinkChanges(bool link);
    void moduleDataChannelChanged(int nTrode, int chan); 


public slots:
  void updateRefTrode();
  void updateRefTrode(int);
  void updateRefChan();
  void updateRefChan(int);
  void updateRefSwitch(bool on);
  void updateFilterSwitch(bool on);
  void updateLowerFilter();
  void updateUpperFilter();
  void updateModuleDataChannel();
  void updateModuleDataUpperFilter();
  void linkBoxClicked(bool checked);
  void setEnabledForStreaming(bool streamOn);  // disables everything if data streaming is on
};

class NtrodeDisplayData : public QObject
{
    Q_OBJECT
public:
    NtrodeDisplayData(QObject *parent, int nTrodeID);
    ~NtrodeDisplayData();
    bool     clustersDefined[NUMCLUSTERS];
    int nTrodeNumber;
    QStringList            channelViewStrings;
    QVector<int>           channelViewXList;
    QVector<int>           channelViewYList;
    int                    currentXdisplay;
    int                    currentYdisplay;
    int                    currentChannelListIndex;
    int                    numberOfChannels;
    QVector<int>            maxAmplitude;
    QVector<int>            minAmplitude;
    QVector<quint8>         pointClusterOwnership; //Each point can belong to one of 256 clusters
    int                     currentBufferIndex;
    int                     scatterBufferSize;

    QVector<QVector<int> >  scatterData;
    QVector <QVector<vertex2d> > waveForms;
    QVector<uint32_t> spikeTimes;
    QList<ScatterPlotMipmap*> mipMaps;

    void setDataShowing();
    void setDataNotShowing();
    QList<ScatterPlotMipmap*> getMipMapPtrs();
    void setMipMapData();
    void setZeroToMax(int newMaxAmplitude);
    void setColorTable(const QList<QColor> &colors);
    QVector<uint32_t> getSpikeTimesForCluster(int clusterInd);
    uint32_t getStartOfLogTime();



private:
  QMutex spikeSortMutex;
  QMutex newSpikeMutex;
  QList<ClusterPolygonSet> polygonData;

  QVector<quint8*>        polyIncludeRecord;
  QVector<quint8*>        polyExcludeRecord;
  QVector<int>            bitClusterOwnership; //Eech bit in the include/exclude vectors is taken up by a polygon for one cluster of this ID
  QAtomicInt              writeBlock;
  uint32_t                beginningOfLog;


private slots:


  QList<ClusterPolygonSet> getPolygonsForCluster(int clusterInd);
  ClusterPolygonSet getPolygonForBitInd(int polyBitIndex);


  void lockSpikeSortMutex();
  void unlockSpikeSortMutex();

public slots:
  void calculatePointsInsidePolygon(int clusterInd, int bitInd);
  void deletePolygonData(int clusterInd, int polyBitIndex);
  void addPolygonData(ClusterPolygonSet p);
  void calculateCluster(int clusterInd);
  int  sortDataPoint(const QVector<int> &coord, int pointInd);
  void toggleClusterOn(int clusterInd, bool on);

  void receiveNewEvent(const QVector <int2d>* waveForm,const int* peaks, uint32_t time);
  void updateScatterChannels(int channelCombo);
  void clearButtonPressed();


signals:

  void spike(int nT, int clNum, uint32_t time);



};


/* NtrodeDisplayWidget contains all display objects and controls for one nTrode*/
class MultiNtrodeDisplayWidget : public QWidget
{
  Q_OBJECT


public:
  MultiNtrodeDisplayWidget(QWidget *parent);
  ~MultiNtrodeDisplayWidget();

  TriggerScopeDisplayWidget *triggerScope;
  void refreshGraphicsWin();
  void setStreamManagerPtr(StreamProcessorManager *mgr);


protected:
  void closeEvent(QCloseEvent* event);
  void moveEvent(QMoveEvent *);
  void resizeEvent(QResizeEvent *);
  void showEvent(QShowEvent *);

private:
  QMutex spikeSortMutex;
  QMutex writeToMipmapMutex;


  QTimer*  scatterPlotTimer;
  QTimer*  scopePlotTimer;
  int      selectedClusterInd;
  int      currentTool;
  int      currentHWAudioChannel;
  int      currentNTrode;
  bool     changingDisplayedNtrode;
  bool     multiViewMode;
  StreamProcessorManager *streamManager;

  QList<NtrodeDisplayData*> nTrodeData;


  QGridLayout            *mainLayout;
  QSplitter              *panelSplitter;
  QGridLayout            *scopeLayout;
  QGridLayout            *scatterLayout;
  QGridLayout            *scatterControlLayout;
  QTabWidget             *scopeTabs;
  QButtonGroup           *clusterSelectButtons;
  QButtonGroup           *toolSelectButtons;
  QList<QColor>          clusterColors;

  QPushButton            *clearButton;
  QPushButton            *clearAllButton;
  QPushButton            *selectSingleView;
  QPushButton            *selectMultiView;
  //QLineEdit              *refEdit;
  QComboBox              *channelViewCombo;
  //QVector<int>           channelViewXList;
  //QVector<int>           channelViewYList;

  QLabel                 *nTrodeLabel;

  //scatterWindow          *scatterDisplay;
  ScatterPlotWindow        *graphicsWin;

  QAtomicInt              writeBlock;

  int                     currentXdisplay;
  int                     currentYdisplay;

private slots:
  void openPSTHWindow(int clusterInd);
  void newSplitterPos(int,int);
  void multiViewButtonPressed();
  void singleViewButtonPressed();
  void multiViewButtonReleased();
  void singleViewButtonReleased();
  void clusterButtonPressed();
  void toolButtonPressed(int toolNum);
  void toggleClusterOn(int clusterInd, bool on);
  void zoom(int amount);
  void lockSpikeSortMutex();
  void unlockSpikeSortMutex();
  void setSingleView();
  void setMultiView();

  void calculatePointsInsidePolygon(int clusterInd, ClusterPolygonSet p, int bitInd);
  void deletePolygonData(int clusterInd, int bitInd);

public slots:

  void setShownNtrode(int nTrodeInd);
  //void receiveNewEvent(int nTrodeInd, const QVector <vertex2d>* waveForm,const int* peaks, uint32_t time);
  void receiveNewEvent(int nTrodeInd, const QVector <int2d>* waveForm,const int* peaks, uint32_t time);

  void updateScatterChannels(int channelCombo);
  void scatterPairSelected(int pairIndex);
  void clearButtonPressed();
  void clearAllButtonPressed();
  void changeAudioChannel(int hardwareChannel);
  void scatterPlotTimerExpired();
  void scopePlotTimerExpired();
  void setSplitterPos(QList<int>);
  void setMaxDisplay(int nTrode, int newMaxDisplay);

signals:
  void windowClosed();
  void windowMoved(QPoint newPos);
  void newWindowSize(QSize newSize);
  void splitterPosChanged(QList<int>);
  void singleViewSet();
  void multiViewSet();
  void spike(int nT, int clNum, uint32_t time);
  void channelClicked(int hwChannel);

};




#endif // display.h

