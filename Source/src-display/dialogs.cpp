/*
Trodes is a free, open-source neuroscience data collection and experimental control toolbox

Copyright (C) 2012 Mattias Karlsson

This program is free software: you can redistribute it and/or modify
                               it under the terms of the GNU General Public License as published by
                               the Free Software Foundation, either version 3 of the License, or
                               (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "globalObjects.h"
#include "dialogs.h"

#include <QtGui>
#include <QGridLayout>


ExportDialog::ExportDialog(QWidget *) {

    QGridLayout *widgetLayout = new QGridLayout();
    QFont labelFont;
    labelFont.setPixelSize(12);
    labelFont.setFamily("Console");

    //Create the spike export controls
    spikeBox = new QGroupBox(tr("Spikes"),this);
    triggerSelector = new QComboBox();
    triggerSelector->setFont(labelFont);
    triggerSelector->addItem("Current thresholds");
    //triggerSelector->addItem("Standard deviation: 3");
    //triggerSelector->addItem("Standard deviation: 4");
    //triggerSelector->addItem("Standard deviation: 5");

    noiseRemoveSelector = new QComboBox();
    noiseRemoveSelector->setFont(labelFont);
    noiseRemoveSelector->addItem("None");
    //noiseRemoveSelector->addItem("Common events");

    triggerModeLabel = new QLabel("Trigger mode");
    triggerModeLabel->setFont(labelFont);
    noiseLabel = new QLabel("Noise exclusion");
    noiseLabel->setFont(labelFont);
    QGridLayout *spikeBoxLayout = new QGridLayout();
    spikeBoxLayout->addWidget(triggerModeLabel,0,0);
    spikeBoxLayout->addWidget(noiseLabel,0,1);
    spikeBoxLayout->addWidget(triggerSelector,1,0);
    spikeBoxLayout->addWidget(noiseRemoveSelector,1,1);
    spikeBox->setLayout(spikeBoxLayout);
    spikeBox->setCheckable(true);
    spikeBox->setChecked(true);
    //spikeBox->setFixedHeight();
    widgetLayout->addWidget(spikeBox,0,0);

    //Create the ModuleData export controls
    ModuleDataBox = new QGroupBox(tr("ModuleData"),this);
    ModuleDataChannelSelector = new QComboBox();
    ModuleDataChannelSelector->setFont(labelFont);
    ModuleDataChannelSelector->addItem("One per nTrode");
    ModuleDataChannelSelector->addItem("All channels");
    ModuleDataFilterSelector = new QComboBox();
    ModuleDataFilterSelector->setFont(labelFont);
    ModuleDataFilterSelector->setEnabled(false);
    ModuleDataChannelLabel = new QLabel("Channels");
    ModuleDataChannelLabel->setFont(labelFont);
    ModuleDataFilterLabel = new QLabel("Filter");
    ModuleDataFilterLabel->setFont(labelFont);
    QGridLayout *ModuleDataBoxLayout = new QGridLayout();
    ModuleDataBoxLayout->addWidget(ModuleDataChannelLabel,0,0);
    ModuleDataBoxLayout->addWidget(ModuleDataFilterLabel,0,1);
    ModuleDataBoxLayout->addWidget(ModuleDataChannelSelector,1,0);
    ModuleDataBoxLayout->addWidget(ModuleDataFilterSelector,1,1);
    ModuleDataBox->setLayout(ModuleDataBoxLayout);
    ModuleDataBox->setCheckable(true);
    ModuleDataBox->setChecked(false);
    widgetLayout->addWidget(ModuleDataBox,1,0);
    ModuleDataBox->setEnabled(false);

    //Add the buttons
    QGridLayout *buttonLayout = new QGridLayout();
    cancelButton = new QPushButton("Cancel");
    exportButton = new QPushButton("Export");
    buttonLayout->addWidget(cancelButton,0,1);
    buttonLayout->addWidget(exportButton,0,2);
    buttonLayout->setColumnStretch(0,1);
    widgetLayout->addLayout(buttonLayout,2,0);

    //Add the progressbar
    progressBar = new QProgressBar();
    progressBar->setVisible(false);
    widgetLayout->addWidget(progressBar,3,0);

    connect(cancelButton,SIGNAL(clicked()),this,SLOT(cancelButtonPushed()));
    connect(exportButton,SIGNAL(clicked()),this,SLOT(exportButtonBushed()));


    setLayout(widgetLayout);
    setWindowTitle(tr("Export Settings"));

}

void ExportDialog::cancelButtonPushed() {
    emit exportCancelled();
    emit closing();
    this->close();
}

void ExportDialog::exportButtonBushed() {
    progressBar->setVisible(true);
    progressBar->setMinimum(0);
    progressBar->setMaximum(playbackFileSize);
    progressBar->setValue(playbackFileCurrentLocation);
    progressCheckTimer = new QTimer(this);
    progressCheckTimer->setInterval(2000);
    connect(progressCheckTimer,SIGNAL(timeout()),this,SLOT(timerExpired()));

    filePlaybackSpeed = 10; //Fast-forward speed
    emit  startExport(spikeBox->isChecked(), ModuleDataBox->isChecked(), triggerSelector->currentIndex(), noiseRemoveSelector->currentIndex(), ModuleDataChannelSelector->currentIndex(), ModuleDataFilterSelector->currentIndex());

    progressCheckTimer->start();

}

void ExportDialog::timerExpired() {

    //filePlaybackSpeed++;
    progressBar->setValue(playbackFileCurrentLocation);
}


soundDialog::soundDialog(int currentGain, int currentThresh, QWidget *parent)
    :QWidget(parent) {


    gainSlider = new QSlider(Qt::Vertical);
    gainSlider->setMaximum(100);
    gainSlider->setMinimum(0);
    gainSlider->setSingleStep(1);
    gainSlider->setValue(currentGain);
    gainSlider->setSizePolicy(QSizePolicy::Expanding,QSizePolicy::Expanding);

    threshSlider = new QSlider(Qt::Vertical);
    threshSlider->setMaximum(100);
    threshSlider->setMinimum(0);
    threshSlider->setSingleStep(1);
    threshSlider->setValue(currentThresh);
    threshSlider->setSizePolicy(QSizePolicy::Expanding,QSizePolicy::Expanding);
    gainDisplay = new QLabel;
    gainDisplay->setNum(currentGain);
    threshDisplay = new QLabel;
    threshDisplay->setNum(currentThresh);

    gainTitle = new QLabel(tr("Gain"));
    threshTitle = new QLabel(tr("Threshold"));

    QGridLayout *mainLayout = new QGridLayout;

    mainLayout->addWidget(gainTitle,0,0,Qt::AlignCenter);
    mainLayout->addWidget(threshTitle,0,1,Qt::AlignCenter);
    mainLayout->addWidget(gainDisplay,1,0,Qt::AlignCenter);
    mainLayout->addWidget(threshDisplay,1,1,Qt::AlignCenter);
    mainLayout->addWidget(gainSlider,2,0,Qt::AlignHCenter);
    mainLayout->addWidget(threshSlider,2,1,Qt::AlignHCenter);
    mainLayout->setRowStretch(2,1);
    mainLayout->setMargin(10);
    connect(gainSlider,SIGNAL(valueChanged(int)),gainDisplay,SLOT(setNum(int)));
    connect(threshSlider,SIGNAL(valueChanged(int)),threshDisplay,SLOT(setNum(int)));

    setLayout(mainLayout);
    setWindowTitle(tr("Sound"));

}


waveformGeneratorDialog::waveformGeneratorDialog(double currentCarrierFreq, int currentFreq, int currentAmp, int currentThresh, QWidget *parent)
//waveformGeneratorDialog::waveformGeneratorDialog(int currentFreq, int currentAmp, QWidget *parent)
    :QWidget(parent)
{

    carrierFreqSpinBox = new QDoubleSpinBox(this);
    carrierFreqSpinBox->setMaximum(10);
    carrierFreqSpinBox->setMinimum(0.0);
    carrierFreqSpinBox->setSingleStep(0.1);
    carrierFreqSpinBox->setValue(currentCarrierFreq);
    carrierFreqSpinBox->setSizePolicy(QSizePolicy::Expanding,QSizePolicy::Expanding);

    freqSlider = new QSlider(Qt::Vertical);
    freqSlider->setMaximum(1000);
    freqSlider->setMinimum(1);
    freqSlider->setSingleStep(1);
    freqSlider->setValue(currentFreq);
    freqSlider->setSizePolicy(QSizePolicy::Expanding,QSizePolicy::Expanding);

    ampSlider = new QSlider(Qt::Vertical);
    ampSlider->setMaximum(1000);
    ampSlider->setMinimum(0);
    ampSlider->setSingleStep(1);
    ampSlider->setValue(currentAmp);
    ampSlider->setSizePolicy(QSizePolicy::Expanding,QSizePolicy::Expanding);

    threshSlider = new QSlider(Qt::Vertical);
    threshSlider->setMaximum(1000);
    threshSlider->setMinimum(0);
    threshSlider->setSingleStep(1);
    threshSlider->setValue(currentThresh);
    threshSlider->setSizePolicy(QSizePolicy::Expanding,QSizePolicy::Expanding);


    freqDisplay = new QLabel;
    freqDisplay->setNum(currentFreq);
    ampDisplay = new QLabel;
    ampDisplay->setNum(currentAmp);
    threshDisplay = new QLabel;
    threshDisplay->setNum(currentThresh);

    carrierFreqTitle = new QLabel(tr("Carrier (Hz)"));
    freqTitle = new QLabel(tr("Frequency (Hz)"));
    ampTitle = new QLabel(tr("Amplitude (uV)"));
    threshTitle = new QLabel(tr("Threshold (uV)"));


    QGridLayout *mainLayout = new QGridLayout;

    mainLayout->addWidget(carrierFreqTitle,0,0,Qt::AlignCenter);
    mainLayout->addWidget(freqTitle,0,1,Qt::AlignCenter);
    mainLayout->addWidget(ampTitle,0,2,Qt::AlignCenter);
    mainLayout->addWidget(threshTitle,0,3,Qt::AlignCenter);

    mainLayout->addWidget(freqDisplay,1,1,Qt::AlignCenter);
    mainLayout->addWidget(ampDisplay,1,2,Qt::AlignCenter);
    mainLayout->addWidget(threshDisplay,1,3,Qt::AlignCenter);

    mainLayout->addWidget(carrierFreqSpinBox,2,0,Qt::AlignHCenter);
    mainLayout->addWidget(freqSlider,2,1,Qt::AlignHCenter);
    mainLayout->addWidget(ampSlider,2,2,Qt::AlignHCenter);
    mainLayout->addWidget(threshSlider,2,3,Qt::AlignHCenter);

    mainLayout->setRowStretch(2,1);
    mainLayout->setMargin(10);

    connect(freqSlider,SIGNAL(valueChanged(int)),freqDisplay,SLOT(setNum(int)));
    connect(ampSlider,SIGNAL(valueChanged(int)),ampDisplay,SLOT(setNum(int)));
    connect(threshSlider,SIGNAL(valueChanged(int)),threshDisplay,SLOT(setNum(int)));

    setLayout(mainLayout);
    setWindowTitle(tr("Waveform generator"));

}

void waveformGeneratorDialog::closeEvent(QCloseEvent* event) {

    emit windowClosed();
    event->accept();
}



spikeGeneratorDialog::spikeGeneratorDialog(double currentCarrierFreq, int currentFreq, int currentAmp, int currentThresh, QWidget *parent)
  :QWidget(parent)

{
  carrierFreqSpinBox = new QDoubleSpinBox(this);
  carrierFreqSpinBox->setMaximum(10);
  carrierFreqSpinBox->setMinimum(0.0);
  carrierFreqSpinBox->setSingleStep(0.1);
  carrierFreqSpinBox->setValue(currentCarrierFreq);
  carrierFreqSpinBox->setSizePolicy(QSizePolicy::Expanding,QSizePolicy::Expanding);

  freqSlider = new QSlider(Qt::Vertical);
  freqSlider->setMaximum(1000);
  freqSlider->setMinimum(1);
  freqSlider->setSingleStep(1);
  freqSlider->setValue(currentFreq);
  freqSlider->setSizePolicy(QSizePolicy::Expanding,QSizePolicy::Expanding);

  ampSlider = new QSlider(Qt::Vertical);
  ampSlider->setMaximum(1000);
  ampSlider->setMinimum(0);
  ampSlider->setSingleStep(1);
  ampSlider->setValue(currentAmp);
  ampSlider->setSizePolicy(QSizePolicy::Expanding,QSizePolicy::Expanding);

  threshSlider = new QSlider(Qt::Vertical);
  threshSlider->setMaximum(1000);
  threshSlider->setMinimum(0);
  threshSlider->setSingleStep(1);
  threshSlider->setValue(currentThresh);
  threshSlider->setSizePolicy(QSizePolicy::Expanding,QSizePolicy::Expanding);


  freqDisplay = new QLabel;
  freqDisplay->setNum(currentFreq);
  ampDisplay = new QLabel;
  ampDisplay->setNum(currentAmp);
  threshDisplay = new QLabel;
  threshDisplay->setNum(currentThresh);

  carrierFreqTitle = new QLabel(tr("Carrier (Hz)"));
  freqTitle = new QLabel(tr("Frequency (Hz)"));
  ampTitle = new QLabel(tr("Amplitude (uV)"));
  threshTitle = new QLabel(tr("Threshold (uV)"));


  QGridLayout *mainLayout = new QGridLayout;

  mainLayout->addWidget(carrierFreqTitle,0,0,Qt::AlignCenter);
  mainLayout->addWidget(freqTitle,0,1,Qt::AlignCenter);
  mainLayout->addWidget(ampTitle,0,2,Qt::AlignCenter);
  mainLayout->addWidget(threshTitle,0,3,Qt::AlignCenter);

  mainLayout->addWidget(freqDisplay,1,1,Qt::AlignCenter);
  mainLayout->addWidget(ampDisplay,1,2,Qt::AlignCenter);
  mainLayout->addWidget(threshDisplay,1,3,Qt::AlignCenter);

  mainLayout->addWidget(carrierFreqSpinBox,2,0,Qt::AlignHCenter);
  mainLayout->addWidget(freqSlider,2,1,Qt::AlignHCenter);
  mainLayout->addWidget(ampSlider,2,2,Qt::AlignHCenter);
  mainLayout->addWidget(threshSlider,2,3,Qt::AlignHCenter);

  mainLayout->setRowStretch(2,1);
  mainLayout->setMargin(10);

  connect(freqSlider,SIGNAL(valueChanged(int)),freqDisplay,SLOT(setNum(int)));
  connect(ampSlider,SIGNAL(valueChanged(int)),ampDisplay,SLOT(setNum(int)));
  connect(threshSlider,SIGNAL(valueChanged(int)),threshDisplay,SLOT(setNum(int)));

  setLayout(mainLayout);
  setWindowTitle(tr("Spikes+Waveform generator"));

}

void spikeGeneratorDialog::closeEvent(QCloseEvent *event)
{
  emit windowClosed();
  event->accept();
}


//-----------------------------------------------------
HeadstageSettingsDialog::HeadstageSettingsDialog(HeadstageSettings settings, QWidget *parent):QWidget(parent)
{
    setMinimumWidth(300);

    currentSettings = settings;
    settingsChanged = false;

    //Create the auto settle controls
    autoSettleBox = new QGroupBox(tr("Auto settle"),this);
    autoSettleBox->setCheckable(true);
    connect(autoSettleBox,SIGNAL(toggled(bool)),this,SLOT(autoSettleOnToggled(bool)));
    QGridLayout *autoSettleLayout = new QGridLayout;

    percentChannelsSlider = new QSlider(Qt::Horizontal);
    percentChannelsSlider->setMaximum(100);
    percentChannelsSlider->setMinimum(0);
    percentChannelsSlider->setSingleStep(1);
    percentChannelsSlider->setValue(currentSettings.percentChannelsForSettle);
    percentChannelsSlider->setSizePolicy(QSizePolicy::Expanding,QSizePolicy::Expanding);

    threshSlider = new QSlider(Qt::Horizontal);
    threshSlider->setMaximum(2000);
    threshSlider->setMinimum(500);
    threshSlider->setSingleStep(1);
    threshSlider->setValue(currentSettings.threshForSettle);
    threshSlider->setSizePolicy(QSizePolicy::Expanding,QSizePolicy::Expanding);

    connect(percentChannelsSlider,SIGNAL(sliderMoved(int)),this,SLOT(percentSliderChanged(int)));
    connect(threshSlider,SIGNAL(sliderMoved(int)),this,SLOT(threshSliderChanged(int)));

    threshIndicator = new QLabel(QString().number(currentSettings.threshForSettle));
    percentIndicator = new QLabel(QString().number(currentSettings.percentChannelsForSettle));

    percentTitle = new QLabel(tr("Channels over thresh (%)"));
    threshTitle = new QLabel(tr("Threshold (uV)"));

    autoSettleLayout->addWidget(threshTitle,0,0,Qt::AlignLeft);
    autoSettleLayout->addWidget(threshSlider,1,0,Qt::AlignLeft);
    autoSettleLayout->addWidget(percentTitle,2,0,Qt::AlignLeft);
    autoSettleLayout->addWidget(percentChannelsSlider,3,0,Qt::AlignLeft);

    autoSettleLayout->addWidget(threshIndicator,1,1,Qt::AlignRight);
    autoSettleLayout->addWidget(percentIndicator,3,1,Qt::AlignRight);

    autoSettleBox->setLayout(autoSettleLayout);
    autoSettleBox->setSizePolicy(QSizePolicy::Expanding,QSizePolicy::Expanding);


    okButton = new TrodesButton();
    okButton->setText("Send");
    okButton->setEnabled(false);
    cancelButton = new TrodesButton();
    cancelButton->setText("Cancel");
    connect(okButton,SIGNAL(pressed()),this,SLOT(okButtonPressed()));
    connect(cancelButton,SIGNAL(pressed()),this,SLOT(cancelButtonPressed()));

    QGridLayout *buttonLayout = new QGridLayout();
    buttonLayout->addWidget(cancelButton,0,1);
    buttonLayout->addWidget(okButton,0,2);
    buttonLayout->setColumnStretch(0,1);
    buttonLayout->setContentsMargins(10,10,10,10);


    QGridLayout *mainLayout = new QGridLayout;
    mainLayout->setMargin(10);
    mainLayout->setContentsMargins(10,10,10,10);
    mainLayout->setVerticalSpacing(10);

    mainLayout->addWidget(autoSettleBox,0,0,Qt::AlignCenter);
    mainLayout->addLayout(buttonLayout,1,0);

    setLayout(mainLayout);
    setWindowTitle(tr("Headstage firmware settings"));

}

void HeadstageSettingsDialog::percentSliderChanged(int value) {
    percentIndicator->setText(QString().number(value));
    currentSettings.percentChannelsForSettle = value;
    settingsChanged = true;
    okButton->setEnabled(true);
}

void HeadstageSettingsDialog::threshSliderChanged(int value) {
    threshIndicator->setText(QString().number(value));
    currentSettings.threshForSettle = value;
    settingsChanged = true;
    okButton->setEnabled(true);
}

void HeadstageSettingsDialog::autoSettleOnToggled(bool on) {
    currentSettings.autoSettleOn = on;
    settingsChanged = true;
    okButton->setEnabled(true);
}

void HeadstageSettingsDialog::okButtonPressed() {
    emit newSettings(currentSettings); //send the new settings to the source controller
    this->close();
}

void HeadstageSettingsDialog::cancelButtonPressed() {
    this->close();
}

void HeadstageSettingsDialog::closeEvent(QCloseEvent* event) {

    emit windowClosed();
    event->accept();
}

void HeadstageSettingsDialog::resizeEvent(QResizeEvent *event) {
    //autoSettleBox->setGeometry(this->geometry());
    autoSettleBox->setFixedWidth(this->geometry().width()-50);
    percentChannelsSlider->setFixedWidth(this->geometry().width()-120);
    threshSlider->setFixedWidth(this->geometry().width()-120);
}



//-----------------------------------------------------
//This dialog pops up when the "Annotate" button is pressed.
//Allows the user to add comments to the recording session. All comments are
//stores in file that is separate from the main recording file, but with the same base name.

CommentDialog::CommentDialog(QString fileNameIn, QWidget *parent)
    :QWidget(parent),
    fileName(fileNameIn) {

    bool enableControls = false;

    if (!fileName.isEmpty()) {
        enableControls = true;
        QFile commentFile(fileName);
    }

    newCommentEdit = new QLineEdit();
    newCommentEdit->setMinimumWidth(200);
    newCommentEdit->setFrame(true);
    newCommentEdit->setStyleSheet("QLineEdit {border: 2px solid lightgray;"
                                "border-radius: 4px;"
                                "padding: 2px;"
                                "background-color: white}");
    newCommentEdit->setEnabled(enableControls);

    connect(newCommentEdit,SIGNAL(returnPressed()),this,SLOT(saveCurrentComment()));
    connect(newCommentEdit,SIGNAL(textChanged(QString)),this,SLOT(somethingEntered()));

    saveButton = new QPushButton();
    saveButton->setText("Save");
    saveButton->setEnabled(false);

    QLabel* historyLabel = new QLabel();
    historyLabel->setText("History:");
    historyLabel->setEnabled(enableControls);
    //lastComment = new QLabel();
    lastComment = new QTextEdit();
    //sec2lastComment = new QLabel();
    lastComment->setStyleSheet("QFrame {border: 2px solid lightgray;"
                                "border-radius: 4px;"
                                "padding: 2px;"
                                "background-color: white}"
                                "QLabel {color : gray;}");
    lastComment->setMinimumWidth(200);
    lastComment->setEnabled(enableControls);

    commentLabel = new QLabel();
    commentLabel->setText("New comment:");
    commentLabel->setEnabled(enableControls);

    QGridLayout *mainLayout = new QGridLayout;
    QGridLayout *commentLayout = new QGridLayout;
    QGridLayout *buttonLayout = new QGridLayout;

    //commentLayout->addWidget(sec2lastComment,0,0,Qt::AlignCenter);
    commentLayout->addWidget(historyLabel,0,0,Qt::AlignLeft);
    commentLayout->addWidget(lastComment,1,0,Qt::AlignCenter);

    commentLayout->setColumnStretch(0,1);
    commentLayout->setContentsMargins(0,0,0,0);

    buttonLayout->addWidget(saveButton,0,2,Qt::AlignHCenter);
    buttonLayout->setColumnStretch(0,1);

    mainLayout->addLayout(commentLayout,1,0);
    //mainLayout->addWidget(historyFrame,1,0,Qt::AlignRight);
    mainLayout->addWidget(commentLabel,2,0,Qt::AlignLeft);
    mainLayout->addWidget(newCommentEdit,3,0,Qt::AlignCenter);
    mainLayout->addLayout(buttonLayout,4,0);
    //mainLayout->setMargin(10);

    connect(saveButton,SIGNAL(pressed()),this,SLOT(saveCurrentComment()));

    setLayout(mainLayout);
    getHistory();

}

void CommentDialog::saveLine() {
    //Saves the current line to file
    if (saveButton->isEnabled()) {
        if (!fileName.isEmpty()) {
            //append the current timestamp to the comment
            QString currentComment = QString("%1  ").arg(currentTimeStamp) + newCommentEdit->text()+"\n";
            //write to the file
            QFile commentFile(fileName);
            commentFile.open(QIODevice::Append);
            commentFile.write(currentComment.toLocal8Bit());
            commentFile.close();
        }
        newCommentEdit->clear();
        //update the history box
        getHistory();
    }
}

void CommentDialog::getHistory() {
    //Read the contents of the file and populate the "history" box

    QString contents;

    if (!fileName.isEmpty()) {
        QFile commentFile(fileName);
        if (commentFile.exists()) {
            //read the entire file
            commentFile.open(QIODevice::ReadOnly);
            contents = QString(commentFile.readAll());
            commentFile.close();
        }
    }

    lastComment->setText(contents);
    //set the scrollbar to show the last few lines
    QScrollBar *bar = lastComment->verticalScrollBar();
    bar->setValue(bar->maximum());

}


void CommentDialog::saveCurrentComment() {

    //add the comment to the file
    saveLine();
    //close();
}


void CommentDialog::somethingEntered() {
    //we don't enable the save button unless something has been entered
    saveButton->setEnabled(true);
}

void CommentDialog::closeEvent(QCloseEvent *event) {

    emit windowOpenState(false);
    event->accept();
    emit windowClosed();
}

//---------------------------------------------

TrodesButton::TrodesButton(QWidget *parent)
    :QPushButton(parent) {

    setStyleSheet("QPushButton {border: 2px solid lightgray;"
                          "border-radius: 4px;"
                          "padding: 2px;}"
                          "QPushButton::pressed {border: 2px solid gray;"
                          "border-radius: 4px;"
                          "padding: 2px;}"
                          "QPushButton::checked {border: 2px solid gray;"
                          "border-radius: 4px;"
                          "padding: 2px;}");

    QFont buttonFont;
    buttonFont.setPointSize(12);



}

void TrodesButton::setRedDown(bool yes) {
    if (yes) {
        setStyleSheet("QPushButton {border: 2px solid lightgrey;"
                              "border-radius: 4px;"
                              "padding: 2px;}"
                              "QPushButton::pressed {border: 2px solid red;"
                              "border-radius: 4px;"
                              "padding: 2px;}"
                              "QPushButton::checked {border: 2px solid red;"
                              "border-radius: 4px;"
                              "padding: 2px;}");
    } else {
        setStyleSheet("QPushButton {border: 2px solid lightgray;"
                              "border-radius: 4px;"
                              "padding: 2px;}"
                              "QPushButton::pressed {border: 2px solid gray;"
                              "border-radius: 4px;"
                              "padding: 2px;}"
                              "QPushButton::checked {border: 2px solid gray;"
                              "border-radius: 4px;"
                              "padding: 2px;}");
    }
}


//-------------------------------------------
RasterPlot::RasterPlot(QWidget *parent) {

    setMinimumHeight(10);
    setMinimumWidth(200);
    minX = 0;
    maxX = 10;
    setSizePolicy(QSizePolicy ::Expanding , QSizePolicy ::Expanding );

    /*
    QVector<qreal> tmpValues;
    for (int i = 0; i < 200; i++) {
        tmpValues.append(i);
    }

    setData(tmpValues);*/

}


void RasterPlot::setXRange(qreal minR, qreal maxR) {
    maxX = maxR;
    minX = minR;
    //xTickLabels.clear();
    //xTickLabels.append(minX);
    //xTickLabels.append(maxX);
    //xTickLabels.append((maxX-minX)/2);
    update();
}

void RasterPlot::setXLabel(QString l) {
    xLabel = l;
    update();
}

void RasterPlot::setYLabel(QString l) {
    yLabel = l;
    update();
}

void RasterPlot::addRaster(const QVector<qreal> &times) {
    eventTimes.push_back(times);
    //setMinimumHeight(eventTimes.length()*4);
    update();
}

void RasterPlot::setRasters(const QVector<QVector<qreal> > &times) {
    eventTimes.clear();
    for (int i=0; i<times.length(); i++) {
        eventTimes.push_back(times[i]);
    }
    update();
}

void RasterPlot::clearRasters() {
    eventTimes.clear();
    update();
}

void RasterPlot::paintEvent(QPaintEvent *event) {
    QPainter painter(this);
    painter.setRenderHint(QPainter::Antialiasing);
    QPen pen;
    //We want the lines to be a set number of pixels in width
    //units per pixel = unitrange/window_width
    pen.setWidth(1);

    //pen.setWidth(100*TLength);

    pen.setColor(Qt::black);
    painter.setPen(pen);
    QFont f;
    f.setPixelSize(10);
    painter.setFont(f);
    //painter.setPen(Qt::green);
    painter.setViewTransformEnabled(true);
    painter.setViewport(0,0,width(), height());


    qreal numRows = eventTimes.length();
    //qreal maxRange = maxY;

    int margin = 30;

    int topOfGraph = 5;
    int bottomOfGraph = height()-5;
    int graphHeight = bottomOfGraph-topOfGraph;

    int leftOfGraph = margin+20;
    int rightOfGraph = width()-margin;
    int graphWidth = rightOfGraph-leftOfGraph;

    int rowSpacing = 1;
    int tickHeight = ((bottomOfGraph - topOfGraph)/numRows) - (2*rowSpacing);
    if (tickHeight < 1) {
        tickHeight = 1;
    }


    if ((maxX-minX) > 0) {
        for (int r=0; r < eventTimes.length(); r++) {
            int currentRowPix = topOfGraph + rowSpacing + (r*((bottomOfGraph - topOfGraph)/numRows));
            for (int e = 0; e < eventTimes[r].length(); e++) {
                if ((eventTimes[r][e] >= minX) && (eventTimes[r][e] <= maxX)) {
                    int xLoc = leftOfGraph+((rightOfGraph-leftOfGraph)*((eventTimes[r][e]-minX)/(maxX-minX)));
                    painter.drawLine(xLoc,currentRowPix,xLoc,currentRowPix+tickHeight);
                }
            }
        }
    }


    /*
    //Draw the axes
    painter.drawLine(leftOfGraph,bottomOfGraph,rightOfGraph+5,bottomOfGraph);
    painter.drawLine(leftOfGraph,bottomOfGraph,leftOfGraph,topOfGraph-5);


    //Put unit labels on the x axis

    for (int i = 0; i < xTickLabels.length(); i++) {
        int xTickLoc = leftOfGraph+((rightOfGraph-leftOfGraph)*(xTickLabels[i]/(maxX-minX)));
        QRect labelBox = QRect(xTickLoc-10, bottomOfGraph, 20, 20);
        painter.drawText(labelBox, Qt::AlignCenter,
                         QString("%1").arg(xTickLabels[i]));
        painter.drawLine(xTickLoc,bottomOfGraph,xTickLoc,bottomOfGraph+2);
    }


    //Put unit labels on the y axis
    for (int i = 0; i < yTickLabels.length(); i++) {
        int yTickLoc = bottomOfGraph-((bottomOfGraph-topOfGraph)*(yTickLabels[i]/(maxY)));
        QRect labelBox= QRect(leftOfGraph-margin, yTickLoc-10, 25, 20);
        painter.drawText(labelBox, Qt::AlignCenter,
                         QString("%1").arg(yTickLabels[i]));
        painter.drawLine(leftOfGraph,yTickLoc,leftOfGraph-2,yTickLoc);

    }

    //Display the axis labels
    QRect YLabelBox1= QRect(leftOfGraph, height()-25, rightOfGraph-leftOfGraph, 25);
    painter.drawText(YLabelBox1, Qt::AlignCenter, xLabel);

    painter.rotate(-90);
    QRect YLabelBox= QRect(-bottomOfGraph, 0, bottomOfGraph-topOfGraph, 25);
    painter.drawText(YLabelBox, Qt::AlignCenter, yLabel);
    painter.rotate(90);
    */

    painter.end();
}

void RasterPlot::resizeEvent(QResizeEvent *event) {

    update();

}



//--------------------------------------------

HistogramPlot::HistogramPlot(QWidget *parent) {

    setMinimumHeight(100);
    setMinimumWidth(200);



    //setSizePolicy(QSizePolicy ::Expanding , QSizePolicy ::Expanding );


    /*
    QVector<qreal> tmpValues;
    for (int i = 0; i < 200; i++) {
        tmpValues.append(i);
    }

    setData(tmpValues);*/

}


void HistogramPlot::setXRange(qreal minR, qreal maxR) {
    maxX = maxR;
    minX = minR;
    xTickLabels.clear();
    xTickLabels.append(minX);
    xTickLabels.append(maxX);
    xTickLabels.append(((maxX-minX)/2)+minX);
    update();
}

void HistogramPlot::setXLabel(QString l) {
    xLabel = l;
    update();
}

void HistogramPlot::setYLabel(QString l) {
    yLabel = l;
    update();
}

void HistogramPlot::setData(QVector<qreal> values) {

    maxY = 0.0;

    barValues.clear();
    for (int i=0; i<values.length(); i++) {
        barValues.append(values[i]);
        if (values[i] > maxY) {
            maxY = values[i];
        }
    }
    minX = 0;
    maxX = values.length();

    xTickLabels.clear();
    xTickLabels.append(minX);
    xTickLabels.append(maxX);
    xTickLabels.append(((maxX-minX)/2)+minX);

    yTickLabels.clear();
    yTickLabels.append(maxY);

    update();


    //qDebug() << sceneRect();
}

void HistogramPlot::paintEvent(QPaintEvent *event) {
    QPainter painter(this);
    painter.setRenderHint(QPainter::Antialiasing);
    QPen pen;
    //We want the lines to be a set number of pixels in width
    //units per pixel = unitrange/window_width
    pen.setWidth(1);

    //pen.setWidth(100*TLength);

    pen.setColor(Qt::black);
    painter.setPen(pen);
    QFont f;
    f.setPixelSize(10);
    painter.setFont(f);
    //painter.setPen(Qt::green);
    painter.setViewTransformEnabled(true);
    painter.setViewport(0,0,width(), height());


    qreal numBars = barValues.length();
    qreal maxRange = maxY;

    int margin = 30;

    int topOfGraph = 5;
    int bottomOfGraph = height()-margin-10;
    int graphHeight = bottomOfGraph-topOfGraph;

    int leftOfGraph = margin+20;
    int rightOfGraph = width()-margin;
    int graphWidth = rightOfGraph-leftOfGraph;



    for (int c=0; c < barValues.length(); c++) {
        QRectF rect = QRectF(leftOfGraph + (c*(graphWidth/numBars)), bottomOfGraph-((barValues[c]/maxRange)*graphHeight), (graphWidth/numBars), ((barValues[c]/maxRange)*graphHeight));
        //QRectF rect = QRectF(c*(width()/numBars), height()-((barValues[c]/maxRange)*height()), (width()/numBars), ((barValues[c]/maxRange)*height()));
        painter.drawRect(rect);
        painter.fillRect(rect,QBrush(QColor(100,100,100)));
    }


    //Draw the axes
    painter.drawLine(leftOfGraph,bottomOfGraph,rightOfGraph+5,bottomOfGraph);
    painter.drawLine(leftOfGraph,bottomOfGraph,leftOfGraph,topOfGraph-5);


    //Put unit labels on the x axis

    for (int i = 0; i < xTickLabels.length(); i++) {
        int xTickLoc = leftOfGraph+((rightOfGraph-leftOfGraph)*((xTickLabels[i]-minX)/(maxX-minX)));
        QRect labelBox = QRect(xTickLoc-10, bottomOfGraph, 20, 20);
        painter.drawText(labelBox, Qt::AlignCenter,
                         QString::number(xTickLabels[i], 'f', 1 ));
        painter.drawLine(xTickLoc,bottomOfGraph,xTickLoc,bottomOfGraph+2);
    }


    //Put unit labels on the y axis
    for (int i = 0; i < yTickLabels.length(); i++) {
        int yTickLoc = bottomOfGraph-((bottomOfGraph-topOfGraph)*(yTickLabels[i]/(maxY)));
        QRect labelBox= QRect(leftOfGraph-margin, yTickLoc-10, 25, 20);
        painter.drawText(labelBox, Qt::AlignCenter,
                         QString::number(yTickLabels[i], 'f', 1 ));
        painter.drawLine(leftOfGraph,yTickLoc,leftOfGraph-2,yTickLoc);

    }


    //Display the axis labels
    QRect YLabelBox1= QRect(leftOfGraph, height()-25, rightOfGraph-leftOfGraph, 25);
    painter.drawText(YLabelBox1, Qt::AlignCenter, xLabel);

    painter.rotate(-90);
    QRect YLabelBox= QRect(-bottomOfGraph, 0, bottomOfGraph-topOfGraph, 25);
    painter.drawText(YLabelBox, Qt::AlignCenter, yLabel);
    painter.rotate(90);

    painter.end();
}

void HistogramPlot::resizeEvent(QResizeEvent *event) {
    //scene->setSceneRect(0,0,event->size().width(),event->size().height());
    //scene->setSceneRect(0,0,event->size().width(),event->size().height());
    update();

}

//-------------------------------------------

PSTHDialog::PSTHDialog(QWidget *parent)
    :QWidget(parent) {

    setMinimumHeight(300);
    setMinimumWidth(300);


    rasterWindow = new RasterPlot(this);
    window = new HistogramPlot(this);
    window->setMaximumHeight(200);
    window->setXLabel("Time relative to event (sec)");
    window->setYLabel("Rate (Hz)");

    QGridLayout *mainLayout = new QGridLayout;


    QGridLayout *plotLayout = new QGridLayout;
    plotLayout->setVerticalSpacing(0);
    plotLayout->setContentsMargins(0,0,0,0);
    plotLayout->addWidget(rasterWindow,0,0);
    plotLayout->addWidget(window,1,0);
    plotLayout->setRowStretch(0,3);
    plotLayout->setRowStretch(1,1);
    mainLayout->addLayout(plotLayout,0,0);

    rangeLabel = new QLabel("+/- msec");
    binsLabel = new QLabel("Bins");
    QFont labelFont;
    labelFont.setPixelSize(10);
    rangeLabel->setFont(labelFont);
    binsLabel->setFont(labelFont);
    rangeLabel->setMaximumHeight(25);
    binsLabel->setMaximumHeight(25);

    rangeSpinBox = new QSpinBox(this);
    rangeSpinBox->setFrame(false);
    rangeSpinBox->setStyleSheet("QSpinBox { background-color: white; }");
    rangeSpinBox->setMinimum(50);
    rangeSpinBox->setMaximum(1000);
    rangeSpinBox->setSingleStep(10);
    rangeSpinBox->setFixedSize(50,22);
    //rangeSpinBox->setAlignment(Qt::AlignRight);
    rangeSpinBox->setFocusPolicy(Qt::NoFocus);
    rangeSpinBox->setToolTip(tr("Display range (+/- msec)"));

    numBinsSpinBox = new QSpinBox(this);
    numBinsSpinBox->setFrame(false);
    numBinsSpinBox->setStyleSheet("QSpinBox { background-color: white; }");
    numBinsSpinBox->setMinimum(20);
    numBinsSpinBox->setMaximum(200);
    numBinsSpinBox->setSingleStep(1);
    numBinsSpinBox->setFixedSize(50,22);
    //rangeSpinBox->setAlignment(Qt::AlignRight);
    numBinsSpinBox->setFocusPolicy(Qt::NoFocus);
    numBinsSpinBox->setToolTip(tr("Number of bins"));

    connect(rangeSpinBox,SIGNAL(valueChanged(int)),this,SLOT(setRange(int)));
    connect(numBinsSpinBox,SIGNAL(valueChanged(int)),this,SLOT(setNumBins(int)));


    /*rangeSpinBox->setMaximumWidth(30);
    numBinsSpinBox->setMaximumWidth(30);
    rangeLabel->setMaximumWidth(30);
    binsLabel->setMaximumWidth(30);*/

    //rangeSpinBox->setMaximumHeight(30);
    //numBinsSpinBox->setMaximumHeight(30);

    QGridLayout *controlLayout = new QGridLayout;
    controlLayout->setVerticalSpacing(0);
    controlLayout->setContentsMargins(0,0,0,0);
    controlLayout->addWidget(rangeLabel,0,1);
    controlLayout->addWidget(binsLabel,0,2);
    controlLayout->addWidget(rangeSpinBox,1,1);
    controlLayout->addWidget(numBinsSpinBox,1,2);
    controlLayout->setColumnStretch(0,1);
    mainLayout->addLayout(controlLayout,1,0);
    mainLayout->setRowStretch(0,1);


    setLayout(mainLayout);
    window->show();
    rasterWindow->show();

    //Remembered settings...
    QSettings settings(QLatin1String("SpikeGadgets"), QLatin1String("Trodes"));

    //Place the window where it was the last session
    settings.beginGroup(QLatin1String("position"));
    QRect tempPosition = settings.value(QLatin1String("PSTHposition")).toRect();
    if (tempPosition.height() > 0) {
        setGeometry(tempPosition);
    }
    settings.endGroup();

    settings.beginGroup(QLatin1String("PSTHsettings"));
    int tempNumBins = settings.value(QLatin1String("numbins")).toInt();
    if (tempNumBins > 0) {
        numBinsSpinBox->setValue(tempNumBins);
    } else {
        numBinsSpinBox->setValue(100);
    }
    int tempMsecRange = settings.value(QLatin1String("msecRange")).toInt();
    if (tempMsecRange > 0) {
        rangeSpinBox->setValue(tempMsecRange);
    } else {
        rangeSpinBox->setValue(500);
    }

    /*absTimeRange = 1.0;
    numBins = 80; //Total number of bins in range
    binSize = (2*absTimeRange)/numBins;*/

    settings.endGroup();


    /*
    QVector<uint32_t> trialTimes;
    QVector<uint32_t> eventTimes;

    for (uint32_t t = 5; t < 1000000; t = t+30000) {
        trialTimes.append(t);
    }

    for (uint32_t t = 5; t < 1000000; t = t+5000) {
        eventTimes.append(t);
    }

    plot(trialTimes,eventTimes);*/

}

void PSTHDialog::resizeEvent(QResizeEvent *event) {
    //Remember the new size for the next session
    QSettings settings(QLatin1String("SpikeGadgets"), QLatin1String("Trodes"));

    settings.beginGroup(QLatin1String("position"));
    settings.setValue(QLatin1String("PSTHposition"), this->geometry());
    settings.endGroup();
}

void PSTHDialog::plot(const QVector<uint32_t> &trialTimesIn, const QVector<uint32_t> &eventTimesIn) {

    trialTimes.clear();
    trialTimes.resize(trialTimesIn.length());
    for (int i=0; i < trialTimesIn.length(); i++) {
        trialTimes[i] = trialTimesIn[i];
    }

    eventTimes.clear();
    eventTimes.resize(eventTimesIn.length());
    for (int i=0; i < eventTimesIn.length(); i++) {
        eventTimes[i] = eventTimesIn[i];
    }

    calcDisplay();

}

void PSTHDialog::setNumBins(int nBins) {
    //absTimeRange = 1.0;
    numBins = nBins; //Total number of bins in range
    binSize = (2*absTimeRange)/numBins;
    calcDisplay();

    QSettings settings(QLatin1String("SpikeGadgets"), QLatin1String("Trodes"));

    settings.beginGroup(QLatin1String("PSTHsettings"));
    settings.setValue(QLatin1String("numbins"), numBins);
    settings.endGroup();
}

void PSTHDialog::setRange(int msecRange) {
    absTimeRange = (qreal)msecRange/1000;
    binSize = (2*absTimeRange)/numBins;
    calcDisplay();
    QSettings settings(QLatin1String("SpikeGadgets"), QLatin1String("Trodes"));

    settings.beginGroup(QLatin1String("PSTHsettings"));
    settings.setValue(QLatin1String("msecRange"), msecRange);
    settings.endGroup();
}

void PSTHDialog::calcDisplay() {



    int sampRate = hardwareConf->sourceSamplingRate;
    int rangeInt = absTimeRange*sampRate;
    QVector<QVector<qreal> > trialEventTimes;

    numTrials = trialTimes.length();
    counts.clear();
    for (int i=0; i < numBins; i++) {
        counts.push_back(0);
    }

    int currentEventInd = 0;
    uint32_t trialWindowStart;
    uint32_t trialWindowEnd;
    for (int trialInd=0; trialInd < trialTimes.length(); trialInd++) {
        QVector<qreal> relEventTimes;

        //Scan the event times until we enter the next trial window
        if (trialTimes[trialInd] > rangeInt) {
            trialWindowStart = trialTimes[trialInd] - rangeInt;
        } else {
            trialWindowStart = 0;
        }
        trialWindowEnd = trialTimes[trialInd] + rangeInt;
        while ((eventTimes[currentEventInd] < trialWindowStart) && (currentEventInd < eventTimes.length())) {
            currentEventInd++;
        }

        //Now bin the spikes within the trial window
        int trialEventInd = 0;
        while ((eventTimes[currentEventInd+trialEventInd] < trialWindowEnd) && ((currentEventInd+trialEventInd) < eventTimes.length())) {
            qreal tmpRelEventTime = ((qreal)eventTimes[currentEventInd+trialEventInd]-(qreal)trialTimes[trialInd])/sampRate;
            relEventTimes.append(tmpRelEventTime);
            for (int i=0; i < numBins; i++) {
                if (tmpRelEventTime <= -absTimeRange + (i*binSize)+binSize) {
                    counts[i]++;
                    break;
                }
            }
            trialEventInd++;
        }

        trialEventTimes.append(relEventTimes);
    }


    QVector<qreal> rates;
    for (int i=0; i < numBins; i++) {
        rates.push_back(counts[i]/(binSize*numTrials));
    }
    window->setData(rates);
    window->setXRange(-absTimeRange, absTimeRange);

    rasterWindow->setXRange(-absTimeRange, absTimeRange);
    rasterWindow->setRasters(trialEventTimes);

    /*
    QVector<qreal> tmpValues;
    for (int i = 0; i < 200; i++) {
        tmpValues.append(i);
    }
    window->setData(tmpValues);
    window->setXRange(-absTimeRange, absTimeRange);

    rasterWindow->setXRange(-absTimeRange, absTimeRange);
    QVector<qreal> faketimes;
    faketimes.append(-1.5);
    faketimes.append(-1);
    faketimes.append(-.75);
    faketimes.append(-.1);
    faketimes.append(1.7);
    faketimes.append(3.3);
    faketimes.append(5.6);
    faketimes.append(7.7);

    rasterWindow->addRaster(faketimes);
    rasterWindow->addRaster(faketimes);
    rasterWindow->addRaster(faketimes);
    rasterWindow->addRaster(faketimes);
    rasterWindow->addRaster(faketimes);
    rasterWindow->addRaster(faketimes);
    rasterWindow->addRaster(faketimes);
    rasterWindow->addRaster(faketimes);
    rasterWindow->addRaster(faketimes);
    rasterWindow->addRaster(faketimes);
    */

}
