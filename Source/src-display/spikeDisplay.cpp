/*
Trodes is a free, open-source neuroscience data collection and experimental control toolbox

Copyright (C) 2012 Mattias Karlsson

This program is free software: you can redistribute it and/or modify
                               it under the terms of the GNU General Public License as published by
                               the Free Software Foundation, either version 3 of the License, or
                               (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include "spikeDisplay.h"
#include "streamDisplay.h"
#include "globalObjects.h"

//Q_DECLARE_METATYPE(QList<int>)
//------------------------------------------------------------------
RubberBandPolygonNode::RubberBandPolygonNode(int nodeNum, QGraphicsItem *parent):
    nodeNum(nodeNum),
    QGraphicsRectItem(parent) {

    //This object is a movable node in a user-drawn polygon. It is a child
    //of a RubberBandPolygon

    dragging = false;
    color = QColor(255,255,255);

}

void RubberBandPolygonNode::setColor(QColor c) {
    color = c;
}


void RubberBandPolygonNode::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget) {

     //Set composition mode to be the inverse of the background
    //painter->setCompositionMode(QPainter::CompositionMode_Difference);
    //painter->setCompositionMode(QPainter::RasterOp_SourceAndNotDestination);



    painter->setOpacity(1);

    QPen pen(color,1);
    painter->setPen(pen);
    painter->setBrush(Qt::NoBrush);
    painter->drawRect(rect());
}

void RubberBandPolygonNode::mouseMoveEvent(QGraphicsSceneMouseEvent *event) {
    //The node is being dragged
    QGraphicsItem::mouseMoveEvent(event);
    dragging = true;
    emit nodeMoved(nodeNum);
}

void RubberBandPolygonNode::mouseReleaseEvent(QGraphicsSceneMouseEvent *event) {
    QGraphicsItem::mouseReleaseEvent(event);
    if (dragging) {
        emit nodeMoveFinished();
    }
    dragging = false;
}
//----------------------------------------------------------
RubberBandPolygon::RubberBandPolygon(QGraphicsItem *parent)
    :QGraphicsPolygonItem(parent) {

    //This is a user-drawn polygon to define ROI's
    dragging = false;
    clusterNum = 0;
    setFlag(QGraphicsItem::ItemIsMovable,true);
    //setFlag(QGraphicsItem::ItemIsSelectable,true);

    //Default axes
    xAxis = -1;
    yAxis = -1;

    color = QColor(255,255,255);

}

RubberBandPolygon::~RubberBandPolygon() {

    //Not sure if this is actually needed
    while (points.length() > 0) {
        removeLastPoint();
    }
}

void RubberBandPolygon::setAxes(int x, int y) {
    xAxis = x;
    yAxis = y;
}

RubberBandPolygon::Axes RubberBandPolygon::getAxes() {
    RubberBandPolygon::Axes a;
    a.x = xAxis;
    a.y = yAxis;
    return a;
}

void RubberBandPolygon::setClusterNum(int cNum) {
    clusterNum = cNum;
}

void RubberBandPolygon::setPolyBitInd(int bitInd) {
    polyBitInd = bitInd;
}

int RubberBandPolygon::getPolyBitInd() {
    return polyBitInd;
}

void RubberBandPolygon::setColor(QColor c) {
    //setBrush(c);
    color = c;
    for (int i=0;i<nodes.length();i++) {
        nodes[i]->setColor(color);
    }
}

QVector<QPoint> RubberBandPolygon::getPoints() {
    //Returns the points in the polygon in amplitude space
    //(integer format, scaled to fit 16-bit integer range)
    QVector<QPoint> polyPoints;
    for (int i=0;i<amplitudeSpacePoints.length();i++) {
        QPoint tmpPoint;
        tmpPoint.setX((int) amplitudeSpacePoints[i].x());
        tmpPoint.setY((int) amplitudeSpacePoints[i].y());
        polyPoints.append(tmpPoint);
    }
    return polyPoints;
}

int RubberBandPolygon::getClusterNum() {
    return clusterNum;
}

void RubberBandPolygon::removeLastPoint() {
    if (points.length()>0) {
        points.removeLast();
        relativePoints.removeLast();
        amplitudeSpacePoints.removeLast();
        setPolygon(QPolygonF(points));
        delete nodes.takeLast();

    }
}

void RubberBandPolygon::moveLastPoint(QPointF newLoc) {
    //move the last point in the polygon to the new location
    //Used as the polygon is being drawn by the user
    if (points.length()>0) {
        points.last().setX(newLoc.x());
        points.last().setY(newLoc.y());
        QPointF newRelativePoint;
        newRelativePoint.setX(newLoc.x()/this->scene()->width());
        newRelativePoint.setY(newLoc.y()/this->scene()->height());
        relativePoints.last().setX(newRelativePoint.x());
        relativePoints.last().setY(newRelativePoint.y());

        amplitudeSpacePoints.last().setX(relativePoints.last().x()*ampWindow.width() + ampWindow.left());

        //Because the polygon drawing origin is at the top-left, we need to convert to an origin that is bottom-left
        amplitudeSpacePoints.last().setY((1-relativePoints.last().y())*ampWindow.height() + ampWindow.top());


        setPolygon(QPolygonF(points));
        nodes.last()->setPos(newLoc.x()-3,newLoc.y()-3);
    }
}

void RubberBandPolygon::setCurrentAmpWindow(QRectF win) {
    ampWindow = win; //(origin at top left)

    //We are either translating or zooming/out.  To calculate
    //polygon position on the screen, we convert from amplitude
    //space to pixel space.
    QPointF tmpPoint;
    for (int i=0; i < points.length(); i++) {

        //Calculate the new relative location (between 0 and 1 on the screen)
        relativePoints[i].setX((amplitudeSpacePoints[i].x()-ampWindow.left())/ampWindow.width());

        //Because the polygon drawing origin is at the top-left, we need to convert to an origin that is bottom-left
        relativePoints[i].setY(1-((amplitudeSpacePoints[i].y()-ampWindow.top())/ampWindow.height()));

        //From the relative location, calculate the x and y pixels
        tmpPoint.setX(relativePoints[i].x()*this->scene()->width());
        tmpPoint.setY(relativePoints[i].y()*this->scene()->height());

        //map from the scene coordinate system to the polygon's
        tmpPoint = mapFromScene(tmpPoint);

        points[i].setX(tmpPoint.x());
        points[i].setY(tmpPoint.y());
        nodes[i]->setX(tmpPoint.x()-3);
        nodes[i]->setY(tmpPoint.y()-3);

    }

    setPolygon(QPolygonF(points));
}

void RubberBandPolygon::addPoint(QPointF newPoint) {
    //Add a new point to the polygon

    points.append(newPoint);
    //We need to calculate the point's relative location on
    //the drawing area (between 0 and 1 for both x and y
    //dimensions. This is important for resizing and for
    //calculating what data falls inside the polygon.


    QPointF newRelativePoint;
    newRelativePoint.setX(newPoint.x()/this->scene()->width());
    newRelativePoint.setY(newPoint.y()/this->scene()->height());
    relativePoints.append(newRelativePoint);

    QPointF pointInAmpSpace;
    pointInAmpSpace.setX(newRelativePoint.x()*ampWindow.width() + ampWindow.left());

    //Because the polygon drawing origin is at the top-left, we need to convert to an origin that is bottom-left
    pointInAmpSpace.setY((1-newRelativePoint.y())*ampWindow.height() + ampWindow.top());
    amplitudeSpacePoints.append(pointInAmpSpace);

    setPolygon(QPolygonF(points));

    //We also create a node that can be dragged by the user.
    RubberBandPolygonNode *tmpNode = new RubberBandPolygonNode(points.length()-1,this);
    nodes.append(tmpNode);
    tmpNode->setRect(0,0,6,6);
    tmpNode->setPos(newPoint.x()-3,newPoint.y()-3);
    tmpNode->setFlag(QGraphicsItem::ItemIsMovable,true);
    tmpNode->setColor(color);
    tmpNode->setVisible(false);
    connect(tmpNode,SIGNAL(nodeMoved(int)),this,SLOT(childNodeMoved(int)));
    connect(tmpNode,SIGNAL(nodeMoveFinished()),this,SLOT(childNodeMoved()));

}

void RubberBandPolygon::childNodeMoved() {
    //this is called after any of the nodes has finished moving
    //emit shapeChanged();
    emit shapeChanged(clusterNum, polyBitInd);
}

void RubberBandPolygon::childNodeMoved(int nodeNum) {

    //this is called as the node is moving-- update polygon shape

    points[nodeNum].setX(nodes[nodeNum]->x()+3);
    points[nodeNum].setY(nodes[nodeNum]->y()+3);
    prepareGeometryChange();
    setPolygon(QPolygonF(points));

    QPolygonF scenePoly = mapToScene(polygon());
    //Also, recalculate the new relative locations
    for (int i=0; i < points.length(); i++) {
        relativePoints[i].setX((scenePoly[i].x()/this->scene()->width()));
        relativePoints[i].setY((scenePoly[i].y()/this->scene()->height()));

        amplitudeSpacePoints[i].setX(relativePoints[i].x()*ampWindow.width() + ampWindow.left());

        //Because the polygon drawing origin is at the top-left, we need to convert to an origin that is bottom-left
        amplitudeSpacePoints[i].setY((1-relativePoints[i].y())*ampWindow.height() + ampWindow.top());
    }

}

void RubberBandPolygon::updateSize() {

    //Called after a resize event
    QPointF tmpPoint;
    setPos(0,0);
    for (int i=0; i < points.length(); i++) {

        tmpPoint.setX(relativePoints[i].x()*this->scene()->width());
        tmpPoint.setY(relativePoints[i].y()*this->scene()->height());
        points[i].setX(tmpPoint.x());
        points[i].setY(tmpPoint.y());
        nodes[i]->setX(tmpPoint.x()-3);
        nodes[i]->setY(tmpPoint.y()-3);

    }
    setPolygon(QPolygonF(points));
}

void RubberBandPolygon::highlight() {
    //Make the draggable nodes visible when the polygon is clicked
    //with the edit tool
    for (int i=0; i<nodes.length();i++) {
        nodes[i]->setVisible(true);
    }
}

void RubberBandPolygon::removeHighlight() {
    //Hide the draggable nodes
    for (int i=0; i<nodes.length();i++) {
        nodes[i]->setVisible(false);
    }
}

bool RubberBandPolygon::isIncludeType() {
    if (type == 0) {
        return true;
    } else {
        return false;
    }
}

bool RubberBandPolygon::isExcludeType() {
    if (type == 1) {
        return true;
    } else {
        return false;
    }
}

bool RubberBandPolygon::isZoneType() {
    if (type == 2) {
        return true;
    } else {
        return false;
    }
}

void RubberBandPolygon::setIncludeType() {

    //This is an include polygon
    type = 0;


}

void RubberBandPolygon::setExcludeType() {

    //This is an exclude polygon
    type = 1;

}

void RubberBandPolygon::setZoneType() {
    type = 2;

}

void RubberBandPolygon::calculateIncludedPoints(bool *inside, int imageWidth, int imageHeight) {
    //Calculate whether or not each pixel in included.
    //The output depends on the type of polygon (include or exclude polygon)

    //Create a new polygon in the shape of this polygon and scale
    //it to the pixel locations of the underlying image
    QVector<QPointF> absPoints;
    absPoints.resize(points.length());
    for (int i=0; i < points.length(); i++) {
        absPoints[i].setX(relativePoints[i].x()*imageWidth);
        absPoints[i].setY(relativePoints[i].y()*imageHeight);
    }
    QPolygonF absPolygon(absPoints);

    //Now we decide if each pixel in the image is included, where 'included'
    //means inside the polygon for include polygons and outside
    //the polygon for exclude polygons
    uint32_t pixnum = 0;
    if (isIncludeType()) {
        for (int h = 0; h < imageHeight; h++) {
            for (int w = 0; w < imageWidth; w++) {
                if (!absPolygon.containsPoint(QPointF(w,h),Qt::OddEvenFill)) {
                    *(inside+pixnum) = false;
                }
                pixnum++;
            }
        }
    } else if (isExcludeType()) {
        for (int h = 0; h < imageHeight; h++) {
            for (int w = 0; w < imageWidth; w++) {
                if (absPolygon.containsPoint(QPointF(w,h),Qt::OddEvenFill)) {
                    *(inside+pixnum) = false;
                }
                pixnum++;
            }
        }
    }
}

void RubberBandPolygon::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget) {
    //Paint the polygon

    //QPen pen(Qt::red,1);
    QPen pen(color,1);

    //pen.setColor(Qt::white);
    painter->setPen(pen);
    painter->setBrush(Qt::NoBrush);

    if (points.length() > 2) {
        painter->drawPolygon(polygon());
    } else if (points.length() == 2) {
        painter->drawLine(QLineF(points[0],points[1]));
    }
}

void RubberBandPolygon::mousePressEvent(QGraphicsSceneMouseEvent *event) {
    //The polygon was clicked
    emit hasHighlight();
    highlight();

    if (event->button() == Qt::RightButton) {
        emit rightClicked(event->pos().toPoint());
    }
}

void RubberBandPolygon::mouseMoveEvent(QGraphicsSceneMouseEvent *event) {

  //The polygon is being dragged.  We need to recalculate
  //the new relative points
  QGraphicsItem::mouseMoveEvent(event);
  QPolygonF scenePoly = mapToScene(polygon());



  /*
  QPointF newRelativePoint;
  newRelativePoint.setX(newPoint.x()/this->scene()->width());
  newRelativePoint.setY(newPoint.y()/this->scene()->height());
  relativePoints.append(newRelativePoint);

  QPointF pointInAmpSpace;
  pointInAmpSpace.setX(newRelativePoint.x()*ampWindow.width() + ampWindow.left());

  //Because the polygon drawing origin is at the top-left, we need to convert to an origin that is bottom-left
  pointInAmpSpace.setY((1-newRelativePoint.y())*ampWindow.height() + ampWindow.top());
  amplitudeSpacePoints.append(pointInAmpSpace);
  */

  for (int i=0; i < points.length(); i++) {
      relativePoints[i].setX((scenePoly[i].x()/this->scene()->width()));
      relativePoints[i].setY((scenePoly[i].y()/this->scene()->height()));

      amplitudeSpacePoints[i].setX(relativePoints[i].x() *ampWindow.width() + ampWindow.left());

      //Because the polygon drawing origin is at the top-left, we need to convert to an origin that is bottom-left
      amplitudeSpacePoints[i].setY((1-relativePoints[i].y())*ampWindow.height() + ampWindow.top());
  }

  dragging = true;


}

void RubberBandPolygon::mouseReleaseEvent(QGraphicsSceneMouseEvent *event) {
    QGraphicsItem::mouseReleaseEvent(event);

    if (dragging) {
        emit shapeChanged(clusterNum, polyBitInd);
    }
    dragging = false;
}
//--------------------------------------------
ScatterPlotMipmap::ScatterPlotMipmap(QRectF space, int numXPixels, int numYPixels)
        :scatterSpace(space), //the full space the the points are plotted in
         currentSpaceWindow(space), //the default view window is the full space
         xPixels(numXPixels), //max number of pixels
         yPixels(numYPixels),
         destWindow(QRect(0,0,100,100)) //default destination is 100 by 100
{
    int tempXRes = numXPixels;
    int tempYRes = numYPixels;

    initialized = false;

    //The images use a common color table to save memory
    colorTable.push_back(qRgb(0,0,0));
    colorTable.push_back(qRgb(255,255,255));

}

void ScatterPlotMipmap::setColorTable(const QList<QColor> &colors) {



    colorTable.clear();
    //colorTable.push_back(qRgb(0,0,0));
    //colorTable.push_back(qRgb(255,255,255));

    //Every color gets 16 shades for density display (16 clusters allowed). The
    //darkestVal variable sets how much to scale the darkest shade
    double darkestVal = 1.0;
    double step = (1.0-darkestVal)/16;
    for (int i=0; i<colors.length();i++) {
        for (int j=0; j < 16; j++) {
            int b = colors[i].blue()*(darkestVal+(step*j));
            int r = colors[i].red()*(darkestVal+(step*j));
            int g = colors[i].green()*(darkestVal+(step*j));
            colorTable.push_back(qRgb(r,g,b));

        }
    }


    /*
    for (int i=0; i<colors.length();i++) {
        colorTable.push_back(colors[i].rgb());
    }
    for (int i=colors.length(); i<128;i++) {
        colorTable.push_back(qRgb(0,0,0));
    }
    for (int i=128; i<255; i++) {
        int greyScale = 255-(2*(i-128));
        //colorTable.push_back(qRgb(255,255,255));
        colorTable.push_back(qRgb(greyScale,greyScale,greyScale));
    }
    */

}

void ScatterPlotMipmap::createImages() {
    if (!initialized) {
        int tempXRes = xPixels;
        int tempYRes = yPixels;
        //We store at least one image (full resolution)
        imageVector.push_back(new QImage(tempXRes,tempYRes,QImage::Format_Indexed8));
        imageVector.last()->setColorTable(colorTable);
        imageVector.last()->fill(255);
        tempXRes = tempXRes/2;
        tempYRes = tempYRes/2;

        //We also store images at repeatedly halved resolution (until we get to a set minimum)
        while ((tempXRes > 50) && (tempYRes > 50)) {
            imageVector.push_back(new QImage(tempXRes,tempYRes,QImage::Format_Indexed8));
            imageVector.last()->setColorTable(colorTable);
            imageVector.last()->fill(255);
            tempXRes = tempXRes/2;
            tempYRes = tempYRes/2;
        }

        initialized = true;
        setWindows(currentSpaceWindow,destWindow);

    }
}

void ScatterPlotMipmap::deleteImages() {
    while (imageVector.length() > 0) {
        delete imageVector.takeLast();
    }
    initialized = false;
}

void ScatterPlotMipmap::addPoint(qreal x, qreal y, int colorIndex) {

    if (initialized) {
        qreal xPixelsPerSpaceUnit;
        qreal yPixelsPerSpaceUnit;

        /*
    if (x > scatterSpace.width()) {
        x = scatterSpace.width()-1;
    }
    if (y > scatterSpace.height()) {
        x = scatterSpace.width()-1;
    }*/
        if (x > scatterSpace.width()) {
            x = scatterSpace.width()-1;
        }
        if (y > scatterSpace.height()) {
            y = scatterSpace.height()-1;
        }

        int xpix;
        int ypix;
        for (int i=0; i<imageVector.length(); i++) {
            xPixelsPerSpaceUnit = (qreal)imageVector[i]->width()/scatterSpace.width();
            yPixelsPerSpaceUnit = (qreal)imageVector[i]->height()/scatterSpace.height();
            xpix = (x-scatterSpace.left())*xPixelsPerSpaceUnit;
            ypix = (y-scatterSpace.top())*yPixelsPerSpaceUnit;
            if (xpix<0) xpix = 0;
            if (ypix<0) ypix = 0;
            //if (xpix>imageVector[i]->width()) xpix = imageVector[i]->width()-1;
            //if (ypix<imageVector[i]->height()) ypix = imageVector[i]->height()-1;

            //imageVector[i]->setPixel((x-scatterSpace.left())*xPixelsPerSpaceUnit,(y-scatterSpace.top())*yPixelsPerSpaceUnit,colorIndex+1);


            //imageVector[i]->setPixel(xpix,ypix, (colorIndex<< 4)+15);

            int oldPixVal = imageVector[i]->pixelIndex(xpix,ypix);
            if (oldPixVal==255) {
                imageVector[i]->setPixel(xpix,ypix,(colorIndex << 4));
            } else if ((oldPixVal & 0x0f) < 15) {
                imageVector[i]->setPixel(xpix,ypix, (colorIndex<< 4)+(oldPixVal & 0x0f)+1);

            }

            /*
            if (colorIndex == 0) {

                int oldPixVal = imageVector[i]->pixelIndex(xpix,ypix);

                if (oldPixVal == 0) {

                    imageVector[i]->setPixel(xpix,ypix,128);
                } else if (oldPixVal < 255) {

                    imageVector[i]->setPixel(xpix,ypix,oldPixVal+1);
                }

            } else {

                imageVector[i]->setPixel(xpix,ypix,colorIndex+1);
            }*/

        }
    }

}

void ScatterPlotMipmap::clearData() {
    for (int i=0; i<imageVector.length(); i++) {
        imageVector[i]->fill(255);
    }
}

void ScatterPlotMipmap::setPoints(int startInd, int endInd, const QVector<int>* inputData, const QVector<quint8> &clusterIDs) {
    //This function repplots the mixmaps using the raw data



    if (initialized) {
        const int* tmpXdata = inputData[axes.first].constData();
        const int* tmpYdata = inputData[axes.second].constData();

        qreal xPixelsPerSpaceUnit;
        qreal yPixelsPerSpaceUnit;

        int x;
        int y;
        for (int i=0; i<imageVector.length(); i++) {
            xPixelsPerSpaceUnit = (qreal)imageVector[i]->width()/scatterSpace.width();
            yPixelsPerSpaceUnit = (qreal)imageVector[i]->height()/scatterSpace.height();
            imageVector[i]->fill(255);
            int xpix;
            int ypix;
            int oldPixVal;
            for (int j = startInd; j < endInd; j++) {
                x = tmpXdata[j];
                y = tmpYdata[j];
                if (x > scatterSpace.width()) {
                    x = scatterSpace.width()-1;
                }
                if (y > scatterSpace.height()) {
                    y = scatterSpace.height()-1;
                }
                xpix = (x-scatterSpace.left())*xPixelsPerSpaceUnit;
                ypix = (y-scatterSpace.top())*yPixelsPerSpaceUnit;
                if (xpix < 0) xpix = 0;
                if (ypix < 0) ypix = 0;
                //if (xpix>imageVector[i]->width()) xpix = imageVector[i]->width()-1;
                //if (ypix<imageVector[i]->height()) ypix = imageVector[i]->height()-1;

                //imageVector[i]->setPixel((tmpXdata[j]-scatterSpace.left())*xPixelsPerSpaceUnit,(tmpYdata[j]-scatterSpace.top())*yPixelsPerSpaceUnit,clusterIDs[j]+1);

                oldPixVal = imageVector[i]->pixelIndex(xpix,ypix);
                if (oldPixVal==255) {
                    imageVector[i]->setPixel(xpix,ypix,(clusterIDs[j] << 4));
                } else if ((oldPixVal & 0x0f) < 15) {
                    imageVector[i]->setPixel(xpix,ypix, (clusterIDs[j]<< 4)+(oldPixVal & 0x0f)+1);
                }

                //imageVector[i]->setPixel(xpix,ypix,clusterIDs[j]+1);


            }
        }

    }
}


void ScatterPlotMipmap::setDestWindow(QRect destWindow) {
    setWindows(currentSpaceWindow,destWindow);
}

void ScatterPlotMipmap::setWindows(QRectF sourceWindow, QRect destinationWindow) {
    currentSpaceWindow = sourceWindow;
    destWindow = destinationWindow;

    //Calculate which of the images should be used, based on the x axis

    if (initialized) {

        qreal desiredXUnitsPerPixel = currentSpaceWindow.width()/destWindow.width();
        qreal xUnitsPerPixel;

        int minIndex;
        for (int i=0; i < imageVector.length(); i++) {
            xUnitsPerPixel = scatterSpace.width()/imageVector[i]->width();

            if (i==0) {
                //minDiffToDesired = abs(desiredXUnitsPerPixel-xUnitsPerPixel); //abs difference
                minIndex = i;
            }  else {

                if (xUnitsPerPixel <= desiredXUnitsPerPixel) {
                    //The detail more than we need, so we keep this index as the best so far
                    minIndex = i;
                } else {
                    //We are now at too coarse of pixel detail, so stop
                    if (i>2) {
                        minIndex = i-2;
                    }
                    break;
                }

            }
        }

        //Remember which image came the closest
        currentImageIndex = minIndex;
        //qDebug() << minIndex << imageVector[minIndex]->width() << imageVector[minIndex]->height();
        //Calculate the pixel window to use for the current mipmap image
        qreal xPixelsPerSpaceUnit = imageVector[minIndex]->width()/scatterSpace.width();
        qreal yPixelsPerSpaceUnit = imageVector[minIndex]->height()/scatterSpace.height();

        sourceImageWindow.setTop((currentSpaceWindow.top()-scatterSpace.top())*yPixelsPerSpaceUnit);
        sourceImageWindow.setBottom((currentSpaceWindow.bottom()-scatterSpace.top())*yPixelsPerSpaceUnit);
        sourceImageWindow.setLeft((currentSpaceWindow.left()-scatterSpace.left())*xPixelsPerSpaceUnit);
        sourceImageWindow.setRight((currentSpaceWindow.right()-scatterSpace.left())*xPixelsPerSpaceUnit);

    }

}

QImage ScatterPlotMipmap::getCurrentImage() {
    //return imageVector[currentImageIndex]->copy(sourceImageWindow).mirrored(false,true);
    if (initialized) {
        //return imageVector[currentImageIndex]->copy();
        return deAlias(imageVector[currentImageIndex]->copy(sourceImageWindow).mirrored(false,true),destWindow);
    } else {
        return QImage(100,100,QImage::Format_Indexed8);
    }
    //return imageVector[currentImageIndex]->copy(sourceImageWindow).mirrored(false,true).scaled(destWindow.size(),Qt::IgnoreAspectRatio,Qt::SmoothTransformation);
}

QImage ScatterPlotMipmap::getCurrentImage(QRect outputWindow) {

    if (initialized) {

        //return imageVector[currentImageIndex]->copy(sourceImageWindow);
        return deAlias(imageVector[currentImageIndex]->copy(sourceImageWindow).mirrored(false,true),outputWindow);
    } else {
        return QImage(100,100,QImage::Format_Indexed8);
    }
}

QImage ScatterPlotMipmap::deAlias(const QImage &orig, QRect window) {
    //Replots an image using a different resolution, making sure that any non-zero points are still visible
    QImage img = QImage(window.size(),QImage::Format_Indexed8);
    img.setColorTable(colorTable);
    img.fill(255);

    qreal xOutPixPerInPix = (window.width()-1)/(qreal)orig.width(); //the ratio of the output image res / the original image res
    qreal yOutPixPerInPix = (window.height()-1)/(qreal)orig.height();
    //qDebug() << orig.size();
    for (int i = 0; i < orig.width(); i++) {
        for (int j=0; j < orig.height(); j++) {
            if (orig.pixelIndex(i,j) != 255) {
                img.setPixel(round(i*xOutPixPerInPix),round(j*yOutPixPerInPix),orig.pixelIndex(i,j));
            }
        }
    }
    return img;
}





//----------------------------------------------
CustomScene::CustomScene(QWidget *parent, int bufferSizeInput)
    :QGraphicsScene(parent) {

    initialized = false;
    currentXdisplay = 0;
    currentYdisplay = 1;
    plotNewPoints = true;
    replotScatter = true;
    displayMulipleProjections = false;

    bufferSize = bufferSizeInput;
    displayData.resize(bufferSize);

    backgroundPic = NULL;

    for (int i = 0; i < bufferSize; i++) {
        displayData[i].x = 0;
        displayData[i].y = 0;
    }



    //mipMap = new ScatterPlotMipmap(QRectF(0.0,0.0,(2000.0*65536)/AD_CONVERSION_FACTOR,(2000.0*65536)/AD_CONVERSION_FACTOR),1000,1000);

}

void CustomScene::setBackgroundPic(QImage *pic) {
    backgroundPic = pic;
}

void CustomScene::setMultipleProjections(bool multiple) {
    displayMulipleProjections = multiple;
    update();
}

void CustomScene::setChannels(int channelX, int channelY, const QVector<int>* inputData) {

    plotNewPoints = false;
    currentXdisplay = channelX;
    currentYdisplay = channelY;
    const int* tmpXdata = inputData[currentXdisplay].constData();
    const int* tmpYdata = inputData[currentYdisplay].constData();

    for (int i = 0; i < bufferSize; i++) {

        displayData[i].x = tmpXdata[i];
        displayData[i].y = tmpYdata[i];
    }
    update();
    plotNewPoints = true;

}

void CustomScene::setDataPoint(const QVector<int> *inputData, int currentBufferIndex) {

    if (plotNewPoints) {
        replotScatter = true;
        displayData[currentBufferIndex].x = inputData[currentXdisplay][currentBufferIndex];
        displayData[currentBufferIndex].y = inputData[currentYdisplay][currentBufferIndex];


        replotScatter = true;
        //update();
    }
}

void CustomScene::mousePressEvent(QGraphicsSceneMouseEvent *event) {

    //check to see if the item pressed was a polygon or the background (a widget showing video)
    if(itemAt(event->scenePos(),QTransform())) {
        QGraphicsItem *clickedItem = itemAt(event->scenePos(),QTransform());
        if (!clickedItem->isWidget()) {
            //a polygon was clicked
            QGraphicsScene::mousePressEvent((event));
            return;
        }
    }
    emit emptySpaceClicked();

}

void CustomScene::drawBackground(QPainter *painter, const QRectF &rect) {
    //if (replotScatter) {


    //painter->setRenderHint(QPainter::SmoothPixmapTransform, true);
    painter->setPen(Qt::green);
    painter->setViewTransformEnabled(true);
    painter->setViewport(0,0,width(), height());

    if (backgroundPic != NULL) {
        painter->drawImage(painter->viewport(),*backgroundPic,backgroundPic->rect());
    }




/*
        painter->setWindow(minAmplitude[currentXdisplay], minAmplitude[currentYdisplay], maxAmplitude[currentXdisplay]-minAmplitude[currentXdisplay], maxAmplitude[currentYdisplay]-minAmplitude[currentYdisplay]);
        painter->fillRect(minAmplitude[currentXdisplay], minAmplitude[currentYdisplay], maxAmplitude[currentXdisplay]-minAmplitude[currentXdisplay], maxAmplitude[currentYdisplay]-minAmplitude[currentYdisplay],QBrush(QColor(0,0,0)));
        //painter->fillRect(rect,QBrush(QColor(0,0,0)));
        painter->beginNativePainting();
        glViewport(0, 0, (GLint)width(), (GLint)height());
        glMatrixMode(GL_PROJECTION);
        glLoadIdentity();
        //glOrtho(0, 1.0, 0, 1.0, -1.0, 1.0);
        glOrtho((GLdouble)minAmplitude[currentXdisplay], (GLdouble)maxAmplitude[currentXdisplay], (GLdouble)minAmplitude[currentYdisplay], (GLdouble)maxAmplitude[currentYdisplay], -1.0, 1.0);
        //glOrtho((GLdouble)rect.left(),(GLdouble)rect.right(),(GLdouble)rect.bottom(),(GLdouble)rect.top(),-1.0,1.0);
        glMatrixMode(GL_MODELVIEW);
        glPushMatrix();
        glPointSize(1);
        glColor3f((GLfloat)1.0,(GLfloat)1.0,(GLfloat)1.0);
        //qglColor(QColor(255,255,255));
        glLoadIdentity();
        //glScalef(1.0/(maxAmplitude[currentXdisplay]), 1.0/(maxAmplitude[currentYdisplay]), 1.0);
        glEnableClientState(GL_VERTEX_ARRAY);
        glVertexPointer(2, GL_FLOAT, 0, displayData.constData());
        glDrawArrays(GL_POINTS,0,bufferSize);
        glDisableClientState(GL_VERTEX_ARRAY);
        glMatrixMode(GL_MODELVIEW);
        glPopMatrix();
        painter->endNativePainting();

*/


    //}


}


//-----------------------------------------------------
//The GraphicsWindow contains the polygons and the underlying video image



//The GraphicsWindow contains the polygons and the underlying video image
GraphicsWindow::GraphicsWindow(QWidget *parent, int bufferSizeInput, int numChannels)
    : QGraphicsView(parent) {

    //dispWin = new VideoDisplayWindow(NULL);

    currentTool = INCLUDETOOL_ID;
    currentCluster = 1;

    currentXdisplay = 0;
    currentYdisplay = 1;
    currentMipmapIndex = 0;
    bufferSize = bufferSizeInput;

    displayMultipleProjections = false;
    isShown = false;



    //setViewport(new QGLWidget(QGLFormat(QGL::DoubleBuffer| QGL::DirectRendering)));
    setViewport(new QWidget());
    setViewportUpdateMode(QGraphicsView::SmartViewportUpdate);
    update();

    maxAmplitude.resize(numChannels);
    for (int i = 0; i < numChannels; i++) {
        maxAmplitude[i] = (400*65536)/AD_CONVERSION_FACTOR;
    }

    minAmplitude.resize(numChannels);
    for (int i = 0; i < numChannels; i++) {
        minAmplitude[i] = 0;
    }

    lowRangeMipmapMax = 1000.0; //in uV, for the low-range mipmap (not yet implemented)
    for (int i = 0; i < numChannels; i++) {
        for (int j = i+1; j < numChannels; j++) {
            mipMaps.push_back(new ScatterPlotMipmap(QRectF(0.0,0.0,(2500.0*65536)/AD_CONVERSION_FACTOR,(2500.0*65536)/AD_CONVERSION_FACTOR),5000,5000));
            mipMaps.last()->axes = qMakePair(i,j);

            //mipMaps_lowRange.push_back(new ScatterPlotMipmap(QRectF(0.0,0.0,(lowRangeMipmapMax*65536)/AD_CONVERSION_FACTOR,(lowRangeMipmapMax*65536)/AD_CONVERSION_FACTOR),5000,5000));
            //mipMaps_lowRange.last()->axes = qMakePair(i,j);
        }
    }

    scene = new CustomScene(this, bufferSizeInput);
    connect(scene,SIGNAL(emptySpaceClicked()),this,SLOT(spaceClicked()));
    //connect(dispWin,SIGNAL(resolutionChanged()),this,SLOT(calculateConsideredPixels()));
    //scene->addWidget(dispWin,Qt::Widget);
    this->setScene(scene);

    currentlyDrawing = false;
    currentlySelectedPolygon = -1;



    //dispWin->show();

    QPen markerPen;
    markerPen.setWidth(1);
    markerPen.setColor(Qt::green);

    setMouseTracking(true);

    setMultipleProjections(false);

}

void GraphicsWindow::setWindowActive(bool active) {


    if (!isShown && active) {
        isShown = true;
        for (int i=0;i < mipMaps.length(); i++) {
            mipMaps[i]->createImages();

            //mipMaps_lowRange[i]->createImages();
        }

    } else if (isShown && !active) {
        isShown = false;
        for (int i=0;i < mipMaps.length(); i++) {
            mipMaps[i]->deleteImages();
            //mipMaps_lowRange[i]->deleteImages();
        }
    }

}

void GraphicsWindow::setData(const QVector<int> *inputData, const QVector<quint8> &clustID) {
    if (isShown) {
        for (int i=0;i < mipMaps.length(); i++) {
            mipMaps[i]->setPoints(0,bufferSize,inputData, clustID);
            //mipMaps_lowRange[i]->setPoints(0,bufferSize,inputData);
        }
    }
}

void GraphicsWindow::setMultipleProjections(bool multiple) {
    displayMultipleProjections = multiple;

    if (displayMultipleProjections) {
        for (int i=0; i<polygons.length(); i++) {
           polygons[i]->setVisible(false);
        }
    } else {
        //Only show the polygons for the current axes
        RubberBandPolygon::Axes ax;
        for (int i=0; i<polygons.length(); i++) {
            ax = polygons[i]->getAxes();
            if ((ax.x == currentXdisplay) && (ax.y == currentYdisplay)) {
                polygons[i]->setVisible(true);
            } else {
                polygons[i]->setVisible(false);
            }
        }


    }

    if (isShown) {
        scene->update();
    }

}
void GraphicsWindow::updateBackgroundPlot() {

    if (isShown) {
        if (displayMultipleProjections) {

            currentBackgroundPic = QImage(width(),height(),QImage::Format_RGB32);

            currentBackgroundPic.fill(qRgb(0,0,0));
            QPainter paint;
            paint.begin(&currentBackgroundPic);
            paint.setViewTransformEnabled(true);

            int numCol;
            int numRows;
            QPen pn;
            pn.setColor(qRgb(100,100,100));
            pn.setWidth(1);

            if (mipMaps.length() <= 6) {
                numCol = 2;
            } else {
                numCol = 3;
            }
            numRows = ceil(mipMaps.length()/numCol);
            int tmpMipmapIndex = 0;
            QRect currentDrawWindow;

            currentDrawWindow.setWidth(currentBackgroundPic.width()/numCol);
            currentDrawWindow.setHeight(currentBackgroundPic.height()/numRows);

            int panelWidth = currentBackgroundPic.width()/numCol;
            int panelHeight = currentBackgroundPic.height()/numRows;

            //Draw the scatter images
            for (int i=0; i<numCol; i++) {
                for (int j=0; j<numRows; j++) {
                    QImage tempImage = mipMaps[tmpMipmapIndex]->getCurrentImage(currentDrawWindow);
                    paint.drawImage(i*panelWidth,j*panelHeight,tempImage);
                    tmpMipmapIndex++;
                }
            }

            //Draw the image borders
            paint.setPen(pn);
            for (int i=0; i<numCol; i++) {
                for (int j=0; j<numRows; j++) {
                    paint.drawRect(i*panelWidth,j*panelHeight,panelWidth,panelHeight);
                }
            }

            paint.end();

        } else {
            //Just plot one panel
            currentBackgroundPic = mipMaps[currentMipmapIndex]->getCurrentImage();

        }
        scene->setBackgroundPic(&currentBackgroundPic);
    }
}

void GraphicsWindow::setDataPoint(const QVector<int> *inputData, int currentBufferIndex, int clustID) {

    //if (displayMultipleProjections) {
        for (int i=0; i<mipMaps.length(); i++) {
            mipMaps[i]->addPoint((qreal)inputData[mipMaps[i]->axes.first][currentBufferIndex],(qreal)inputData[mipMaps[i]->axes.second][currentBufferIndex],clustID);
        }

        //currentBackgroundPic = mipMaps[currentMipmapIndex]->getCurrentImage();
        //scene->setBackgroundPic(&currentBackgroundPic);
    //} else {


        scene->setDataPoint(inputData, currentBufferIndex);
    //}
    //displayData[currentBufferIndex].x = inputData[currentXdisplay][currentBufferIndex];
    //displayData[currentBufferIndex].y = inputData[currentYdisplay][currentBufferIndex];

}

void GraphicsWindow::clearData() {
    for (int i=0; i<mipMaps.length(); i++) {
        mipMaps[i]->clearData();
    }

    updateBackgroundPlot();
}

void GraphicsWindow::setChannels(int channelX, int channelY, const QVector<int> *inputData, const QVector<quint8> &clustID) {
    currentXdisplay = channelX;
    currentYdisplay = channelY;

    if (!displayMultipleProjections) {
        for (int i=0; i<mipMaps.length(); i++) {
            if ((mipMaps[i]->axes.first == currentXdisplay) && (mipMaps[i]->axes.second == currentYdisplay)) {
                currentMipmapIndex = i;
                mipMaps[i]->setPoints(0,bufferSize,inputData, clustID);
                break;
            }
        }

        currentBackgroundPic = mipMaps[currentMipmapIndex]->getCurrentImage();
        //scene->setBackgroundPic(&currentBackgroundPic);
    }

    //For older opengl plotting
    //scene->setChannels(channelX, channelY, inputData);

    //Only show the polygons for the current axes
    RubberBandPolygon::Axes ax;
    for (int i=0; i<polygons.length(); i++) {
        ax = polygons[i]->getAxes();
        if ((ax.x == currentXdisplay) && (ax.y == currentYdisplay)) {
            polygons[i]->setVisible(true);
        } else {
            polygons[i]->setVisible(false);
        }
    }

    scene->update();
}

void GraphicsWindow::setMaxDisplay(int channel, int maxDisplayVal) {
    //Changes the maximum amplitude of the display range
    //maxAmplitude[channel] = (maxDisplayVal*65536)/AD_CONVERSION_FACTOR;


    setMaxDisplay(maxDisplayVal);

    //scene->setMaxDisplay(channel,maxDisplayVal);



}

void GraphicsWindow::setMaxDisplay(int maxDisplayVal) {

    if (maxDispVal_uV != maxDisplayVal) {


        for (int i=0; i<maxAmplitude.length(); i++) {
            maxAmplitude[i] = (maxDisplayVal*65536)/AD_CONVERSION_FACTOR;
        }

        maxDispVal_uV = maxDisplayVal;



        QRectF dWin = QRectF(0.0,0.0,(qreal)maxAmplitude[0],(qreal)maxAmplitude[0]);

        for (int i=0; i<mipMaps.length();i++) {
            mipMaps[i]->setWindows(dWin,QRect(0,0,width(),height()));
        }

        updateBackgroundPlot();

        //currentBackgroundPic = mipMaps[currentMipmapIndex]->getCurrentImage();
        //scene->setBackgroundPic(&currentBackgroundPic);


        //For older opengl plotting
        //scene->setMaxDisplay(maxDisplayVal);

        for (int i=0;i<polygons.length();i++) {
            polygons[i]->setCurrentAmpWindow(QRectF(0.0,0.0,(qreal)maxAmplitude[currentXdisplay],(qreal)maxAmplitude[currentYdisplay]));
        }





    }
}


void GraphicsWindow::addExcludePolygon() {
    RubberBandPolygon *newPoly = new RubberBandPolygon();
    newPoly->setExcludeType();

    connect(newPoly,SIGNAL(hasHighlight()),this,SLOT(polygonHighlighted()));
    connect(newPoly,SIGNAL(shapeChanged(int,int)),this,SLOT(calculatePointsInsidePolygon(int,int)));

    polygons.append(newPoly);
    scene->addItem(newPoly);
}

void GraphicsWindow::addZonePolygon() {
    RubberBandPolygon *newPoly = new RubberBandPolygon();
    newPoly->setZoneType();

    connect(newPoly,SIGNAL(hasHighlight()),this,SLOT(polygonHighlighted()));

    polygons.append(newPoly);
    scene->addItem(newPoly);
}

void GraphicsWindow::addIncludePolygon() {
    int polyBitInd;
    bool foundEmptySlot = false;
    for (int i=0; i<polygonsTaken.length(); i++) {
        if (!polygonsTaken[i]) {
            polyBitInd = i;
            polygonsTaken[i] = true;
            foundEmptySlot = true;
        }
    }
    if (!foundEmptySlot) {
        polygonsTaken.append(true);
        polyBitInd = polygonsTaken.length()-1;
    }

    RubberBandPolygon *newPoly = new RubberBandPolygon();
    newPoly->setIncludeType();
    newPoly->setPolyBitInd(polyBitInd);
    newPoly->setAxes(currentXdisplay,currentYdisplay);

    connect(newPoly,SIGNAL(hasHighlight()),this,SLOT(polygonHighlighted()));
    connect(newPoly,SIGNAL(shapeChanged(int,int)),this,SLOT(calculatePointsInsidePolygon(int,int)));

    polygons.append(newPoly);

    polygons.last()->setCurrentAmpWindow(QRectF(0.0,0.0,(qreal)maxAmplitude[currentXdisplay],(qreal)maxAmplitude[currentYdisplay]));
    polygons.last()->setColor(QColor(0,255,0));
    scene->addItem(newPoly);

}

void GraphicsWindow::spaceClicked() {
    //the background was clicked, so turn off all highlights
    setNoHighlight();
}

void GraphicsWindow::setNoHighlight() {
    for (int i=0; i<polygons.length(); i++) {
        polygons[i]->removeHighlight();
    }
    currentlySelectedPolygon = -1;
}

void GraphicsWindow::polygonHighlighted() {
    RubberBandPolygon* highlightedPoly = dynamic_cast<RubberBandPolygon*>(sender());

    for (int i=0; i<polygons.length(); i++) {
        if (polygons[i] == highlightedPoly) {
            currentlySelectedPolygon = i;
        } else {
            polygons[i]->removeHighlight();
        }
    }
}

void GraphicsWindow::setTool(int toolNum) {
    currentTool = toolNum;
    if (currentTool != EDITTOOL_ID) {
        //We only allow highlighting when the edit tool is active
        setNoHighlight();
    }
}

void GraphicsWindow::setClusterColors(const QList<QColor> colors) {
    clusterColors = colors;
    for (int i = 0; i < mipMaps.length(); i++) {
        mipMaps[i]->setColorTable(colors);
    }
}

void GraphicsWindow::setCurrentCluster(int clust) {
    currentCluster = clust;
}

void GraphicsWindow::calculatePointsInsidePolygon(int clustInd, int polygonBitIndex) {


    //Find which polygon send the message (can be done with sender() too)
    /*
    int polyInd;
    ClusterPolygonSet polyInfo;
    for (int i=0; i<polygons.length(); i++) {
        if (polygons[i]->getPolyBitInd() == polygonBitIndex) {
            //This polygon has the right bit index, so it's a match
            polyInfo.xAxis = polygons[i]->getAxes().x;
            polyInfo.yAxis = polygons[i]->getAxes().y;
            polyInfo.points = polygons[i]->getPoints();
            polyInfo.bitInd = polygonBitIndex;
            break;
        }
    }*/

    emit lockSpikeSorting();
    emit newClusterCalculationNeeded(clustInd, polygonBitIndex);
    emit unlockSpikeSorting();

}

QList<ClusterPolygonSet> GraphicsWindow::getPolygonsForCluster(int clusterInd) {
    QList<ClusterPolygonSet> polyInfo;
    for (int i=0; i<polygons.length(); i++) {
        if (polygons[i]->getClusterNum() == clusterInd) {
            //This polygon belongs to the desired cluster
            ClusterPolygonSet newPoly;
            newPoly.xAxis = polygons[i]->getAxes().x;
            newPoly.yAxis = polygons[i]->getAxes().y;
            newPoly.points = polygons[i]->getPoints();
            newPoly.bitInd = polygons[i]->getPolyBitInd();
            polyInfo.append(newPoly);
        }
    }
    return polyInfo;
}

void GraphicsWindow::calculateConsideredPixels() {
    //This function is called every time a polygon
    //is changed or whenever we need to recalculate
    //which pixels are included in the current set of
    //polygons.

    /*
    QSize currentRes = dispWin->getResolution();
    quint32 totalPix = currentRes.width()*currentRes.height();
    QVector<bool> isIncluded;

    isIncluded.resize(totalPix);
    //start by assuming that every pixel is included
    for (int i=0; i<isIncluded.length(); i++) {
        isIncluded[i] = true;
    }

    if (currentRes.height() > 0) {
        //Calculate which pixels to include, going through each polygon one by one
        for (int i=0; i < polygons.length(); i++) {
            polygons[i]->calculateIncludedPoints(isIncluded.data(),currentRes.width(),currentRes.height());
        }
        emit newIncludeCalculation(isIncluded);
    }
    */
}

void GraphicsWindow::mousePressEvent(QMouseEvent *event) {
    //When the window is clicked, the behavior depends on
    //which tool is currently selected.

    if (currentlyDrawing) {
        polygons.last()->addPoint(event->localPos());
        return;
    }

    if (!displayMultipleProjections) {
        if (currentTool == INCLUDETOOL_ID) {

            for (int i = 0; i < polygons.length(); i++) {
                if ((polygons[i]->getClusterNum() == currentCluster) && (polygons[i]->getAxes().x == currentXdisplay) && (polygons[i]->getAxes().y == currentYdisplay)){
                    //There is already a polygon for this cluster on this projection
                    ClusterPolygonSet p;
                    p.bitInd = polygons[i]->getPolyBitInd();
                    p.xAxis = polygons[i]->getAxes().x;
                    p.yAxis = polygons[i]->getAxes().y;
                    p.points = polygons[i]->getPoints();


                    polygonsTaken[polygons[i]->getPolyBitInd()] = false;
                    delete polygons.takeAt(i);
                    //setNoHighlight();
                    emit lockSpikeSorting();
                    emit polygonDeleted(currentCluster,p.bitInd);
                    emit unlockSpikeSorting();
                    break;
                }
            }

            addIncludePolygon();
            polygons.last()->setClusterNum(currentCluster);
            polygons.last()->setColor(clusterColors[currentCluster]);


            currentlyDrawing = true;
            polygons.last()->addPoint(event->localPos());
            polygons.last()->addPoint(event->localPos());

        } else if ((currentTool == EDITTOOL_ID)&&(itemAt(event->pos()))) {
            // Clicked on polygon, pass on event to children
            QGraphicsView::mousePressEvent(event);
        } else if (currentTool == PANTOOL_ID) {
            //used to manually designate animal location
        }
    }

}

void GraphicsWindow::mouseMoveEvent(QMouseEvent *event) {

    if (currentlyDrawing) {
        polygons.last()->moveLastPoint(event->localPos());
    } else {
        QGraphicsView::mouseMoveEvent(event);
    }
}

void GraphicsWindow::mouseDoubleClickEvent(QMouseEvent *event) {
    if (currentlyDrawing) {
        currentlyDrawing = false;
        polygons.last()->removeLastPoint();
        //calculateConsideredPixels();
        calculatePointsInsidePolygon(currentCluster, polygons.last()->getPolyBitInd());

    }
}

void GraphicsWindow::wheelEvent(QWheelEvent *wheelEvent) {
    qreal ypos = wheelEvent->pos().y();

    int changeAmount;
    if (wheelEvent->delta() > 0) {
        changeAmount = -25;
    } else {
        changeAmount = 25;
    }

    emit zoom(changeAmount);

}

void GraphicsWindow::keyPressEvent(QKeyEvent *event) {

    if ((event->key() == Qt::Key_Delete)||(event->key() == Qt::Key_Backspace)) {
        //delete the selected polygon
        if (currentlySelectedPolygon != -1) {

            int selectedClusterInd = polygons[currentlySelectedPolygon]->getClusterNum();
            int selectedPolyBitIndex = polygons[currentlySelectedPolygon]->getPolyBitInd();
            ClusterPolygonSet p;
            p.bitInd = polygons[currentlySelectedPolygon]->getPolyBitInd();
            p.xAxis = polygons[currentlySelectedPolygon]->getAxes().x;
            p.yAxis = polygons[currentlySelectedPolygon]->getAxes().y;
            p.points = polygons[currentlySelectedPolygon]->getPoints();
            polygonsTaken[polygons[currentlySelectedPolygon]->getPolyBitInd()] = false;
            delete polygons.takeAt(currentlySelectedPolygon);
            setNoHighlight();
            //calculateConsideredPixels();
            //calculatePointsInsidePolygon(selectedClusterInd,selectedPolyBitIndex);

            emit polygonDeleted(selectedClusterInd,selectedPolyBitIndex);

        }
    }
}

void GraphicsWindow::refresh() {
    for (int i=0; i<mipMaps.length();i++) {
        mipMaps[i]->setDestWindow(QRect(0,0,this->width(),this->height()));
    }

    scene->setSceneRect(0,0,this->width(),this->height());

    for (int i=0; i<polygons.length(); i++) {
        polygons[i]->updateSize();
    }

    updateBackgroundPlot();
    //currentBackgroundPic = mipM
}

void GraphicsWindow::resizeEvent(QResizeEvent *event) {

    //dispWin->resize(event->size().width(),event->size().height());

    for (int i=0; i<mipMaps.length();i++) {
        mipMaps[i]->setDestWindow(QRect(0,0,event->size().width(),event->size().height()));
    }

    scene->setSceneRect(0,0,event->size().width(),event->size().height());

    for (int i=0; i<polygons.length(); i++) {
        polygons[i]->updateSize();
    }

    updateBackgroundPlot();
    //currentBackgroundPic = mipMaps[currentMipmapIndex]->getCurrentImage();
    //scene->setBackgroundPic(&currentBackgroundPic);


}








//The GraphicsWindow contains the polygons and the underlying video image
ScatterPlotWindow::ScatterPlotWindow(QWidget *parent, int bufferSizeInput)
    : QGraphicsView(parent) {

    //dispWin = new VideoDisplayWindow(NULL);

    currentTool = INCLUDETOOL_ID;
    currentCluster = 1;
    currentNTrode = 0;

    currentMouseXPanel = -1;
    currentMouseYPanel = -1;

    currentXdisplay = 0;
    currentYdisplay = 1;
    currentMipmapIndex = 0;
    bufferSize = bufferSizeInput;

    displayMultipleProjections = false;

    int numTrodes = spikeConf->ntrodes.length();
    polygons.resize(numTrodes);
    polygonsTaken.resize(numTrodes);

    //setViewport(new QGLWidget(QGLFormat(QGL::SampleBuffers | QGL::DirectRendering)));
    setViewport(new QGLWidget(QGLFormat(QGL::DoubleBuffer| QGL::DirectRendering)));
    setViewportUpdateMode(QGraphicsView::SmartViewportUpdate);
    //update();

    /*
    maxAmplitude.resize(numTrodes);
    minAmplitude.resize(numTrodes);
    for (int trNum=0;trNum<numTrodes;trNum++) {
        maxAmplitude[trNum].resize(numChannels);
        for (int i = 0; i < numChannels; i++) {
            maxAmplitude[trNum][i] = (400*65536)/AD_CONVERSION_FACTOR;
        }


        minAmplitude[trNum].resize(numChannels);
        for (int i = 0; i < numChannels; i++) {
            minAmplitude[trNum][i] = 0;
        }
    }*/

    lowRangeMipmapMax = 1000.0; //in uV, for the low-range mipmap (not yet implemented)

    scene = new CustomScene(this, bufferSizeInput);
    connect(scene,SIGNAL(emptySpaceClicked()),this,SLOT(spaceClicked()));
    //connect(dispWin,SIGNAL(resolutionChanged()),this,SLOT(calculateConsideredPixels()));
    //scene->addWidget(dispWin,Qt::Widget);
    this->setScene(scene);

    currentlyDrawing = false;
    currentlySelectedPolygon = -1;


    QPen markerPen;
    markerPen.setWidth(1);
    markerPen.setColor(Qt::green);

    setMouseTracking(true);
    setMultipleProjections(false);

}



void ScatterPlotWindow::setDisplayedNTrode(int nTrodeInd, QVector<int> minDisplay, QVector<int> maxDisplay, QList<ScatterPlotMipmap*> mmPtrs) {


    for (int i=0; i<polygons[currentNTrode].length(); i++) {
       polygons[currentNTrode][i]->setVisible(false);
    }



    currentNTrode = nTrodeInd;
    setMipMapPtrs(mmPtrs);
    setDisplayWindow(minDisplay, maxDisplay);


    if (!displayMultipleProjections) {


        for (int i=0; i<polygons[nTrodeInd].length(); i++) {
            polygons[nTrodeInd][i]->setVisible(true);
        }

    }

    //updateBackgroundPlot();
    //scene->update();


}

void ScatterPlotWindow::setMipMapPtrs(QList<ScatterPlotMipmap *> mmPtrs) {
    mipMaps = mmPtrs;
}


void ScatterPlotWindow::setData(const QVector<int> *inputData, const QVector<quint8> &clustID) {

    for (int i=0;i < mipMaps.length(); i++) {
        mipMaps[i]->setPoints(0,bufferSize,inputData, clustID);
        //mipMaps_lowRange[i]->setPoints(0,bufferSize,inputData);
    }

}

void ScatterPlotWindow::setMultipleProjections(bool multiple) {
    displayMultipleProjections = multiple;

    if (displayMultipleProjections) {
        for (int i=0; i<polygons[currentNTrode].length(); i++) {
           polygons[currentNTrode][i]->setVisible(false);
        }
    } else {
        //Only show the polygons for the current axes
        RubberBandPolygon::Axes ax;
        for (int i=0; i<polygons[currentNTrode].length(); i++) {
            ax = polygons[currentNTrode][i]->getAxes();
            if ((ax.x == currentXdisplay) && (ax.y == currentYdisplay)) {
                polygons[currentNTrode][i]->setVisible(true);
            } else {
                polygons[currentNTrode][i]->setVisible(false);
            }
        }


    }

    scene->update();

}
void ScatterPlotWindow::updateBackgroundPlot() {



    if (displayMultipleProjections) {

        currentBackgroundPic = QImage(width(),height(),QImage::Format_RGB32);

        currentBackgroundPic.fill(qRgb(50,50,50));
        QPainter paint;
        paint.begin(&currentBackgroundPic);
        paint.setViewTransformEnabled(true);

        int numCol;
        int numRows;
        QPen pn;
        pn.setColor(qRgb(100,100,100));
        pn.setWidth(1);


        numCol = floor(sqrt(mipMaps.length()));
        numRows = ceil(mipMaps.length()/(double)numCol);

        int tmpMipmapIndex = 0;
        QRect currentDrawWindow;

        currentDrawWindow.setWidth(currentBackgroundPic.width()/numCol);
        currentDrawWindow.setHeight(currentBackgroundPic.height()/numRows);

        int panelWidth = currentBackgroundPic.width()/numCol;
        int panelHeight = currentBackgroundPic.height()/numRows;

        int marginY = (height()-(panelHeight*numRows))/2;
        int marginX = (width()-(panelWidth*numCol))/2;


        //Draw the scatter images
        for (int i=0; i<numRows; i++) {
            for (int j=0; j<numCol; j++) {

                if (tmpMipmapIndex < mipMaps.length()) {
                    QImage tempImage = mipMaps[tmpMipmapIndex]->getCurrentImage(currentDrawWindow);
                    paint.drawImage((j*panelWidth)+marginX,(i*panelHeight)+marginY,tempImage);
                    tmpMipmapIndex++;
                }
            }
        }

        //Draw the image borders
        paint.setPen(pn);
        for (int i=0; i<numCol; i++) {
            for (int j=0; j<numRows; j++) {              
                paint.drawRect((i*panelWidth)+marginX,(j*panelHeight)+marginY,panelWidth,panelHeight);
            }
        }

        //Hightlight the panel that the mouse is over
        if (currentMouseXPanel > -1) {
            pn.setColor(qRgb(150,20,20));
            pn.setWidth(2);
            paint.setPen(pn);
            paint.drawRect((currentMouseXPanel*panelWidth)+marginX,(currentMouseYPanel*panelHeight)+marginY,panelWidth,panelHeight);
        }

        paint.end();

    } else {
        //Just plot one panel

        currentBackgroundPic = mipMaps[currentMipmapIndex]->getCurrentImage();

    }
    scene->setBackgroundPic(&currentBackgroundPic);




}

void ScatterPlotWindow::setDataPoint(const QVector<int> *inputData, int currentBufferIndex, int clustID) {

    //if (displayMultipleProjections) {
        for (int i=0; i<mipMaps.length(); i++) {
            mipMaps[i]->addPoint((qreal)inputData[mipMaps[i]->axes.first][currentBufferIndex],(qreal)inputData[mipMaps[i]->axes.second][currentBufferIndex],clustID);
        }

        //currentBackgroundPic = mipMaps[currentMipmapIndex]->getCurrentImage();
        //scene->setBackgroundPic(&currentBackgroundPic);
    //} else {


        scene->setDataPoint(inputData, currentBufferIndex);
    //}
    //displayData[currentBufferIndex].x = inputData[currentXdisplay][currentBufferIndex];
    //displayData[currentBufferIndex].y = inputData[currentYdisplay][currentBufferIndex];

}

void ScatterPlotWindow::clearData() {
    for (int i=0; i<mipMaps.length(); i++) {
        mipMaps[i]->clearData();
    }

    updateBackgroundPlot();
}

void ScatterPlotWindow::setChannels(int channelX, int channelY, const QVector<int> *inputData, const QVector<quint8> &clustID) {
    currentXdisplay = channelX;
    currentYdisplay = channelY;

    if (!displayMultipleProjections) {
        for (int i=0; i<mipMaps.length(); i++) {
            if ((mipMaps[i]->axes.first == currentXdisplay) && (mipMaps[i]->axes.second == currentYdisplay)) {
                currentMipmapIndex = i;
                //mipMaps[i]->setPoints(0,bufferSize,inputData, clustID);
                break;
            }
        }


        currentBackgroundPic = mipMaps[currentMipmapIndex]->getCurrentImage();
        scene->setBackgroundPic(&currentBackgroundPic);
    }

    //For older opengl plotting
    //scene->setChannels(channelX, channelY, inputData);

    //Only show the polygons for the current axes
    if (!displayMultipleProjections) {
        RubberBandPolygon::Axes ax;
        for (int i=0; i<polygons[currentNTrode].length(); i++) {
            ax = polygons[currentNTrode][i]->getAxes();
            if ((ax.x == currentXdisplay) && (ax.y == currentYdisplay)) {
                polygons[currentNTrode][i]->setVisible(true);
            } else {
                polygons[currentNTrode][i]->setVisible(false);
            }
        }
    }

    scene->update();
}

void ScatterPlotWindow::setDisplayWindow(QVector<int> minDisplay, QVector<int> maxDisplay) {

    maxAmplitude.resize(maxDisplay.length());
    minAmplitude.resize(minDisplay.length());
    for (int i=0; i<maxAmplitude.length(); i++) {
        maxAmplitude[i] = (maxDisplay[i]*65536)/AD_CONVERSION_FACTOR;
        minAmplitude[i] = (minDisplay[i]*65536)/AD_CONVERSION_FACTOR;
    }

    int panelWidth = width();
    int panelHeight = height();
    if (displayMultipleProjections) {
        int numCol, numRows;


        numCol = floor(sqrt(mipMaps.length()));
        /*
        if (mipMaps.length() <= 6) {
            numCol = 2;
        } else {
            numCol = 3;
        }*/
        numRows = ceil(mipMaps.length()/(double)numCol);
        panelWidth = currentBackgroundPic.width()/numCol;
        panelHeight = currentBackgroundPic.height()/numRows;

    }

    for (int i=0; i<mipMaps.length();i++) {

        int tmpX = mipMaps[i]->axes.first;
        int tmpY = mipMaps[i]->axes.second;
        QRectF dWin = QRectF((qreal)minAmplitude[tmpX],(qreal)minAmplitude[tmpY],(qreal)maxAmplitude[tmpX],(qreal)maxAmplitude[tmpY]);

        mipMaps[i]->setWindows(dWin,QRect(0,0,panelWidth,panelHeight));

    }

    //updateBackgroundPlot();

    //currentBackgroundPic = mipMaps[currentMipmapIndex]->getCurrentImage();
    //scene->setBackgroundPic(&currentBackgroundPic);


    //For older opengl plotting
    //scene->setMaxDisplay(maxDisplayVal);


    for (int i=0; i<polygons[currentNTrode].length(); i++) {
        RubberBandPolygon::Axes ax = polygons[currentNTrode][i]->getAxes();
        polygons[currentNTrode][i]->setCurrentAmpWindow(QRectF((qreal)minAmplitude[ax.x],(qreal)minAmplitude[ax.y],(qreal)maxAmplitude[ax.x],(qreal)maxAmplitude[ax.y]));

    }

}

void ScatterPlotWindow::setMaxDisplay(int channel, int maxDisplayVal) {
    //Changes the maximum amplitude of the display range
    //maxAmplitude[channel] = (maxDisplayVal*65536)/AD_CONVERSION_FACTOR;


    setMaxDisplay(maxDisplayVal);

    //scene->setMaxDisplay(channel,maxDisplayVal);



}

void ScatterPlotWindow::setMaxDisplay(int maxDisplayVal) {




        for (int i=0; i<maxAmplitude.length(); i++) {
            maxAmplitude[i] = (maxDisplayVal*65536)/AD_CONVERSION_FACTOR;
        }

        maxDispVal_uV = maxDisplayVal;



        QRectF dWin = QRectF(0.0,0.0,(qreal)maxAmplitude[0],(qreal)maxAmplitude[0]);

        for (int i=0; i<mipMaps.length();i++) {
            mipMaps[i]->setWindows(dWin,QRect(0,0,width(),height()));
        }

        updateBackgroundPlot();

        //currentBackgroundPic = mipMaps[currentMipmapIndex]->getCurrentImage();
        //scene->setBackgroundPic(&currentBackgroundPic);


        //For older opengl plotting
        //scene->setMaxDisplay(maxDisplayVal);

        for (int i=0;i<polygons[currentNTrode].length();i++) {
            polygons[currentNTrode][i]->setCurrentAmpWindow(QRectF(0.0,0.0,(qreal)maxAmplitude[currentXdisplay],(qreal)maxAmplitude[currentYdisplay]));
        }


}


void ScatterPlotWindow::addExcludePolygon() {
    RubberBandPolygon *newPoly = new RubberBandPolygon();
    newPoly->setExcludeType();

    connect(newPoly,SIGNAL(hasHighlight()),this,SLOT(polygonHighlighted()));
    connect(newPoly,SIGNAL(shapeChanged(int,int)),this,SLOT(calculatePointsInsidePolygon(int,int)));

    polygons[currentNTrode].append(newPoly);
    scene->addItem(newPoly);
}

void ScatterPlotWindow::addZonePolygon() {
    RubberBandPolygon *newPoly = new RubberBandPolygon();
    newPoly->setZoneType();

    connect(newPoly,SIGNAL(hasHighlight()),this,SLOT(polygonHighlighted()));

    polygons[currentNTrode].append(newPoly);
    scene->addItem(newPoly);
}

void ScatterPlotWindow::addIncludePolygon() {
    int polyBitInd;
    bool foundEmptySlot = false;

    for (int i=0; i<polygonsTaken[currentNTrode].length(); i++) {
        if (!polygonsTaken[currentNTrode][i]) {
            polyBitInd = i;
            polygonsTaken[currentNTrode][i] = true;
            foundEmptySlot = true;
        }
    }

    if (!foundEmptySlot) {
        polygonsTaken[currentNTrode].append(true);
        polyBitInd = polygonsTaken[currentNTrode].length()-1;
    }

    RubberBandPolygon *newPoly = new RubberBandPolygon();
    newPoly->setIncludeType();
    newPoly->setPolyBitInd(polyBitInd);
    newPoly->setAxes(currentXdisplay,currentYdisplay);

    connect(newPoly,SIGNAL(hasHighlight()),this,SLOT(polygonHighlighted()));
    connect(newPoly,SIGNAL(shapeChanged(int,int)),this,SLOT(calculatePointsInsidePolygon(int,int)));
    connect(newPoly,SIGNAL(rightClicked(QPoint)),this,SLOT(polygonRightClicked(const QPoint&)));
    polygons[currentNTrode].append(newPoly);

    polygons[currentNTrode].last()->setCurrentAmpWindow(QRectF(0.0,0.0,(qreal)maxAmplitude[currentXdisplay],(qreal)maxAmplitude[currentYdisplay]));
    polygons[currentNTrode].last()->setColor(QColor(0,255,0));
    scene->addItem(newPoly);


}

void ScatterPlotWindow::spaceClicked() {
    //the background was clicked, so turn off all highlights
    setNoHighlight();
}

void ScatterPlotWindow::setNoHighlight() {
    for (int i=0; i<polygons[currentNTrode].length(); i++) {
        polygons[currentNTrode][i]->removeHighlight();
    }
    currentlySelectedPolygon = -1;
}

void ScatterPlotWindow::polygonHighlighted() {
    RubberBandPolygon* highlightedPoly = dynamic_cast<RubberBandPolygon*>(sender());

    for (int i=0; i<polygons[currentNTrode].length(); i++) {
        if (polygons[currentNTrode][i] == highlightedPoly) {
            currentlySelectedPolygon = i;
        } else {
            polygons[currentNTrode][i]->removeHighlight();
        }
    }
}

void ScatterPlotWindow::polygonRightClicked(const QPoint& pos) {


    RubberBandPolygon* clickedPoly = dynamic_cast<RubberBandPolygon*>(sender());
    int polygonNumberClicked = -1;
    for (int i=0; i<polygons[currentNTrode].length(); i++) {
        if (polygons[currentNTrode][i] == clickedPoly) {
            polygonNumberClicked = i;
            break;
        }
    }

    if (polygonNumberClicked > -1) {
        showPolygonContextMenu(pos,currentNTrode,polygonNumberClicked);
    }
}

void ScatterPlotWindow::showPolygonContextMenu(const QPoint& pos, int nTrode, int polygonNumber) {

    QPoint globalPos = this->mapToGlobal(pos);
    QMenu channelMenu;
    //channelMenu.addAction("nTrode settings...");

    channelMenu.addAction("Create PSTH...");
    //channelMenu.addAction("Change background color...");
    QAction* selectedItem = channelMenu.exec(globalPos);
    if (selectedItem) {
       // something was chosen, do stuff
       if (selectedItem->text() == "Create PSTH...") {
            //Open PSTH dialog
           emit PSTHRequested(polygons[nTrode][polygonNumber]->getClusterNum());

       } else if (selectedItem->text() == "Change nTrode color...") {
           //showNtrodeColorSelector(channel);
       }  else if (selectedItem->text() == "Change background color...") {
           //showBackgroundColorSelector();
       }

    } else {
      // nothing was chosen
    }
}

void ScatterPlotWindow::setTool(int toolNum) {
    currentTool = toolNum;
    if (currentTool != EDITTOOL_ID) {
        //We only allow highlighting when the edit tool is active
        setNoHighlight();
    }
}

void ScatterPlotWindow::setClusterColors(const QList<QColor> colors) {
    clusterColors = colors;
    /*
    for (int i = 0; i < mipMaps.length(); i++) {
        mipMaps[i]->setColorTable(colors);
    }*/
}

void ScatterPlotWindow::setCurrentCluster(int clust) {
    currentCluster = clust;
}

void ScatterPlotWindow::calculatePointsInsidePolygon(int clustInd, int polygonBitIndex) {

    ClusterPolygonSet polyInfo;
    for (int i=0; i<polygons[currentNTrode].length(); i++) {
        if (polygons[currentNTrode][i]->getPolyBitInd() == polygonBitIndex) {
            //This polygon has the right bit index, so it's a match
            polyInfo.xAxis = polygons[currentNTrode][i]->getAxes().x;
            polyInfo.yAxis = polygons[currentNTrode][i]->getAxes().y;
            polyInfo.points = polygons[currentNTrode][i]->getPoints();
            polyInfo.bitInd = polygonBitIndex;
            polyInfo.clusterInd = clustInd;
            break;
        }
    }

    emit lockSpikeSorting();
    emit newClusterCalculationNeeded(clustInd, polyInfo, polygonBitIndex );
    emit unlockSpikeSorting();

}

QList<ClusterPolygonSet> ScatterPlotWindow::getPolygonsForCluster(int clusterInd) {
    QList<ClusterPolygonSet> polyInfo;
    for (int i=0; i<polygons[currentNTrode].length(); i++) {
        if (polygons[currentNTrode][i]->getClusterNum() == clusterInd) {
            //This polygon belongs to the desired cluster
            ClusterPolygonSet newPoly;
            newPoly.xAxis = polygons[currentNTrode][i]->getAxes().x;
            newPoly.yAxis = polygons[currentNTrode][i]->getAxes().y;
            newPoly.points = polygons[currentNTrode][i]->getPoints();
            newPoly.bitInd = polygons[currentNTrode][i]->getPolyBitInd();
            polyInfo.append(newPoly);
        }
    }
    return polyInfo;
}


void ScatterPlotWindow::mousePressEvent(QMouseEvent *event) {
    //When the window is clicked, the behavior depends on
    //which tool is currently selected.

    if (event->button() == Qt::LeftButton) {
        if (currentlyDrawing) {
            polygons[currentNTrode].last()->addPoint(event->localPos());
            return;
        }

        if (!displayMultipleProjections) {
            if (currentTool == INCLUDETOOL_ID) {

                for (int i = 0; i < polygons[currentNTrode].length(); i++) {

                    if ((polygons[currentNTrode][i]->getClusterNum() == currentCluster) && (polygons[currentNTrode][i]->getAxes().x == currentXdisplay) && (polygons[currentNTrode][i]->getAxes().y == currentYdisplay)){
                        //There is already a polygon for this cluster on this projection
                        ClusterPolygonSet p;
                        p.bitInd = polygons[currentNTrode][i]->getPolyBitInd();
                        p.xAxis = polygons[currentNTrode][i]->getAxes().x;
                        p.yAxis = polygons[currentNTrode][i]->getAxes().y;
                        p.points = polygons[currentNTrode][i]->getPoints();

                        polygonsTaken[currentNTrode][polygons[currentNTrode][i]->getPolyBitInd()] = false;
                        delete polygons[currentNTrode].takeAt(i);
                        //setNoHighlight();
                        emit lockSpikeSorting();
                        emit polygonDeleted(currentCluster,p.bitInd);
                        emit unlockSpikeSorting();
                        break;
                    }
                }

                addIncludePolygon();

                polygons[currentNTrode].last()->setClusterNum(currentCluster);
                polygons[currentNTrode].last()->setColor(clusterColors[currentCluster]);



                currentlyDrawing = true;
                polygons[currentNTrode].last()->addPoint(event->localPos());
                polygons[currentNTrode].last()->addPoint(event->localPos());


            } else if ((currentTool == EDITTOOL_ID)&&(itemAt(event->pos()))) {
                // Clicked on polygon, pass on event to children
                QGraphicsView::mousePressEvent(event);
            } else if (currentTool == PANTOOL_ID) {
                //used to manually designate animal location
            }
        } else {
            if (currentMouseXPanel > -1) {
                emit mouseOverChannels(-1,-1);
                emit channelPairSelected(currentPairIndex);
            }
        }
    } else {
        QGraphicsView::mousePressEvent(event);
    }

}

void ScatterPlotWindow::mouseMoveEvent(QMouseEvent *event) {

    if (currentlyDrawing) {
        //A poolygon is currently being drawn, so we update the shape of the polygon
        //to track the mouse's location.

        polygons[currentNTrode].last()->moveLastPoint(event->localPos());
    } else if (displayMultipleProjections) {
        //If all projection pairs are displayed, the user can click
        //on a panel to select it for single-view mode. Here we calculate
        //which panel the mouse is over.

        int numCol;
        int numRows;

        numCol = floor(sqrt(mipMaps.length()));
        numRows = ceil(mipMaps.length()/(double)numCol);

        int panelWidth = currentBackgroundPic.width()/numCol;
        int panelHeight = currentBackgroundPic.height()/numRows;

        int marginY = (height()-(panelHeight*numRows))/2;
        int marginX = (width()-(panelWidth*numCol))/2;



        currentMouseXPanel = (event->localPos().x()-marginX)/panelWidth;
        currentMouseYPanel = (event->localPos().y()-marginY)/panelHeight;

        if (((currentMouseYPanel*numCol)+currentMouseXPanel >= mipMaps.length()) || ((currentMouseYPanel*numCol)+currentMouseXPanel < 0)) {
            currentMouseXPanel = -1;
            currentMouseYPanel = -1;
            currentMouseChannels.first = -1;
            currentMouseChannels.second = -1;
            currentPairIndex = -1;
        } else {

            currentMouseChannels.first = mipMaps[(currentMouseYPanel*numCol)+currentMouseXPanel]->axes.first;
            currentMouseChannels.second = mipMaps[(currentMouseYPanel*numCol)+currentMouseXPanel]->axes.second;
            currentPairIndex = (currentMouseYPanel*numCol)+currentMouseXPanel;
        }

        emit mouseOverChannels(currentMouseChannels.first, currentMouseChannels.second);

    } else {
        QGraphicsView::mouseMoveEvent(event);
    }
}

void ScatterPlotWindow::leaveEvent(QEvent *event) {
    currentMouseXPanel = -1;
    currentMouseYPanel = -1;
    emit mouseOverChannels(-1,-1);
}

void ScatterPlotWindow::mouseDoubleClickEvent(QMouseEvent *event) {
    if (currentlyDrawing) {
        currentlyDrawing = false;
        polygons[currentNTrode].last()->removeLastPoint();
        //calculateConsideredPixels();
        calculatePointsInsidePolygon(currentCluster, polygons[currentNTrode].last()->getPolyBitInd());

    }
}

void ScatterPlotWindow::wheelEvent(QWheelEvent *wheelEvent) {
    qreal ypos = wheelEvent->pos().y();

    int changeAmount;
    if (wheelEvent->delta() > 0) {
        changeAmount = -25;
    } else {
        changeAmount = 25;
    }

    emit zoom(changeAmount);

}

void ScatterPlotWindow::keyPressEvent(QKeyEvent *event) {

    if ((event->key() == Qt::Key_Delete)||(event->key() == Qt::Key_Backspace)) {
        //delete the selected polygon
        if (currentlySelectedPolygon != -1) {

            int selectedClusterInd = polygons[currentNTrode][currentlySelectedPolygon]->getClusterNum();
            int selectedPolyBitIndex = polygons[currentNTrode][currentlySelectedPolygon]->getPolyBitInd();
            ClusterPolygonSet p;
            p.bitInd = polygons[currentNTrode][currentlySelectedPolygon]->getPolyBitInd();
            p.xAxis = polygons[currentNTrode][currentlySelectedPolygon]->getAxes().x;
            p.yAxis = polygons[currentNTrode][currentlySelectedPolygon]->getAxes().y;
            p.points = polygons[currentNTrode][currentlySelectedPolygon]->getPoints();
            polygonsTaken[currentNTrode][polygons[currentNTrode][currentlySelectedPolygon]->getPolyBitInd()] = false;
            delete polygons[currentNTrode].takeAt(currentlySelectedPolygon);
            setNoHighlight();
            //calculateConsideredPixels();
            //calculatePointsInsidePolygon(selectedClusterInd,selectedPolyBitIndex);

            emit polygonDeleted(selectedClusterInd,selectedPolyBitIndex);

        }
    }
}

void ScatterPlotWindow::refresh() {
    for (int i=0; i<mipMaps.length();i++) {
        mipMaps[i]->setDestWindow(QRect(0,0,this->width(),this->height()));
    }

    scene->setSceneRect(0,0,this->width(),this->height());

    for (int i=0; i<polygons.length(); i++) {
        polygons[currentNTrode][i]->updateSize();
    }

    updateBackgroundPlot();
    //currentBackgroundPic = mipM
}

void ScatterPlotWindow::resizeEvent(QResizeEvent *event) {


    //dispWin->resize(event->size().width(),event->size().height());
    int panelWidth = event->size().width();
    int panelHeight = event->size().height();
    if (displayMultipleProjections) {
        int numCol, numRows;
        numCol = floor(sqrt(mipMaps.length()));

        numRows = ceil(mipMaps.length()/(double)numCol);
        panelWidth = event->size().width()/numCol;
        panelHeight = event->size().height()/numRows;

    }

    for (int i=0; i<mipMaps.length();i++) {
        mipMaps[i]->setDestWindow(QRect(0,0,panelWidth,panelHeight));
    }

    scene->setSceneRect(0,0,event->size().width(),event->size().height());



    for (int i=0; i<polygons[currentNTrode].length(); i++) {

        polygons[currentNTrode][i]->updateSize();
    }

    updateBackgroundPlot();

    //currentBackgroundPic = mipMaps[currentMipmapIndex]->getCurrentImage();
    //scene->setBackgroundPic(&currentBackgroundPic);




}










//NtrodeDisplayWidget contains all plot and control objects for one nTrode.
NtrodeDisplayData::NtrodeDisplayData(QObject *parent, int nTrodeID) :
  QObject(parent)
{



    nTrodeNumber = nTrodeID;
    currentBufferIndex = -1;
    currentXdisplay = 0;
    currentYdisplay = 1;
    currentChannelListIndex = 0;
    writeBlock = 0;
    beginningOfLog = 0;

    for (int i=0; i<NUMCLUSTERS; i++) {
        clustersDefined[i] = false;
    }

    numberOfChannels = spikeConf->ntrodes[nTrodeID]->hw_chan.length();

    //For very large nTrodes, we need to limit the resolution of the mipmaps
    int mipMapRes;
    if (numberOfChannels <= 4) {
        mipMapRes = 5000;
    } else if ((numberOfChannels > 4) && (numberOfChannels <= 8))
        mipMapRes = 2500;
    else {
        mipMapRes = 1000;
    }

    scatterBufferSize = 100000;

    if (numberOfChannels > 1) {
        scatterData.resize(numberOfChannels);
    } else {

        scatterData.resize(2);
    }

    for (int i = 0; i < scatterData.size() ; i++) {
        scatterData[i].resize(scatterBufferSize);
    }

    pointClusterOwnership.resize(scatterBufferSize);
    pointClusterOwnership.fill(0);

    spikeTimes.resize(scatterBufferSize);
    spikeTimes.fill(0);

    if (numberOfChannels > 1) {
        maxAmplitude.resize(numberOfChannels);
        minAmplitude.resize(numberOfChannels);
        for (int i = 0; i < numberOfChannels; i++) {
            maxAmplitude[i] = spikeConf->ntrodes[nTrodeID]->maxDisp[i];
            minAmplitude[i] = 0;
            for (int j = i+1; j < numberOfChannels; j++) {
                mipMaps.push_back(new ScatterPlotMipmap(QRectF(0.0,0.0,(2500.0*65536)/AD_CONVERSION_FACTOR,(2500.0*65536)/AD_CONVERSION_FACTOR),mipMapRes,mipMapRes));
                mipMaps.last()->axes = qMakePair(i,j);

                //mipMaps_lowRange.push_back(new ScatterPlotMipmap(QRectF(0.0,0.0,(lowRangeMipmapMax*65536)/AD_CONVERSION_FACTOR,(lowRangeMipmapMax*65536)/AD_CONVERSION_FACTOR),5000,5000));
                //mipMaps_lowRange.last()->axes = qMakePair(i,j);
            }
        }
    } else {
        maxAmplitude.resize(2);
        minAmplitude.resize(2);
        for (int i = 0; i < 2; i++) {
            maxAmplitude[i] = spikeConf->ntrodes[nTrodeID]->maxDisp[0];
            minAmplitude[i] = 0;
            for (int j = i+1; j < 2; j++) {
                mipMaps.push_back(new ScatterPlotMipmap(QRectF(0.0,0.0,(2500.0*65536)/AD_CONVERSION_FACTOR,(2500.0*65536)/AD_CONVERSION_FACTOR),mipMapRes,mipMapRes));
                mipMaps.last()->axes = qMakePair(i,j);

                //mipMaps_lowRange.push_back(new ScatterPlotMipmap(QRectF(0.0,0.0,(lowRangeMipmapMax*65536)/AD_CONVERSION_FACTOR,(lowRangeMipmapMax*65536)/AD_CONVERSION_FACTOR),5000,5000));
                //mipMaps_lowRange.last()->axes = qMakePair(i,j);
            }
        }
    }

    waveForms.resize(numberOfChannels);
    for (int i = 0; i < numberOfChannels; i++) {
        waveForms[i].resize(POINTSINWAVEFORM);
        for (int j = 0; j < POINTSINWAVEFORM; j++) {
            waveForms[i][j].x = j;
            waveForms[i][j].y = 0;
        }
    }

    QString tmpChannelCombo;
    if (numberOfChannels > 1) {
        for (int i = 0; i < numberOfChannels; i++) {

            for (int j = i+1; j < numberOfChannels; j++) {
                channelViewXList.push_back(i);
                channelViewYList.push_back(j);
                tmpChannelCombo = QString("Channels %1").arg(i+1) + QString(", ") + QString("%1").arg(j+1);
                channelViewStrings << tmpChannelCombo;
            }

        }
    } else {
        channelViewXList.push_back(0);
        channelViewYList.push_back(1);
        tmpChannelCombo = QString("Ch: 1 Max, 1 Min");
        channelViewStrings << tmpChannelCombo;

    }

}

NtrodeDisplayData::~NtrodeDisplayData() {

    for (int i=0; i<polyIncludeRecord.length();i++) {
        delete[] polyIncludeRecord[i];
        delete[] polyExcludeRecord[i];
    }

    while (mipMaps.length() > 0) {
        mipMaps.last()->deleteImages();
        delete mipMaps.takeLast();
    }

}

void NtrodeDisplayData::setDataShowing() {

        for (int i=0;i < mipMaps.length(); i++) {
            mipMaps[i]->createImages();
        }
        setMipMapData();
}

void NtrodeDisplayData::setDataNotShowing() {
    for (int i=0;i < mipMaps.length(); i++) {
        mipMaps[i]->deleteImages();
        //mipMaps_lowRange[i]->deleteImages();
    }
}

void NtrodeDisplayData::setMipMapData() {

    //We need use the current buffer with spike history to create the scatterplot
    //mipmaps.  The problem is that making all the mipmaps takes time and spikes are still coming in to
    //the buffer from other threads.  So, we quickly make a copy of the buffer (while locking
    //the threads that send spike data in) and use the copy instead.

    QVector<quint8>         pointClusterOwnershipCopy; //Each point can belong to one of 256 clusters
    pointClusterOwnershipCopy.resize(scatterBufferSize);

    QVector<QVector<int> >  scatterDataCopy;
    scatterDataCopy.resize(scatterData.length());

    for (int i = 0; i < scatterData.size() ; i++) {
        scatterDataCopy[i].resize(scatterBufferSize);
    }

    //Here we lock the threads that adds data to the buffer.  We want this to be as brief as possible
    newSpikeMutex.lock();
    for (int i = 0; i < scatterData.size() ; i++) {
        memcpy(scatterDataCopy[i].data(), scatterData[i].constData(), scatterBufferSize*sizeof(int));
    }
    memcpy(pointClusterOwnershipCopy.data(),pointClusterOwnership.constData(),scatterBufferSize*sizeof(quint8));
    newSpikeMutex.unlock();


    for (int i=0;i < mipMaps.length(); i++) {
        mipMaps[i]->setPoints(0,scatterBufferSize,scatterDataCopy.constData(), pointClusterOwnershipCopy);
        //mipMaps_lowRange[i]->setPoints(0,bufferSize,inputData);
    }

}

void NtrodeDisplayData::setZeroToMax(int newMaxAmplitude) {
    for (int i=0; i<maxAmplitude.length();i++) {
        maxAmplitude[i] = newMaxAmplitude;
        minAmplitude[i] = 0;
    }


}

void NtrodeDisplayData::setColorTable(const QList<QColor> &colors) {
    for (int i=0;i<mipMaps.length();i++) {
        mipMaps[i]->setColorTable(colors);
    }
}

QList<ScatterPlotMipmap*> NtrodeDisplayData::getMipMapPtrs() {
    QList<ScatterPlotMipmap*> list;
    for (int i = 0; i<mipMaps.length();i++) {
        list.append(mipMaps[i]);
    }
    return list;
}

QList<ClusterPolygonSet> NtrodeDisplayData::getPolygonsForCluster(int clusterInd) {
    QList<ClusterPolygonSet> polyInfo;
    for (int i=0; i<polygonData.length(); i++) {
        if (polygonData[i].clusterInd == clusterInd) {
            //This polygon belongs to the desired cluster
            ClusterPolygonSet newPoly;
            newPoly.xAxis = polygonData[i].xAxis;
            newPoly.yAxis = polygonData[i].yAxis;
            newPoly.points = polygonData[i].points;
            newPoly.bitInd = polygonData[i].bitInd;
            newPoly.clusterInd = polygonData[i].clusterInd;
            polyInfo.append(newPoly);
        }
    }
    return polyInfo;
}

ClusterPolygonSet NtrodeDisplayData::getPolygonForBitInd(int polyBitIndex) {
    int index = -1;
    for (int i=0; i<polygonData.length(); i++) {
        if (polygonData[i].bitInd == polyBitIndex) {
            index = i;
            break;
        }
    }
    if (index > -1) {
        return polygonData[index];
    }   else {
        ClusterPolygonSet p;
        return p;
    }
}

void NtrodeDisplayData::addPolygonData(ClusterPolygonSet p) {
    bool replacedEntry = false;
    //Check if an entry for this bitind already exists, if so replace it
    for (int i=0; i<polygonData.length(); i++) {
        if (polygonData[i].bitInd == p.bitInd) {
            polygonData[i].xAxis = p.xAxis;
            polygonData[i].yAxis = p.yAxis;
            polygonData[i].points = p.points;
            polygonData[i].bitInd = p.bitInd;
            polygonData[i].clusterInd = p.clusterInd;
            replacedEntry = true;
            break;
        }
    }
    if (!replacedEntry) {
        polygonData.append(p);
    }
}


void NtrodeDisplayData::deletePolygonData(int clusterInd, int bitInd) {

    bitClusterOwnership[bitInd] = -1; //keep a record of which cluster the polygon belongs to
    for (int i=0; i<polygonData.length(); i++) {
        if (polygonData[i].bitInd == bitInd) {
            polygonData.takeAt(i);
            break;
        }
    }
    calculateCluster(clusterInd);

    //Replot the mipmaps
    //graphicsWin->setData(scatterData.constData(), pointClusterOwnership);


}

uint32_t NtrodeDisplayData::getStartOfLogTime() {
    //Returns the start of the time range for the saved spikes
    return beginningOfLog;
}

QVector<uint32_t> NtrodeDisplayData::getSpikeTimesForCluster(int clusterInd) {
    QVector<uint32_t> outTimes;
    if (clustersDefined[clusterInd]) {
        //Here we lock the threads that adds data to the buffer.  We want this to be as brief as possible
        newSpikeMutex.lock();
        for (int i = 0; i < pointClusterOwnership.length(); i++) {
            if (pointClusterOwnership[i] == clusterInd) {
                outTimes.append(spikeTimes[i]);
            }
        }
        newSpikeMutex.unlock();

    }
    return outTimes;

}

void NtrodeDisplayData::calculatePointsInsidePolygon(int clusterInd, int bitInd) {
    //qDebug() << "Calculating points inside cluster" ;
    ClusterPolygonSet polySet = getPolygonForBitInd(bitInd);

    if (!clustersDefined[clusterInd]) {
        toggleClusterOn(clusterInd,true);
    }

    if ( (floor(bitInd/8)+1) > polyIncludeRecord.length() ) {
        //We need to increase the size of the include and exclude records
        polyIncludeRecord.append(new quint8[scatterBufferSize]);
        polyExcludeRecord.append(new quint8[scatterBufferSize]);
        for (int i=0;i<8;i++) {
            bitClusterOwnership.append(-1);
        }
    }

    int recordVectorIndex = floor(bitInd/8);  //The index into polyIncludeRecord
    int subIndex = bitInd % 8;  //The bit to use of the 8-bit uint
    bitClusterOwnership[(recordVectorIndex*8)+subIndex] = clusterInd; //keep a record of which cluster the polygon belongs to

    QPolygon amplitudeSpacePolygon(polySet.points);

    QPoint p;

    for (int pointInd = 0; pointInd < scatterBufferSize; pointInd++) {

        p.setX(scatterData[polySet.xAxis][pointInd]);
        p.setY(scatterData[polySet.yAxis][pointInd]);

        //We keep a record of which points are included and exluded by each polygon
        //If all points are in scope for every polygon, these are exact compliments,
        //but not if some polygons anly have scope over a subset of points. Might be overkill
        //but will allow easy growth to full clustering capabilities if needed.

        //A point is considered part of a cluster if it is included at least once and never
        //excluded by any polygons

        if (amplitudeSpacePolygon.containsPoint(p,Qt::OddEvenFill)) {
            //The point is inside the polygon
            polyIncludeRecord[recordVectorIndex][pointInd] |= (1 << subIndex); //turns bit to 1
            polyExcludeRecord[recordVectorIndex][pointInd] &= ~(1 << subIndex); //turns bit to 0

        } else {
            //The point is outside the polygon
            polyExcludeRecord[recordVectorIndex][pointInd] |= (1 << subIndex);
            polyIncludeRecord[recordVectorIndex][pointInd] &= ~(1 << subIndex);
        }
    }

    //Calculate which points now belong to the cluster
    calculateCluster(clusterInd);

    //Replot the mipmaps
    //graphicsWin->setData(scatterData.constData(), pointClusterOwnership);

}

void NtrodeDisplayData::calculateCluster(int clusterInd) {
    //Calculates which points belong to the given cluster
    bool *beenIncluded = new bool[scatterBufferSize];
    bool *beenExcluded = new bool[scatterBufferSize];
    for (int pointInd = 0; pointInd < scatterBufferSize; pointInd++) {
        beenExcluded[pointInd] = false;
        beenIncluded[pointInd] = false;
    }

    bool foundPolygon = false;
    for (int bitInd=0; bitInd< bitClusterOwnership.length(); bitInd++) {
        if (bitClusterOwnership[bitInd] == clusterInd) {
            foundPolygon = true;
            //This bit has a polygon for the designated cluster
            int recordVectorIndex = floor(bitInd/8);  //The index into polyIncludeRecord
            int subIndex = bitInd % 8;  //The bit to use of the 8-bit uint
            for (int pointInd = 0; pointInd < scatterBufferSize; pointInd++) {
                if (polyIncludeRecord[recordVectorIndex][pointInd] & (1 << subIndex)) {
                    beenIncluded[pointInd] = true;
                }

                if (polyExcludeRecord[recordVectorIndex][pointInd] & (1 << subIndex)) {
                    beenExcluded[pointInd] = true;
                }

            }

        }
    }

    if (!foundPolygon) {
        toggleClusterOn(clusterInd,false);
        //clustersDefined[clusterInd] = false;
    }

    //This storage method assumes there will be no overlap in clusters, which may not be the case.
    int numSpikes = 0;
    for (int pointInd = 0; pointInd < scatterBufferSize; pointInd++) {
        if (beenIncluded[pointInd] && !beenExcluded[pointInd]) {
            pointClusterOwnership[pointInd] = (quint8)clusterInd;
            numSpikes++;
        } else if (pointClusterOwnership[pointInd] == clusterInd) {
            //This point no longer belongs in the cluster
            pointClusterOwnership[pointInd] = 0;
        }
    }

    delete[] beenIncluded;
    delete[] beenExcluded;

}

int NtrodeDisplayData::sortDataPoint(const QVector<int> &coord, int pointInd) {
    //Determine which cluster the point belongs to.

    bool beenExluded = false;
    bool beenIncluded = false;
    int bitInd;
    int recordVectorIndex;
    int subIndex;
    int outCluster = 0;
    bool foundCluster = false;

    for (int cl=0;cl<NUMCLUSTERS;cl++) {
        if (clustersDefined[cl]) {
            beenExluded = false;
            beenIncluded = false;
            QList<ClusterPolygonSet> polyInfo = getPolygonsForCluster(cl);
            for (int polyInd=0; polyInd<polyInfo.length();polyInd++) {
               bitInd = polyInfo[polyInd].bitInd;
               recordVectorIndex = floor(bitInd/8);  //The index into polyIncludeRecord
               subIndex = bitInd % 8;  //The bit to use of the 8-bit uint
               QPolygon amplitudeSpacePolygon(polyInfo[polyInd].points);
               QPoint p(coord[polyInfo[polyInd].xAxis],coord[polyInfo[polyInd].yAxis]);
               if (amplitudeSpacePolygon.containsPoint(p,Qt::OddEvenFill)) {
                   //The point is inside the polygon
                   beenIncluded = true;
                   polyIncludeRecord[recordVectorIndex][pointInd] |= (1 << subIndex); //turns bit to 1
                   polyExcludeRecord[recordVectorIndex][pointInd] &= ~(1 << subIndex); //turns bit to 0

               } else {
                   //The point is outside the polygon
                   beenExluded = true;
                   polyExcludeRecord[recordVectorIndex][pointInd] |= (1 << subIndex);
                   polyIncludeRecord[recordVectorIndex][pointInd] &= ~(1 << subIndex);
               }

            }

            if (!(foundCluster) && (beenIncluded) && !(beenExluded)) {
                outCluster = cl;
            }
        }

    }
    //This storage method assumes there will be no overlap in clusters, which may not be the case.
    pointClusterOwnership[pointInd] = (quint8)outCluster;

    return outCluster; //default 0

}

void NtrodeDisplayData::toggleClusterOn(int clusterInd, bool on) {
    clustersDefined[clusterInd] = on;

}


void NtrodeDisplayData::lockSpikeSortMutex() {
    spikeSortMutex.lock();
}

void NtrodeDisplayData::unlockSpikeSortMutex() {
    spikeSortMutex.unlock();
}



void NtrodeDisplayData::clearButtonPressed() {

    for (int i = 0; i < scatterData.size(); i++) {
        scatterData[i].fill(0);
        pointClusterOwnership.fill(0);
    }
    spikeTimes.fill(0);
    beginningOfLog = currentTimeStamp;


    //graphicsWin->clearData();

}

void NtrodeDisplayData::updateScatterChannels(int channelCombo) {

    currentXdisplay = channelViewXList[channelCombo];
    currentYdisplay = channelViewYList[channelCombo];
    //graphicsWin->setChannels(currentXdisplay, currentYdisplay, scatterData.constData(),pointClusterOwnership);

}

void NtrodeDisplayData::receiveNewEvent(const QVector<int2d>* waveForm, const int* peaks, uint32_t time) {

    //spikeSortMutex.lock();

    //waveform contains the waveform data for all channels
    //peaks contains the peak amplitude for all channels, followed by the minimum amplitude for all channels

    //int numChannels = triggerScope->scopeWindows.length();


    newSpikeMutex.lock();
    int numChannels = spikeConf->ntrodes[nTrodeNumber]->hw_chan.length();
    currentBufferIndex = (currentBufferIndex+1) % scatterBufferSize;

    QVector<int> coord;

    if (numChannels > 1) {
        for (int i = 0; i < numChannels; i++) {
            scatterData[i][currentBufferIndex] = peaks[i];
            coord.append(peaks[i]);
            for (int j = 0; j < POINTSINWAVEFORM; j++) {
                waveForms[i][j].y = waveForm[i][j].y;
            }

            //triggerScope->scopeWindows[i]->plotData(waveForm, i);
        }
    } else {
        //there is only one channel, so in order to plot a scatter, we need to display two different parameters
        scatterData[0][currentBufferIndex] = peaks[0]; //peak amplitude
        scatterData[1][currentBufferIndex] = -peaks[1]; //min amplitude
        coord.append(peaks[0]);
        coord.append(-peaks[1]);
        for (int j = 0; j < POINTSINWAVEFORM; j++) {
            waveForms[0][j].y = waveForm[0][j].y;
        }
        //triggerScope->scopeWindows[0]->plotData(waveForm, 0);
    }

    //If the buffer has looped, then the beginning of the log
    //range will be the spike time we are about to replace in memory
    if (spikeTimes[currentBufferIndex] != 0) {
        beginningOfLog = spikeTimes[currentBufferIndex];
    }
    spikeTimes[currentBufferIndex] = time;

    //Sort the spike
    int spikeCluster = sortDataPoint(coord,currentBufferIndex);

    //We now know which cluster the spike belongs to, so we emit the message
    if (spikeCluster > 0) {
        emit spike(nTrodeNumber,spikeCluster,time);
    }
    newSpikeMutex.unlock();

    //graphicsWin->setDataPoint(scatterData.constData(),currentBufferIndex, spikeCluster);

    //spikeSortMutex.unlock();

}









//NtrodeDisplayWidget contains all plot and control objects for one nTrode.
MultiNtrodeDisplayWidget::MultiNtrodeDisplayWidget(QWidget *parent) :
  QWidget(parent)
{


    multiViewMode = false;

    currentNTrode = -1;
    currentHWAudioChannel = 0;
    currentXdisplay = 0;
    currentYdisplay = 1;
    writeBlock = 0;
    changingDisplayedNtrode = false;
    streamManager = NULL;

    //default cluster colors
    clusterColors.push_back(QColor(255,255,255));
    clusterColors.push_back(QColor(255,0,0));
    clusterColors.push_back(QColor(0,255,0));
    clusterColors.push_back(QColor(0,0,255));
    clusterColors.push_back(QColor(200,200,0));
    clusterColors.push_back(QColor(0,255,255));
    clusterColors.push_back(QColor(255,0,255));
    clusterColors.push_back(QColor(135,25,255));
    clusterColors.push_back(QColor(125,90,50));

    clusterColors.push_back(QColor(125,90,50));
    clusterColors.push_back(QColor(125,90,50));
    clusterColors.push_back(QColor(125,90,50));
    clusterColors.push_back(QColor(125,90,50));
    clusterColors.push_back(QColor(125,90,50));
    clusterColors.push_back(QColor(125,90,50));
    clusterColors.push_back(QColor(0,0,0));

    for (int i=0; i < spikeConf->ntrodes.length(); i++) {
        nTrodeData.append(new NtrodeDisplayData(NULL,i));
        nTrodeData.last()->setColorTable(clusterColors);
    }

    panelSplitter = new QSplitter;
    panelSplitter->setOrientation(Qt::Horizontal);
    connect(panelSplitter,SIGNAL(splitterMoved(int,int)),this,SLOT(newSplitterPos(int,int)));

    mainLayout = new QGridLayout(this);
    scopeLayout = new QGridLayout();
    scatterLayout = new QGridLayout();
    scatterControlLayout = new QGridLayout();
    QGridLayout *viewSelectLayout = new QGridLayout();
    viewSelectLayout->setSpacing(1);



    nTrodeLabel = new QLabel(this);
    nTrodeLabel->setAlignment(Qt::AlignCenter);
    QFont labelFont;
    labelFont.setPixelSize(20);
    labelFont.setFamily("Console");
    nTrodeLabel->setFont(labelFont);
    clearButton = new QPushButton(this);
    clearAllButton = new QPushButton(this);
    //clearButton->setFlat(true);
    clearButton->setStyleSheet("QPushButton {border: 2px solid lightgray;"
                          "border-radius: 4px;"
                          "padding: 2px;}"
                          "QPushButton::pressed {border: 2px solid gray;"
                          "border-radius: 4px;"
                          "padding: 2px;}");
    clearAllButton->setStyleSheet("QPushButton {border: 2px solid lightgray;"
                          "border-radius: 4px;"
                          "padding: 2px;}"
                          "QPushButton::pressed {border: 2px solid gray;"
                          "border-radius: 4px;"
                          "padding: 2px;}");
    clearButton->setText("Clear");
    clearButton->setFixedWidth(60);
    connect(clearButton, SIGNAL(released()), this, SLOT(clearButtonPressed()));

    clearAllButton->setText("Clear all");
    clearAllButton->setFixedWidth(70);
    connect(clearAllButton, SIGNAL(released()), this, SLOT(clearAllButtonPressed()));


    //Single/multi scatter plot view selection
    selectSingleView = new TrodesButton(this);
    selectSingleView->setText("Single");
    selectSingleView->setCheckable(false);
    selectSingleView->setDown(true);
    connect(selectSingleView,SIGNAL(pressed()),this,SLOT(singleViewButtonPressed()));
    connect(selectSingleView,SIGNAL(released()),this,SLOT(singleViewButtonReleased()));
    selectMultiView = new TrodesButton(this);
    selectMultiView->setText("Multi");
    selectMultiView->setCheckable(false);
    connect(selectMultiView,SIGNAL(pressed()),this,SLOT(multiViewButtonPressed()));
    connect(selectMultiView,SIGNAL(released()),this,SLOT(multiViewButtonReleased()));



    scatterControlLayout->addWidget(nTrodeLabel,0,0);

    QGridLayout* clearButtonLayout = new QGridLayout();
    clearButtonLayout->setSpacing(1);
    clearButtonLayout->addWidget(clearButton,0,0);
    clearButtonLayout->addWidget(clearAllButton,0,1);
    scatterControlLayout->addLayout(clearButtonLayout,0,1);
    //scatterControlLayout->addWidget(clearButton,0,1);
    //scatterControlLayout->addWidget(clearAllButton,0,2);
    viewSelectLayout->addWidget(selectSingleView,0,0);
    viewSelectLayout->addWidget(selectMultiView,0,1);
    scatterControlLayout->addLayout(viewSelectLayout,1,0);


    channelViewCombo = new QComboBox(this);
    //scatterDisplay = new scatterWindow(this, scatterBufferSize, scatterData.size());

    connect(channelViewCombo,SIGNAL(currentIndexChanged(int)),this,SLOT(updateScatterChannels(int)));

    clusterSelectButtons = new QButtonGroup(this);
    for (int i=0; i < 8; i++) {
        clusterSelectButtons->addButton(new TrodesButton(),i);
        clusterSelectButtons->button(i)->setFixedSize(25,25);
        clusterSelectButtons->button(i)->setProperty("userData",i+1);
        clusterSelectButtons->button(i)->setCheckable(true);

        //Set the text color to the cluster's color
        QPalette pal = clusterSelectButtons->button(i)->palette();
        pal.setBrush(QPalette::ButtonText, clusterColors[i+1]);
        //pal.setBrush(QPalette::Background  , clusterColors[i+1]);
        clusterSelectButtons->button(i)->setAutoFillBackground(true);
        clusterSelectButtons->button(i)->setPalette(pal);
        clusterSelectButtons->button(i)->setText(QString("%1").arg(i+1));
        //clusterSelectButtons->button(i)->setStyleSheet("background-color: #ff0000");

        connect(clusterSelectButtons->button(i),SIGNAL(pressed()),this,SLOT(clusterButtonPressed()));
    }

    clusterSelectButtons->setExclusive(true);
    clusterSelectButtons->button(0)->setChecked(true);
    selectedClusterInd = 1;

    QString path = QApplication::applicationDirPath();
    QPixmap arrowPixmap(path +  "/arrow.png");
    QPixmap handPixmap(path + "/hand.png");
    QPixmap polyPixmap(path + "/polygon.png");
    QIcon includeButtonIcon(polyPixmap);
    QIcon editButtonIcon(arrowPixmap);
    QIcon panButtonIcon(handPixmap);

    toolSelectButtons = new QButtonGroup(this);
    TrodesButton *includeButton = new TrodesButton();
    includeButton->setFixedSize(25,25);
    includeButton->setCheckable(true);
    includeButton->setChecked(true);
    includeButton->setIcon(includeButtonIcon);
    toolSelectButtons->addButton(includeButton,INCLUDETOOL_ID);

    TrodesButton *polyEditButton = new TrodesButton();
    polyEditButton->setFixedSize(25,25);
    polyEditButton->setCheckable(true);
    polyEditButton->setIcon(editButtonIcon);
    toolSelectButtons->addButton(polyEditButton,EDITTOOL_ID);

    TrodesButton *panButton = new TrodesButton();
    panButton->setFixedSize(25,25);
    panButton->setCheckable(true);
    panButton->setIcon(panButtonIcon);
    toolSelectButtons->addButton(panButton,PANTOOL_ID);

    toolSelectButtons->setExclusive(true);

    currentTool = INCLUDETOOL_ID;
    connect(toolSelectButtons,SIGNAL(buttonPressed(int)),SLOT(toolButtonPressed(int)));

    //The layout for the cluster selection buttons
    QGridLayout *clusterButtonLayout = new QGridLayout();
    clusterButtonLayout->setSpacing(1);
    for (int i=0; i < clusterSelectButtons->buttons().length(); i++) {
        clusterButtonLayout->addWidget(clusterSelectButtons->button(i),floor(i/4),i%4);
    }
    scatterControlLayout->addLayout(clusterButtonLayout,1,1);

    QGridLayout *toolButtonLayout = new QGridLayout();
    toolButtonLayout->setSpacing(1);
    toolButtonLayout->addWidget(includeButton,0,0);
    toolButtonLayout->addWidget(polyEditButton,0,1);
    toolButtonLayout->addWidget(panButton,0,2);

    scatterControlLayout->addLayout(toolButtonLayout,2,1);
    scatterControlLayout->addWidget(channelViewCombo,2,0);
    //scatterControlLayout->addWidget(refEdit,2,1);
    scatterControlLayout->setColumnStretch(2,2);

    scatterLayout->addLayout(scatterControlLayout,0,0); //done with the scatter control layout


    //Add the scatter plot to the layout

    graphicsWin = new ScatterPlotWindow(this,200000);
    graphicsWin->setClusterColors(clusterColors);
    connect(graphicsWin,SIGNAL(newClusterCalculationNeeded(int,ClusterPolygonSet, int)),this,SLOT(calculatePointsInsidePolygon(int,ClusterPolygonSet, int)));
    connect(graphicsWin,SIGNAL(polygonDeleted(int,int)),this,SLOT(deletePolygonData(int,int)));
    connect(graphicsWin,SIGNAL(zoom(int)),this,SLOT(zoom(int)));
    connect(graphicsWin,SIGNAL(lockSpikeSorting()),this,SLOT(lockSpikeSortMutex()));
    connect(graphicsWin,SIGNAL(unlockSpikeSorting()),this,SLOT(unlockSpikeSortMutex()));
    connect(graphicsWin,SIGNAL(channelPairSelected(int)),this,SLOT(scatterPairSelected(int)));
    connect(graphicsWin,SIGNAL(PSTHRequested(int)),this,SLOT(openPSTHWindow(int)));
    //graphicsWin->setViewport(graphicsWin);
    scatterLayout->addWidget(graphicsWin,1,0);




    //Everything created so far gets added to the left side of the splitter
    QWidget *leftSide = new QWidget(this);
    leftSide->setLayout(scatterLayout);
    panelSplitter->addWidget(leftSide);



    //On the right side of the splitter we place the spike waveform display and controls
    triggerScope = new TriggerScopeDisplayWidget(this);
    //connect(triggerScope, SIGNAL(newThreshold(int,int,int)), spikeConf, SLOT(setThresh(int,int,int)) );
    //connect(triggerScope, SIGNAL(newThreshold(int,int)), triggerThreads[nTrodeID], SLOT(setThresh(int,int)) );
    //connect(triggerScope, SIGNAL(newMaxDisplaySignal(int,int)),graphicsWin,SLOT(setMaxDisplay(int,int)));
    connect(triggerScope,SIGNAL(zoom(int)),this,SLOT(zoom(int)));
    connect(triggerScope,SIGNAL(setAudioChannel(int)),this,SIGNAL(channelClicked(int)));
    connect(graphicsWin,SIGNAL(mouseOverChannels(int,int)),triggerScope,SLOT(setCurrentDisplayPair(int,int)));
    //triggerScope->setTraceColor(spikeConf->ntrodes[nTrodeID]->color);
    panelSplitter->addWidget(triggerScope);




    mainLayout->addWidget(panelSplitter,0,0);


    setShownNtrode(0);


    scatterPlotTimer = new QTimer(this);
    scopePlotTimer = new QTimer(this);
    connect(scatterPlotTimer, SIGNAL(timeout()), SLOT(scatterPlotTimerExpired()));
    scatterPlotTimer->start(100); //this value dictates how often the scatter plots are plotted (in ms)
    connect(scopePlotTimer, SIGNAL(timeout()), SLOT(scopePlotTimerExpired()));
    scopePlotTimer->start(40); //this value dictates how often the spike waveforms are plotted (in ms)

    //Remembered settings...
    QSettings settings(QLatin1String("SpikeGadgets"), QLatin1String("Trodes"));

    //Place the window where it was last
    settings.beginGroup(QLatin1String("spikeWindow"));
    QRect tempPosition = settings.value(QLatin1String("position")).toRect();
    if (tempPosition.height() > 0) {
        setGeometry(tempPosition);
    }
    settings.endGroup();

}

MultiNtrodeDisplayWidget::~MultiNtrodeDisplayWidget() {

    scopePlotTimer->stop();
    scatterPlotTimer->stop();
    while (nTrodeData.length() > 0) {
        delete nTrodeData.takeLast();
    }

}

void MultiNtrodeDisplayWidget::setShownNtrode(int nTrodeInd) {

    writeToMipmapMutex.lock();
    changingDisplayedNtrode = true;
    if (currentNTrode > -1) {
        nTrodeData[currentNTrode]->setDataNotShowing();
    }
    currentNTrode = nTrodeInd;
    nTrodeLabel->setText(QString("%1").arg(spikeConf->ntrodes[currentNTrode]->nTrodeId));


    int numChannels = spikeConf->ntrodes[currentNTrode]->hw_chan.length();

    channelViewCombo->clear(); 
    channelViewCombo->addItems(nTrodeData[currentNTrode]->channelViewStrings);
    channelViewCombo->setCurrentIndex(nTrodeData[currentNTrode]->currentChannelListIndex);



    for (int i=1; i < clusterSelectButtons->buttons().length()+1; i++) {
        toggleClusterOn(i, nTrodeData[currentNTrode]->clustersDefined[i]);
    }


    triggerScope->setDisplayedNTrode(currentNTrode);

    nTrodeData[currentNTrode]->setDataShowing();
    graphicsWin->setDisplayedNTrode(currentNTrode, nTrodeData[currentNTrode]->minAmplitude,nTrodeData[currentNTrode]->maxAmplitude,nTrodeData[currentNTrode]->mipMaps);
    changingDisplayedNtrode = false;
    updateScatterChannels(channelViewCombo->currentIndex());
    writeToMipmapMutex.unlock();


}

void MultiNtrodeDisplayWidget::singleViewButtonReleased() {
    if (!multiViewMode) {
        selectSingleView->setDown(true);
        selectMultiView->setDown(false);
    }

}

void MultiNtrodeDisplayWidget::multiViewButtonReleased() {
    if (multiViewMode) {
        selectMultiView->setDown(true);
        selectSingleView->setDown(false);
    }

}

void MultiNtrodeDisplayWidget::singleViewButtonPressed() {

    setSingleView();
    emit singleViewSet();
}

void MultiNtrodeDisplayWidget::multiViewButtonPressed() {
    if (nTrodeData[currentNTrode]->numberOfChannels > 1) {
        setMultiView();
        emit multiViewSet();
    } else {

        //selectMultiView->setChecked(false);
        selectMultiView->setDown(false);
        //selectSingleView->setChecked(true);
        selectSingleView->setDown(true);
    }
}

void MultiNtrodeDisplayWidget::clusterButtonPressed() {

    selectedClusterInd = QObject::sender()->property("userData").toInt();
    //qDebug() << "Cluster button pressed: " << selectedClusterInd;
    graphicsWin->setCurrentCluster(selectedClusterInd);
}

void MultiNtrodeDisplayWidget::toolButtonPressed(int toolNum) {
    currentTool = toolNum;
    graphicsWin->setTool(toolNum);
}

void MultiNtrodeDisplayWidget::deletePolygonData(int clusterInd, int bitInd) {


    nTrodeData[currentNTrode]->deletePolygonData(clusterInd,bitInd);
    nTrodeData[currentNTrode]->setMipMapData();

    /*
    for (int i=0;i < nTrodeData[currentNTrode]->mipMaps.length(); i++) {
        nTrodeData[currentNTrode]->mipMaps[i]->setPoints(0,nTrodeData[currentNTrode]->scatterBufferSize,nTrodeData[currentNTrode]->scatterData.constData(), nTrodeData[currentNTrode]->pointClusterOwnership);
        //mipMaps_lowRange[i]->setPoints(0,bufferSize,inputData);
    }*/

    //Replot the mipmaps
    //graphicsWin->setData(nTrodeData[currentNTrode]->scatterData.constData(), nTrodeData[currentNTrode]->pointClusterOwnership);


}

void MultiNtrodeDisplayWidget::calculatePointsInsidePolygon(int clusterInd, ClusterPolygonSet p, int bitInd) {
    qDebug() << "Calculating points inside cluster" << clusterInd << bitInd ;

    nTrodeData[currentNTrode]->addPolygonData(p);
    nTrodeData[currentNTrode]->calculatePointsInsidePolygon(clusterInd,bitInd);

    nTrodeData[currentNTrode]->setMipMapData();
    /*
    for (int i=0;i < nTrodeData[currentNTrode]->mipMaps.length(); i++) {
        nTrodeData[currentNTrode]->mipMaps[i]->setPoints(0,nTrodeData[currentNTrode]->scatterBufferSize,nTrodeData[currentNTrode]->scatterData.constData(), nTrodeData[currentNTrode]->pointClusterOwnership);
        //mipMaps_lowRange[i]->setPoints(0,bufferSize,inputData);
    }*/
    //Replot the mipmaps


    //graphicsWin->setData(nTrodeData[currentNTrode]->scatterData.constData(), nTrodeData[currentNTrode]->pointClusterOwnership);

}



void MultiNtrodeDisplayWidget::toggleClusterOn(int clusterInd, bool on) {
    //clustersDefined[clusterInd] = on;
    nTrodeData[currentNTrode]->clustersDefined[clusterInd] = on;


    if (on) {
        clusterSelectButtons->button(clusterInd-1)->setText("-"+ QString("%1").arg(clusterInd) + "-");
    } else {
        clusterSelectButtons->button(clusterInd-1)->setText(QString("%1").arg(clusterInd));

    }
}

void MultiNtrodeDisplayWidget::zoom(int changeAmount) {


    //int trodeNum = streamConf->trodeIndexLookupByHWChan[dispHWChannels[selection]];
    //int trodeChannel = streamConf->trodeChannelLookupByHWChan[dispHWChannels[selection]];
    int currentMaxDisp = spikeConf->ntrodes[currentNTrode]->maxDisp[0];
    int newMaxDisp = currentMaxDisp+changeAmount;

    if (newMaxDisp >= 50) {
        spikeConf->setMaxDisp(currentNTrode,newMaxDisp);
    }
}

void MultiNtrodeDisplayWidget::setMaxDisplay(int nTrode, int newMaxDisplay) {

    nTrodeData[nTrode]->setZeroToMax(newMaxDisplay);
    if (nTrode == currentNTrode) {


        graphicsWin->setDisplayWindow(nTrodeData[nTrode]->minAmplitude,nTrodeData[nTrode]->maxAmplitude);


        triggerScope->setMaxDisplay(newMaxDisplay);
    }

}

void MultiNtrodeDisplayWidget::lockSpikeSortMutex() {
    spikeSortMutex.lock();
}

void MultiNtrodeDisplayWidget::unlockSpikeSortMutex() {
    spikeSortMutex.unlock();
}

void MultiNtrodeDisplayWidget::setSingleView() {

    if (multiViewMode) {
        multiViewMode = false;

        selectSingleView->setDown(true);
        selectMultiView->setDown(false);

        graphicsWin->setMultipleProjections(false);
        channelViewCombo->setEnabled(true);
        updateScatterChannels(channelViewCombo->currentIndex());
        graphicsWin->setDisplayWindow(nTrodeData[currentNTrode]->minAmplitude,nTrodeData[currentNTrode]->maxAmplitude);

    }
}

void MultiNtrodeDisplayWidget::setMultiView() {


    if (!multiViewMode) {
        multiViewMode = true;

        selectSingleView->setDown(false);
        selectMultiView->setDown(true);

        graphicsWin->setMultipleProjections(true);
        channelViewCombo->setEnabled(false);
        graphicsWin->setDisplayWindow(nTrodeData[currentNTrode]->minAmplitude,nTrodeData[currentNTrode]->maxAmplitude);

    }

}


void MultiNtrodeDisplayWidget::closeEvent(QCloseEvent* event) {
    //qDebug() << "Close nTrode " << nTrodeNumber;
    emit windowClosed();
    event->accept();
}

void MultiNtrodeDisplayWidget::moveEvent(QMoveEvent *event) {

    QSettings settings(QLatin1String("SpikeGadgets"), QLatin1String("Trodes"));

    settings.beginGroup(QLatin1String("spikeWindow"));
    settings.setValue(QLatin1String("position"), this->geometry());
    settings.endGroup();

    /*
    QPoint newpos = event->pos();
    newpos.setX(frameGeometry().x());
    newpos.setY(frameGeometry().y());

    //newpos.setY(newpos.y()-22);

    emit windowMoved(newpos);
    */
}

void MultiNtrodeDisplayWidget::showEvent(QShowEvent *) {

    //Remembered settings...
    QSettings settings(QLatin1String("SpikeGadgets"), QLatin1String("Trodes"));

    //Place the window where it was last
    settings.beginGroup(QLatin1String("spikeWindow"));
    QRect tempPosition = settings.value(QLatin1String("position")).toRect();
    if (tempPosition.height() > 0) {
        setGeometry(tempPosition);
    }
    settings.endGroup();

}

void MultiNtrodeDisplayWidget::resizeEvent(QResizeEvent *event) {

    QSettings settings(QLatin1String("SpikeGadgets"), QLatin1String("Trodes"));

    settings.beginGroup(QLatin1String("spikeWindow"));
    settings.setValue(QLatin1String("position"), this->geometry());
    settings.endGroup();

    //QSize newSize = event->size();
    //emit newWindowSize(newSize);
}

void MultiNtrodeDisplayWidget::refreshGraphicsWin() {
    graphicsWin->refresh();
}

void MultiNtrodeDisplayWidget::setStreamManagerPtr(StreamProcessorManager *mgr) {
    streamManager = mgr;
}

void MultiNtrodeDisplayWidget::openPSTHWindow(int clusterInd) {


    if (!streamManager->getPSTHTriggerID().isEmpty()) {
        PSTHDialog *newDialog = new PSTHDialog();
        newDialog->setAttribute(Qt::WA_DeleteOnClose); //deletes the object when the window is closed

        newDialog->setWindowTitle(streamManager->getPSTHTriggerID() + " vs nTrode " + QString().number(spikeConf->ntrodes[currentNTrode]->nTrodeId) + ", unit " + QString().number(clusterInd) );
        newDialog->show();

        uint32_t startOfRange = nTrodeData[currentNTrode]->getStartOfLogTime();
        QVector<uint32_t> dEventTimes = streamManager->getDigitalEventTimes();
        //Remove any trigger events that occured before the range of the saved spikes
        while (!dEventTimes.isEmpty() && dEventTimes.front() < startOfRange) {
            dEventTimes.pop_front();
        }

        if (streamManager != NULL) {
            newDialog->plot(dEventTimes,nTrodeData[currentNTrode]->getSpikeTimesForCluster(clusterInd));
        }
    } else {
        QMessageBox* msgBox = new QMessageBox( this );
         msgBox->setAttribute( Qt::WA_DeleteOnClose ); //makes sure the msgbox is deleted automatically when closed
         msgBox->setStandardButtons( QMessageBox::Ok );
         msgBox->setWindowTitle( tr("Error") );
         msgBox->setText( tr("No PSTH channel selected.") );

         //msgBox->setModal( false ); // if you want it non-modal

    }

}

void MultiNtrodeDisplayWidget::newSplitterPos(int index, int newPos) {
    emit splitterPosChanged(panelSplitter->sizes());
}

void MultiNtrodeDisplayWidget::setSplitterPos(QList<int> panelSizes) {
    panelSplitter->setSizes(panelSizes);
}

void MultiNtrodeDisplayWidget::scatterPlotTimerExpired() {
    if (!exportMode && currentNTrode > -1) {

        //graphicsWin->update();
        //scatterDisplay->update();
        graphicsWin->updateBackgroundPlot();
        graphicsWin->scene->update();
    }

}

void MultiNtrodeDisplayWidget::scopePlotTimerExpired() {
    if (!exportMode && currentNTrode > -1) {
        //triggerScope->scopeWindows->plotData(waveForms.data());
        triggerScope->scopeWindows->plotData(nTrodeData[currentNTrode]->waveForms.data());

        /*
        int numChannels = triggerScope->scopeWindows.length();
        for (int i = 0; i < numChannels; i++) {

            triggerScope->scopeWindows[i]->plotData(waveForms.data(), i);
        }*/

    }

}

void MultiNtrodeDisplayWidget::clearAllButtonPressed() {
    //Clears scatterplots for all nTrodes
    for (int i=0; i < nTrodeData.length(); i++) {
        nTrodeData[i]->clearButtonPressed();
    }

    graphicsWin->clearData();
    //graphicsWin->setChannels(currentXdisplay, currentYdisplay, scatterData.constData());
    //scatterDisplay->setChannels(currentXdisplay, currentYdisplay, scatterData.constData());

}

void MultiNtrodeDisplayWidget::clearButtonPressed() {
    //Clears scatterplot for the current nTrode
    nTrodeData[currentNTrode]->clearButtonPressed();

    graphicsWin->clearData();
    //graphicsWin->setChannels(currentXdisplay, currentYdisplay, scatterData.constData());
    //scatterDisplay->setChannels(currentXdisplay, currentYdisplay, scatterData.constData());

}

void MultiNtrodeDisplayWidget::updateScatterChannels(int channelCombo) {


    if (!changingDisplayedNtrode) {
        currentXdisplay = nTrodeData[currentNTrode]->channelViewXList[channelCombo];
        currentYdisplay = nTrodeData[currentNTrode]->channelViewYList[channelCombo];
        nTrodeData[currentNTrode]->currentXdisplay = currentXdisplay;
        nTrodeData[currentNTrode]->currentYdisplay = currentYdisplay;
        nTrodeData[currentNTrode]->currentChannelListIndex = channelCombo;
        graphicsWin->setChannels(currentXdisplay, currentYdisplay, nTrodeData[currentNTrode]->scatterData.constData(),nTrodeData[currentNTrode]->pointClusterOwnership);
    }
    //scatterDisplay->setChannels(currentXdisplay, currentYdisplay, scatterData.constData());

}

void MultiNtrodeDisplayWidget::scatterPairSelected(int pairIndex) {
    setSingleView();
    channelViewCombo->setCurrentIndex(pairIndex);

}

void MultiNtrodeDisplayWidget::receiveNewEvent(int nTrodeInd, const QVector<int2d>* waveForm, const int* peaks, uint32_t time) {


    if (nTrodeInd == currentNTrode) {
        spikeSortMutex.lock();
        nTrodeData[nTrodeInd]->receiveNewEvent(waveForm,peaks,time);
        if (!changingDisplayedNtrode) {
            writeToMipmapMutex.lock();
            graphicsWin->setDataPoint(nTrodeData[currentNTrode]->scatterData.constData(),nTrodeData[currentNTrode]->currentBufferIndex, nTrodeData[currentNTrode]->pointClusterOwnership[nTrodeData[currentNTrode]->currentBufferIndex]);
            writeToMipmapMutex.unlock();
        }
        spikeSortMutex.unlock();
    } else {
        nTrodeData[nTrodeInd]->receiveNewEvent(waveForm,peaks,time);
    }



}

void MultiNtrodeDisplayWidget::changeAudioChannel(int hardwareChannel) {

    int channelFound = -1;
    if (triggerScope->nTrodeNumber == streamConf->trodeIndexLookupByHWChan.at(hardwareChannel))
      channelFound = streamConf->trodeChannelLookupByHWChan.at(hardwareChannel);

    triggerScope->updateAudioHighlight(channelFound);

}





//TriggerScopeSettingsWidget displays the 'settings' tab for each nTrode.
TriggerScopeSettingsWidget::TriggerScopeSettingsWidget(QWidget *parent, int trodeNum) :
  QWidget(parent) {

    nTrodeNumber = trodeNum;
    widgetLayouts.push_back(new QGridLayout(this));
    //widgetLayouts.push_back(new QGridLayout(this));
    //widgetLayouts.push_back(new QGridLayout(this));
    //widgetLayouts.push_back(new QGridLayout(this));

    widgetLayouts[0]->setSpacing(0);
    widgetLayouts[0]->setContentsMargins(7,2,7,2);

    //Create the reference controls
    refBox = new QGroupBox(tr("Digital reference"),this);
    widgetLayouts.push_back(new QGridLayout(refBox));
    RefTrodeBox = new QComboBox(this);
    for (int i = 1; i < spikeConf->ntrodes.length()+1; i++)
      RefTrodeBox->addItem(QString::number(i));
    RefTrodeBox->setFixedWidth(50);


    RefChannelBox = new QComboBox(this);
    labels.push_back(new QLabel(tr("nTrode"),this));
    labels.push_back(new QLabel(tr("Channel"),this));
    widgetLayouts[1]->addWidget(labels[0],0,0,Qt::AlignCenter);
    widgetLayouts[1]->addWidget(labels[1],0,1,Qt::AlignCenter);
    widgetLayouts[1]->addWidget(RefTrodeBox,1,0,Qt::AlignCenter);
    widgetLayouts[1]->addWidget(RefChannelBox,1,1,Qt::AlignCenter);
    widgetLayouts[1]->setVerticalSpacing(0);
    refBox->setLayout(widgetLayouts[1]);
    refBox->setCheckable(true);
    refBox->setChecked(spikeConf->ntrodes[nTrodeNumber]->refOn);
    refBox->setFixedHeight(75);
    widgetLayouts[0]->addWidget(refBox,1,0);

    //Create the filter controls
    filterBox = new QGroupBox(tr("Spike filter"),this);
    widgetLayouts.push_back(new QGridLayout(filterBox));
    labels.push_back(new QLabel(tr("Low"),this));
    labels.push_back(new QLabel(tr("High"),this));
    lowFilterCutoffBox = new QComboBox(this);
    QList<int> lowList;
    for (int i = 200;i < 700;i+=100) {
        lowList.append(i);
        lowFilterCutoffBox->addItem(QString::number(lowList.last()));
    }
    //Set the current lower filter setting from config file (find nearest available)
    for (int i=0;i<lowList.length();i++) {
        if (lowList[i] <= spikeConf->ntrodes[nTrodeNumber]->lowFilter) {
            lowFilterCutoffBox->setCurrentIndex(i);
        }
    }
    lowFilterCutoffBox->setFixedWidth(60);
    highFilterCutoffBox = new QComboBox(this);
    QList<int> highList;
    for (int i = 5000;i < 7000;i+=1000) {
        highList.append(i);
        highFilterCutoffBox->addItem(QString::number(highList.last()));
    }
    //Set the current upper filter setting from config file (find nearest available)
    for (int i=0;i<highList.length();i++) {
        if (highList[i] <= spikeConf->ntrodes[nTrodeNumber]->highFilter) {
            highFilterCutoffBox->setCurrentIndex(i);
        }
    }
    highFilterCutoffBox->setFixedWidth(70);
    widgetLayouts[2]->addWidget(labels[2],0,0,Qt::AlignCenter);
    widgetLayouts[2]->addWidget(labels[3],0,1,Qt::AlignCenter);
    widgetLayouts[2]->addWidget(lowFilterCutoffBox,1,0,Qt::AlignCenter);
    widgetLayouts[2]->addWidget(highFilterCutoffBox,1,1,Qt::AlignCenter);
    widgetLayouts[2]->setVerticalSpacing(0);
    filterBox->setLayout(widgetLayouts[2]);
    filterBox->setCheckable(true);
    filterBox->setChecked(spikeConf->ntrodes[nTrodeNumber]->filterOn);
    filterBox->setFixedHeight(75);
    widgetLayouts[0]->addWidget(filterBox,2,0);


    //Create the ModuleData controls
    ModuleDataFilterBox = new QGroupBox(tr("Local field potential"),this);
    widgetLayouts.push_back(new QGridLayout(ModuleDataFilterBox));
    labels.push_back(new QLabel(tr("Channel"),this));
    labels.push_back(new QLabel(tr("High cutoff"),this));
    ModuleDataHighFilterCutoffBox = new QComboBox(this);
    QList<int> ModuleDatahighList;
    //TODO avaliable filter values should be specified by filter object, not hardcoded
    for (int i = 100;i <= 500;i+=100) {
        ModuleDatahighList.append(i);
        ModuleDataHighFilterCutoffBox->addItem(QString::number(ModuleDatahighList.last()));
    }
    for (int i=0;i<ModuleDatahighList.length();i++) {
        if (ModuleDatahighList[i] <= spikeConf->ntrodes[nTrodeNumber]->moduleDataHighFilter) {
            ModuleDataHighFilterCutoffBox->setCurrentIndex(i);
        }
    }
    ModuleDataChannelBox = new QComboBox(this);
    for (int i = 0; i < spikeConf->ntrodes[nTrodeNumber]->hw_chan.length(); i++)
      ModuleDataChannelBox->addItem(QString::number(i+1));

    ModuleDataChannelBox->setCurrentIndex(spikeConf->ntrodes[nTrodeNumber]->moduleDataChan);
    //Disabling the ability to select main LFP channel until Issue #30 is fixed
    ModuleDataChannelBox->setEnabled(false);

    widgetLayouts[3]->addWidget(labels[4],0,0,Qt::AlignCenter);
    widgetLayouts[3]->addWidget(labels[5],0,1,Qt::AlignCenter);
    widgetLayouts[3]->addWidget(ModuleDataChannelBox,1,0,Qt::AlignCenter);
    widgetLayouts[3]->addWidget(ModuleDataHighFilterCutoffBox,1,1,Qt::AlignCenter);
    widgetLayouts[3]->setVerticalSpacing(0);
    ModuleDataFilterBox->setLayout(widgetLayouts[3]);
    ModuleDataFilterBox->setCheckable(false);
    ModuleDataFilterBox->setFixedHeight(75);
    widgetLayouts[0]->addWidget(ModuleDataFilterBox,3,0);


    //Add a label at the top to show which nTrode this is controlling
    QFont labelFont;
    labelFont.setPixelSize(12);
    labelFont.setFamily("Console");
    QGridLayout* IDlayout = new QGridLayout();
    QLabel* trodeLabel = new QLabel(this);
    trodeLabel->setFont(labelFont);
    trodeLabel->setText(QString("nTrode ")+QString::number(trodeNum+1));
    IDlayout->addWidget(trodeLabel,0,0);
    linkCheckBox = new QCheckBox(this);
    linkCheckBox->setText("Link settings");
    linkCheckBox->setFont(labelFont);
    if (linkChangesBool) {
        linkCheckBox->setChecked(true);
    }
    connect(linkCheckBox,SIGNAL(toggled(bool)),this,SLOT(linkBoxClicked(bool)));
    IDlayout->addWidget(linkCheckBox,0,2);
    IDlayout->setColumnStretch(1,1);
    widgetLayouts[0]->addLayout(IDlayout,0,0);


    RefTrodeBox->setCurrentIndex(spikeConf->ntrodes[nTrodeNumber]->refNTrode);
    RefChannelBox->clear();
    for (int i = 0; i < spikeConf->ntrodes[spikeConf->ntrodes[nTrodeNumber]->refNTrode]->hw_chan.length(); i++)
        RefChannelBox->addItem(QString::number(i+1));
    RefChannelBox->setCurrentIndex(spikeConf->ntrodes[nTrodeNumber]->refChan);
    setLayout(widgetLayouts[0]);
    setWindowTitle(tr("nTrode Settings"));

    //Connect the combo box controls
    connect(RefTrodeBox,SIGNAL(currentIndexChanged(int)),this,SLOT(updateRefTrode(int)));
    connect(RefChannelBox,SIGNAL(currentIndexChanged(int)),this,SLOT(updateRefChan(int)));
    connect(highFilterCutoffBox,SIGNAL(currentIndexChanged(int)),this,SLOT(updateUpperFilter()));
    connect(lowFilterCutoffBox,SIGNAL(currentIndexChanged(int)),this,SLOT(updateLowerFilter()));
    connect(ModuleDataChannelBox,SIGNAL(currentIndexChanged(int)),this,SLOT(updateModuleDataChannel()));
    connect(ModuleDataHighFilterCutoffBox,SIGNAL(currentIndexChanged(int)),this,SLOT(updateModuleDataUpperFilter()));

    //Connect the group box check (on/off) controls
    connect(refBox,SIGNAL(toggled(bool)),this,SLOT(updateRefSwitch(bool)));
    connect(filterBox,SIGNAL(toggled(bool)),this,SLOT(updateFilterSwitch(bool)));
}

TriggerScopeSettingsWidget::~TriggerScopeSettingsWidget() {

}

void TriggerScopeSettingsWidget::setEnabledForStreaming(bool streamOn) {
    linkCheckBox->setEnabled(!streamOn);
    refBox->setEnabled(!streamOn);
    filterBox->setEnabled(!streamOn);
    ModuleDataFilterBox->setEnabled(!streamOn);
    RefTrodeBox->setEnabled(!streamOn);
    RefChannelBox->setEnabled(!streamOn);
    lowFilterCutoffBox->setEnabled(!streamOn);
    highFilterCutoffBox->setEnabled(!streamOn);
    ModuleDataHighFilterCutoffBox->setEnabled(!streamOn);


    //Disabling the ability to select main LFP channel until Issue #30 is fixed
    //ModuleDataChannelBox->setEnabled(false);
    ModuleDataChannelBox->setEnabled(!streamOn);
}

void TriggerScopeSettingsWidget::linkBoxClicked(bool checked) {

    emit toggleLinkChanges(checked);
}

void TriggerScopeSettingsWidget::updateRefChan(int) {
    updateRefChan();
}


void TriggerScopeSettingsWidget::updateRefChan() {

    int newRefTrode  = RefTrodeBox->currentText().toInt()-1;
    int newRefChannel  = RefChannelBox->currentText().toInt()-1;


    //When the ref trode menu is changed, the ref channel menu is cleared, and the
    //index is temporarity changed to -1.  We ignore the -1 signal.

    if (newRefChannel > -1) {
        qDebug() << "Changing ref channel" << newRefChannel;

        if (linkChangesBool) {
            //int nTrodeIndex = RefTrodeBox->currentIndex();
            //int channelIndex = RefChannelBox->currentIndex();
            emit changeAllRefs(newRefTrode,newRefChannel);
        } else {

            spikeConf->setReference(nTrodeNumber,newRefTrode,newRefChannel);

        }
        emit updateAudioSettings();
    }
}

void TriggerScopeSettingsWidget::updateRefSwitch(bool on) {

    if (linkChangesBool) {
        emit toggleAllRefs(on);
    } else {
        spikeConf->setRefSwitch(nTrodeNumber,on);
        emit updateAudioSettings();
    }
}

void TriggerScopeSettingsWidget::updateFilterSwitch(bool on) {

    if (linkChangesBool) {
        emit toggleAllFilters(on);
    } else {
        spikeConf->setFilterSwitch(nTrodeNumber,on);
        emit updateAudioSettings();
    }
}


void TriggerScopeSettingsWidget::updateLowerFilter() {
    int newLowCutoff = lowFilterCutoffBox->currentText().toInt();
    int newHighCutoff = highFilterCutoffBox->currentText().toInt();

    if (linkChangesBool) {     
        emit changeAllFilters(newLowCutoff,newHighCutoff);
    } else {
        spikeConf->setLowFilter(nTrodeNumber,newLowCutoff);
        emit updateAudioSettings();
    }
}

void TriggerScopeSettingsWidget::updateUpperFilter() {
    int newLowCutoff = lowFilterCutoffBox->currentText().toInt();
    int newHighCutoff = highFilterCutoffBox->currentText().toInt();

    if (linkChangesBool) {
       emit changeAllFilters(newLowCutoff,newHighCutoff);
    } else {
        spikeConf->setHighFilter(nTrodeNumber,newHighCutoff);
        emit updateAudioSettings();
    }
}

void TriggerScopeSettingsWidget::updateModuleDataChannel() {
    int newChannel = ModuleDataChannelBox->currentText().toInt();
    spikeConf->setModuleDataChan(nTrodeNumber,newChannel-1); //convert back to 0-based
    // we also need to broadcast this change to the modules
    emit moduleDataChannelChanged(nTrodeNumber, newChannel-1);
}

void TriggerScopeSettingsWidget::updateModuleDataUpperFilter() {
    int newCutoff = ModuleDataHighFilterCutoffBox->currentText().toInt();
    spikeConf->setModuleDataHighFilter(nTrodeNumber,newCutoff);

}

void TriggerScopeSettingsWidget::updateRefTrode(int notused) {
    updateRefTrode();
}


void TriggerScopeSettingsWidget::updateRefTrode() {

    int newRefTrode  = RefTrodeBox->currentText().toInt()-1;
    RefChannelBox->clear();
    for (int i = 0; i < spikeConf->ntrodes[newRefTrode]->hw_chan.length(); i++)
        RefChannelBox->addItem(QString::number(i+1));
}

//TriggerScopeDisplayWidget displays all ScopeWindows for one nTrode.
TriggerScopeDisplayWidget::TriggerScopeDisplayWidget(QWidget *parent) :
  QFrame(parent)
{

  emitMaxDispSignal = true;
  emitThreshSignal = true;

  //nTrodeNumber = nTrodeNum;
  //numberOfChannels = numChannels;
  QGridLayout *mainLayout = new QGridLayout();
  QGridLayout *controlLayout = new QGridLayout();
  setStyleSheet("QFrame {border: 2px solid lightgray;"
                        "border-radius: 4px;"
                        "padding: 2px;}");


  QFrame *controlFrame = new QFrame(this);

  controlFrame->setStyleSheet("QFrame {border: 2px solid lightgray;"
                              "border-radius: 4px;"
                              "padding: 2px;"
                              "background-color: white}");


  scopeLayout = new QGridLayout();
  scopeLayout->setHorizontalSpacing(10);


  //if true, all channels in the nTrode change together
  linkMaxDisplaySettings = true;
  linkThreshSettings = true;


  QFont labelFont;
  labelFont.setPixelSize(8);
  labelFont.setFamily("Console");
  maxLabel = new QLabel(this);
  maxLabel->setStyleSheet("QFrame {border: 0px solid white;"
                              "border-radius: 4px;"
                              "padding: 0px;}");
  maxLabel->setFont(labelFont);
  maxLabel->setAlignment(Qt::AlignRight);
  maxLabel->setFixedWidth(25);
  zeroLabel = new QLabel(this);
  zeroLabel->setStyleSheet("QFrame {border: 0px solid white;"
                              "border-radius: 4px;"
                              "padding: 0px;}");
  zeroLabel->setFont(labelFont);
  zeroLabel->setAlignment(Qt::AlignRight);
  zeroLabel->setFixedWidth(25);
  minLabel = new QLabel(this);
  minLabel->setStyleSheet("QFrame {border: 0px solid white;"
                              "border-radius: 4px;"
                              "padding: 0px;}");
  minLabel->setFont(labelFont);
  minLabel->setAlignment(Qt::AlignRight);
  minLabel->setFixedWidth(25);
  maxLabel->setText(QString("%1").arg(-100));
  minLabel->setText(QString("%1").arg(100));
  zeroLabel->setText(QString("%1").arg(0));

  maxDispControl = new QSpinBox(this);
  maxDispControl->setFrame(false);
  maxDispControl->setStyleSheet("QSpinBox { background-color: white; }");
  maxDispControl->setMinimum(50);
  maxDispControl->setMaximum(4000);
  maxDispControl->setSingleStep(25);
  maxDispControl->setFixedSize(50,22);
  maxDispControl->setAlignment(Qt::AlignRight);
  maxDispControl->setFocusPolicy(Qt::NoFocus);
  maxDispControl->setToolTip(tr("Display range (+/- uV)"));
  QFont font(maxDispControl->font());

#ifdef Q_WS_WIN
 font.setPointSize(8);
#else
  font.setPointSize(10);
#endif

  font.setFamily("console");
  maxDispControl->setFont(font);
  //maxDispControl->setValue(spikeConf->ntrodes[nTrodeNumber]->maxDisp[0]);
  connect(maxDispControl,SIGNAL(valueChanged(int)),this,SLOT(updateMaxDisplay()));

  QLabel *maxDispLabel = new QLabel(this);
  maxDispLabel->setStyleSheet("QFrame {border: 0px solid white;"
                              "border-radius: 4px;"
                              "padding: 0px;"
                              "background-color: white}");
  maxDispLabel->setContentsMargins(0,0,0,0);
  maxDispLabel->setFrameStyle(QFrame::NoFrame);
  maxDispLabel->setFont(font);
  //maxDispLabel->setFixedSize(50,22);
  maxDispLabel->setAlignment(Qt::AlignRight|Qt::AlignVCenter);
  //maxDispLabel->setFixedWidth(75);
  maxDispLabel->setText(QString("Zoom range:"));

  threshControl = new QSpinBox(this);
  threshControl->setFrame(false);
  //threshControl->setPalette(pal);
  threshControl->setStyleSheet("QSpinBox { background-color: white; }");
  //threshControl->setButtonSymbols(QAbstractSpinBox::NoButtons);
  threshControl->setMinimum(10);
  threshControl->setMaximum(4000);
  threshControl->setSingleStep(10);
  threshControl->setFixedSize(50,22);
  threshControl->setFont(font);
  threshControl->setAlignment(Qt::AlignRight);
  threshControl->setFocusPolicy(Qt::NoFocus);
  threshControl->setToolTip(tr("Trigger threshold (uV)"));
  //threshControl->setValue(spikeConf->ntrodes[nTrodeNumber]->thresh[0]);
  connect(threshControl,SIGNAL(valueChanged(int)),this,SLOT(updateThreshhold()));

  QLabel *threshLabel = new QLabel(this);
  threshLabel->setStyleSheet("QFrame {border: 0px solid white;"
                              "border-radius: 4px;"
                              "padding: 0px;"
                              "background-color: white}");
  threshLabel->setContentsMargins(0,0,0,0);

  threshLabel->setFont(font);
  threshLabel->setAlignment(Qt::AlignRight|Qt::AlignVCenter);
  //threshLabel->setFixedWidth(75);
  threshLabel->setText(QString("Threshold:"));

  controlLayout->addWidget(maxDispLabel,0,1,Qt::AlignRight);
  controlLayout->addWidget(threshLabel,1,1,Qt::AlignRight);
  controlLayout->addWidget(maxDispControl,0,2,Qt::AlignLeft);
  controlLayout->addWidget(threshControl,1,2,Qt::AlignLeft);
  //controlLayout->setColumnStretch(0,1);
  controlLayout->setVerticalSpacing(2);
  controlLayout->setHorizontalSpacing(0);
  controlLayout->setContentsMargins(0,0,0,0);
  //mainLayout->addLayout(controlLayout,0,0,Qt::AlignCenter);
  controlFrame->setLayout(controlLayout);
  controlFrame->setFrameStyle(QFrame::StyledPanel | QFrame::Sunken);
  controlFrame->setLineWidth(3);
  controlFrame->setContentsMargins(7,6,4,6);
  mainLayout->addWidget(controlFrame,0,0,Qt::AlignRight);


  triggerButtonLayout = new QGridLayout();

  /*
  for (int i=0;i<numChannels; i++) {

    triggerSwitches.push_back(new QPushButton(this));
    triggerSwitches[i]->setFlat(true);

    triggerSwitches[i]->setStyleSheet("QPushButton {border: 2px solid lightgray;"
                          "border-radius: 4px;"
                          "padding: 2px;}");

    triggerSwitches[i]->setFont(labelFont);
    //triggerSwitches[i]->setFixedWidth(25);
    triggerSwitches[i]->setToolTip(tr("Trigger on/off"));
    triggerSwitches[i]->setAutoExclusive(false);
    triggerSwitches[i]->setCheckable(true);
    triggerSwitches[i]->setChecked(spikeConf->ntrodes[nTrodeNumber]->triggerOn[i]);

    triggerSwitchMapper->setMapping(triggerSwitches[i], i);
    connect(triggerSwitches[i], SIGNAL(clicked()), triggerSwitchMapper, SLOT(map()));
    if (triggerSwitches[i]->isChecked()) {
        triggerSwitches[i]->setText("On");
    } else {
        triggerSwitches[i]->setText("Off");
    }

    triggerButtonLayout->addWidget(triggerSwitches[i],0,i,Qt::AlignCenter);

    threshholds.push_back(0);
    maxDisplays.push_back(0);

  }
  */

  scopeLayout->addLayout(triggerButtonLayout,0,1);



  //Create a layout for the scope window y axis labels
  QGridLayout* rangeLayout = new QGridLayout();
  rangeLayout->setContentsMargins(0,0,0,0);
  rangeLayout->setMargin(0);
  rangeLayout->addWidget(maxLabel,0,0);
  rangeLayout->addWidget(minLabel,4,0);
  rangeLayout->addWidget(zeroLabel,2,0);
  rangeLayout->setRowStretch(1,1);
  rangeLayout->setRowStretch(3,1);
  scopeLayout->addLayout(rangeLayout,1,0);


  scopeWindows = new ScopeWindow(this);
  //scopeWindows->nTrodeNum = nTrodeNum;
  connect(scopeWindows,SIGNAL(clicked(int)),this,SLOT(updateAudio(int)));
  connect(scopeWindows,SIGNAL(zoom(int)),this,SIGNAL(zoom(int)));
  scopeWindows->setMinimumWidth(50);
  //scopeWinLayout->addWidget(scopeWindows,0,1,Qt::AlignCenter);
  //scopeWinLayout->setColumnStretch(1,1);
  scopeLayout->addWidget(scopeWindows,1,1);

  mainLayout->addLayout(scopeLayout,1,0);


 /*
  for (int i=0;i<numChannels; i++) {
      setMaxDisplay_display(i,spikeConf->ntrodes[nTrodeNumber]->maxDisp[i]);
      setThreshhold_display(i,spikeConf->ntrodes[nTrodeNumber]->thresh[i]);
  }
*/

  //connect(this, SIGNAL(threshEditChanged(int)), this, SLOT(updateThreshhold(int)));
  //connect(this, SIGNAL(maxDispEditChanged(int)), this, SLOT(updateMaxDisplay(int)));
  //connect(this, SIGNAL(audioChanged(int)), this, SLOT(updateAudio(int)));
  //connect(spikeConf,SIGNAL(newMaxDisplay(int,int)),this,SLOT(setMaxDisplay(int,int)));
  //connect(spikeConf,SIGNAL(newThreshold(int,int)),this,SLOT(setThreshold(int,int)));


  setLayout(mainLayout);

  /*
  if (nTrodeNum == 0) {
      //audioButton[0]->setChecked(true);
      updateAudio(0);
      audioChanged(0);
  }*/


}

void TriggerScopeDisplayWidget::setDisplayedNTrode(int nTrodeInd) {
    while (triggerSwitches.length() > 0) {
        //triggerButtonLayout->removeItem()
        delete triggerSwitches.takeLast();
    }

    nTrodeNumber = nTrodeInd;
    numberOfChannels = spikeConf->ntrodes[nTrodeNumber]->hw_chan.length();
    scopeWindows->setNTrode(nTrodeNumber);
    setMaxDisplay(spikeConf->ntrodes[nTrodeNumber]->maxDisp[0]);
    setThreshold(spikeConf->ntrodes[nTrodeNumber]->thresh[0]);
    setTraceColor(spikeConf->ntrodes[nTrodeNumber]->color);

    for (int i=0;i<numberOfChannels; i++) {

      triggerSwitches.push_back(new QPushButton());
      triggerSwitches[i]->setFlat(true);

      triggerSwitches[i]->setStyleSheet("QPushButton {border: 2px solid lightgray;"
                            "border-radius: 4px;"
                            "padding: 2px;}");

      //triggerSwitches[i]->setFont(labelFont);
      //triggerSwitches[i]->setFixedWidth(25);
      triggerSwitches[i]->setToolTip(tr("Trigger on/off"));
      triggerSwitches[i]->setAutoExclusive(false);
      triggerSwitches[i]->setCheckable(true);
      triggerSwitches[i]->setChecked(spikeConf->ntrodes[nTrodeNumber]->triggerOn[i]);
      triggerSwitches[i]->setProperty("channel",QVariant(i));

      //triggerSwitchMapper->setMapping(triggerSwitches[i], i);
      connect(triggerSwitches[i], SIGNAL(clicked()), this, SLOT(triggerButtonToggled()));
      if (triggerSwitches[i]->isChecked()) {
          triggerSwitches[i]->setText("On");
      } else {
          triggerSwitches[i]->setText("Off");
      }

      triggerButtonLayout->addWidget(triggerSwitches[i],0,i,Qt::AlignCenter);

    }


}

void TriggerScopeDisplayWidget::triggerButtonToggled() {
    int channel = sender()->property("channel").toInt();
    if (triggerSwitches[channel]->isChecked()) {
        triggerSwitches[channel]->setText("On");
    } else {
        triggerSwitches[channel]->setText("Off");
    }

    spikeConf->setTriggerMode(nTrodeNumber,channel,triggerSwitches[channel]->isChecked());

}

/*
void TriggerScopeDisplayWidget::updateTriggerMode(int channel) {

    if (triggerSwitches[channel]->isChecked()) {
        triggerSwitches[channel]->setText("On");
    } else {
        triggerSwitches[channel]->setText("Off");
    }

    spikeConf->setTriggerMode(nTrodeNumber,channel,triggerSwitches[channel]->isChecked());
}*/

void TriggerScopeDisplayWidget::turnOffAudio() {

    updateAudio(-1);

}

void TriggerScopeDisplayWidget::setTraceColor(QColor color) {

    scopeWindows->traceColor = color;

}

void TriggerScopeDisplayWidget::setMaxDisplay(int newMax) {

    maxDisplay = newMax;
    emitMaxDispSignal = false;
    maxDispControl->setValue(newMax);
    emitMaxDispSignal = true;
    scopeWindows->setMaxDisplay(maxDisplay);
    maxLabel->setText(QString("%1").arg(-newMax));
    minLabel->setText(QString("%1").arg(newMax));

}

void TriggerScopeDisplayWidget::setThreshold(int newThreshold) {

    threshold = newThreshold;
    emitThreshSignal = false;
    threshControl->setValue(newThreshold);
    emitThreshSignal = true;
    scopeWindows->setThresh(threshold);

}

void TriggerScopeDisplayWidget::updateAudio(int channel)
{
  if (channel > -1) {
    //emit(hasAudio());

    int hw_chan = spikeConf->ntrodes[nTrodeNumber]->hw_chan[channel];
    emit setAudioChannel(hw_chan);
    emit updateAudioSettings();
    if (numberOfChannels > 1) {
        scopeWindows->setHighlight(channel);
    } else {
        scopeWindows->setNoHighlight();
    }

  } else {
    scopeWindows->setNoHighlight();
  }
}

void TriggerScopeDisplayWidget::updateAudioHighlight(int channel)
{
  if (channel > -1) {
    //int hw_chan = spikeConf->ntrodes[nTrodeNumber]->hw_chan[channel];
    //emit setAudioChannel(hw_chan);
    if (numberOfChannels > 1) {
        scopeWindows->setHighlight(channel);
    } else {
        scopeWindows->setNoHighlight();
    }

  } else {
    scopeWindows->setNoHighlight();
  }
}

void TriggerScopeDisplayWidget::updateThreshhold() {
    //updates threshold for all channels in the nTrode
    int newThresh = threshControl->value();
    threshold = newThresh;

    if (emitThreshSignal) {

        if (spikeConf->ntrodes[nTrodeNumber]->thresh[0] != threshold) {
            for (int i=0; i<spikeConf->ntrodes[nTrodeNumber]->thresh.length(); i++) {
                spikeConf->setThresh(nTrodeNumber,newThresh);
            }
        }
        scopeWindows->setThresh(newThresh);
    }

}

void TriggerScopeDisplayWidget::updateMaxDisplay() {
    //updates max display for all channels in the nTrode

    int newMaxDisplay = maxDispControl->value();
    maxDisplay = newMaxDisplay;

    if (emitMaxDispSignal) {
        if (newMaxDisplay < 0) {
            newMaxDisplay = 0;
        }
        maxLabel->setText(QString("%1").arg(-newMaxDisplay));
        minLabel->setText(QString("%1").arg(newMaxDisplay));

        if (spikeConf->ntrodes[nTrodeNumber]->maxDisp[0] != maxDisplay) {
            for (int i=0; i<spikeConf->ntrodes[nTrodeNumber]->maxDisp.length(); i++) {
                spikeConf->setMaxDisp(nTrodeNumber,maxDisplay);
            }
        }

        scopeWindows->setMaxDisplay(newMaxDisplay);

    }

}

void TriggerScopeDisplayWidget::setCurrentDisplayPair(int ch1, int ch2) {
    pairChannel1 = ch1;
    pairChannel2 = ch2;

    for (int i=0; i < triggerSwitches.length(); i++) {
        if (i==pairChannel1 || i==pairChannel2) {
            triggerSwitches[i]->setStyleSheet("QPushButton {border: 2px solid red;"
                                  "border-radius: 4px;"
                                  "padding: 2px;}");
        } else {
            triggerSwitches[i]->setStyleSheet("QPushButton {border: 2px solid lightgrey;"
                                  "border-radius: 4px;"
                                  "padding: 2px;}");
        }
    }

}

/*
void TriggerScopeDisplayWidget::setThreshhold_display(int channel, int newThresh) {
    threshholds[channel] = newThresh;
    //threshSpin[channel]->setValue(newThresh);
    scopeWindows->setThresh(newThresh);
}

void TriggerScopeDisplayWidget::setMaxDisplay_display(int channel, int newMaxDisplay) {
    maxDisplays[channel] = newMaxDisplay;
    scopeWindows->setMaxDisplay(newMaxDisplay);
    maxLabel->setText(QString("%1").arg(newMaxDisplay));
    minLabel->setText(QString("%1").arg(-newMaxDisplay));

}*/


//ScopeWindow plots the spike waveform for one channel.  It does not use openGL.
ScopeWindow::ScopeWindow(QWidget *parent):QWidget(parent) {

  setSizePolicy(QSizePolicy::Expanding,QSizePolicy::Expanding);

  setAutoFillBackground(false);

  pairChannel1 = -1;
  pairChannel2 = -1;

  traceColor = QColor(255,255,255);
  nTrodeNum = -1;
  maxAmplitude = (400*65536)/AD_CONVERSION_FACTOR;
  thresh = 0;
  beforeTriggerLength = 10;
  afterTriggerLength = 30;
  numChannels = 0;
  setMinimumHeight(80);
  //setFixedWidth(50);
  //setMinimumWidth(40);

  /*
  waveFormData.resize(numChannels);

  int xPointScale = 1000/(numChannels*(beforeTriggerLength + afterTriggerLength));
  for (int i = 0; i < numChannels; i++) {
      waveFormData[i].resize(beforeTriggerLength+afterTriggerLength);
      for (int j = 0; j < (beforeTriggerLength+afterTriggerLength); j++) {

          waveFormData[i][j].setX((xPointScale*(beforeTriggerLength+afterTriggerLength)*i+(xPointScale*j)));
          waveFormData[i][j].setY(0);
      }
  }*/



}


ScopeWindow::~ScopeWindow()
{

}

void ScopeWindow::setNTrode(int nTrodeInd) {
    nTrodeNum = nTrodeInd;
    numChannels = spikeConf->ntrodes[nTrodeNum]->hw_chan.length();
    waveFormData.resize(numChannels);

    int xPointScale = 1000/(numChannels*(beforeTriggerLength + afterTriggerLength));
    for (int i = 0; i < numChannels; i++) {
        waveFormData[i].resize(beforeTriggerLength+afterTriggerLength);
        for (int j = 0; j < (beforeTriggerLength+afterTriggerLength); j++) {

            waveFormData[i][j].setX((xPointScale*(beforeTriggerLength+afterTriggerLength)*i+(xPointScale*j)));
            waveFormData[i][j].setY(0);
        }
    }

    maxAmplitude = (spikeConf->ntrodes[nTrodeNum]->maxDisp[0]*65536)/AD_CONVERSION_FACTOR;
    thresh = spikeConf->ntrodes[nTrodeNum]->thresh[0];


}

void ScopeWindow::setCurrentDisplayPair(int ch1, int ch2) {
    pairChannel1 = ch1;
    pairChannel2 = ch2;
}

void ScopeWindow::plotData(const QVector<vertex2d> *waveForm) {

    for (int channel = 0; channel < numChannels; channel++) {
        for (int i = 0; i < waveFormData[channel].size(); i++) {
            waveFormData[channel][i].setY(-waveForm[channel][i].y);
        }
    }
    update();

}



void ScopeWindow::paintEvent(QPaintEvent *) {


    if (maxAmplitude >= 50) {

        int xPointScale = 1000/(numChannels*(beforeTriggerLength + afterTriggerLength));
        int singlePanelWidth = xPointScale*(beforeTriggerLength + afterTriggerLength);
        int windowWidth = numChannels*singlePanelWidth;

        QPainter painter(this);
        painter.setRenderHint(QPainter::Antialiasing);
        QPen pen;

        //We want the lines to be a fixed number of pixels in width
        //units per pixel = unitrange/window_height
        //pen.setWidth(2);
        pen.setWidth((maxAmplitude)/this->geometry().height());



        //paint gray background
        painter.setViewport(0,0,width(), height());
        painter.setWindow(0, -maxAmplitude, windowWidth, 2*maxAmplitude);
        painter.fillRect(0,-maxAmplitude,windowWidth,2*maxAmplitude,QBrush(streamConf->backgroundColor));

        if (highlight) {
            painter.fillRect(singlePanelWidth*highlightChannel,-maxAmplitude,singlePanelWidth,2*maxAmplitude,QBrush(QColor(150,100,100)));
        }



        //paint threshold line
        //pen.setStyle(Qt::DotLine);
        pen.setColor(Qt::gray);
        painter.setPen(pen);
        //painter.setPen(Qt::DotLine);
        painter.drawLine(0,-thresh,windowWidth,-thresh);

        //paint waveform
        //pen.setColor(traceColor);
        pen.setColor(spikeConf->ntrodes[nTrodeNum]->color);
        //int newWidth = (.1*maxAmplitude)/this->geometry().height();
        pen.setWidth(4);
        //pen.setWidth((2*maxAmplitude)/this->geometry().height());

        //pen.setWidth((.1*maxAmplitude)/this->geometry().height());

        painter.setPen(pen);
        for (int i=0; i < numChannels; i++) {
            painter.drawPolyline(waveFormData[i]);
        }

        int backgroundDarkness = streamConf->backgroundColor.toCmyk().black();
        if (backgroundDarkness > 150) {
            pen.setColor(Qt::white);
        } else {
          pen.setColor(Qt::black);
        }

        pen.setWidth((2*maxAmplitude)/this->geometry().height());
        painter.setPen(pen);

        //paint zero line
        painter.drawLine(0,0,windowWidth,0);

        //Draw the image borders
        //pen.setColor(qRgb(100,100,100));
        pen.setWidth(1);
        painter.setPen(pen);
        for (int i=0; i<numChannels; i++) {

            painter.drawRect(i*singlePanelWidth,-maxAmplitude,singlePanelWidth,2*maxAmplitude);

        }

        /*
        pen.setColor(qRgb(250,20,20));
        pen.setWidth(2);
        painter.setPen(pen);
        if (pairChannel1 > -1) {
            painter.drawRect(pairChannel1*singlePanelWidth,-maxAmplitude,singlePanelWidth,2*maxAmplitude);
        }
        if (pairChannel2 > -1) {
            painter.drawRect(pairChannel2*singlePanelWidth,-maxAmplitude,singlePanelWidth,2*maxAmplitude);
        }
        */
        painter.end();

    }

}


void ScopeWindow::mousePressEvent(QMouseEvent *mouseEvent) {
    qreal xpos = mouseEvent->localPos().x();
    //qDebug() << ypos << "/" << height();
    int selection = floor((numChannels * (xpos/width())));

    emit clicked(selection);
}

void ScopeWindow::wheelEvent(QWheelEvent *wheelEvent) {
    qreal ypos = wheelEvent->pos().y();

    int changeAmount;
    if (wheelEvent->delta() > 0) {
        changeAmount = -25;
    } else {
        changeAmount = 25;
    }

    emit zoom(changeAmount);
}

void ScopeWindow::setHighlight(int channel) {
    highlight = true;
    highlightChannel = channel;
    update();
}

void ScopeWindow::setNoHighlight() {
    highlight = false;
    update();
}

void ScopeWindow::setMaxDisplay(int maxDisplayVal){
  maxAmplitude = (maxDisplayVal*65536)/AD_CONVERSION_FACTOR;
  update();
}

void ScopeWindow::setThresh(int threshVal){
  thresh = (threshVal*65536)/AD_CONVERSION_FACTOR;
  update();
}





