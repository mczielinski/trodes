#include "mainWindow.h"
#include "trodesDataInterface.h"

MainWindow::MainWindow(QStringList arguments, QWidget *parent) :
  QMainWindow(parent)
{

  QMenu *fileMenu = new QMenu("&File",this);
  menuBar()->addMenu(fileMenu);
  QAction *actionQuit = new QAction("&Quit", this);
  fileMenu->addAction(actionQuit);
  connect(actionQuit, SIGNAL(triggered()), this, SLOT(close()));

  // Set layout
  QVBoxLayout *layout = new QVBoxLayout;

  QLabel *descLabel = new QLabel(this);
  descLabel->setText("Current system clock\nfrom continous data");
  layout->addWidget(descLabel);
  clock = new QLabel(this);
  clock->setFont(QFont("Arial", 48, QFont::Bold));
  layout->addWidget(clock);

  // Set layout in QWidget
  QWidget *window = new QWidget();
  window->setLayout(layout);

  // Set QWidget as the central layout of the main window
  setCentralWidget(window);

  statusBar()->showMessage("Processing arguments.");

  QString serverAddress, serverPort, configFilename;
  int optionInd = 1;
  while (optionInd < arguments.length()) {
      qDebug() << "Option: " << arguments.at(optionInd);
      if (arguments.length() > optionInd+1 )
        qDebug() << "Option(+1): " << arguments.at(optionInd+1);
      if ((arguments.at(optionInd).compare("-serverAddress",Qt::CaseInsensitive)==0) && (arguments.length() > optionInd+1)) {
          serverAddress = arguments.at(optionInd+1);
          optionInd++;
      } else if ((arguments.at(optionInd).compare("-serverPort",Qt::CaseInsensitive)==0) && (arguments.length() > optionInd+1)) {
          serverPort = arguments.at(optionInd+1);
          optionInd++;
      }
      else if ((arguments.at(optionInd).compare("-trodesConfig",Qt::CaseInsensitive)==0) && (arguments.length() > optionInd+1)) {
          configFilename = arguments.at(optionInd+1);
          optionInd++;
      }
      optionInd++;
  }

  statusBar()->showMessage("Establishing Trodes interface.");

  TrodesMessageInterface *trodesMessages = new TrodesMessageInterface(configFilename, serverAddress, serverPort);
  QThread *msgThread = new QThread();
  trodesMessages->moveToThread(msgThread);
  connect(msgThread, SIGNAL(started()), trodesMessages, SLOT(setup()));
  connect(trodesMessages, SIGNAL(quitReceived()), this, SLOT(close()));
  connect(trodesMessages, SIGNAL(statusMessage(QString)), this, SLOT(changeStatusBarMessage(QString)));
  connect(trodesMessages, SIGNAL(newTime(quint32)), this, SLOT(changeTimestampLabel(quint32)));
  msgThread->start();

  changeTimestampLabel(0);
}

MainWindow::~MainWindow()
{
}

void MainWindow::changeStatusBarMessage(QString msg)
{
    statusBar()->showMessage(msg);
}

void MainWindow::changeTimestampLabel(quint32 time)
{
    uint seconds = time / (sourceSamplingRate);
    uint minutes = (seconds / (60));
    uint hours = minutes / 60;
    minutes = minutes % 60;
    seconds = seconds % 60;
    clock->setText(QString("%1:%2:%3").arg(hours)
                   .arg((uint)minutes,2,10,QChar('0'))
                   .arg((uint)seconds,2,10,QChar('0')));
}




//void MainWindow::sendNewAnimalPosition(quint32 time, qint16 x, qint16 y) {
//    //Here we send out the animal's position to all modules that have requested that data
//    if (moduleNet->dataServerStarted) {
//        //qDebug() << "Position" << time << x << y;
//        for (int i = 0; i < moduleNet->dataServer->messageHandlers.length(); i++) {
//            //first check if streaming is turned on by the recieving module
//            if (moduleNet->dataServer->messageHandlers[i]->isModuleDataStreamingOn()) {
//                moduleNet->dataServer->messageHandlers[i]->sendAnimalPosition(time, x, y, cameraNum);
//            }
//        }
//    }
//}
