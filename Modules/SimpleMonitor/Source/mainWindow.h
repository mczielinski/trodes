#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QtWidgets>

class MainWindow : public QMainWindow
{
  Q_OBJECT

public:
  explicit MainWindow(QStringList arguments, QWidget *parent = 0);
  ~MainWindow();

  QLabel *clock;

public slots:
  void changeStatusBarMessage(QString);
  void changeTimestampLabel(quint32 time);

signals:

private:
};

#endif // MAINWINDOW_H
