/*
Trodes is a free, open-source neuroscience data collection and experimental control toolbox

Copyright (C) 2012 Mattias Karlsson

This program is free software: you can redistribute it and/or modify
                               it under the terms of the GNU General Public License as published by
                               the Free Software Foundation, either version 3 of the License, or
                               (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include "console.h"

#include <QScrollBar>
#include <QtGui>
#include <QtCore/QDebug>
#include <QMimeData>


Console::Console(QWidget *parent)
    : QTextEdit(parent)
    , localEchoEnabled(true)
    ,currentlySendingMessage(false)
    ,hasReceivedTime(false)
    ,waitingForDelayCalc(false)
    ,waitingForCompileSignal(false)
{
    document()->setMaximumBlockCount(100);
    QPalette p = palette();
    p.setColor(QPalette::Background, Qt::gray);

    //p.setColor(QPalette::Base, Qt::gray);
    p.setColor(QPalette::Text, Qt::black);

    setPalette(p);
    sendTimer = new QTimer(this);
    connect(sendTimer,SIGNAL(timeout()),this,SLOT(sendNextByte()));
    receivedTime = 0;
    estimatedDelay = 0;
    setFontPointSize(10);

}

void Console::disconnected() {
    receivedTime = 0;
    estimatedDelay = 0;
    currentSendMessage.clear();
    hasReceivedTime = false;

}

void Console::resizeEvent(QResizeEvent *event) {
    QRect currentGeometry = this->geometry();

    /*
    if (currentGeometry.width() < 500) {

        QTextCursor cursor = textCursor();
        selectAll();
        setFontPointSize(8);
        setTextCursor( cursor );

    } else {

        QTextCursor cursor = textCursor();
        selectAll();
        setFontPointSize(12);
        setTextCursor( cursor );
    }*/

    QTextEdit::resizeEvent(event);
}

void Console::putData(const QByteArray &data)
{
    //Data comes in directly from the serial line here

    bool gotBeginSessionMessage = false;
    serialInputStream.append(data);
    //Pick out complete lines from the input stream
    //and search for stateScript messages
    bool moreLinesLeft = true;
    bool gotCompile = false;

    while (moreLinesLeft) {
        if (serialInputStream.contains("\r\n")) {
            //Line terminator detected
            int index = serialInputStream.indexOf("\r\n");
            QString tmpLine =  serialInputStream.mid(0,index);
            serialInputStream.remove(0,index+2);
            emit newLineReceived(tmpLine);
            lastLineReceived = tmpLine;


            //Check if we are in the middle of the clock sync routine.
            //1) Upon connection, we send the "clock(reset)" command.
            //2) the controller sends "Clock reset to 0" string
            //3) we send "clock()" and start a timer.
            //4) the controller tells us the current time and we time the total time from send to receive"
            //5) we now know the complete loop time, and we estimate the delay as half of that


            if (tmpLine.contains("Clock reset to 0")) {
                //Just got reset statement.

                QString sendLine = QString("clock();\r\n");
                emit getData(sendLine.toLocal8Bit());
                //start timer
                localTime.start();
                waitingForDelayCalc = true;
                //insertPlainText(sendLine);

            } else if ((tmpLine.contains("current time"))&&(waitingForDelayCalc)) {

                qint64 timeElapsed = localTime.nsecsElapsed();
                estimatedDelay = (timeElapsed/2)/1000;
                QStringList splitLine = tmpLine.split(" ");
                bool foundTime = false;
                receivedTime = splitLine.at(0).toInt(&foundTime);

                if (foundTime) {
                    qDebug() << "Estimated line delay in uS: " << estimatedDelay << " ";
                    //emit newTime(localTime.elapsed()+receivedTime+round(((double)estimatedDelay)/1000));
                    hasReceivedTime = true;
                }
                waitingForDelayCalc = false;

            } else if ((tmpLine.contains("Ready to begin session"))) {
                gotBeginSessionMessage = true;


            } else if (tmpLine.contains("~")) {
                gotCompile = true;

            } else {
                //Check if the line has a timestamp
                //if so, recalibrate the local clock

                QStringList splitLine = tmpLine.split(" ");
                bool foundTime = false;
                qint64 tmpTime = splitLine.at(0).toInt(&foundTime);

                if (foundTime) {

                    receivedTime = tmpTime;
                    localTime.restart();

                }
            }


        } else {
            moreLinesLeft = false;
        }

    }

    if (!currentlySendingMessage || waitingForCompileSignal) {
        insertPlainText(QString(data));
        QScrollBar *bar = verticalScrollBar();
        bar->setValue(bar->maximum());

    } else {
        //Hold the message until we are done sending data,
        //and display it later.
        returnMessage.append(data);
    }

    /*
    if (gotBeginSessionMessage) {
        emit sendFinished();
    }*/

    if (gotCompile) {
        waitingForCompileSignal = false;
    }


}

quint32 Console::currentTime() {
    if (hasReceivedTime) {
        return localTime.elapsed()+receivedTime+round(((double)estimatedDelay)/1000);
    } else {
        return 0;
    }
}


void Console::sendData(const QByteArray &data)
{
    //For sending long scripts, in order to prevent the microcontroller
    //buffer from filling up, we schedule the send slowly with a timer
    currentSendMessage.append(data);
    if (!currentlySendingMessage) {
        currentlySendingMessage = true;
        numCallsPastDuringWait = 0;
        compileError = false;
        sendTimer->start(1);
    }
    //insertPlainText(QString(data));
    //emit getData(data);
}

void Console::sendString(QString text) {
    sendData(text.toLocal8Bit());
}

void Console::sendNextByte() {
    if (currentSendMessage.length() > 0) {
        if (waitingForCompileSignal) {
            //waiting for compile signal-- increment counter
            //if the counter reaches a set value, assume a compile error happened and
            //get rid of the rest of the text waiting to be sent.
            numCallsPastDuringWait++;
            if (numCallsPastDuringWait > 200) {
                currentSendMessage.clear();
                compileError = true;
                currentlySendingMessage = false;
                sendTimer->stop();
                waitingForCompileSignal = false;
                numCallsPastDuringWait = 0;
                qDebug() << "Compile failed";
                QMessageBox messageBox;
                messageBox.critical(0,"Error","An error occured while attempting to compile!");
                messageBox.setFixedSize(500,200);

                return;
            }
        } else {
            numCallsPastDuringWait = 0;
        }
        QByteArray sendByte;
        sendByte.setRawData(currentSendMessage.data(),1);
        if ((*currentSendMessage.data() < 32) || (!waitingForCompileSignal)) {

            if (*sendByte.data() == 37) {
                //% comment -- ignore rest of line
                while ((*currentSendMessage.data() != 10) && currentSendMessage.length() > 0) {
                    currentSendMessage.remove(0,1);
                }
                return;
            }

            insertPlainText(QString(sendByte));

            //Change tabs to spaces
            if (*sendByte.data() == 9) {
                *sendByte.data() = 32;
            }


            emit getData(sendByte);
            if (*currentSendMessage.data() == 59 && hasReceivedTime && currentSendMessage.length() > 5) {

                waitingForCompileSignal = true;
                //qDebug() << "Waiting " << currentSendMessage.length();

            }

            currentSendMessage.remove(0,1);

            QScrollBar *bar = verticalScrollBar();
            bar->setValue(bar->maximum());
        }
    } else {
        currentlySendingMessage = false;
        sendTimer->stop();
        if (~compileError) {
            qDebug() << "Compile succeeded";
            emit sendFinished();
        }
        waitingForCompileSignal = false;
        if (returnMessage.length() > 0) {
            insertPlainText(QString(returnMessage));
            QScrollBar *bar = verticalScrollBar();
            bar->setValue(bar->maximum());
            returnMessage.clear();
        }
    }
}

void Console::setLocalEchoEnabled(bool set)
{
    localEchoEnabled = set;
}

void Console::keyPressEvent(QKeyEvent *e)
{
    char backspaceVal = 8;

    if (e->matches(QKeySequence::Paste)) { //if the paste key sequence was pressed
        paste();
    } else {
        switch (e->key()) {
        case Qt::Key_Backspace:

            if (localEchoEnabled) {
                QTextEdit::keyPressEvent(e);
            }

            //emit getData(QByteArray(&backspaceVal)); //send char 8 to the controller
            emit getData(e->text().toLocal8Bit());
            //e->accept();
            break;

        case Qt::Key_Left:
        case Qt::Key_Right:
        case Qt::Key_Up:
        case Qt::Key_Down:
            // skip processing
            break;
        default:
            if (localEchoEnabled)
                QTextEdit::keyPressEvent(e);
            emit getData(e->text().toLocal8Bit());


        }
    }
}

void Console::insertFromMimeData(const QMimeData* source) {

    //paste command was given, so we send the data
    QByteArray pasteText = source->data("text/plain");
    sendData(pasteText);
    //putData(pasteText);
    //emit getData(pasteText);

}

/*
void Console::mousePressEvent(QMouseEvent *e)
{
    Q_UNUSED(e)
    setFocus();
}

void Console::mouseDoubleClickEvent(QMouseEvent *e)
{
    Q_UNUSED(e)
}

void Console::contextMenuEvent(QContextMenuEvent *e)
{
    Q_UNUSED(e)
}
*/
