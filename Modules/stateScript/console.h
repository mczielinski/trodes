/*
Trodes is a free, open-source neuroscience data collection and experimental control toolbox

Copyright (C) 2012 Mattias Karlsson

This program is free software: you can redistribute it and/or modify
                               it under the terms of the GNU General Public License as published by
                               the Free Software Foundation, either version 3 of the License, or
                               (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef CONSOLE_H
#define CONSOLE_H

#include <QPlainTextEdit>
#include <QtGui>
#include <QMessageBox>

class Console : public QTextEdit {

    Q_OBJECT

signals:
    void getData(const QByteArray &data);

public:
    explicit Console(QWidget *parent = 0);

    void putData(const QByteArray &data); //for displaying text only
    void sendData(const QByteArray &data); //for displaying text and sending via serial;

    void setLocalEchoEnabled(bool set);
    QString lastLineReceived;
    quint32 currentTime();

protected:
    virtual void keyPressEvent(QKeyEvent *e);
    virtual void insertFromMimeData(const QMimeData *source);
    void resizeEvent(QResizeEvent *);
    /*
    virtual void mousePressEvent(QMouseEvent *e);
    virtual void mouseDoubleClickEvent(QMouseEvent *e);
    virtual void contextMenuEvent(QContextMenuEvent *e);
    */
private:
    bool waitingForCompileSignal;
    int numCallsPastDuringWait;
    bool compileError;
    bool localEchoEnabled;
    QByteArray currentSendMessage;
    QByteArray returnMessage;
    QTimer *sendTimer;
    bool currentlySendingMessage;
    QString   serialInputStream;
    QElapsedTimer localTime;
    qint64 receivedTime;
    qint64 estimatedDelay;
    bool hasReceivedTime;
    bool waitingForDelayCalc;

private slots:
    void sendNextByte();

public slots:

    void sendString(QString text);
    void disconnected();


signals:
    void newLineReceived(QString text);
    void newTime(qint64 time);
    void sendFinished();

};

#endif // CONSOLE_H
