/*
Trodes is a free, open-source neuroscience data collection and experimental control toolbox

Copyright (C) 2012 Mattias Karlsson

This program is free software: you can redistribute it and/or modify
                               it under the terms of the GNU General Public License as published by
                               the Free Software Foundation, either version 3 of the License, or
                               (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include <QtGui>
#include "dialogs.h"
#include "QGridLayout"
#include <QtSerialPort/QSerialPortInfo>
#include <QFileDialog>
#include <QPaintDevice>



CameraDialog::CameraDialog(bool connected, QString programPath,QStringList arguments, QWidget *parent)
    :QWidget(parent),
     currentPath(programPath),
     currentArguments(arguments) {


    pathEdit = new QLineEdit();
    pathEdit->setFixedWidth(130);
    pathEdit->setText(currentPath);
    argumentsEdit = new QLineEdit();
    argumentsEdit->setFixedWidth(80);
    argumentsEdit->setText(currentArguments.join(" "));

    endButton = new QPushButton();
    endButton->setText("Stop");
    startButton = new QPushButton();
    startButton->setText("Start");

    pathDisplay = new QLabel();
    pathDisplay->setText("Path");
    argumentsDisplay = new QLabel();
    argumentsDisplay->setText("Arguments");



    QGridLayout *mainLayout = new QGridLayout;
    QGridLayout *editLayout = new QGridLayout;

    QGridLayout *buttonLayout = new QGridLayout;

    editLayout->setContentsMargins(0,0,0,0);
    editLayout->setMargin(0);
    editLayout->setSpacing(0);

    buttonLayout->setContentsMargins(0,0,0,0);
    buttonLayout->setMargin(0);
    buttonLayout->setSpacing(2);


    editLayout->addWidget(pathDisplay,0,0,Qt::AlignCenter);
    editLayout->addWidget(argumentsDisplay,0,1,Qt::AlignCenter);
    editLayout->addWidget(pathEdit,1,0,Qt::AlignCenter);
    editLayout->addWidget(argumentsEdit,1,1,Qt::AlignCenter);
    buttonLayout->addWidget(endButton,0,1,Qt::AlignHCenter);
    buttonLayout->addWidget(startButton,0,2,Qt::AlignHCenter);
    buttonLayout->setColumnStretch(0,1);


    mainLayout->addLayout(editLayout,1,0);
    mainLayout->addLayout(buttonLayout,2,0);
    mainLayout->setMargin(10);

    connect(startButton,SIGNAL(pressed()),this,SLOT(startButtonPressed()));
    connect(endButton,SIGNAL(pressed()),this,SLOT(endButtonPressed()));
    //connect(addressSelector,SIGNAL(activated(int)),this,SLOT(updateValues(int)));

    setLayout(mainLayout);
    setWindowTitle(tr("Camera"));

    if (connected) {

        pathEdit->setEnabled(false);
        argumentsEdit->setEnabled(false);

        startButton->setEnabled(false);
        pathDisplay->setEnabled(false);
        argumentsDisplay->setEnabled(false);
    } else {
        endButton->setEnabled(false);

    }

}

/*
void SocketDialog::updateValues(int currentSelectionInd) {

    //Called whenever something is selected from the drop down menu of addresses
    QString currentSelection = trodesHosts[currentSelectionInd];
    if (!currentSelection.isEmpty()) {
        QStringList currentItems = currentSelection.split(" ");
        if (currentItems.length() >= 2) {
            addressEdit->setText(currentItems[0]);
            portEdit->setText(currentItems[1]);

        }
    }
}
*/

void CameraDialog::startButtonPressed() {


    currentPath = pathEdit->text();
    currentArguments = argumentsEdit->text().split(" ");

    emit cameraStart(currentPath,currentArguments);
}

void CameraDialog::endButtonPressed() {
    emit cameraStop();
    close();
}


void CameraDialog::connectionSuccessfull() {

     //we are connected, so close the window

     close();

}

void CameraDialog::closeEvent(QCloseEvent *event) {

    emit windowOpenState(false);
    event->accept();
    emit windowClosed();
}


//-----------------------------------------------------
SocketDialog::SocketDialog(TrodesClient* client ,bool connected, QWidget *parent)
    :QWidget(parent),
      currentAddress(client->getCurrentAddress()),
     currentPort(client->getCurrentPort()) {

    addressSelector = new QComboBox();

    if (!connected) {

        /*
        trodesHosts = client->findAvailableHosts();

        for (int i=0; i < trodesHosts.length(); i++) {

            QString tmpString = trodesHosts[i];
            QStringList stringParts = tmpString.split(" ");
            if (stringParts.length() > 2) {
                //a string identifier was given by the host program
                //addressSelector->addItem(stringParts[2]);
                QString itemText;
                for (int parts = 2; parts < stringParts.length(); parts++) {
                    itemText.append(stringParts[parts]);
                    itemText.append(" ");

                }
                addressSelector->addItem(itemText);
            } else {
                //no identifier
                addressSelector->addItem("No ID");
            }
        } */
    } else {
        addressSelector->addItem("Connected");
    }


    //addressSelector->addItems(client->findAvailableHosts());
    /*
    QString currentSelection = addressSelector->currentText();
    if ((!connected) && (!currentSelection.isEmpty())) {
        QStringList currentItems = currentSelection.split(" ");
        if (currentItems.length() >= 2) {
            currentAddress = currentItems[0];
            currentPort = currentItems[1].toUInt();
        }
    }*/

    addressEdit = new QLineEdit();
    addressEdit->setFixedWidth(130);
    //addressEdit->setText(currentAddress);
    portEdit = new QLineEdit();
    portEdit->setFixedWidth(80);
    //portEdit->setText(QString("%1").arg(currentPort));
    portEdit->setValidator(new QIntValidator(1, 65535, this));
    //addressSelector = new QComboBox();
    //addressSelector->addItems(client->findAvailableHosts());


    disconnectButton = new QPushButton();
    disconnectButton->setText("Disconnect");
    connectButton = new QPushButton();
    connectButton->setText("Connect");

    addressDisplay = new QLabel();
    addressDisplay->setText("Address");
    portDisplay = new QLabel();
    portDisplay->setText("Port");



    QGridLayout *mainLayout = new QGridLayout;
    QGridLayout *socketLayout = new QGridLayout;
    QGridLayout *dropDownLayout = new QGridLayout;
    QGridLayout *buttonLayout = new QGridLayout;

    socketLayout->setContentsMargins(0,0,0,0);
    socketLayout->setMargin(0);
    socketLayout->setSpacing(0);
    buttonLayout->setContentsMargins(0,0,0,0);
    buttonLayout->setMargin(0);
    buttonLayout->setSpacing(2);


    socketLayout->addWidget(addressDisplay,0,0,Qt::AlignCenter);
    dropDownLayout->addWidget(addressSelector,0,0,Qt::AlignCenter);
    socketLayout->addWidget(portDisplay,0,1,Qt::AlignCenter);
    socketLayout->addWidget(addressEdit,1,0,Qt::AlignCenter);
    socketLayout->addWidget(portEdit,1,1,Qt::AlignCenter);
    buttonLayout->addWidget(disconnectButton,0,1,Qt::AlignHCenter);
    buttonLayout->addWidget(connectButton,0,2,Qt::AlignHCenter);
    buttonLayout->setColumnStretch(0,1);

    mainLayout->addLayout(dropDownLayout,0,0);
    mainLayout->addLayout(socketLayout,1,0);
    mainLayout->addLayout(buttonLayout,2,0);
    mainLayout->setMargin(10);

    connect(connectButton,SIGNAL(pressed()),this,SLOT(connectButtonPressed()));
    connect(disconnectButton,SIGNAL(pressed()),this,SLOT(disconnectButtonPressed()));
    connect(addressSelector,SIGNAL(activated(int)),this,SLOT(updateValues(int)));

    setLayout(mainLayout);
    setWindowTitle(tr("Connect"));

    if (connected) {
        currentAddress = client->getCurrentAddress();
        currentPort = client->getCurrentPort();
        portEdit->setText(QString("%1").arg(currentPort));
        addressEdit->setText(currentAddress);

        addressEdit->setEnabled(false);
        portEdit->setEnabled(false);
        addressSelector->setEnabled(false);
        connectButton->setEnabled(false);
        addressDisplay->setEnabled(false);
        portDisplay->setEnabled(false);
    } else {
        disconnectButton->setEnabled(false);
        if (trodesHosts.length() > 0) {
            updateValues(addressSelector->currentIndex());
        }
    }

}

void SocketDialog::updateValues(int currentSelectionInd) {

    //Called whenever something is selected from the drop down menu of addresses
    QString currentSelection = trodesHosts[currentSelectionInd];
    if (!currentSelection.isEmpty()) {
        QStringList currentItems = currentSelection.split(" ");
        if (currentItems.length() >= 2) {
            addressEdit->setText(currentItems[0]);
            portEdit->setText(currentItems[1]);

        }
    }
}

void SocketDialog::connectButtonPressed() {


    currentAddress = addressEdit->text();
    currentPort = portEdit->text().toUInt();
    emit tcpConnect(currentAddress,currentPort);
}

void SocketDialog::disconnectButtonPressed() {
    emit disconnect();
    close();
}


void SocketDialog::connectionSuccessfull() {

     //we are connected, so close the window

     close();

}

void SocketDialog::closeEvent(QCloseEvent *event) {

    emit windowOpenState(false);
    event->accept();
    emit windowClosed();
}


//--------------------------------------------------------------
ConnectDialog::ConnectDialog(QString currentPortIn, bool connected, QWidget *parent)
    :QWidget(parent),
     currentPort(currentPortIn) {

    //portSelect = new PortComboBox();
    portSelect = new QComboBox();
    portSelect->setFixedWidth(150);
    disconnectButton = new QPushButton();
    disconnectButton->setText("Disconnect");
    connectButton = new QPushButton();
    connectButton->setText("Connect");

    portDisplay = new QLabel();
    portDisplay->setText("Port");

    QGridLayout *mainLayout = new QGridLayout;
    QGridLayout *portLayout = new QGridLayout;
    QGridLayout *buttonLayout = new QGridLayout;

    portLayout->setContentsMargins(0,0,0,0);
    portLayout->setMargin(0);
    portLayout->setSpacing(0);
    buttonLayout->setContentsMargins(0,0,0,0);
    buttonLayout->setMargin(0);
    buttonLayout->setSpacing(2);


    portLayout->addWidget(portDisplay,0,0,Qt::AlignCenter);
    portLayout->addWidget(portSelect,1,0,Qt::AlignCenter);

    buttonLayout->addWidget(disconnectButton,0,1,Qt::AlignHCenter);
    buttonLayout->addWidget(connectButton,0,2,Qt::AlignHCenter);
    buttonLayout->setColumnStretch(0,1);

    mainLayout->addLayout(portLayout,0,0);
    mainLayout->addLayout(buttonLayout,1,0);
    mainLayout->setMargin(10);

    connect(connectButton,SIGNAL(pressed()),this,SLOT(connectButtonPressed()));
    connect(disconnectButton,SIGNAL(pressed()),this,SLOT(disconnectButtonPressed()));

    setLayout(mainLayout);
    //setWindowTitle(tr("Controller"));

    if (connected) {
        //if we are already connected, just populate the combobox with that port
        //and disable it
        portSelect->addItem(currentPortIn);
        portSelect->setEnabled(false);
        connectButton->setEnabled(false);
    } else {
        //Otherwise, populate the port
        disconnectButton->setEnabled(false);
        fillPortsInfo();
    }

}

void ConnectDialog::connectButtonPressed() {

    currentPort = portSelect->currentText();
    emit connectButtonPressed(currentPort);
}

void ConnectDialog::disconnectButtonPressed() {
    emit disconnect();
    close();
}


void ConnectDialog::connectionSuccessfull() {

     //we are connected, so close the window

     close();

}

void ConnectDialog::closeEvent(QCloseEvent *event) {

    emit windowOpenState(false);
    event->accept();
    emit windowClosed();

}

void ConnectDialog::fillPortsInfo() {

    portSelect->clear();
    foreach (const QSerialPortInfo &info, QSerialPortInfo::availablePorts()) {
        QStringList list;
        list << info.portName()
             << info.description()
             << info.manufacturer()
             << info.systemLocation()
             << (info.vendorIdentifier() ? QString::number(info.vendorIdentifier(), 16) : QString())
             << (info.productIdentifier() ? QString::number(info.productIdentifier(), 16) : QString());

        portSelect->addItem(list.first(), list);
    }
    portSelect->addItem("ECU");
}



//-----------------------------------------------------------

//-----------------------------------------------------
LanguageDialog::LanguageDialog(QString currentInitSelection, QWidget *parent)
    :QWidget(parent),
    currentInitSelection(currentInitSelection){


    //currentLanguage = "";
    availableLanguages << "Matlab" << "Python";
    languageSelector = new QComboBox();
    languageSelector->setMinimumWidth(200);
    languageSelector->addItems(availableLanguages);

    QSettings settings(QLatin1String("SpikeGadgets"), QLatin1String("stateScript"));
    settings.beginGroup(QLatin1String("paths"));
    QStringList tempPathList = settings.value(QLatin1String("localScriptLanguages")).toStringList();
    if (!tempPathList.isEmpty()) {
        languagePaths = tempPathList;
    } else {
        languagePaths << "" << "";
    }

    QString preferredLanguage = settings.value(QLatin1String("preferredLanguage")).toString();
    if (!preferredLanguage.isEmpty()) {
        int index = availableLanguages.indexOf(preferredLanguage);
        if ((index > -1)&&(index < languageSelector->count())) {
            languageSelector->setCurrentIndex(index);
        }
    }

    settings.endGroup();


    pathEdit = new QLineEdit();
    pathEdit->setMinimumWidth(300);
    pathEdit->setText(languagePaths[languageSelector->currentIndex()]);
    currentPath = languagePaths[languageSelector->currentIndex()];
    connect(pathEdit,SIGNAL(returnPressed()),this,SLOT(saveCurrentPath()));

    selectPathButton = new QPushButton();
    selectPathButton->setText("...");
    connect(selectPathButton,SIGNAL(pressed()),this,SLOT(openFileDialog()));

    /*
    disconnectButton = new QPushButton();
    disconnectButton->setText("Disconnect");
    connectButton = new QPushButton();
    connectButton->setText("Start");
    */
    cancelButton = new QPushButton();
    cancelButton->setText("Cancel");
    okButton = new QPushButton();
    okButton->setText("Ok");

    pathDisplay = new QLabel();
    pathDisplay->setText("Location:");

    QGridLayout *mainLayout = new QGridLayout;
    QGridLayout *pathLayout = new QGridLayout;
    QGridLayout *dropDownLayout = new QGridLayout;
    QGridLayout *buttonLayout = new QGridLayout;

    //pathLayout->setContentsMargins(0,0,0,0);
    //pathLayout->setMargin(0);
    //pathLayout->setSpacing(0);
    //pathLayout->setContentsMargins(0,0,0,0);
    //pathLayout->setMargin(0);
    //pathLayout->setSpacing(2);


    pathLayout->addWidget(pathDisplay,0,0,Qt::AlignCenter);
    dropDownLayout->addWidget(languageSelector,0,0,Qt::AlignCenter);
    pathLayout->addWidget(pathEdit,1,0,Qt::AlignCenter);
    pathLayout->addWidget(selectPathButton,1,1,Qt::AlignCenter);
    pathLayout->setColumnStretch(0,1);


    //buttonLayout->addWidget(disconnectButton,0,1,Qt::AlignHCenter);
    //buttonLayout->addWidget(connectButton,0,2,Qt::AlignHCenter);
    buttonLayout->addWidget(cancelButton,0,1,Qt::AlignHCenter);
    buttonLayout->addWidget(okButton,0,2,Qt::AlignHCenter);
    buttonLayout->setColumnStretch(0,1);

    mainLayout->addLayout(dropDownLayout,0,0);
    mainLayout->addLayout(pathLayout,1,0);
    mainLayout->addLayout(buttonLayout,2,0);
    mainLayout->setMargin(10);

    //connect(connectButton,SIGNAL(pressed()),this,SLOT(connectButtonPressed()));
    //connect(disconnectButton,SIGNAL(pressed()),this,SLOT(disconnectButtonPressed()));
    connect(okButton,SIGNAL(pressed()),this,SLOT(okButtonPressed()));
    connect(cancelButton,SIGNAL(pressed()),this,SLOT(cancelButtonPressed()));
    connect(languageSelector,SIGNAL(activated(int)),this,SLOT(updateValues(int)));

    setLayout(mainLayout);
    pathEdit->setSizePolicy(QSizePolicy::Expanding,QSizePolicy::Fixed);
    setWindowTitle(tr("Select observer language"));

    /*
    if (connected) {
        currentAddress = client->getCurrentAddress();
        currentPort = client->getCurrentPort();
        portEdit->setText(QString("%1").arg(currentPort));
        addressEdit->setText(currentAddress);

        addressEdit->setEnabled(false);
        portEdit->setEnabled(false);
        addressSelector->setEnabled(false);
        connectButton->setEnabled(false);
        addressDisplay->setEnabled(false);
        portDisplay->setEnabled(false);
    } else {
        disconnectButton->setEnabled(false);
        if (trodesHosts.length() > 0) {
            updateValues(addressSelector->currentIndex());
        }
    }*/

}

void LanguageDialog::updateValues(int index) {
    pathEdit->setText(languagePaths[index]);
    currentPath = languagePaths[index];
}


void LanguageDialog::okButtonPressed() {

    QSettings settings(QLatin1String("SpikeGadgets"), QLatin1String("stateScript"));

    settings.beginGroup(QLatin1String("paths"));
    settings.setValue(QLatin1String("preferredLanguage"), availableLanguages[languageSelector->currentIndex()]);
    settings.endGroup();

    emit setLanguage(availableLanguages[languageSelector->currentIndex()],pathEdit->text());
    close();
}

void LanguageDialog::cancelButtonPressed() {
    close();
}

void LanguageDialog::connectButtonPressed() {

    QSettings settings(QLatin1String("SpikeGadgets"), QLatin1String("stateScript"));

    settings.beginGroup(QLatin1String("paths"));
    settings.setValue(QLatin1String("preferredLanguage"), availableLanguages[languageSelector->currentIndex()]);
    settings.endGroup();


    emit startEngine(availableLanguages[languageSelector->currentIndex()],pathEdit->text(),currentInitSelection);
    //currentAddress = addressEdit->text();
    //currentPort = portEdit->text().toUInt();
    //emit tcpConnect(currentAddress,currentPort);
}

void LanguageDialog::disconnectButtonPressed() {
    emit disconnect();
    close();
}


void LanguageDialog::connectionSuccessfull() {

     //we are connected, so close the window

     close();

}

void LanguageDialog::saveCurrentPath() {

    QString fileName = pathEdit->text();
    currentPath = fileName;
    languagePaths[languageSelector->currentIndex()] = fileName;
    QSettings settings(QLatin1String("SpikeGadgets"), QLatin1String("stateScript"));

    settings.beginGroup(QLatin1String("paths"));
    settings.setValue(QLatin1String("localScriptLanguages"), languagePaths);
    settings.endGroup();

}

void LanguageDialog::openFileDialog() {
    //dialog to find language executable


    if (!QDir(currentPath).exists()) {

        #ifdef linux
        currentPath = "/Home/";
        #endif
        #ifdef WIN32
        currentPath = "C:\\";
        #endif
        #ifdef __APPLE__
        currentPath = "/Users/";
        #endif
    }



    QString fileName = QFileDialog::getOpenFileName(this, tr("Create File"),currentPath);

    if (!(fileName == "")) {
        pathEdit->setText(fileName);

        saveCurrentPath();
        //emit recordFileOpened(fileName);
    }
}



void LanguageDialog::closeEvent(QCloseEvent *event) {

    emit windowOpenState(false);
    event->accept();
    emit windowClosed();
}

//---------------------------------------------

TrodesButton::TrodesButton(QWidget *parent)
    :QPushButton(parent) {

    setStyleSheet("QPushButton {border: 2px solid lightgray;"
                          "border-radius: 4px;"
                          "padding: 2px;}"
                          "QPushButton::pressed {border: 2px solid gray;"
                          "border-radius: 4px;"
                          "padding: 2px;}");

    QFont buttonFont;
    buttonFont.setPointSize(12);


}





