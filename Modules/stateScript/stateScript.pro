#-------------------------------------------------
#
# Project created by QtCreator 2014-02-10T14:04:36
#
#-------------------------------------------------

QT       += core gui serialport network xml

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = stateScript
TEMPLATE = app

CONFIG += DISTRIBUTIONMODE

DISTRIBUTIONMODE {
DEFINES += FILEPATHSINONEFOLDER
}

#stuff specific for mac
macx {
INCLUDEPATH += ../../Source/Libraries/Mac
HEADERS    += ../../Source/Libraries/Mac/WinTypes.h
HEADERS    += ../../Source/Libraries/Mac/ftd2xx.h
LIBS       += /usr/local/lib/libftd2xx.dylib
ICON        = trodesMacIcon_ss.icns
OTHER_FILES += Info.plist
QMAKE_INFO_PLIST += Info.plist
}

unix {
INCLUDEPATH += ../../Source/Libraries/Linux
INCLUDEPATH += /usr/local/include
LIBS += -L/usr/local/lib #store ftdi lib here
HEADERS    += ../../Source/Libraries/Linux/WinTypes.h
HEADERS    += ../../Source/Libraries/Linux/ftd2xx.h

LIBS       += -lftd2xx
}

win32 {
RC_ICONS += trodesWindowsIcon_ss.ico
INCLUDEPATH += ../../Source/Libraries/Windows
HEADERS    += ftd2xx.h
LIBS      += -L../../Source/Libraries/Windows
LIBS      += -lftd2xx
#LIBS       += -L$$quote($$PWD../../Source/Libraries/Windows) -lftd2xx
}


INCLUDEPATH  += ../../Source/src-main
INCLUDEPATH  += ../../Source/src-config

SOURCES += main.cpp\
        mainwindow.cpp \
    console.cpp \
    scripteditor.cpp \
    highlighter.cpp \
    localcallbackwidget.cpp \
    dialogs.cpp \
    ../../Source/src-main/trodessocket.cpp \
    ../../Source/src-config/configuration.cpp \
    fileselectorwindow.cpp

HEADERS  += mainwindow.h \
    console.h \
    scripteditor.h \
    highlighter.h \
    localcallbackwidget.h \
    dialogs.h \
    ../../Source/src-main/trodessocket.h \
    ../../Source/src-config/configuration.h \
    fileselectorwindow.h

