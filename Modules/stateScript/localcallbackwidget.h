/*
Trodes is a free, open-source neuroscience data collection and experimental control toolbox

Copyright (C) 2012 Mattias Karlsson

This program is free software: you can redistribute it and/or modify
                               it under the terms of the GNU General Public License as published by
                               the Free Software Foundation, either version 3 of the License, or
                               (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef LOCALCALLBACKWIDGET_H
#define LOCALCALLBACKWIDGET_H

#include <QtWidgets>
#include <QtGui>

class localCallbackWidget : public QTextEdit {

    Q_OBJECT

public:
    explicit localCallbackWidget(QWidget *parent = 0);
    ~localCallbackWidget();

protected:
    virtual void keyPressEvent(QKeyEvent *e);

private:
    QProcess *scriptingProgram;
    QByteArray currentEntryLine;
    QTextStream processOutputStream;
    bool programRunning;
    QString initiationCommand;
    QString scriptLanguage;
    QString comDirectory;
    bool    useComDir;
    quint64 numCharRead;
    QTimer *fileReadTimer;

private slots:
    void readDataFromProcess();
    void sendText(QByteArray text);
    void sendInitiationText();
    void readMatlabOutputFile();

public slots:
    void sendText(QString text);
    void startEngine(QString language, QString path, QString initCommand);
    void killEngine();

signals:

    void textEntered(QByteArray text);
    void stateScriptCommandReceived(QString text);
    void programStarted();
};

#endif // LOCALCALLBACKWIDGET_H
