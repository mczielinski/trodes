QT       += core gui xml widgets multimedia

cache()

TARGET = RewardGui
TEMPLATE = app
DESTDIR = .
CONFIG += c++11

INCLUDEPATH += ../../Source/src-config
INCLUDEPATH += ../../Source/src-main

SOURCES += main.cpp\
        rewardControl.cpp\
        sharedfunctions.cpp \
        ../../Source/src-config/configuration.cpp \
        ../../Source/src-main/trodesSocket.cpp



HEADERS  += \
    rewardControl.h \
    sharedinclude.h \
    ../../Source/src-config/configuration.h \
    ../../Source/src-main/trodesSocket.h \
    ../../Source/src-main/trodesSocketDefines.h
