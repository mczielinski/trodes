#-------------------------------------------------
#
# Project created by QtCreator 2014-08-20T20:54:50
#
#-------------------------------------------------

QT       += opengl core gui network xml multimedia

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = cameraModule
TEMPLATE = app

UI_DIR = ./ui
MOC_DIR = ./moc
OBJECTS_DIR = ./obj


INCLUDEPATH  += ../../Source/src-main
INCLUDEPATH  += ../../Source/src-config
INCLUDEPATH  += ./src

#unix: QMAKE_CXXFLAGS += -D__STDC_CONSTANT_MACROS
QMAKE_CFLAGS += -g -O3
# Requied for some C99 defines
DEFINES += __STDC_CONSTANT_MACROS

#-------------------------------------------------
# Not sure what these are needed for
macx {
ICON        = ./src/trodesMacIcon_camera.icns
OTHER_FILES += ./src/Info.plist
QMAKE_INFO_PLIST += ./src/Info.plist
}
#-------------------------------------------------

#-------------------------------------------------
# FFMPEG flags
unix {
INCLUDEPATH += /usr/local/include /usr/include
#LIBS += -L"/usr/local/lib" -L"/usr/lib" -lavcodec -lavformat -lswscale -lavutil -lbz2 -lswresample -lx264
#LIBS += -L"/usr/local/lib" -L"/usr/lib" -lavcodec -lavformat -lswscale -lavutil -lbz2 -lx264
LIBS += -L"/usr/local/lib" -L"/usr/lib" -lavcodec -lavformat -lswscale -lavutil -lx264
}

macx {
INCLUDEPATH += /usr/local/include
LIBS += -L"/usr/local/lib" -lavcodec -lavformat -lswscale -lavutil -lbz2
LIBS += -L"/usr/local/lib" -lx264

}

win32 {
RC_ICONS += ./src/trodesWindowsIcon_camera.ico
INCLUDEPATH += ../cameraModule/libraries/win32
INCLUDEPATH += ../cameraModule/libraries/win32/libx264
FFMPEG_LIBRARY_PATH = ../cameraModule/libraries/win32
LIBS += -L$$FFMPEG_LIBRARY_PATH
#LIBS      += -L./Libraries/Windows/ -lftd2xx
#LIBS       += -L$$quote($$PWD/libraries/win32) -lavutil -lavcodec -lavformat -lswscale
LIBS       += -lavutil -lavcodec -lavformat -lswscale -lx264-142
}
#-------------------------------------------------


#-------------------------------------------------
# Settings required for Allied Vision GigE libraries
CONFIG += AVTGIGECAMERA

AVTGIGECAMERA {
DEFINES += AVT_GIGE
SOURCES += src/avtWrapper.cpp
HEADERS += src/avtWrapper.h

macx {
LIBS += -L"$$PWD/libraries/osx/GigESDK/bin-pc/x64" -lPvAPI
INCLUDEPATH += "$$PWD/libraries/osx/GigESDK/inc-pc"

copy_libPvAPI.commands = "cp $$PWD/libraries/osx/GigESDK/bin-pc/x64/libPvAPI.dylib $$OUT_PWD/cameraModule.app/Contents/MacOS/"
first.depends = $(first) copy_libPvAPI
export(first.depends)
export(copy_libPvAPI.commands)
QMAKE_EXTRA_TARGETS += first copy_libPvAPI
}
win32 {
LIBS += -L"$$PWD/libraries/win32/GigESDK/lib-pc" -lPvAPI
INCLUDEPATH += "$$PWD/libraries/win32/GigESDK/inc-pc"
}

unix {
DEFINES += _x64
LIBS += -L"$$PWD/libraries/linux/GigESDK/bin-pc/x64" -lPvAPI
INCLUDEPATH += "$$PWD/libraries/linux/GigESDK/inc-pc"
}
}
#-------------------------------------------------


#----------------------------------------------------------
# Settings required for GigE Vision (aravis) interface

#CONFIG += GIGECAMERA

GIGECAMERA {
DEFINES += ARAVIS
QMAKE_CXXFLAGS += -std=gnu++11
SOURCES += src/araviswrapper.cpp
HEADERS += src/araviswrapper.h

unix: LIBS += -L/usr/local/lib/ -laravis-0.4

INCLUDEPATH += /usr/local/include/aravis-0.4
DEPENDPATH += /usr/local/include/aravis-0.4


unix:!macx: LIBS += -L/usr/lib/x86_64-linux-gnu/ -lglib-2.0 -lgobject-2.0

INCLUDEPATH += /usr/include/glib-2.0
DEPENDPATH += /usr/include/glib-2.0

INCLUDEPATH += /usr/lib/x86_64-linux-gnu/glib-2.0/include/

INCLUDEPATH += /usr/include/glib-2.0/gobject/
}
#-----------------------------------------------------------



SOURCES += src/main.cpp\
        src/mainwindow.cpp \
        src/videoDisplay.cpp \
        ../../Source/src-main/trodesSocket.cpp \
        ../../Source/src-config/configuration.cpp \
        src/videoDecoder.cpp \
        src/videoEncoder.cpp \
        src/dialogs.cpp \
        src/abstractCamera.cpp \
       src/webcamWrapper.cpp



HEADERS  += src/mainwindow.h \
        src/videoDisplay.h \
        ../../Source/src-main/trodesSocket.h \
        ../../Source/src-config/configuration.h \
        src/videoDecoder.h \
        src/videoEncoder.h \
        src/dialogs.h \
        src/abstractCamera.h \
        src/webcamWrapper.h









