/*
Trodes is a free, open-source neuroscience data collection and experimental control toolbox

Copyright (C) 2012 Mattias Karlsson

This program is free software: you can redistribute it and/or modify
                               it under the terms of the GNU General Public License as published by
                               the Free Software Foundation, either version 3 of the License, or
                               (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "mainwindow.h"


MainWindow::MainWindow(QStringList arguments, QWidget *parent)
    : QMainWindow(parent) {

    programIsClosing = false;
    sourceDialogOpen = false;
    toolsDialogOpen = false;

    trackingOn = false;
    loggingOn = false;
    currentTool = 1;  //fix tool
    justSeekedFlag = false;

    moduleInstance = 1;

    qDebug() << "Camera module starting.";

    //Place the window in the upper right hand corner.
    resize(400, 300);
    QScreen *screenInfo = QGuiApplication::primaryScreen();
    QRect ScreenRect = screenInfo->availableGeometry();
    int x;
    int y;

    //For some reason, window positioning is not the same across platforms.
#ifdef WIN32
    x = ScreenRect.right()-420;
    y = ScreenRect.top();
#else
    x = ScreenRect.right()-400;
    y = ScreenRect.top()-300;
#endif

    move(x,y);

QString lastCameraOpen;

    //Used the saved system settings from the last session as the default start settings
    //Any command-line inputs will override this below
    QSettings settings(QLatin1String("SpikeGadgets"), QLatin1String("cameraModule"));
    settings.beginGroup(QLatin1String("processor"));
    int tempThresh = settings.value(QLatin1String("thresh")).toInt();
    if (tempThresh < 0) {
        tempThresh = 0;
    }
    bool tmpTrackDark = settings.value(QLatin1String("trackDarkPixels")).toBool();
    trackSettings.currentThresh = tempThresh;
    trackSettings.trackDark = tmpTrackDark;


    int tempRing = settings.value(QLatin1String("ringSize")).toInt();
    if (tempRing < 0) {
        tempRing = 0;
    }
    bool tmpRingOn = settings.value(QLatin1String("ringOn")).toBool();
    trackSettings.currentRingSize = tempRing;
    trackSettings.ringOn = tmpRingOn;


    bool ok;
    int tempLEDColor = settings.value(QLatin1String("ledColorPair")).toInt(&ok);
    if (ok) {
        trackSettings.LEDColorPair = tempLEDColor;
    } else {
        trackSettings.LEDColorPair = LED_COLOR_WHITE_WHITE;
    }

    bool tmpTwoLEDs = settings.value(QLatin1String("twoLEDs")).toBool();
    trackSettings.twoLEDs = tmpTwoLEDs;

    settings.endGroup();

    clockRate = 30000;

    //Place the window where it was the last session
    settings.beginGroup(QLatin1String("position"));
    QRect tempPosition = settings.value(QLatin1String("position")).toRect();
    if (tempPosition.height() > 0) {
        setGeometry(tempPosition);
    }
    settings.endGroup();




    //Create a NULL networkConf variable (it's a global variable)
    networkConf = NULL;




    //Make the layout
    mainLayout = new QGridLayout();
    mainLayout->setVerticalSpacing(2);


    //isTcpClientConnected = false;
    //tcpClient = NULL;
    moduleID = -10;
    videoStreaming = false;
    fileInitiated = false;
    fileOpen = false;
    recording = false;

    cameraNum = 0;

    inputFileOpen = false;
    playbackMode = false;
    inputFileName = "";
    fileStartTime = 0;
    fileEndTime = 0;
    videoWasPlayingBeforeSliderPress = false;
    isVideoFilePlaying = false;
    isVideoSliderPressed = false;

    //Parse the command line options
    QString resValueX;
    QString resValueY;
    QString trodesConfigFile;
    //QString serverAddress;
    //QString serverPortValue;
    QString threshValue;
    QString cameraNumValue;

    int optionInd = 1;
    while (optionInd < arguments.length()) {
        if ((arguments.at(optionInd).compare("-resolutionx",Qt::CaseInsensitive)==0) && (arguments.length() > optionInd+1)) {
            resValueX = arguments.at(optionInd+1);
            optionInd++;
        } else if ((arguments.at(optionInd).compare("-resolutiony",Qt::CaseInsensitive)==0) && (arguments.length() > optionInd+1)) {
            resValueY = arguments.at(optionInd+1);
            optionInd++;
        } else if ((arguments.at(optionInd).compare("-trodesConfig",Qt::CaseInsensitive)==0) && (arguments.length() > optionInd+1)) {
            trodesConfigFile = arguments.at(optionInd+1);
            // parse the config file
            nsParseTrodesConfig(trodesConfigFile);
            qDebug() << "FSGui parsing trodesConfig file " << trodesConfigFile;
            optionInd++;
        } else if ((arguments.at(optionInd).compare("-serverAddress",Qt::CaseInsensitive)==0) && (arguments.length() > optionInd+1)) {
            serverAddress = arguments.at(optionInd+1);
            optionInd++;
        } else if ((arguments.at(optionInd).compare("-serverPort",Qt::CaseInsensitive)==0) && (arguments.length() > optionInd+1)) {
            serverPortValue= arguments.at(optionInd+1);
            optionInd++;
        } else if ((arguments.at(optionInd).compare("-threshold",Qt::CaseInsensitive)==0) && (arguments.length() > optionInd+1)) {
            threshValue = arguments.at(optionInd+1);
            optionInd++;
        } else if ((arguments.at(optionInd).compare("-cameraNum",Qt::CaseInsensitive)==0) && (arguments.length() > optionInd+1)) {
            cameraNumValue = arguments.at(optionInd+1);
            optionInd++;
        }
        optionInd++;
    }

    if ((!resValueY.isEmpty()) && (!resValueX.isEmpty()))   {
        bool ok1;
        bool ok2;
        resX = resValueX.toInt(&ok1);
        resY = resValueX.toInt(&ok2);
        if (!ok1 || !ok2) {
            //Conversion to int didn't work
            resX = -1;
            resY = -1;
        }
    } else {
        resX = -1;
        resY = -1;
    }

    if (!threshValue.isEmpty()) {
        bool ok1;
        int tmpCurrentThresh = threshValue.toInt(&ok1);
        if (ok1) {
            trackSettings.currentThresh = tmpCurrentThresh;
        }
    }

    if (!cameraNumValue.isEmpty()) {
        bool ok1;
        int tmpCameraNum = cameraNumValue.toInt(&ok1);
        if (ok1) {
            cameraNum = tmpCameraNum;
        }
    }


    //------------------------------------------
    /*
    Module connection protocol:
    1) Trodes (or other master GUI) launches module X, or module X is started directly by the user
    2) Module X defines the provided and needed datatypes
    3) Module X creates a client and connects to Trodes (or the master GUI)
    4) When trodes responds with a module ID, a module server is automatically started if the module has any data available
    5) Module X gives Trodes it’s DataTypesAvailable structure, and it’s data server address
    6) Trodes fills its DataAvailable table entries with the given data, and checks that there are no repeats with other modules
    7) Trodes sends the current DataAvailable list to all currently connected modules.
    ...............
    8) Each module starts one client per needed data type and connects to the proper server
    */



    // Create the  moduleNet structure. This is required for all modules
    moduleNet = new TrodesModuleNetwork();
    // Define the data type that this module provides
    moduleNet->dataProvided.dataType = TRODESDATATYPE_POSITION;

    //Find server address of Trodes (or the master GUI)
    //tcpClient = new TrodesClient(this);
    bool connectionSuccess;
    if ((!serverAddress.isEmpty()) &&  (!serverPortValue.isEmpty())) {
        //The address of the server was specified in the command line,
        //so we use that
        connectionSuccess = moduleNet->trodesClientConnect(serverAddress,serverPortValue.toUInt(), true);

    } else {
        //Try to find an existing Trodes server or look in config file.  This will eventually be done
        //with Zeroconf.
        connectionSuccess = moduleNet->trodesClientConnect(true);


    }

    //connectionSuccess = true;

    if (!connectionSuccess) {
        qDebug() << "Starting in standalone mode";
        currentOperationMode = CAMERAMODULE_STANDALONEMODE | CAMERAMODULE_FILEPLAYBACKMODE;

    } else {
        qDebug() << "Starting in slave mode";
        //For now, we always start in camera streaming mode.  However, file playback is in the plans...
        currentOperationMode = CAMERAMODULE_SLAVEMODE | CAMERAMODULE_CAMERASTREAMINGMODE;
    }



    //Top control panel setup----------------------------------
    int numRightHandButtons = 5;
    int numLeftHandButtons = 2;
    int numButtons = numRightHandButtons + numLeftHandButtons;
    QGridLayout* headerLayout = new QGridLayout(); //contains the buttons and clock at the top of the screen
    headerLayout->setContentsMargins(QMargins(0,5,0,0));
    headerLayout->setHorizontalSpacing(3);

    //Time display
    QTime mainClock(0,0,0,0);
    QFont labelFont;
    labelFont.setPixelSize(20);
    labelFont.setFamily("Console");
    timeLabel  = new QLabel;
    timeLabel->setText(mainClock.toString("hh:mm:ss"));
    timeLabel->setFont(labelFont);
    //timeLabel->setMinimumWidth(100);
    timeLabel->setAlignment(Qt::AlignLeft);
    //pullTimer = new QTimer(this);
    //connect(pullTimer, SIGNAL(timeout()), this, SLOT(updateTime()));
    //pullTimer->start(100); //update timer every 100 ms
    headerLayout->addWidget(timeLabel,0,numButtons);

    settingsButton = new TrodesButton();
    settingsButton->setCheckable(false);
    settingsButton->setFocusPolicy(Qt::NoFocus);
    settingsButton->setText("Settings");
    connect(settingsButton,SIGNAL(pressed()),this,SLOT(settingsButtonPressed()));
    //threshButton->setFixedSize(70,20);
    headerLayout->addWidget(settingsButton,0,numButtons-1);

    sourceButton = new TrodesButton();
    sourceButton->setCheckable(false);
    sourceButton->setFocusPolicy(Qt::NoFocus);

    if (currentOperationMode & CAMERAMODULE_CAMERASTREAMINGMODE) {
        sourceButton->setText("Source");
    } else {
        sourceButton->setText(" File ");
    }
    connect(sourceButton,SIGNAL(pressed()),this,SLOT(sourceButtonPressed()));
    //sourceButton->setFixedSize(70,20);
    headerLayout->addWidget(sourceButton,0,numButtons-2);

    toolsButton = new TrodesButton();
    toolsButton->setCheckable(true);
    toolsButton->setFocusPolicy(Qt::NoFocus);
    connect(toolsButton,SIGNAL(pressed()),this,SLOT(toolsButtonPressed()));
    //connect(toolsButton,SIGNAL(released()),this,SLOT(toolsButtonReleased()));
    headerLayout->addWidget(toolsButton,0,numButtons-3);
    toolsButton->setText("Tools");


    trackButton = new TrodesButton();
    trackButton->setCheckable(true);
    trackButton->setFocusPolicy(Qt::NoFocus);

    if (currentOperationMode & CAMERAMODULE_CAMERASTREAMINGMODE) {
        trackButton->setText("Track");
    } else {
        trackButton->setText("Log");
        trackButton->setRedDown(true);
        trackButton->setEnabled(false);
    }
    connect(trackButton,SIGNAL(pressed()),this,SLOT(trackingButtonPressed()));
    connect(trackButton,SIGNAL(released()),this,SLOT(trackingButtonReleased()));
    headerLayout->addWidget(trackButton,0,numButtons-4);


    //pause and play buttons

    pauseButton = new TrodesButton();
    playButton = new TrodesButton();
    QPixmap playPixmap("playImage.png");
    QPixmap pausePixmap("pauseImage.png");

    QIcon pauseButtonIcon(pausePixmap);
    QIcon playButtonIcon(playPixmap);

    pauseButton->setIcon(pauseButtonIcon);
    playButton->setIcon(playButtonIcon);

    pauseButton->setFocusPolicy(Qt::NoFocus);
    playButton->setFocusPolicy(Qt::NoFocus);

    pauseButton->setIconSize(QSize(10,10));
    playButton->setIconSize(QSize(15,15));

    //pauseButton->setFixedSize(50,20);
    //playButton->setFixedSize(50,20);

    pauseButton->setToolTip(tr("Pause"));
    playButton->setToolTip(tr("Play file"));

    pauseButton->setVisible(false);
    playButton->setVisible(false);
    playButton->setCheckable(true);

    headerLayout->addWidget(playButton,0,0);
    headerLayout->addWidget(pauseButton,0,1);

    connect(pauseButton,SIGNAL(pressed()),this,SLOT(pauseButtonPressed()));
    connect(playButton,SIGNAL(pressed()),this,SLOT(playButtonPressed()));
    connect(pauseButton,SIGNAL(released()),this,SLOT(pauseButtonReleased()));
    connect(playButton,SIGNAL(released()),this,SLOT(playButtonReleased()));


    headerLayout->setColumnStretch(numLeftHandButtons,1);
    mainLayout->addLayout(headerLayout,0,0);

    QGridLayout* fileControlLayout = new QGridLayout(); //contains the buttons and clock at the top of the screen
    fileControlLayout->setContentsMargins(QMargins(0,0,0,0));
    fileControlLayout->setHorizontalSpacing(5);
    labelFont.setPixelSize(10);
    fileStartTimeLabel = new QLineEdit();

    //fileStartTimeLabel->setE
    fileStartTimeLabel->setStyleSheet("* { background-color: rgba(0, 0, 0, 0); "
                                      "    border: none}");
    fileStartTimeLabel->setText("00:00:00");
    fileStartTimeLabel->setFont(labelFont);
    fileStartTimeLabel->setFixedHeight(10);
    fileStartTimeLabel->setFixedWidth(50);
    connect(fileStartTimeLabel,SIGNAL(editingFinished()),this,SLOT(newStartTimeEntered()));


    fileControlLayout->addWidget(fileStartTimeLabel,0,0);

    filePositionSlider = new MySlider();
    filePositionSlider->setFocusPolicy(Qt::NoFocus);
    filePositionSlider->setOrientation(Qt::Horizontal);
    filePositionSlider->setFixedHeight(20);
    filePositionSlider->setMinimum(0);
    filePositionSlider->setMaximum(TIMESLIDERSTEPS);
    filePositionSlider->setSingleStep(1);
    //filePositionSlider->setStyle(new MyStyle(filePositionSlider->style()));

    connect(filePositionSlider,SIGNAL(sliderMoved(int)),this,SLOT(playbackSliderPositionChanged(int)));
    connect(filePositionSlider,SIGNAL(sliderPressed()),this,SLOT(playbackSliderPressed()));
    connect(filePositionSlider,SIGNAL(sliderReleased()),this,SLOT(playbackSliderReleased()));
    fileControlLayout->addWidget(filePositionSlider,0,1);


    fileEndTimeLabel = new QLineEdit();
    fileEndTimeLabel->setFocusPolicy(Qt::NoFocus);
    fileEndTimeLabel->setStyleSheet("* { background-color: rgba(0, 0, 0, 0); "
                                      "    border: none}");
    fileEndTimeLabel->setText("00:00:00");
    fileEndTimeLabel->setFont(labelFont);
    fileEndTimeLabel->setFixedHeight(10);
    fileEndTimeLabel->setFixedWidth(50);
    connect(fileEndTimeLabel,SIGNAL(editingFinished()),this,SLOT(newEndTimeEntered()));
    fileControlLayout->addWidget(fileEndTimeLabel,0,2);

    //File controls start off disabled
    fileStartTimeLabel->setVisible(false);
    filePositionSlider->setVisible(false);
    fileEndTimeLabel->setVisible(false);

    mainLayout->addLayout(fileControlLayout,1,0);




    //Statusbar setup---------------------------
    QString tmpStatus = "";
    //if (!trodesConfigValue.isEmpty()) tmpStatus.append("Trodesconfig: " + trodesConfigValue + " ");
    //if (!resValueY.isEmpty()) tmpStatus.append("Resolution: " + resValueY + " by "+ resValueX + " ");

    statusbar = new QStatusBar(this);
    setStatusBar(statusbar);
    statusbar->showMessage(tmpStatus);



    if (connectionSuccess) {
        //connect(tcpClient,SIGNAL(connected()),this,SLOT(setTcpClientConnected()));
        //connect(tcpClient,SIGNAL(socketDisconnected()),this,SLOT(setTcpClientDisconnected()));
        //connect(tcpClient,SIGNAL(moduleIDReceived(qint8)),this,SLOT(setModuleID(qint8)));
        connect(moduleNet->trodesClient,SIGNAL(openFileEventReceived(QString)),this,SLOT(setupFile(QString)));
        connect(moduleNet->trodesClient,SIGNAL(currentTimeReceived(quint32)),this,SLOT(setTime(quint32)));
        connect(moduleNet->trodesClient,SIGNAL(timeRateReceived(quint32)),this,SLOT(setTimeRate(quint32)));
        connect(moduleNet->trodesClient,SIGNAL(startAquisitionEventReceived()),this,SLOT(startRecording()));
        connect(moduleNet->trodesClient,SIGNAL(stopAquisitionEventReceived()),this,SLOT(stopRecording()));
        connect(moduleNet->trodesClient,SIGNAL(closeFileEventReceived()),this,SLOT(closeFile()));
        connect(moduleNet->trodesClient,SIGNAL(quitCommandReceived()),this,SLOT(close()));
        connect(moduleNet->trodesClient,SIGNAL(instanceReveivedFromServer(int)),this,SLOT(setModuleInstance(int)));

        moduleNet->sendTimeRateRequest();
        //moduleNet->trodesClient->sendTimeRateRequest();
    }

    //connect(tcpClient,SIGNAL(socketErrorHappened(QString)),this,SLOT(displaySocketError(QString)));
    //connect(tcpClient,SIGNAL(stateScriptCommandReceived(QString)),this,SLOT(receiveMessageFromTcpServer(QString)));


    graphicsWindow = new GraphicsWindow(this);
    graphicsWindow->setFocusPolicy(Qt::NoFocus);
    //QGraphicsView *view = new QGraphicsView(graphicsWindow);

    //displayWindow = new VideoDisplayWindow(NULL);

#ifdef ARAVIS
    GigEVideoDisplayController* gige_video_cntl = new GigEVideoDisplayController(this, displayWindow, moduleNet->trodesClient, encoder);
#else

    videoController = new VideoDisplayController(this,displayWindow,moduleNet->trodesClient);

    connect(videoController,SIGNAL(videoStreamStart(int,int)),this,SLOT(videoStarted(int,int)));
    connect(videoController,SIGNAL(processorCreated()),this,SLOT(connectProcessor()));

    connect(videoController,SIGNAL(newAnimalPos(quint32,qint16,qint16)),this,SLOT(sendNewAnimalPosition(quint32,qint16,qint16)));
    connect(videoController,SIGNAL(filePlaybackReady()),this,SLOT(activatePlaybackControls()));
    connect(videoController,SIGNAL(filePlaybackClosed()),this,SLOT(turnOffPlayback()));
    connect(videoController,SIGNAL(videoFileTimeRate(quint32)),this,SLOT(setTimeRate(quint32)));
    connect(videoController,SIGNAL(newVideoTimeStamp(quint32)),this,SLOT(setTime(quint32)));
    connect(videoController,SIGNAL(videoFileTimeRange(quint32,quint32)),this,SLOT(setVideoTimeRange(quint32,quint32)));
    connect(videoController,SIGNAL(filePlaybackStopped()),this,SLOT(setFilePlaybackStopped()));
    connect(videoController,SIGNAL(getUserTrackingInput()),this,SLOT(getUserInput()));
    connect(videoController,SIGNAL(sig_newSliderRange(quint32,quint32)),this,SLOT(setSliderTimeRange(quint32,quint32)));
    connect(videoController,SIGNAL(signal_nextFileNeeded()),this,SLOT(nextFileRequested()));
    connect(filePositionSlider,SIGNAL(newRange(int,int)),videoController,SLOT(newSliderRange(int,int)));

    connect(graphicsWindow,SIGNAL(userInput1(QPoint)),videoController,SLOT(userInput1(QPoint)));
    connect(graphicsWindow,SIGNAL(userInput2(QPoint)),videoController,SLOT(userInput2(QPoint)));
    //connect(videoController->imageProcessor,SIGNAL(fileOpened()),this,SLOT(setFileOpen()));
    connect(this,SIGNAL(signal_startRecording()),videoController,SLOT(startRecording()));
    connect(this,SIGNAL(signal_stopRecording()),videoController,SLOT(stopRecording()));
    connect(this,SIGNAL(signal_createFile(QString)),videoController,SIGNAL(signal_createFile(QString)));
    connect(this,SIGNAL(signal_closeFile()),videoController,SIGNAL(signal_closeFile()));
    connect(this,SIGNAL(signal_startPlayback()),videoController,SLOT(startPlayback()));
    connect(this,SIGNAL(signal_startFastPlayback()),videoController,SLOT(startFastPlayback()));
    connect(this,SIGNAL(signal_pausePlayback()),videoController,SLOT(pausePlayback()));
    connect(this, SIGNAL(signal_stepFrameForward()),videoController,SLOT(stepForward()));
    connect(this, SIGNAL(signal_stepFrameBackward()),videoController,SLOT(stepBackward()));
    connect(this,SIGNAL(signal_createPlaybackLogFile(QString)),videoController,SIGNAL(signal_createPlaybackLogFile(QString)));
    connect(this,SIGNAL(signal_closePlaybackLogFile()),videoController,SIGNAL(signal_closePlaybackLogFile()));

    connect(this,SIGNAL(signal_newSettings(TrackingSettings)),videoController,SIGNAL(signal_newSettings(TrackingSettings)));

    //The video controller is placed in a separate thread to make sure that it is not slowed down by the GUI
    QThread *videoControllerThread = new QThread;
    connect(videoControllerThread,SIGNAL(started()),videoController,SLOT(startController()));

    if (connectionSuccess) {

        //If we are connected to Trodes, start the first camera found automatically.
        //TODO: if Trodes is in playback mode, we want to open a file instead
        videoController->setLiveCameraMode(true);
    }
    connect(videoController, SIGNAL(finished()), videoControllerThread, SLOT(quit()));
    connect(videoController, SIGNAL(finished()), videoController, SLOT(deleteLater()));
    connect(videoControllerThread, SIGNAL(finished()), videoControllerThread, SLOT(deleteLater()));
    videoController->moveToThread(videoControllerThread);
    videoControllerThread->start();


#endif

    //mainLayout->addWidget(displayWindow,2,0);
    mainLayout->addWidget(graphicsWindow,2,0);
    graphicsWindow->show();
    //displayWindow->show();
    QWidget *window = new QWidget();
    window->setLayout(mainLayout);
    setCentralWidget(window);


    moduleNet->sendModuleName("Camera");
    qDebug() << "Camera module started";

    trackSettings.currentOperationMode = currentOperationMode;

    /*
    if (currentOperationMode & CAMERAMODULE_SLAVEMODE) {

        trackSettings.ringOn = false;
    }*/
    newSettings(trackSettings);
    setTracking(trackingOn);


    if (currentOperationMode & CAMERAMODULE_FILEPLAYBACKMODE) {
        setCurrentTool(1); //User input tool
        graphicsWindow->setTool(1);
        setTracking(true);
        trackButton->setDown(false);
    }

    if (connectionSuccess) {
        //Ask the master module what state it should be in (saving, etc)
        moduleNet->sendCurrentStateRequest();

    }


    //setThresh(trackSettings.currentThresh,trackSettings.trackDark);
    //setRing(trackSettings.currentRingSize,trackSettings.ringOn);

    //QMetaObject::connectSlotsByName(this);
    //retranslateUi();

}

MainWindow::~MainWindow() {


}

void MainWindow::connectProcessor() {
    //Once the VideoController thread has started, the image processor is created and this slot is called
    connect(videoController->imageProcessor,SIGNAL(newImage_signal(QImage*)),graphicsWindow->dispWin,SLOT(newImage(QImage*)));
    connect(videoController->imageProcessor,SIGNAL(fileOpened()),this,SLOT(setFileOpenedStatus()));
    connect(videoController->imageProcessor,SIGNAL(recordingStarted()),this,SLOT(setRecordStatusOn()));
    connect(videoController->imageProcessor,SIGNAL(recordingStopped()),this,SLOT(setRecordStatusOff()));
    connect(videoController->imageProcessor,SIGNAL(newLocation(QPoint)),graphicsWindow,SLOT(newLocation(QPoint))); //For one-point tracking
    connect(videoController->imageProcessor,SIGNAL(newLocation(QPoint,QPoint,QPoint)),graphicsWindow,SLOT(newLocation(QPoint,QPoint,QPoint))); //For 2-led tracking
    connect(graphicsWindow,SIGNAL(newIncludeCalculation(QVector<bool>)),videoController->imageProcessor,SLOT(setIncludedPixels(QVector<bool>)));
    //connect(graphicsWindow,SIGNAL(newIncludeCalculation(PixIncludeArray)),videoController->imageProcessor,SLOT(setIncludedPixels(PixIncludeArray)));

}

void MainWindow::closeEvent(QCloseEvent* event) {



    if (!programIsClosing) {
        programIsClosing = true;
        videoController->endProcessor(); //stops the image processor and camera input
        videoController->closeDown();
        connect(&closeTimer,SIGNAL(timeout()),this,SLOT(closeAfterDelay()));
        closeTimer.start(250);
        event->ignore();
    } else {

        moduleNet->trodesClient->sendQuit();
        moduleNet->disconnectClient();
        QThread::msleep(250);
        event->accept();

    }

    //event->accept();

}

void MainWindow::setModuleInstance(int instanceNum) {
    moduleInstance = instanceNum;
    qDebug() << "Camera Module instance " << moduleInstance;
    setWindowTitle(QString("Camera %1").arg(moduleInstance));
}

void MainWindow::setCurrentTool(int toolNum) {
    currentTool = toolNum;
}

void MainWindow::closeAfterDelay() {
    close();
}

void MainWindow::setFilePlaybackStopped() {
    isVideoFilePlaying = false;
}

void MainWindow::pauseButtonPressed() {

     playButton->setDown(false);
     emit signal_pausePlayback();
     //videoController->pausePlayback();
     isVideoFilePlaying = false;

}
void MainWindow::pauseButtonReleased() {
        pauseButton->setDown(true);
}

void MainWindow::playButtonPressed() {
    pauseButton->setDown(false);
    playButton->setBlinkOff();

    if (loggingOn) {
        emit signal_startFastPlayback();
    } else {
        emit signal_startPlayback();
    }
    //videoController->startPlayback();
    isVideoFilePlaying = true;

    //connectToSource();

}
void MainWindow::playButtonReleased() {
    playButton->setDown(true);
    playButton->setChecked(false);

}

void MainWindow::getUserInput() {
    //The tracking algorithm is confused, so we need user input
    isVideoFilePlaying = false;
    playButton->setDown(false);
    setCurrentTool(1); //User input tool
    graphicsWindow->setTool(1);



}

void MainWindow::playbackSliderPositionChanged(int position) {

    //videoController->seekRelativeFrame(position);
    quint32 totalVideoTime = fileEndTime-fileStartTime;
    setTime(fileStartTime + (quint32)((double) position * ((double)totalVideoTime/TIMESLIDERSTEPS)));
}

void MainWindow::playbackSliderPressed() {
    isVideoSliderPressed = true;
    if (isVideoFilePlaying) {
        //videoController->pausePlayback();
        emit signal_pausePlayback();
        videoWasPlayingBeforeSliderPress = true;
        isVideoFilePlaying = false;
    } else {

        videoWasPlayingBeforeSliderPress = false;
    }
}

void MainWindow::playbackSliderReleased() {

    justSeekedFlag = true; //used to clear the log marks on the slider
    videoController->seekRelativeFrame(filePositionSlider->value());
    if (videoWasPlayingBeforeSliderPress) {
        //videoController->startPlayback();
        emit signal_startPlayback();
        isVideoFilePlaying = true;
    }
    isVideoSliderPressed = false;

}

void MainWindow::newStartTimeEntered() {
    //The start of the time range was entered with a string (00:32:45)
    quint32 newTime;
    QString tString = fileStartTimeLabel->text();
    if (convertTimeString(tString,newTime)) {
        if ((newTime >= fileStartTime) && (newTime <=endSliderTime)) {
            startSliderTime = newTime;
        }
    }

    setSliderTimeRange(startSliderTime,endSliderTime); //sets the labels
    int newSliderPos = (double)(startSliderTime-fileStartTime)/((double)(fileEndTime-fileStartTime)/TIMESLIDERSTEPS);
    filePositionSlider->setStart(newSliderPos); //Move the start slider

    videoController->newTimeRange(startSliderTime,endSliderTime); //update the video controller (which controls video playback)
    fileStartTimeLabel->clearFocus();
}

void MainWindow::newEndTimeEntered() {
    //The end of the time range was entered with a string (01:32:45)
    quint32 newTime;
    QString tString = fileEndTimeLabel->text();
    if (convertTimeString(tString,newTime)) {
        if ((newTime <= fileEndTime) && (newTime >=startSliderTime)) {
            endSliderTime = newTime;
        }
    }

    setSliderTimeRange(startSliderTime,endSliderTime); //Update the start and end time strings
    int newSliderPos = (double)(endSliderTime-fileStartTime)/((double)(fileEndTime-fileStartTime)/TIMESLIDERSTEPS);
    filePositionSlider->setEnd(newSliderPos); //Move the end slider

    videoController->newTimeRange(startSliderTime,endSliderTime); //update the video controller (which controls video playback)
    fileEndTimeLabel->clearFocus();
}

bool MainWindow::convertTimeString(QString tstring, quint32 &t) {
    //Convert the time string to number of seconds
    QTime tm = QTime::fromString(tstring,"h:mm:ss");
    if (tm.isValid()) {
        t = abs(tm.secsTo(QTime(0,0,0,0)))*clockRate;
        return true;
    } else {
        return false;
    }

}

void MainWindow::setFullTimeRange() {
    //Resets the start and end range to the full file range

    startSliderTime = fileStartTime;
    endSliderTime = fileEndTime;

    setSliderTimeRange(startSliderTime,endSliderTime); //Update the start and end time strings
    int newSliderPos = (double)(endSliderTime-fileStartTime)/((double)(fileEndTime-fileStartTime)/TIMESLIDERSTEPS);
    filePositionSlider->setEnd(newSliderPos); //Move the end slider

    newSliderPos = (double)(startSliderTime-fileStartTime)/((double)(fileEndTime-fileStartTime)/TIMESLIDERSTEPS);
    filePositionSlider->setStart(newSliderPos); //Move the end slider

    videoController->newTimeRange(startSliderTime,endSliderTime); //update the video controller (which controls video playback)
}

void MainWindow::rewindFileToStartRange() {
    //Rewinds the file to the start range marker

    double currentSliderLoc = (double)(startSliderTime-fileStartTime)/(double)(fileEndTime-fileStartTime);
    filePositionSlider->setValue((int)(TIMESLIDERSTEPS*currentSliderLoc));

    videoController->seekToTime(startSliderTime);

}

void MainWindow::setVideoTimeRange(quint32 start, quint32 end) {
    //Sets the start and end time of the entire video
    fileStartTime = start;
    fileEndTime = end;
    setSliderTimeRange(start, end);
}

void MainWindow::setSliderTimeRange(quint32 start, quint32 end) {
    //This sets the text in the start and end time displays

    startSliderTime = start;
    endSliderTime = end;

    quint32 convertTime = start;

    //QTime currentTime;
    QString currentTimeString("");
    uint32_t tmpTimeStamp = convertTime;
    int hoursPassed = floor(tmpTimeStamp/(clockRate*60*60));
    tmpTimeStamp = tmpTimeStamp - (hoursPassed*60*60*clockRate);
    int minutesPassed = floor(tmpTimeStamp/(clockRate*60));
    tmpTimeStamp = tmpTimeStamp - (minutesPassed*60*clockRate);
    int secondsPassed = floor(tmpTimeStamp/(clockRate));
    tmpTimeStamp = tmpTimeStamp - (secondsPassed*clockRate);
    //int tenthsPassed = floor(((tmpTimeStamp*10)/sourceSamplingRate));
    //int32_t currentTimeStamp = rawData.timestamps[rawData.writeIdx];

    if (hoursPassed < 10)
        currentTimeString.append("0");
    currentTimeString.append(QString::number(hoursPassed));
    currentTimeString.append(":");
    if (minutesPassed < 10)
        currentTimeString.append("0");
    currentTimeString.append(QString::number(minutesPassed));
    currentTimeString.append(":");
    if (secondsPassed < 10)
        currentTimeString.append("0");
    currentTimeString.append(QString::number(secondsPassed));

    //qDebug() << currentTimeString;
    fileStartTimeLabel->setText(currentTimeString);

    //Now do the end time
    convertTime = end;

    //QTime currentTime;
    currentTimeString = "";
    tmpTimeStamp = convertTime;
    hoursPassed = floor(tmpTimeStamp/(clockRate*60*60));
    tmpTimeStamp = tmpTimeStamp - (hoursPassed*60*60*clockRate);
    minutesPassed = floor(tmpTimeStamp/(clockRate*60));
    tmpTimeStamp = tmpTimeStamp - (minutesPassed*60*clockRate);
    secondsPassed = floor(tmpTimeStamp/(clockRate));
    tmpTimeStamp = tmpTimeStamp - (secondsPassed*clockRate);
    //int tenthsPassed = floor(((tmpTimeStamp*10)/sourceSamplingRate));
    //int32_t currentTimeStamp = rawData.timestamps[rawData.writeIdx];

    if (hoursPassed < 10)
        currentTimeString.append("0");
    currentTimeString.append(QString::number(hoursPassed));
    currentTimeString.append(":");
    if (minutesPassed < 10)
        currentTimeString.append("0");
    currentTimeString.append(QString::number(minutesPassed));
    currentTimeString.append(":");
    if (secondsPassed < 10)
        currentTimeString.append("0");
    currentTimeString.append(QString::number(secondsPassed));

    fileEndTimeLabel->setText(currentTimeString);


}

void MainWindow::trackingButtonPressed() {

    if (currentOperationMode & CAMERAMODULE_CAMERASTREAMINGMODE) {
        //Toggle tracking mode for live camera feed
        trackingOn = !trackingOn;
        setTracking(trackingOn);
    } else {
        //Open file for offline tracking
        emit signal_pausePlayback();

        if (!loggingOn) {

            QSettings settings(QLatin1String("SpikeGadgets"), QLatin1String("cameraModule"));
            settings.beginGroup(QLatin1String("paths"));
            QString tempPath = settings.value(QLatin1String("inputFilePath")).toString();
            QStringList splitFromExtension = tempPath.split(".h264");
            tempPath = splitFromExtension.at(0);

            QString fileName = QFileDialog::getSaveFileName(0, "Create log file", tempPath, "tracking files (*.videoPositionTracking)");
            if (!fileName.isEmpty()) {

                //Save the folder in system setting for the next session
                QFileInfo fi(fileName);
                //QSettings settings(QLatin1String("SpikeGadgets"), QLatin1String("cameraModule"));
                //settings.beginGroup(QLatin1String("paths"));
                //settings.setValue(QLatin1String("inputFilePath"), fi.absoluteFilePath());
                //settings.endGroup();
                rewindFileToStartRange();
                playButton->setBlinkOn();
                loggingOn = true;
                emit signal_createPlaybackLogFile(fileName);

            } else {
                trackButton->setDown(false);
            }


        } else {
            QMessageBox::StandardButton reply;
            reply = QMessageBox::question(this, "Close log file", "CLose log file?",
                                          QMessageBox::Close|QMessageBox::Cancel);
            if (reply == QMessageBox::Close) {
                loggingOn = false;
                filePositionSlider->clearLogMarks();
                trackButton->setDown(false);
                //emit signal_
            }

        }

    }

}

void MainWindow::trackingButtonReleased() {
    if (currentOperationMode & CAMERAMODULE_CAMERASTREAMINGMODE) {
        if (trackingOn) {
            trackButton->setDown(true);
        } else {
            trackButton->setDown(false);
        }
    } else {
        if (loggingOn) {
            trackButton->setDown(true);
        } else {
            trackButton->setDown(false);
        }
    }
}

void MainWindow::setTracking(bool tOn) {
    trackButton->setDown(tOn);
    videoController->imageProcessor->setTracking(tOn);
    graphicsWindow->locMarkerOn(tOn);
}

void MainWindow::turnOffPlayback() {
    playButton->setVisible(false);
    pauseButton->setVisible(false);
    pauseButton->setDown(false);
    playButton->setDown(false);
    fileStartTimeLabel->setVisible(false);
    filePositionSlider->setVisible(false);
    fileEndTimeLabel->setVisible(false);
    inputFileOpen = false;
    playbackMode = false;
    inputFileName = "";
    fileStartTime = 0;
    fileEndTime = 0;
    isVideoFilePlaying = false;
}

void MainWindow::activatePlaybackControls() {
    playButton->setVisible(true);
    pauseButton->setVisible(true);
    pauseButton->setDown(true);
    playButton->setDown(false);

    fileStartTimeLabel->setVisible(true);
    filePositionSlider->setVisible(true);
    fileEndTimeLabel->setVisible(true);
}


void MainWindow::sendNewAnimalPosition(quint32 time, qint16 x, qint16 y) {
    //Here we send out the animal's position to all modules that have requested that data
    if (moduleNet->dataServerStarted) {
        //qDebug() << "Position" << time << x << y;
        for (int i = 0; i < moduleNet->dataServer->messageHandlers.length(); i++) {
            //first check if streaming is turned on by the recieving module
            if (moduleNet->dataServer->messageHandlers[i]->isModuleDataStreamingOn()) {
                moduleNet->dataServer->messageHandlers[i]->sendAnimalPosition(time, x, y, cameraNum);
                //qDebug() << "camera module sending Position" << time << x << y;

            }
        }
    }

}

void MainWindow::turnOnPlayback(QString fileName) {

    //Possible danger here-- this is a direct call to an object that lives in another thread.
    //So this function will actually be called in this thread, while subsequent frames are processed in the
    //videoController thread. Need to double check for race conditions.
    if (videoController->inputFileSelected(fileName)) {

        inputFileOpen = true;
        playbackMode = true;
        inputFileName = fileName;
        newSettings(trackSettings);

        setFullTimeRange(); //Reset the start and end range.
        rewindFileToStartRange(); //seek to beginning

    } else {
        //TODO: open error dialog
    }

}

void MainWindow::toolsButtonPressed() {
    toolsButton->setDown(false);
    if (toolsDialogOpen) {
        emit closeDialogs();
        return;
    }

    ToolsDialog *newDialog = new ToolsDialog(currentTool, this);
    //newDialog->setWindowFlags(Qt::Popup);
    Qt::WindowFlags flags = 0;
    flags |= Qt::Popup;
    flags |= Qt::FramelessWindowHint;
    newDialog->setWindowFlags(flags);
    //newDialog->setAttribute(Qt::WA_TranslucentBackground,true);

    //newDialog->setWindowFlags(Qt::Popup| Qt::FramelessWindowHint|Qt::WA_TranslucentBackground);

    //For some reason, windows and Mac give different geometry info for the sound button. Will probably need to
    //add something for Linux here too.

    newDialog->setGeometry(QRect(this->geometry().x()+toolsButton->x(),this->geometry().y()+toolsButton->y()+toolsButton->height(),40,100));
    newDialog->setFixedWidth(84);
    newDialog->setFixedHeight(124);


    toolsDialogOpen = true;
    connect(this, SIGNAL(closeDialogs()),newDialog,SLOT(close()));
    connect(newDialog,SIGNAL(windowClosed()), this, SLOT(setToolsMenuClosed()));
    //specific signal/slot connections here
    connect(newDialog,SIGNAL(toolActivated(int)),this,SLOT(setCurrentTool(int)));
    connect(newDialog,SIGNAL(toolActivated(int)),graphicsWindow,SLOT(setTool(int)));

    newDialog->show();
}

void MainWindow::sourceButtonPressed() {
    sourceButton->setDown(false);

    if (sourceDialogOpen) {
        emit closeDialogs();
        return;
    }

    if (currentOperationMode & CAMERAMODULE_CAMERASTREAMINGMODE) {
        SourceDialog *newDialog = new SourceDialog(videoController->availableCameras(), this);
        //newDialog->setWindowFlags(Qt::Popup);
        Qt::WindowFlags flags = 0;
        flags |= Qt::Popup;
        flags |= Qt::FramelessWindowHint;
        newDialog->setWindowFlags(flags);
        //newDialog->setAttribute(Qt::WA_TranslucentBackground,true);

        //newDialog->setWindowFlags(Qt::Popup| Qt::FramelessWindowHint|Qt::WA_TranslucentBackground);


        //newDialog->setGeometry(QRect(this->x()+sourceButton->x()+8,this->y()+sourceButton->y()+48,40,100));

        newDialog->setGeometry(QRect(this->geometry().x()+sourceButton->x(),this->geometry().y()+sourceButton->y()+sourceButton->height(),40,200));

        newDialog->setFixedWidth(150);
        newDialog->setFixedHeight(25*videoController->availableCameras().length());


        sourceDialogOpen = true;
        connect(this, SIGNAL(closeDialogs()),newDialog,SLOT(close()));
        connect(newDialog,SIGNAL(cameraSelected(int)),videoController,SLOT(newCameraSelected(int)));
        connect(newDialog, SIGNAL(inputFileSelected(QString)),this,SLOT(turnOnPlayback(QString)));
        //connect(newDialog,SIGNAL(newThresh(int,bool)),this,SLOT(setThresh(int,bool)));
        connect(newDialog,SIGNAL(windowClosed()), this, SLOT(setSourceMenuClosed()));
        newDialog->show();


    } else { //Open a dialog to select a playback file
        //Used the saved system settings from the last session as the default folder
        QSettings settings(QLatin1String("SpikeGadgets"), QLatin1String("cameraModule"));
        settings.beginGroup(QLatin1String("paths"));
        QString tempPath = settings.value(QLatin1String("inputFilePath")).toString();

        settings.endGroup();

        QString fileName = QFileDialog::getOpenFileName(0, QString("Select file to open"),tempPath,"h264 files (*.h264 *.mpg)");
        if (!fileName.isEmpty()) {

            //Save the folder in system setting for the next session
            QFileInfo fi(fileName);
            QSettings settings(QLatin1String("SpikeGadgets"), QLatin1String("cameraModule"));
            settings.beginGroup(QLatin1String("paths"));
            settings.setValue(QLatin1String("inputFilePath"), fi.absoluteFilePath());
            settings.endGroup();

            //Load the config file
            turnOnPlayback(fi.absoluteFilePath());
            trackButton->setEnabled(true);
            //emit inputFileSelected(fi.absoluteFilePath());
            //close();
        }
    }
}

void MainWindow::setSourceMenuClosed() {
    sourceDialogOpen = false;
}
void MainWindow::setToolsMenuClosed() {
    toolsDialogOpen = false;
}

void MainWindow::settingsButtonPressed() {
    settingsButton->setDown(false);

    SettingsDialog *newDialog = new SettingsDialog(trackSettings, this);


    //newDialog->setWindowFlags(Qt::Popup);
    Qt::WindowFlags flags = 0;
    flags |= Qt::Popup;
    flags |= Qt::FramelessWindowHint;
    newDialog->setWindowFlags(flags);
    //newDialog->setAttribute(Qt::WA_TranslucentBackground,true);

    //For some reason, windows and Mac give different geometry info for the sound button. Will probably need to
    //add something for Linux here too.


    newDialog->setGeometry(QRect(this->geometry().x()+settingsButton->x(),this->geometry().y()+settingsButton->y()+settingsButton->height(),40,100));
    newDialog->setFixedWidth(125);
    newDialog->setFixedHeight(250);



    connect(this, SIGNAL(closeDialogs()),newDialog,SLOT(close()));
    connect(newDialog,SIGNAL(settingsChanged(TrackingSettings)),this,SLOT(newSettings(TrackingSettings)));
    //connect(newDialog,SIGNAL(newThresh(int,bool)),this,SLOT(setThresh(int,bool)));
    //connect(newDialog,SIGNAL(newRing(int,bool)),this,SLOT(setRing(int,bool)));
    //connect(newDialog,SIGNAL(windowClosed()), SLOT(deleteLater()));


    newDialog->show();
}

void MainWindow::videoStarted(int resolutionY, int resolutionX) {
    if ((resolutionX != resX) || (resolutionY != resY)) {
        //For now, if the actual resolution does not match the command line input,
        //we change it.  TODO: implement image downsampling.
        resY = resolutionY;
        resX = resolutionX;
    }
    videoStreaming = true;

    if (fileInitiated && !fileOpen) {
        emit signal_createFile(fileName);

    }

}

void MainWindow::setRecordStatusOn() {
    statusbar->showMessage(QString("Status: recording   %1").arg(fileName));
}

void MainWindow::setRecordStatusOff() {
    statusbar->showMessage("Status: recording paused");
}

void MainWindow::setFileOpenedStatus() {
    statusbar->showMessage(QString("Status: file created   %1").arg(fileName));
    fileOpen = true;
    //sourceButton->setEnabled(false);
}

void MainWindow::setFileClosedStatus() {
    statusbar->showMessage("Status: file closed");

    fileInitiated = false;
    fileOpen = false;
    //sourceButton->setEnabled(true);
}


void MainWindow::startRecording() {
    /*
    if (fileOpen) {
        statusbar->showMessage("Status: recording");
    }
    recording = true;
    */
    sourceButton->setEnabled(false);
    emit signal_startRecording();
}

void MainWindow::stopRecording() {
    statusbar->showMessage("Status: not recording");
    recording = false;
    sourceButton->setEnabled(true);
    emit signal_stopRecording();
}

void MainWindow::nextFileRequested() {

    qDebug() << "Closing and creating new file...";

    QFileInfo fi(baseFileName);

    QString fileBaseName = fi.baseName();
    fileName = fi.absolutePath()+QString(QDir::separator())+fileBaseName;


    QFileInfo fileCheck(fileName + ".1.h264");
    int numChecks = 1;

    while (fileCheck.exists()) {

        numChecks++;
        fileCheck.setFile(fileName + QString(".%1").arg(numChecks) + ".h264");
    }
    if (numChecks > 0) {
        fileName = fileName + QString(".%1").arg(numChecks);
    }
    fileInitiated = true;
    emit signal_createFile(fileName);
    //closeFile();
    //setupFile(baseFileName);
}

void MainWindow::setupFile(QString filename) {
    //Create live recording file

    qDebug() << "File setup signal received.";

    if (!fileInitiated) {
        baseFileName = filename;  //This is the filename without any extra numbering.


        QFileInfo fi(filename);

        QString fileBaseName = fi.baseName();
        fileName = fi.absolutePath()+QString(QDir::separator())+fileBaseName;

        int numChecks = moduleInstance;
        QFileInfo fileCheck(fileName + QString(".%1.h264").arg(moduleInstance));


        while (fileCheck.exists()) {

            numChecks++;
            fileCheck.setFile(fileName + QString(".%1").arg(numChecks) + ".h264");
        }
        if (numChecks > 0) {
            fileName = fileName + QString(".%1").arg(numChecks);
        }
        fileInitiated = true;

        //TODO: This should be moved to a slot triggered when the file is actually opened
        //statusbar->showMessage("Status: file created");
        //fileOpen = true;
        if (videoStreaming) {
            //createFile();
            emit signal_createFile(fileName);
        }

    }
}

void MainWindow::setFileOpen() {
    statusbar->showMessage(QString("Status: file %1 created").arg(fileName));
    fileOpen = true;
}

/*
void MainWindow::createFile() {

    if (fileInitiated && videoStreaming) {
        int width=resX;
        int height=resY;
        //int bitrate=1000000;
        int bitrate=1000000;
        int gop = 40;
        int fps = 25;

        encoder->createFile(fileName+".mpg",width,height,bitrate,gop,fps);
        encoder->createTimestampFile(fileName+".tstamps");
        fileOpen = true;

    }
}*/

void MainWindow::closeFile() {
    //Close live recording file

    if (fileOpen) {
        stopRecording();
        qDebug() << "Emmiting close signal";
        emit signal_closeFile();
        //encoder->close();
    }
    setFileClosedStatus();

}

void MainWindow::newSettings(TrackingSettings t) {
    trackSettings = t;
    //videoController->imageProcessor->newTrackingSettings(trackSettings);
    graphicsWindow->newSettings(trackSettings);
    emit signal_newSettings(t);

    QSettings settings(QLatin1String("SpikeGadgets"), QLatin1String("cameraModule"));

    settings.beginGroup(QLatin1String("processor"));
    settings.setValue(QLatin1String("thresh"), trackSettings.currentThresh);
    settings.setValue(QLatin1String("trackDarkPixels"), trackSettings.trackDark);
    settings.setValue(QLatin1String("ringSize"), trackSettings.currentRingSize);
    settings.setValue(QLatin1String("ringOn"), trackSettings.ringOn);
    settings.setValue(QLatin1String("ledColorPair"), trackSettings.LEDColorPair);
    settings.setValue(QLatin1String("twoLEDs"), trackSettings.twoLEDs);

    settings.endGroup();
}

/*
void MainWindow::setThresh(int newThresh, bool trackDarkPix) {
    if ((newThresh >= 0) && (newThresh <= 256)) {
        trackSettings.currentThresh = newThresh;
        trackSettings.trackDark = trackDarkPix;
        videoController->setThresh(newThresh, trackDarkPix);

        QSettings settings(QLatin1String("SpikeGadgets"), QLatin1String("cameraModule"));

        settings.beginGroup(QLatin1String("processor"));
        settings.setValue(QLatin1String("thresh"), newThresh);
        settings.setValue(QLatin1String("trackDarkPixels"), trackDarkPix);
        settings.endGroup();

    }
}

void MainWindow::setRing(int ringSize, bool ringOn) {

    trackSettings.currentRingSize = ringSize;
    trackSettings.ringOn = ringOn;
    videoController->imageProcessor->newRing(ringSize,ringOn);
    QSettings settings(QLatin1String("SpikeGadgets"), QLatin1String("cameraModule"));
    settings.beginGroup(QLatin1String("processor"));
    settings.setValue(QLatin1String("ringSize"), ringSize);
    settings.setValue(QLatin1String("ringOn"), ringOn);
    settings.endGroup();
}
*/

/*void MainWindow::setTcpClientConnected() {
    isTcpClientConnected = true;
    qDebug() << "connected";
    tcpClient->setDataType(TRODESDATATYPE_MESSAGING,0);
    //DataTypeSpec requestData;
    //tcpClient->sendDataRequest(requestData);
}

void MainWindow::setTcpClientDisconnected() {
    isTcpClientConnected = false;
    qDebug() << "disconnected";
}

void MainWindow::tcpConnect(QString addressIn, quint16 portIn) {


    tcpClient->setAddress(addressIn);
    tcpClient->setPort(portIn);
    tcpClient->connectToHost();

}

void MainWindow::setModuleID(qint8 ID) {
    qDebug() << "Got module ID";
    networkConf->myModuleID = ID;

    // Now that we've received the module ID, we can start our local data server and
    // send out a message indicating that this module provides position data

    tcpServer->startServer("camera module");
    dataProvided.moduleID = ID;
    dataProvided.hostName = QHostInfo::localHostName();
    dataProvided.hostPort = tcpServer->serverPort();
    dataProvided.posData = true;

    QByteArray temp;
    QDataStream msg(&temp,QIODevice::ReadWrite);
    dataProvided.insertToDataStream(msg);
    tcpClient->sendMessage(TRODESMESSAGE_DATATYPEPROVIDED, temp);

    //moduleID = ID;
    qDebug() << "Camera Module ID is " << ID;
}*/

void MainWindow::setTimeRate(quint32 inTimeRate) {
    qDebug() << "Time rate: " << inTimeRate;
    clockRate = inTimeRate;
    videoController->imageProcessor->clockRate = inTimeRate;

}

void MainWindow::setTime(quint32 t) {
    bool backwardsJump = (t < currentTime);

    currentTime = t;

    //QTime currentTime;
    QString currentTimeString("");
    uint32_t tmpTimeStamp = currentTime;
    int hoursPassed = floor(tmpTimeStamp/(clockRate*60*60));
    tmpTimeStamp = tmpTimeStamp - (hoursPassed*60*60*clockRate);
    int minutesPassed = floor(tmpTimeStamp/(clockRate*60));
    tmpTimeStamp = tmpTimeStamp - (minutesPassed*60*clockRate);
    int secondsPassed = floor(tmpTimeStamp/(clockRate));
    tmpTimeStamp = tmpTimeStamp - (secondsPassed*clockRate);
    //int tenthsPassed = floor(((tmpTimeStamp*10)/sourceSamplingRate));
    //int32_t currentTimeStamp = rawData.timestamps[rawData.writeIdx];

    if (hoursPassed < 10)
        currentTimeString.append("0");
    currentTimeString.append(QString::number(hoursPassed));
    currentTimeString.append(":");
    if (minutesPassed < 10)
        currentTimeString.append("0");
    currentTimeString.append(QString::number(minutesPassed));
    currentTimeString.append(":");
    if (secondsPassed < 10)
        currentTimeString.append("0");
    currentTimeString.append(QString::number(secondsPassed));
    //currentTimeString.append(".");
    //currentTimeString.append(QString::number(tenthsPassed));
    timeLabel->setText(currentTimeString);
    //qDebug() << "Current time: " << currentTime;

    if (inputFileOpen && (fileEndTime-fileStartTime > 0) && !isVideoSliderPressed) {
        double currentSliderLoc = (double)(t-fileStartTime)/(double)(fileEndTime-fileStartTime);

        filePositionSlider->setValue((int)(TIMESLIDERSTEPS*currentSliderLoc));
        if (loggingOn) {

            filePositionSlider->markCurrentLocation();

            if (justSeekedFlag) {
                filePositionSlider->clearLogMarksAfterCurrentPos();
                justSeekedFlag = false;
            }

        }
    }
}

void MainWindow::resizeEvent(QResizeEvent *) {
    emit closeDialogs();

    //Remember the new size for the next session
    QSettings settings(QLatin1String("SpikeGadgets"), QLatin1String("cameraModule"));
    settings.beginGroup(QLatin1String("position"));
    settings.setValue(QLatin1String("position"), this->geometry());
    settings.endGroup();
}

void MainWindow::moveEvent(QMoveEvent *) {
    emit closeDialogs();

    //Remember the new position for the next session
    QSettings settings(QLatin1String("SpikeGadgets"), QLatin1String("cameraModule"));
    settings.beginGroup(QLatin1String("position"));
    settings.setValue(QLatin1String("position"), this->geometry());
    settings.endGroup();
}

void MainWindow::keyPressEvent(QKeyEvent *event) {
    //A keyboard key was pressed.

    if (event->key() == Qt::Key_Escape) {
        //Escape key presses the pause button

        if (inputFileOpen && playbackMode && isVideoFilePlaying) {
            pauseButtonPressed();
            pauseButton->setDown(true);
        }

    } else if ((event->key() == Qt::Key_Enter) || (event->key() == Qt::Key_Return)) {
        //The enter button toggles play/pause

        if (inputFileOpen && playbackMode && !isVideoFilePlaying) {
            playButtonPressed();
            playButton->setDown(true);
        } else if (inputFileOpen && playbackMode && isVideoFilePlaying) {
            pauseButtonPressed();
            pauseButton->setDown(true);
        }


    } else if (event->key() == Qt::Key_Left) {
        if (inputFileOpen && playbackMode && !isVideoFilePlaying) {
            //Step back one frame
            emit signal_stepFrameBackward();
        }

    } else if (event->key() == Qt::Key_Right) {
        if (inputFileOpen && playbackMode && !isVideoFilePlaying) {
            //Step forward one frame
            emit signal_stepFrameForward();
        }
    }
}

/*bool MainWindow::eventFilter(QObject *obj, QEvent *event)
{

    if (event->type() == QEvent::KeyPress) {
      qDebug() << "keypress event";
      QKeyEvent *keyEvent = static_cast<QKeyEvent*>(event);
      if ( keyEvent->key() == Qt::Key_Backspace ) {
        qDebug() << "BACKSPace";
      }
    }


}*/



//-----------------------------------------------------------------------------
MySlider::MySlider(QWidget *parent):QSlider(parent) {
    draggingStart = false;
    draggingEnd = false;
    draggingKnob = false;
    startBorder = 0;
    endBorder = TIMESLIDERSTEPS;
    for (int i=0; i<TIMESLIDERSTEPS; i++) {
        logMarker[i] = false;
    }
}

void MySlider::markCurrentLocation() {
    logMarker[value()] = true;
}

void MySlider::clearLogMarks() {
    for (int i=0; i<TIMESLIDERSTEPS; i++) {
        logMarker[i] = false;
    }
    update();
}

void MySlider::clearLogMarksAfterCurrentPos() {
    for (int i=value()+1; i<TIMESLIDERSTEPS; i++) {
        logMarker[i] = false;
    }
    update();
}


void MySlider::mousePressEvent ( QMouseEvent * event ) {

    QStyleOptionSlider opt;
      initStyleOption(&opt);
      QRect sr = style()->subControlRect(QStyle::CC_Slider, &opt, QStyle::SC_SliderHandle, this);
      int clickVal = minimum() + ((maximum()-minimum()) * (event->x()-5)) / (width()-10);

      /*
      if (event->button() == Qt::LeftButton && sr.contains(event->pos()) == true) {
          if ((clickVal >= startBorder) && (clickVal <= endBorder)) {
            QSlider::mousePressEvent(event);
          }
      }*/

      int allowedClickError = TIMESLIDERSTEPS/50;
      if (event->button() == Qt::LeftButton) {

        //If the user clicked the main knob, and the click was closer to the main knob then the others...
        if ((abs(clickVal-value()) < allowedClickError) && (abs(clickVal-value()) < abs(clickVal-startBorder)) && (abs(clickVal-value()) < abs(clickVal-endBorder)) ){
              setValue(clickVal);
              draggingKnob = true;
              update();
              emit sliderPressed();
              event->accept();
        //If the user slicked the start knob, and the main knob was not nearby...
        } else if ((abs(clickVal-startBorder) < allowedClickError) && (abs(value()-startBorder)>(allowedClickError/4)) ){
            startBorder = clickVal;
            draggingStart = true;
            update();
            event->accept();
        //If the user slicked the end knob, and the main knob was not nearby...
        } else if ((abs(clickVal-endBorder) < allowedClickError) && (abs(value()-endBorder)>(allowedClickError/4)) ){
            endBorder = clickVal;
            draggingEnd = true;
            update();
            event->accept();
        //Otherwise, put the main knob where the user clicked.
        } else if ((clickVal >= startBorder) && (clickVal <= endBorder)) {
            setValue(clickVal);
            draggingKnob = true;
            update();
            emit sliderMoved(clickVal);
            emit sliderPressed();

            event->accept();

            //event->accept();
            //QSlider::mousePressEvent(event);
        }

      }

}

void MySlider::mouseReleaseEvent(QMouseEvent *event) {
    if (draggingStart && startBorder > value()) {
        setValue(startBorder);
        emit sliderReleased();
    } else if (draggingEnd && endBorder < value()) {
        setValue(endBorder);
        emit sliderReleased();
    } else if (draggingKnob){
        emit sliderReleased();
        //QSlider::mouseReleaseEvent(event);
    }

    draggingStart = false;
    draggingEnd = false;
    draggingKnob = false;
}

void MySlider::mouseMoveEvent(QMouseEvent *event) {
    int clickVal = minimum() + ((maximum()-minimum()) * (event->x()-5)) / (width()-10);
    if (draggingStart) {
        //Dragging the start border slider
        if ((clickVal >= 0) && (clickVal <= endBorder)) {
            startBorder = clickVal;
            emit newRange(startBorder,endBorder);
            update();
        }
    } else if (draggingEnd) {
        //Dragging the end border slider
        if ((clickVal >= startBorder) && (clickVal <= TIMESLIDERSTEPS)) {
            endBorder = clickVal;
            emit newRange(startBorder,endBorder);
            update();
        }
    } else if (draggingKnob) {
        //Dragging the main position slider
        if ((clickVal >= startBorder) && (clickVal <= endBorder)) {
            setValue(clickVal);
            emit sliderMoved(clickVal);
            update();
        }
    }
    /*
    else {
        if ((clickVal >= startBorder) && (clickVal <= endBorder)) {
          QSlider::mouseMoveEvent(event);
        }

    }*/

}

void MySlider::paintEvent(QPaintEvent *event) {

    QStylePainter painter(this);
    painter.setRenderHint(QPainter::Antialiasing);

    double pixPerUnit;
    if ((maximum()-minimum()) > 0) {
        pixPerUnit = (width()-10)/(double)(maximum()-minimum()) ;
    } else {
        pixPerUnit = 0;
    }

    double startSliderPos = (startBorder*pixPerUnit)+5;
    double endSliderPos = (endBorder*pixPerUnit)+5;
    double knobPos = (value()*pixPerUnit)+5;

    double pixPerLogMarkerStep = (double)(width()-10)/TIMESLIDERSTEPS;

    QPen linePen;

    linePen.setWidth(2);
    linePen.setBrush(Qt::red);
    painter.setPen(linePen);
    for (int i=0; i < TIMESLIDERSTEPS; i++) {
        if (logMarker[i]) {
            painter.drawPoint((i*pixPerLogMarkerStep)+5,2);
        }
    }


    linePen.setWidth(2);
    linePen.setBrush(Qt::lightGray);
    painter.setPen(linePen);

    //Draw the groove line
    QPainterPath rangePath;
    rangePath.moveTo(5, height()/2);
    rangePath.lineTo(width()-5, height()/2);
    painter.drawPath(rangePath);


    //Draw the range line
    linePen.setWidth(3);
    linePen.setBrush(Qt::gray);
    painter.setPen(linePen);
    QPainterPath groovePath;
    groovePath.moveTo(startSliderPos, height()/2);
    groovePath.lineTo(endSliderPos, height()/2);
    painter.drawPath(groovePath);


    //Draw the start range slider knob
    //qDebug() << value() << width() << pixPerUnit;

    QPainterPath startSlider;


    startSlider.addRoundedRect(startSliderPos-2.5,height()/4,5.0,height()/2,5,5.0);
    painter.fillPath(startSlider,QBrush(Qt::gray));

    linePen.setBrush(Qt::black);
    linePen.setWidth(1);
    painter.setPen(linePen);
    painter.drawPath(startSlider);


    //Draw the start range slider knob
    QPainterPath endSlider;
    endSlider.addRoundedRect(endSliderPos-2.5,height()/4,5.0,height()/2,5,5.0);
    painter.fillPath(endSlider,QBrush(Qt::gray));
    painter.drawPath(endSlider);


    //Draw the main slider knob
    QPainterPath mainSliderKnob;
    //mainSliderKnob.addRoundedRect(knobPos-3,0,6.0,height(),5,5.0);

    mainSliderKnob.addRoundedRect(knobPos-4,(height()/2)-4.0,8.0,8.0,5.0,5.0);
    painter.fillPath(mainSliderKnob,QBrush(QColor(150,150,200)));
    //painter.fillPath(mainSliderKnob,QBrush(Qt::gray));
    painter.drawPath(mainSliderKnob);

    painter.end();
}

void MySlider::setStart(int newStartBorder) {

    if ((newStartBorder >= 0) && (newStartBorder <= endBorder)) {
        startBorder = newStartBorder;
        //emit newRange(startBorder,endBorder);
        update();
    } else if (newStartBorder > endBorder) {
        //The start value is greater than the end value, set use the end value
        startBorder = endBorder;
        //emit newRange(startBorder,endBorder);
        update();
    } else if (newStartBorder < 0) {
        startBorder = 0;
        //emit newRange(startBorder,endBorder);
        update();
    }

    if (startBorder > value()) {
        setValue(startBorder);
        emit sliderReleased();
    } else if (endBorder < value()) {
        setValue(endBorder);
        emit sliderReleased();
    }

}

void MySlider::setEnd(int newEndBorder) {
    if ((newEndBorder <= TIMESLIDERSTEPS) && (newEndBorder >= startBorder)) {
        endBorder = newEndBorder;
        //emit newRange(startBorder,endBorder);
        update();
    } else if (newEndBorder < startBorder) {
        //The end value is less than the start value, set use the start value
        endBorder = startBorder;
        //emit newRange(startBorder,endBorder);
        update();
    } else if (newEndBorder > TIMESLIDERSTEPS) {
        endBorder = TIMESLIDERSTEPS;
        //emit newRange(startBorder,endBorder);
        update();
    }


    if (startBorder > value()) {
        setValue(startBorder);
        //emit sliderReleased();
    } else if (endBorder < value()) {
        setValue(endBorder);
        //emit sliderReleased();
    }
}
