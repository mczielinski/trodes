#ifndef AVTWRAPPER_H
#define AVTWRAPPER_H

#include <QtWidgets>
#include "abstractCamera.h"

#ifdef __APPLE__
#define _OSX
#define _x64
#endif

#ifdef linux
#define _LINUX
#endif

#ifdef WIN32
#define _WINDOWS
#endif

#ifdef _WINDOWS
#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
#define PVDECL           __stdcall
//#include "StdAfx.h"
#endif

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#ifdef _WINDOWS
#define WIN32_LEAN_AND_MEAN
#include <Winsock2.h>
#include <Windows.h>
#endif

#if defined(_LINUX) || defined(_QNX) || defined(_OSX)
#include <unistd.h>
#include <time.h>
#include <signal.h>
#include <arpa/inet.h>
#endif

#include <PvApi.h>
#include <ImageLib.h>

#ifndef _WINDOWS
#define TRUE 0
#endif

#define MAX_CAMERA_LIST 20
// number of frames to be used
#define FRAMESCOUNT 3

// camera's data
typedef struct
{
    unsigned long   UID;
    tPvHandle       Handle;
    tPvFrame        Frames[FRAMESCOUNT];
    tPvUint32       Counter;
    bool            Abort;
    unsigned long   Discarded; //Count of missing frames.
    char            Filename[20];

} tCamera;

struct CameraIPName
{
    QString cameraName;
    unsigned long ipAddress;
};

class AvtCameraController : public AbstractCamera {

Q_OBJECT

public:
    AvtCameraController();
    ~AvtCameraController();
    QStringList availableCameras(); //Get a list of available device names

private:
    tCamera cameraInfo;
    bool isCameraOpen;
    bool isCameraAcquiring;
    bool initialized;
    bool isBlackAndWhite;
    QTimer frameTriggerTimer;
    QVector<CameraIPName> detectedCameras;
    QVector<QRgb> colorTable;
    int frameIndex;

public slots:
    bool open(int cameraID); //opens a camera using the index of the list from availableCameras()
    void close(); //closes the camera
    bool start(); //starts acquistion
    void stop(); //stops acquisition


private slots:
    void getFrame();

signals:


};

#endif // AVTWRAPPER_H
