#include "abstractCamera.h"

AbstractCamera::AbstractCamera() {
    fmt = Fmt_Invalid;
}

void AbstractCamera::sendFrameSignals(QImage *img, bool flip) {
    numFramesRecieved++;

        emit newFrame(img,flip);
        emit newFrame();

}

void AbstractCamera::setFormat(AbstractCamera::videoFmt format) {
    fmt = format;
    emit formatSet(fmt);
}

AbstractCamera::videoFmt AbstractCamera::getFormat() {
    return fmt;
}

