/*
Trodes is a free, open-source neuroscience data collection and experimental control toolbox

Copyright (C) 2012 Mattias Karlsson

This program is free software: you can redistribute it and/or modify
                               it under the terms of the GNU General Public License as published by
                               the Free Software Foundation, either version 3 of the License, or
                               (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "videoDisplay.h"



//VideoDisplayController::VideoDisplayController(QObject *parent, VideoDisplayWindow *displayWidget, TrodesClient *tcpClient, VideoWriter *writer):

#ifdef ARAVIS
typedef struct {
	VideoDisplayWindow* display;
	GigECameraInterface* camera;
} GigEVideoObj;

void test_callback(GigECameraInterface* camera, VideoDisplaySurface* surf) {
	qDebug() << ("callback!");
}

void GigEVideoDisplayController::new_frame_callback(ArvStream *stream, void* callback_data) {
	GigEVideoObj* gige_video = reinterpret_cast<GigEVideoObj*>(callback_data);
	uchar* raw_frame = gige_video->camera->get_current_frame();

	int height = gige_video->camera->get_current_height();
	int width = gige_video->camera->get_current_width();

	QImage* frame_img = new QImage(raw_frame, width, height, QImage::Format_Mono);
	//gige_video->display->newImage(frame_img);
	//gige_video->display->videoSurface()->present(frame);
}

GigEVideoDisplayController::GigEVideoDisplayController(QObject *parent, VideoDisplayWindow *displayWidget, TrodesClient *tcpClient, VideoEncoder *encoder) : tcpClient(tcpClient) {
    
	imageProcessor = new VideoImageProcessor(this,encoder);

	_arv = AravisWrapper::get_instance();	
	// Try opening first camera
	_cur_camera = _arv->open_camera(0);
	GigEVideoObj* gige_video = (GigEVideoObj*) g_malloc(sizeof(*gige_video));

	gige_video->display = displayWidget;
	gige_video->camera = _cur_camera;

	int height = gige_video->camera->get_current_height();
	int width = gige_video->camera->get_current_width();

	//connect(this, SIGNAL(dummy()), displayWidget, SLOT(dumber()));
	
	_cur_camera->set_new_frame_callback((void*)&GigEVideoDisplayController::new_frame_callback, gige_video);
	
	qDebug() << "starting aquisition";
	_cur_camera->start_aquisition();
}

GigEVideoDisplayController::~GigEVideoDisplayController() {
}
#endif

VideoDisplayController::VideoDisplayController(QObject *parent, VideoDisplayWindow *displayWidget, TrodesClient *tcpClient):
	tcpClient(tcpClient) {

    //cameraInput = NULL;
    liveCameraMode = false;
    decoder = NULL;
    currentCameraType = -1;
    currentCameraNum = -1;

}

void VideoDisplayController::startController() {
    //This is called once the thread has started

    imageProcessor = new VideoImageProcessor(this);
    connect(this,SIGNAL(signal_startRecording()),imageProcessor,SLOT(startRecording()));
    connect(this,SIGNAL(signal_stopRecording()),imageProcessor,SLOT(stopRecording()));
    connect(this,SIGNAL(signal_createFile(QString)),imageProcessor,SLOT(createFile(QString)));
    connect(this,SIGNAL(signal_closeFile()),imageProcessor,SLOT(closeFile()));
    connect(this,SIGNAL(sig_UserInput1(QPoint)),imageProcessor,SLOT(userInput1(QPoint)));
    connect(this,SIGNAL(sig_UserInput2(QPoint)),imageProcessor,SLOT(userInput2(QPoint)));
    connect(this,SIGNAL(signal_createPlaybackLogFile(QString)),imageProcessor,SLOT(createPlaybackLogFile(QString)));
    connect(this,SIGNAL(signal_closePlaybackLogFile()),imageProcessor,SLOT(closePlaybackLogFile()));

    connect(this,SIGNAL(signal_newSettings(TrackingSettings)),imageProcessor,SLOT(newTrackingSettings(TrackingSettings)));
    //connect(imageProcessor,SIGNAL(newImage_signal(QImage)),this,SIGNAL(newFrame(QImage))); //To paint the image on the screen
    connect(imageProcessor,SIGNAL(newAnimalLocation(quint32,qint16,qint16)),this,SIGNAL(newAnimalPos(quint32,qint16,qint16)));
    connect(imageProcessor,SIGNAL(badLocation()),this,SLOT(pauseForBadLoc()));
    connect(this, SIGNAL(sig_seekFrame(quint64)),this,SLOT(seekFrame(quint64)));
    connect(this,SIGNAL(newVideoTimeStamp(quint32)),imageProcessor,SLOT(newTimestamp(quint32)));


    cameraControllers.push_back(new WebcamWrapper());

#ifdef AVT_GIGE
    cameraControllers.push_back(new AvtCameraController());
#endif

    connect(tcpClient,SIGNAL(currentTimeReceived(quint32)),imageProcessor,SLOT(newTimestamp(quint32)));


    //Move image processor to new thread
    /*
    imageProcessorThread = new QThread;
    imageProcessor->moveToThread(imageProcessorThread);
    connect(imageProcessor, SIGNAL(finished()), imageProcessorThread, SLOT(quit()));
    connect(imageProcessor, SIGNAL(finished()), imageProcessor, SLOT(deleteLater()));
    connect(imageProcessor,SIGNAL(streamStarted(int,int)),this,SIGNAL(videoStreamStart(int,int)));
    connect(imageProcessorThread, SIGNAL(started()), imageProcessor, SLOT(initialize()));
    connect(imageProcessorThread, SIGNAL(finished()), imageProcessorThread, SLOT(deleteLater()));
    imageProcessorThread->start();
    */
    connect(imageProcessor,SIGNAL(streamStarted(int,int)),this,SIGNAL(videoStreamStart(int,int)));
    imageProcessor->initialize();

    if (tcpClient->isConnected()) {
        //connect(surface,SIGNAL(newFrame()),tcpClient,SLOT(sendTimeRequest()));
    }

    connect(&displayFirstFrameTimer,SIGNAL(timeout()),this,SLOT(displayFirstFrameFromFile()));
    connect(this,SIGNAL(signal_startFirstFrameTimer()),this,SLOT(startFirstFrameTimer()));

    //connect the frame signals for each camera controller
    for (int i = 0;i < cameraControllers.length();i++) {
        connect(cameraControllers[i],SIGNAL(newFrame(QImage*,bool)),imageProcessor,SLOT(newImage(QImage*,bool)));
        connect(cameraControllers[i],SIGNAL(formatSet(AbstractCamera::videoFmt)),imageProcessor,SLOT(setVideoFormat(AbstractCamera::videoFmt)));
        if (tcpClient->isConnected()) {
            connect(cameraControllers[i],SIGNAL(newFrame()),tcpClient,SLOT(sendTimeRequest()));
        }
    }

    frameReadTimer = new QTimer(this);
    connect(frameReadTimer,SIGNAL(timeout()),this,SLOT(readNextFrameFromFile()));
    if (liveCameraMode) {
        startFirstCameraFound();
    }
    emit processorCreated();
}

bool VideoDisplayController::inputFileSelected(QString fileName) {

    if (decoder) {
        //delete the encoder if it exists
        closePlaybackFile();
    }

    decoder = new H264_Decoder();

    if( !decoder->load(fileName,25.0)) {

        qDebug() << QString("File %1 not found").arg(fileName);
        return false;
    }

    int tries = 0;
    bool readsuccess = false;
    while (tries < 5) {
        if (!decoder->readFrame()) {
            tries++;
            qDebug() << "Could not read frame.";
        } else {
            readsuccess = true;
            break;
        }
    }

    if (!readsuccess) {
        qDebug() << "File load failed.  Could not read a frame";
        //Need an error dialog to pop up
        return false;
    }

    playbackTimeStamps.clear();
    currentFrameNum = 0;
    QFileInfo fileInfo(fileName);

    QFile timeStampFile;

    timeStampFile.setFileName(fileInfo.absolutePath()+"/"+fileInfo.completeBaseName()+".videoTimeStamps");
    qDebug() << "Opening file: " << timeStampFile.fileName();
    if (timeStampFile.exists()) {
        if (!timeStampFile.open(QIODevice::ReadOnly)) {
            qDebug() << "Error: cannot open time stamp file";
        } else {
            QString line;

            line += timeStampFile.readLine();
            if (line.contains("<Start settings>")) {

                bool endOfHeaderFound = false;
                while (!timeStampFile.atEnd() && !endOfHeaderFound) {
                    line.clear();
                    line += timeStampFile.readLine();
                    if (line.contains("Clock rate:")) {
                        QString clockRateString = line.remove(0,12);
                        clockRateString.chop(1);
                        bool ok = false;
                        int readClockRate;
                        readClockRate = clockRateString.toInt(&ok);
                        if (ok) {
                          emit videoFileTimeRate((quint32)readClockRate);
                        } else {
                            emit videoFileTimeRate(30000);
                        }

                    } else if (line.contains("<End settings>")) {
                        endOfHeaderFound = true;
                    }
                }
            } else {
                //No header exists
                timeStampFile.seek(0);
            }
            //char timeStampData[4];
            quint32 timeStampData;
            quint32* timeStampDataPtr = &timeStampData;

            while (!timeStampFile.atEnd()) {

                if (timeStampFile.read((char*)timeStampDataPtr,4) < 4) {
                    qDebug() << "Not enough data";
                    break;
                }
                playbackTimeStamps.append(timeStampData);
            }
            timeStampFile.close();
            sliderStart = 0;
            sliderEnd = playbackTimeStamps.length();
            emit videoFileTimeRange(playbackTimeStamps[0],playbackTimeStamps.last());
            //emit videoFileTimeRate(30000);



        }
    } else {
        qDebug() << "No time stamp file found!";
    }

    //QFile file;
    //file.setFileName(fileName);
    //QVideoDecoder decoder;



    closeCurrentCamera();


    /*
    if (cameraInput != NULL) {
        cameraInput->unload();
        cameraInput->deleteLater();
        cameraInput = NULL;

    }*/

    //Display the first frame
    displayFirstFrameFromFile();



    //emit signal_startFirstFrameTimer();
    //displayFirstFrameTimer.start(250);

    return true;
}

void VideoDisplayController::startFirstFrameTimer() {
    displayFirstFrameTimer.start(250);
}

void VideoDisplayController::stepForward() {
    readNextFrameFromFile();
}

void VideoDisplayController::stepBackward() {
    quint64 desiredFrame = currentFrameNum - 1;
    seekFrame(desiredFrame);
}

void VideoDisplayController::seekFrame(quint64 desiredframe) {
    bool frameTimerRunning = frameReadTimer->isActive();
    frameReadTimer->stop();
    if (!decoder->seekFrame(desiredframe)) {
        qDebug() << "failed to seek";
        return;
    }
    currentFrameNum = desiredframe;
    QImage *newFrame;
    decoder->createQImageFromFrame(newFrame);

    //Should be a signal (separate thread)
    imageProcessor->newImage(newFrame,false);


    if (!playbackTimeStamps.isEmpty() && playbackTimeStamps.length() > currentFrameNum) {

        imageProcessor->seekPositionFile(playbackTimeStamps[currentFrameNum]); //should be a signal
        emit newVideoTimeStamp(playbackTimeStamps[currentFrameNum]);
    }
    if (frameTimerRunning) {
        frameReadTimer->start(1000/25);
    }

}

void VideoDisplayController::setLiveCameraMode(bool on) {
    liveCameraMode = on;
}

void VideoDisplayController::seekToTime(quint32 t) {
    quint64 desiredframe = 0;
    if (!playbackTimeStamps.isEmpty()) {
        while ((playbackTimeStamps.at(desiredframe) < t) && (desiredframe < playbackTimeStamps.length())) {
            desiredframe++;
        }
    } else {
        return;
    }

    //We emit a signal instead of a direct call becuase this function gets called from
    //another thread.
    emit sig_seekFrame(desiredframe);
    //seekFrame(desiredframe);
}

void VideoDisplayController::seekRelativeFrame(int position) {
    //position is an integer between 0 and TIMESLIDERSTEPS, with TIMESLIDERSTEPS being the end of the file.

    quint64 desiredframe;
    if (!playbackTimeStamps.isEmpty()) {
         //emit newVideoTimeStamp(playbackTimeStamps[currentFrameNum]);
        double framesPerUnit = (double)(playbackTimeStamps.length()-1)/TIMESLIDERSTEPS;
        desiredframe = position*framesPerUnit;
    } else {
        return;
    }

    //We emit a signal instead of a direct call becuase this function gets called from
    //another thread.
    emit sig_seekFrame(desiredframe);
    //seekFrame(desiredframe);

}

void VideoDisplayController::newSliderRange(int start, int end) {


    quint32 startPos;
    quint32 endPos;
    quint32 desiredframe;
    if (!playbackTimeStamps.isEmpty()) {
         //emit newVideoTimeStamp(playbackTimeStamps[currentFrameNum]);
        double framesPerUnit = (double)(playbackTimeStamps.length()-1)/TIMESLIDERSTEPS;
        desiredframe = start*framesPerUnit;
        if (desiredframe >= playbackTimeStamps.length()) {
            desiredframe = playbackTimeStamps.length()-1;
        }
        startPos = playbackTimeStamps[desiredframe];
        sliderStart = desiredframe;

        desiredframe = end*framesPerUnit;
        if (desiredframe >= playbackTimeStamps.length()) {
            desiredframe = playbackTimeStamps.length()-1;
        }
        endPos = playbackTimeStamps[desiredframe];
        sliderEnd = desiredframe;

        emit sig_newSliderRange(startPos,endPos);
    }
}

void VideoDisplayController::newTimeRange(quint32 start, quint32 end) {
    //This is called when the time range was given via string input (instead of with the slider)

    quint32 desiredStartframe = 0;
    quint32 desiredEndframe = 0;
    if (!playbackTimeStamps.isEmpty()) {
         //emit newVideoTimeStamp(playbackTimeStamps[currentFrameNum]);
        while ((playbackTimeStamps.at(desiredStartframe) < start) && (desiredStartframe < playbackTimeStamps.length())) {
            desiredStartframe++;
        }

        while ((playbackTimeStamps.at(desiredEndframe) < end) && (desiredEndframe < playbackTimeStamps.length())) {
            desiredEndframe++;
        }

        sliderStart = desiredStartframe;
        sliderEnd = desiredEndframe;

    }
}

void VideoDisplayController::startPlayback() {
    //seekFrame(1);
    fastPlayback = false;
    imageProcessor->setFastPlayback(false);
    frameReadTimer->start(1000/25);

}

void VideoDisplayController::startFastPlayback() {
    fastPlayback = true;
    imageProcessor->setFastPlayback(true);
    frameReadTimer->start(100/25);
}

void VideoDisplayController::pausePlayback() {
    frameReadTimer->stop();
}

void VideoDisplayController::closePlaybackFile() {
    frameReadTimer->stop();
    if (decoder) {
        delete decoder;
        decoder = NULL;
    }
    emit filePlaybackClosed();
}

void VideoDisplayController::pauseForBadLoc() {
    if (!liveCameraMode) {
        pausePlayback();
        emit getUserTrackingInput();
    }
}

void VideoDisplayController::userInput1(QPoint loc) {

    if (!liveCameraMode && !frameReadTimer->isActive()) {
        emit sig_UserInput1(loc);
    }
}

void VideoDisplayController::userInput2(QPoint loc) {
    if (!liveCameraMode && !frameReadTimer->isActive()) {
        emit sig_UserInput2(loc);
    }
}

void VideoDisplayController::displayFirstFrameFromFile() {
    //Display the first frame of the file

    QImage *newFrame;
    //decoder->getFrame(newFrame);
    decoder->createQImageFromFrame(newFrame);
    imageProcessor->newImage(newFrame, false);
    displayFirstFrameTimer.stop();
    if (!playbackTimeStamps.isEmpty() && playbackTimeStamps.length() > currentFrameNum) {
         emit newVideoTimeStamp(playbackTimeStamps[currentFrameNum]);
    }

    emit filePlaybackReady();

}

void VideoDisplayController::readNextFrameFromFile() {
    //This function is called regularly by the frameReadTimer when the
    //play button is pressed
    if (currentFrameNum >= sliderEnd) {
        frameReadTimer->stop();
        emit filePlaybackStopped();
        return;
    }

    if (decoder != NULL) {
        if (!decoder->readFrame()) {
            qDebug() << "End of file reached";
            qDebug() << "Number of expected frames: " << playbackTimeStamps.length();
            frameReadTimer->stop();
            emit filePlaybackStopped();
            //seekFrame(0);
            return;
        }

        QImage *newFrame;
        decoder->createQImageFromFrame(newFrame);
        //decoder->getFrame(newFrame);

        imageProcessor->newImage(newFrame, false);

        currentFrameNum++;
        if (!playbackTimeStamps.isEmpty() && playbackTimeStamps.length() > currentFrameNum) {
            emit newVideoTimeStamp(playbackTimeStamps[currentFrameNum]);
        }
    }
}

QStringList VideoDisplayController::availableCameras() {

    QStringList cameraNames;
    for (int i = 0; i < cameraControllers.length(); i++) {
        QStringList tempCamList = cameraControllers[i]->availableCameras();
        cameraNames.append(tempCamList);
    }
    return cameraNames;
}

void VideoDisplayController::closeCurrentCamera() {
    if (currentCameraType > -1) {
        cameraControllers[currentCameraType]->stop();
        cameraControllers[currentCameraType]->close();       
    }
    currentCameraType = -1;
    currentCameraNum = -1;
}

void VideoDisplayController::startFirstCameraFound() {

    int lastCameraOpen = 0;
    QString lastCameraOpenName = "";
    QSettings settings(QLatin1String("SpikeGadgets"), QLatin1String("cameraModule"));
    settings.beginGroup(QLatin1String("camera"));
    lastCameraOpen = settings.value(QLatin1String("lastCameraNum")).toInt();
    lastCameraOpenName = settings.value(QLatin1String("lastCameraName")).toString();
    settings.endGroup();

    QVector<int> cameraType; //unique for each driver type
    QVector<int> cameraNum; //for each driver, count the number of cameras available
    QStringList cameraNames;
    for (int i = 0; i < cameraControllers.length(); i++) {
        QStringList tempCamList = cameraControllers[i]->availableCameras();
        cameraNames.append(tempCamList);
        for (int j = 0; j < tempCamList.length(); j++) {
            cameraType.append(i);
            cameraNum.append(j);
        }
    }

    //The last open camera may still be available, so start that
    if ((lastCameraOpen > -1) && (cameraType.length() > lastCameraOpen)&&(lastCameraOpenName == cameraNames[lastCameraOpen])) {
        newCameraSelected(lastCameraOpen);
    } else if (cameraType.length() > 0) {
        newCameraSelected(0);
    }
}

void VideoDisplayController::newCameraSelected(int cameraID) {

    if (decoder) {
        //delete the encoder if it exists
        closePlaybackFile();
    }

    bool fileInitiated = false;
    if (imageProcessor->isFileCreated()) {
        //A file for saving video is open.  We need to start a new one for this camera.
        fileInitiated = true;
        //imageProcessor->closeFile();
        //emit signal_closeFile();
    }

    closeCurrentCamera();

    QVector<int> cameraType; //unique for each driver type
    QVector<int> cameraNum; //for each driver, count the number of cameras available
    QStringList cameraNames;
    for (int i = 0; i < cameraControllers.length(); i++) {
        QStringList tempCamList = cameraControllers[i]->availableCameras();
        cameraNames.append(tempCamList);
        for (int j = 0; j < tempCamList.length(); j++) {
            cameraType.append(i);
            cameraNum.append(j);
        }
    }

    if (cameraType.length() > cameraID) {

        if (!cameraControllers[cameraType[cameraID]]->open(cameraNum[cameraID])) {
            qDebug() << "Error opening camera";
            currentCameraType = -1;
            currentFrameNum = -1;
            return;
        }

        QSettings settings(QLatin1String("SpikeGadgets"), QLatin1String("cameraModule"));
        settings.beginGroup(QLatin1String("camera"));
        settings.setValue(QLatin1String("lastCameraNum"), cameraID);
        settings.setValue(QLatin1String("lastCameraName"), cameraNames[cameraID]);
        settings.endGroup();

        qDebug() << "Camera opened";
        if (!cameraControllers[cameraType[cameraID]]->start()) {
            qDebug() << "Error starting camera";
            closeCurrentCamera();
            return;
        }

        qDebug() << "Camera started";
        currentCameraType = cameraType[cameraID];
        currentFrameNum = cameraNum[cameraID];


        if (fileInitiated) {
            emit signal_nextFileNeeded();

        }

    } else {
        qDebug() << "Error in camera selection";


    }

}

VideoDisplayController::~VideoDisplayController() {

    for (int i = 0; i < cameraControllers.length(); i++) {
        delete cameraControllers[i];
    }
}

void VideoDisplayController::closeDown() {
    emit finished();
}

/*
void VideoDisplayController::setThresh(int thresh, bool trackDarkPix) {

    imageProcessor->newThreshold(thresh, trackDarkPix);

}*/

void VideoDisplayController::endProcessor() {

    /*
    if (cameraInput != NULL) {
        cameraInput->unload();
        cameraInput->deleteLater();
        cameraInput = NULL;
    }*/

    imageProcessor->endProcessing();
}

void VideoDisplayController::startRecording() {
    emit signal_startRecording();
}

void VideoDisplayController::stopRecording() {
    emit signal_stopRecording();
}

//------------------------------------------------------------------
RubberBandPolygonNode::RubberBandPolygonNode(int nodeNum, QGraphicsItem *parent):

    //This object is a movable node in a user-drawn polygon. It is a child
    //of a RubberBandPolygon
    nodeNum(nodeNum),
    QGraphicsRectItem(parent) {

    dragging = false;

}


void RubberBandPolygonNode::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget) {

     //Set composition mode to be the inverse of the background
    //painter->setCompositionMode(QPainter::CompositionMode_Difference);
    painter->setCompositionMode(QPainter::RasterOp_SourceAndNotDestination);



    painter->setOpacity(1);

    QPen pen(Qt::white,1);
    painter->setPen(pen);
    painter->setBrush(Qt::NoBrush);
    painter->drawRect(rect());
}

void RubberBandPolygonNode::mouseMoveEvent(QGraphicsSceneMouseEvent *event) {
    //The node is being dragged
    QGraphicsItem::mouseMoveEvent(event);
    dragging = true;
    emit nodeMoved(nodeNum);
}

void RubberBandPolygonNode::mouseReleaseEvent(QGraphicsSceneMouseEvent *event) {
    QGraphicsItem::mouseReleaseEvent(event);
    if (dragging) {
        emit nodeMoveFinished();
    }
    dragging = false;
}
//----------------------------------------------------------
RubberBandPolygon::RubberBandPolygon(QGraphicsItem *parent)
    :QGraphicsPolygonItem(parent) {

    //This is a user-drawn polygon to define ROI's
    dragging = false;
    setFlag(QGraphicsItem::ItemIsMovable,true);
    //setFlag(QGraphicsItem::ItemIsSelectable,true);

}

RubberBandPolygon::~RubberBandPolygon() {

    //Not sure if this is actually needed
    while (points.length() > 0) {
        removeLastPoint();
    }
}

void RubberBandPolygon::removeLastPoint() {
    if (points.length()>0) {
        points.removeLast();
        relativePoints.removeLast();
        setPolygon(QPolygonF(points));
        delete nodes.takeLast();

    }
}

void RubberBandPolygon::moveLastPoint(QPointF newLoc) {
    //move the last point in the polygon to the new location
    //Used as the polygon is being drawn by the user
    if (points.length()>0) {
        points.last().setX(newLoc.x());
        points.last().setY(newLoc.y());
        QPointF newRelativePoint;
        newRelativePoint.setX(newLoc.x()/this->scene()->width());
        newRelativePoint.setY(newLoc.y()/this->scene()->height());
        relativePoints.last().setX(newRelativePoint.x());
        relativePoints.last().setY(newRelativePoint.y());

        setPolygon(QPolygonF(points));
        nodes.last()->setPos(newLoc.x()-3,newLoc.y()-3);
    }
}

void RubberBandPolygon::addPoint(QPointF newPoint) {
    //Add a new point to the polygon

    points.append(newPoint);
    //We need to calculate the point's relative location on
    //the drawing area (between 0 and 1 for both x and y
    //dimensions. This is important for resizing and for
    //calculating what data falls inside the polygon.
    QPointF newRelativePoint;
    newRelativePoint.setX(newPoint.x()/this->scene()->width());
    newRelativePoint.setY(newPoint.y()/this->scene()->height());
    relativePoints.append(newRelativePoint);


    setPolygon(QPolygonF(points));

    //We also create a node that can be dragged by the user.
    RubberBandPolygonNode *tmpNode = new RubberBandPolygonNode(points.length()-1,this);
    nodes.append(tmpNode);
    tmpNode->setRect(0,0,6,6);
    tmpNode->setPos(newPoint.x()-3,newPoint.y()-3);
    tmpNode->setFlag(QGraphicsItem::ItemIsMovable,true);
    tmpNode->setVisible(false);
    connect(tmpNode,SIGNAL(nodeMoved(int)),this,SLOT(childNodeMoved(int)));
    connect(tmpNode,SIGNAL(nodeMoveFinished()),this,SLOT(childNodeMoved()));

}

void RubberBandPolygon::childNodeMoved() {
    //this is called after any of the nodes has finished moving
    emit shapeChanged();
}

void RubberBandPolygon::childNodeMoved(int nodeNum) {

    //this is called as the node is moving-- update polygon shape

    points[nodeNum].setX(nodes[nodeNum]->x()+3);
    points[nodeNum].setY(nodes[nodeNum]->y()+3);
    prepareGeometryChange();
    setPolygon(QPolygonF(points));

    QPolygonF scenePoly = mapToScene(polygon());
    //Also, recalculate the new relative locations
    for (int i=0; i < points.length(); i++) {

        relativePoints[i].setX(scenePoly[i].x()/this->scene()->width());
        relativePoints[i].setY(scenePoly[i].y()/this->scene()->height());
    }

}

void RubberBandPolygon::updateSize() {

    //Called after a resize event
    QPointF tmpPoint;
    setPos(0,0);
    for (int i=0; i < points.length(); i++) {
        tmpPoint.setX(relativePoints[i].x()*this->scene()->width());
        tmpPoint.setY(relativePoints[i].y()*this->scene()->height());
        points[i].setX(tmpPoint.x());
        points[i].setY(tmpPoint.y());
        nodes[i]->setX(tmpPoint.x()-3);
        nodes[i]->setY(tmpPoint.y()-3);

    }
    setPolygon(QPolygonF(points));
}

void RubberBandPolygon::highlight() {
    //Make the draggable nodes visible when the polygon is clicked
    //with the edit tool
    for (int i=0; i<nodes.length();i++) {
        nodes[i]->setVisible(true);
    }
}

void RubberBandPolygon::removeHighlight() {
    //Hide the draggable nodes
    for (int i=0; i<nodes.length();i++) {
        nodes[i]->setVisible(false);
    }
}

bool RubberBandPolygon::isIncludeType() {
    if (type == 0) {
        return true;
    } else {
        return false;
    }
}

bool RubberBandPolygon::isExcludeType() {
    if (type == 1) {
        return true;
    } else {
        return false;
    }
}

bool RubberBandPolygon::isZoneType() {
    if (type == 2) {
        return true;
    } else {
        return false;
    }
}

void RubberBandPolygon::setIncludeType() {
    type = 0;
    setBrush( Qt::green );

}

void RubberBandPolygon::setExcludeType() {
    type = 1;
    setBrush( Qt::red );
}

void RubberBandPolygon::setZoneType() {
    type = 2;
    setBrush( Qt::yellow );
}

void RubberBandPolygon::calculateIncludedPoints(bool *inside, int imageWidth, int imageHeight) {
    //Calculate whether or not each pixel in included.
    //The output depends on the type of polygon (include or exclude polygon)

    //Create a new polygon in the shape of this polygon and scale
    //it to the pixel locations of the underlying image
    QVector<QPointF> absPoints;
    absPoints.resize(points.length());
    for (int i=0; i < points.length(); i++) {
        absPoints[i].setX(relativePoints[i].x()*imageWidth);
        absPoints[i].setY(relativePoints[i].y()*imageHeight);
    }
    QPolygonF absPolygon(absPoints);

    //Now we decide if each pixel in the image is included, where 'included'
    //means inside the polygon for include polygons and outside
    //the polygon for exclude polygons
    uint32_t pixnum = 0;
    if (isIncludeType()) {
        for (int h = 0; h < imageHeight; h++) {
            for (int w = 0; w < imageWidth; w++) {
                if (!absPolygon.containsPoint(QPointF(w,h),Qt::OddEvenFill)) {
                    *(inside+pixnum) = false;
                }
                pixnum++;
            }
        }
    } else if (isExcludeType()) {
        for (int h = 0; h < imageHeight; h++) {
            for (int w = 0; w < imageWidth; w++) {
                if (absPolygon.containsPoint(QPointF(w,h),Qt::OddEvenFill)) {
                    *(inside+pixnum) = false;
                }
                pixnum++;
            }
        }
    }
}

void RubberBandPolygon::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget) {
    //Paint the polygon

    //Set the composition mode so that the fill is see-through, and the color
    //depends on the polygon type
    painter->setCompositionMode(QPainter::CompositionMode_SourceOver);
    painter->setOpacity(.2);
    if (type==0) {
        painter->setBrush( Qt::green );
    } else if (type == 1) {
        painter->setBrush( Qt::red );
    } else if (type == 1) {
        painter->setBrush( Qt::yellow );
    }
    if (points.length() > 2) {
        painter->drawPolygon(polygon());
    }

    //Set the composition mode so that the polygon's lines are the inverse of the background.
    //painter->setCompositionMode(QPainter::CompositionMode_Difference);
    painter->setCompositionMode(QPainter::RasterOp_SourceAndNotDestination);
    painter->setOpacity(1);

    QPen pen(Qt::white,1);
    //pen.setWidth(2);
    //pen.setColor(Qt::white);
    painter->setPen(pen);
    painter->setBrush(Qt::NoBrush);

    if (points.length() > 2) {
        painter->drawPolygon(polygon());
    } else if (points.length() == 2) {
        painter->drawLine(QLineF(points[0],points[1]));
    }
}

void RubberBandPolygon::mousePressEvent(QGraphicsSceneMouseEvent *event) {
    //The polygon was clicked
    emit hasHighlight();
    highlight();
}

void RubberBandPolygon::mouseMoveEvent(QGraphicsSceneMouseEvent *event) {

  //The polygon is being dragged.  We need to recalculate
  //the new relative points
  QGraphicsItem::mouseMoveEvent(event);
  QPolygonF scenePoly = mapToScene(polygon());

  for (int i=0; i < points.length(); i++) {

      relativePoints[i].setX(scenePoly[i].x()/this->scene()->width());
      relativePoints[i].setY(scenePoly[i].y()/this->scene()->height());
  }
  dragging = true;

}

void RubberBandPolygon::mouseReleaseEvent(QGraphicsSceneMouseEvent *event) {
    QGraphicsItem::mouseReleaseEvent(event);
    if (dragging) {
        emit shapeChanged();
    }
    dragging = false;
}

//The GraphicsWindow contains the polygons and the underlying video image
GraphicsWindow::GraphicsWindow(QWidget *parent):
    QGraphicsView(parent) {

    dispWin = new VideoDisplayWindow(NULL);
    scene = new CustomScene;
    connect(scene,SIGNAL(emptySpaceClicked()),this,SLOT(spaceClicked()));
    connect(dispWin,SIGNAL(resolutionChanged()),this,SLOT(calculateConsideredPixels()));
    scene->addWidget(dispWin,Qt::Widget);
    this->setScene(scene);

    currentlyDrawing = false;
    currentlySelectedPolygon = -1;
    currentIncludePolygon = -1;  //there can only be one or zero include polygons

    trackSettings.ringOn = false;
    showTwoLeds = false;

    //addIncludePolygon();

    dispWin->show();

    medianLocMarker = new QGraphicsEllipseItem();
    LED1Marker = new QGraphicsEllipseItem();
    LED2Marker = new QGraphicsEllipseItem();
    ringMarker = new QGraphicsEllipseItem();
    directionMarker = new QGraphicsLineItem();

    QPen markerPen;
    markerPen.setWidth(1);
    markerPen.setColor(Qt::green);

    LED1Marker->setPen(markerPen);
    LED1Marker->setRect(1,1,6,6);
    LED2Marker->setPen(markerPen);
    LED2Marker->setRect(1,1,6,6);
    medianLocMarker->setPen(markerPen);
    medianLocMarker->setRect(1,1,6,6);

    directionMarker->setPen(markerPen);


    ringMarker->setPen(markerPen);
    ringMarker->setRect(1,1,20,20);
    scene->addItem(medianLocMarker);
    scene->addItem(ringMarker);
    scene->addItem(LED1Marker);
    scene->addItem(LED2Marker);
    scene->addItem(directionMarker);

    setViewportUpdateMode(QGraphicsView::FullViewportUpdate);


}

void GraphicsWindow::locMarkerOn(bool on) {
    isTrackingOn = on;
    /*
    if (showTwoLeds) {
        LED1Marker->setVisible(isTrackingOn);
        LED2Marker->setVisible(isTrackingOn);
        medianLocMarker->setVisible(isTrackingOn);
    } else {
        LED1Marker->setVisible(false);
        LED2Marker->setVisible(false);
        medianLocMarker->setVisible(isTrackingOn);
    }
    ringMarker->setVisible(isTrackingOn && trackSettings.ringOn);
    */
    newSettings(trackSettings);
}

void GraphicsWindow::newSettings(TrackingSettings s) {


    //trackSettings.ringOn = s.ringOn;
    trackSettings = s;
    QSize res = dispWin->getResolution();

    ringSize.setWidth((float)s.currentRingSize/res.width());
    ringSize.setHeight((float)s.currentRingSize/res.height());

    ringMarker->setRect(1,1,2*scene->width()*ringSize.width(),2*scene->height()*ringSize.height());
    ringMarker->setVisible(isTrackingOn && trackSettings.ringOn);

    medianLocMarker->setVisible(isTrackingOn);

    LED1Marker->setVisible(isTrackingOn && trackSettings.twoLEDs && !trackSettings.trackDark);
    LED2Marker->setVisible(isTrackingOn && trackSettings.twoLEDs && !trackSettings.trackDark);
    directionMarker->setVisible(isTrackingOn && trackSettings.twoLEDs && !trackSettings.trackDark);

    if (trackSettings.trackDark) {
        //The marker should be light if tracking dark pixels, and dark if tracking light
        QPen markerPen;
        markerPen.setWidth(1);
        markerPen.setColor(Qt::green);
        medianLocMarker->setPen(markerPen);
    } else {
        if (!trackSettings.twoLEDs) {
            QPen markerPen;
            markerPen.setWidth(1);
            markerPen.setColor(Qt::blue);
            LED1Marker->setPen(markerPen);
            LED2Marker->setPen(markerPen);
            directionMarker->setPen(markerPen);
            medianLocMarker->setPen(markerPen);
        } else {
            QPen markerPen1;
            QPen markerPen2;
            QPen markerPen3;
            markerPen1.setWidth(1);
            markerPen2.setWidth(1);
            markerPen3.setWidth(1);
            markerPen1.setColor(Qt::green);
            markerPen2.setColor(Qt::red);
            markerPen3.setColor(Qt::blue);

            LED1Marker->setPen(markerPen1);
            LED2Marker->setPen(markerPen2);
            directionMarker->setPen(markerPen3);
            medianLocMarker->setPen(markerPen3);
        }
    }


}

void GraphicsWindow::newLocation(QPoint p) {
    //A single point is being tracked

    /*
    if (showTwoLeds) {
        showTwoLeds = false;
        LED1Marker->setVisible(false);
        LED2Marker->setVisible(false);
        medianLocMarker->setVisible(isTrackingOn);
    }*/
    QSize res = dispWin->getResolution();
    medianLoc.setX((float)p.x()/res.width());
    medianLoc.setY((float)p.y()/res.height());

    qreal x = scene->width()*medianLoc.x();
    qreal y = scene->height()*medianLoc.y();

    medianLocMarker->setX(x-3);
    medianLocMarker->setY(y-3);
    ringMarker->setX(x-(scene->width()*ringSize.width()));
    ringMarker->setY(y-(scene->height()*ringSize.height()));

}

void GraphicsWindow::newLocation(QPoint p1, QPoint p2, QPoint midPoint) {
    //Two LED locations are being tracked


    /*
    if (!showTwoLeds) {
        //qDebug() << "Got here" << p1 << p2 << midPoint;
        showTwoLeds = true;
        LED1Marker->setVisible(isTrackingOn);
        LED2Marker->setVisible(isTrackingOn);
        directionMarker->setVisible(isTrackingOn);
        //medianLocMarker->setVisible(false);
    }*/

    QSize res = dispWin->getResolution();
    LED1Loc.setX((float)p1.x()/res.width());
    LED1Loc.setY((float)p1.y()/res.height());
    LED2Loc.setX((float)p2.x()/res.width());
    LED2Loc.setY((float)p2.y()/res.height());

    qreal x1 = scene->width()*LED1Loc.x();
    qreal y1 = scene->height()*LED1Loc.y();
    qreal x2 = scene->width()*LED2Loc.x();
    qreal y2 = scene->height()*LED2Loc.y();

    medianLoc.setX((float)midPoint.x()/res.width());
    medianLoc.setY((float)midPoint.y()/res.height());

    qreal midx = scene->width()*medianLoc.x();
    qreal midy = scene->height()*medianLoc.y();

    LED1Marker->setX(x1-3);
    LED1Marker->setY(y1-3);
    LED2Marker->setX(x2-3);
    LED2Marker->setY(y2-3);
    ringMarker->setX(midx-(scene->width()*ringSize.width()));
    ringMarker->setY(midy-(scene->height()*ringSize.height()));
    medianLocMarker->setX(midx-3);
    medianLocMarker->setY(midy-3);

    directionMarker->setLine(x1,y1,x2,y2);

}

void GraphicsWindow::addExcludePolygon() {
    RubberBandPolygon *newPoly = new RubberBandPolygon();
    newPoly->setExcludeType();

    connect(newPoly,SIGNAL(hasHighlight()),this,SLOT(polygonHighlighted()));
    connect(newPoly,SIGNAL(shapeChanged()),this,SLOT(calculateConsideredPixels()));

    polygons.append(newPoly);
    scene->addItem(newPoly);
}

void GraphicsWindow::addZonePolygon() {
    RubberBandPolygon *newPoly = new RubberBandPolygon();
    newPoly->setZoneType();

    connect(newPoly,SIGNAL(hasHighlight()),this,SLOT(polygonHighlighted()));

    polygons.append(newPoly);
    scene->addItem(newPoly);
}

void GraphicsWindow::addIncludePolygon() {
    RubberBandPolygon *newPoly = new RubberBandPolygon();
    newPoly->setIncludeType();

    connect(newPoly,SIGNAL(hasHighlight()),this,SLOT(polygonHighlighted()));
    connect(newPoly,SIGNAL(shapeChanged()),this,SLOT(calculateConsideredPixels()));

    polygons.append(newPoly);
    scene->addItem(newPoly);

}

void GraphicsWindow::spaceClicked() {
    //the background was clicked, so turn off all highlights
    setNoHighlight();
}

void GraphicsWindow::setNoHighlight() {
    for (int i=0; i<polygons.length(); i++) {
        polygons[i]->removeHighlight();
    }
    currentlySelectedPolygon = -1;
}

void GraphicsWindow::polygonHighlighted() {
    RubberBandPolygon* highlightedPoly = dynamic_cast<RubberBandPolygon*>(sender());

    for (int i=0; i<polygons.length(); i++) {
        if (polygons[i] == highlightedPoly) {
            currentlySelectedPolygon = i;
        } else {
            polygons[i]->removeHighlight();
        }
    }
}

void GraphicsWindow::setTool(int toolNum) {
    currentTool = toolNum;
    if (currentTool != EDITTOOL_ID) {
        //We only allow highlighting when the edit tool is active
        setNoHighlight();
    }
}

void GraphicsWindow::calculateConsideredPixels() {
    //This function is called every time a polygon
    //is changed or whenever we need to recalculate
    //which pixels are included in the current set of
    //polygons.

    QSize currentRes = dispWin->getResolution();
    quint32 totalPix = currentRes.width()*currentRes.height();
    QVector<bool> isIncluded;

    isIncluded.resize(totalPix);
    //start by assuming that every pixel is included
    for (int i=0; i<isIncluded.length(); i++) {
        isIncluded[i] = true;
    }

    if (currentRes.height() > 0) {
        //Calculate which pixels to include, going through each polygon one by one
        for (int i=0; i < polygons.length(); i++) {
            polygons[i]->calculateIncludedPoints(isIncluded.data(),currentRes.width(),currentRes.height());
        }
        emit newIncludeCalculation(isIncluded);
    }
}

void GraphicsWindow::mousePressEvent(QMouseEvent *event) {
    //When the window is clicked, the behavior depends on
    //which tool is currently selected.

    if (currentlyDrawing) {
        polygons.last()->addPoint(event->localPos());
        return;
    }

    if (currentTool == POINTTOOL_ID) {

        QSize currentRes = dispWin->getResolution();
        QPointF newRelativePoint;
        newRelativePoint.setX(event->localPos().x()/this->scene->width());
        newRelativePoint.setY(event->localPos().y()/this->scene->height());
        //qDebug() << newRelativePoint;
        QPoint pixelPoint;
        pixelPoint.setX(newRelativePoint.x()*currentRes.width());
        pixelPoint.setY(newRelativePoint.y()*currentRes.height());
        //For two-point tracking we use the left and right mouse buttons to
        //set each point
        if (event->buttons() == Qt::RightButton) {
            emit userInput2(pixelPoint);
        } else if (event->buttons() == Qt::LeftButton) {
            emit userInput1(pixelPoint);
        }
        //newLocation(pixelPoint);
    } else if (currentTool == INCLUDETOOL_ID) {

        if (currentIncludePolygon != -1) {
            delete polygons.takeAt(currentIncludePolygon);
        }
        addIncludePolygon();
        currentIncludePolygon = polygons.length()-1;

        currentlyDrawing = true;
        polygons.last()->addPoint(event->localPos());
        polygons.last()->addPoint(event->localPos());

    } else if (currentTool == EXCLUDETOOL_ID) {

        addExcludePolygon();

        currentlyDrawing = true;
        polygons.last()->addPoint(event->localPos());
        polygons.last()->addPoint(event->localPos());
    } else if (currentTool == ZONETOOL_ID) {

        addZonePolygon();

        currentlyDrawing = true;
        polygons.last()->addPoint(event->localPos());
        polygons.last()->addPoint(event->localPos());

    } else if ((currentTool == EDITTOOL_ID)&&(itemAt(event->pos()))) {
        // Clicked on polygon, pass on event to children
        QGraphicsView::mousePressEvent(event);
    } else if (currentTool == POINTTOOL_ID) {
        //used to manually designate animal location
    }

}

void GraphicsWindow::mouseMoveEvent(QMouseEvent *event) {
    if (currentlyDrawing) {
        polygons.last()->moveLastPoint(event->localPos());
        //dispWin->update();
    } else {
        QGraphicsView::mouseMoveEvent(event);
    }
}

void GraphicsWindow::mouseDoubleClickEvent(QMouseEvent *event) {
    if (currentlyDrawing) {
        currentlyDrawing = false;
        polygons.last()->removeLastPoint();
        calculateConsideredPixels();

    }
}

void GraphicsWindow::keyPressEvent(QKeyEvent *event) {

    if ((event->key() == Qt::Key_Delete)||(event->key() == Qt::Key_Backspace)) {
        //delete the selected polygon
        if (currentlySelectedPolygon != -1) {
            //if an include polygon, note that there is no inlcude polygon anymore
            if (currentlySelectedPolygon == currentIncludePolygon) {
                currentIncludePolygon = -1;
            }
            delete polygons.takeAt(currentlySelectedPolygon);
            setNoHighlight();
            calculateConsideredPixels();

        }
    }


}

void GraphicsWindow::resizeEvent(QResizeEvent *event) {

    dispWin->resize(event->size().width(),event->size().height());
    scene->setSceneRect(0,0,event->size().width(),event->size().height());

    for (int i=0; i<polygons.length(); i++) {
        polygons[i]->updateSize();
    }

    qreal x = scene->width()*medianLoc.x();
    qreal y = scene->height()*medianLoc.y();

    medianLocMarker->setX(x-3);
    medianLocMarker->setY(y-3);
    ringMarker->setRect(1,1,2*scene->width()*ringSize.width(),2*scene->height()*ringSize.height());

    ringMarker->setX(x-(scene->width()*ringSize.width()));
    ringMarker->setY(y-(scene->height()*ringSize.height()));
}


//------------------------------------------------

VideoDisplayWindow::VideoDisplayWindow(QWidget *parent):
    QWidget(parent) {

    setAutoFillBackground(false);
    //setAttribute(Qt::WA_NoSystemBackground, true);

    QPalette palette = this->palette();
    palette.setColor(QPalette::Background, Qt::black);
    setPalette(palette);

}

VideoDisplayWindow::~VideoDisplayWindow() {

}

QSize VideoDisplayWindow::getResolution() {
    return currentImage.size();
}

void VideoDisplayWindow::newImage(QImage *image) {


    //A new image has come in.  Copy it and delete the original.  This is a bit wasteful, so we
    //might want to use the pointer and delete it when the next frame comes it.
    currentImage = image->copy();
    delete image;

    QSize newSize = getResolution();

    if ((newSize.height() != currentSize.height())||(newSize.width() != currentSize.width())) {
        currentSize = newSize;
        emit resolutionChanged();
    }

    update();
}

void VideoDisplayWindow::enterEvent(QEvent *event) {
    //mouse entered video display area-- change cursor to cross-hair
    this->setCursor(Qt::CrossCursor);
}

void VideoDisplayWindow::leaveEvent(QEvent *event) {
    //mouse left video display area-- change back to arrow
    this->setCursor(Qt::ArrowCursor);
}

void VideoDisplayWindow::paintEvent(QPaintEvent *event) {

    QPainter painter(this);
     //Set the painter to use a smooth scaling algorithm.
     //painter.setRenderHint(QPainter::SmoothPixmapTransform, 1);
     painter.setRenderHint(QPainter::HighQualityAntialiasing, 1);
     painter.drawImage(this->rect(), currentImage);

 }


 void VideoDisplayWindow::resizeEvent(QResizeEvent *event) {
     QWidget::resizeEvent(event);
 }


 //------------------------------------------------
 //FrameBundle is a container for a Qimage frame along with the system timestamp
 FrameBundle::FrameBundle():
    imagePtr(NULL),
    timestamp(0),
    videoFieldFilled(false),
    timeFieldFilled(false),
    locationFieldFilled(false),
    xloc(0),
    yloc(0),
    xloc2(0),
    yloc2(0)
 {

 }

 void FrameBundle::setImage(QImage *imagePtrIn) {

     imagePtr = imagePtrIn;
     videoFieldFilled = true;
 }

 void FrameBundle::setTime(quint32 timestampIn) {

     timestamp = timestampIn;
     timeFieldFilled = true;
 }

 void FrameBundle::setLocation(qint16 x1, qint16 y1, qint16 x2, qint16 y2) {
     xloc = x1;
     yloc = y1;
     xloc2 = x2;
     yloc2 = y2;
     locationFieldFilled = true;
 }


 void FrameBundle::deleteFrame() {

     delete imagePtr;

     imagePtr = NULL;
     timestamp = 0;
     xloc = 0;
     yloc = 0;
     videoFieldFilled = false;
     timeFieldFilled = false;
     locationFieldFilled = false;
 }

 bool FrameBundle::bothFieldsFilled() {
     if (videoFieldFilled && timeFieldFilled) {
         return true;
     } else {
         qDebug() << "Video: " << videoFieldFilled << " time: " << timeFieldFilled;
         return false;
     }
 }

 //------------------------------------------------------------------------
 VideoImageBuffer::VideoImageBuffer(QObject *parent):
     bufferSize(100),
     imageWriteHead(0),
     timestampWriteHead(0),
     frameReadHead(0),
     lastFrameReadHead(0)
 {
     frameBundleBuffer = new FrameBundle[bufferSize];

 }

 VideoImageBuffer::~VideoImageBuffer() {
     for (int i=0; i < bufferSize; i++) {
         frameBundleBuffer[i].deleteFrame();
     }
     delete [] frameBundleBuffer;
 }

 void VideoImageBuffer::addImage(QImage *imagePtr) {

     if (frameBundleBuffer[imageWriteHead].imagePtr) {
         frameBundleBuffer[imageWriteHead].deleteFrame();
     }
     frameBundleBuffer[imageWriteHead].setImage(imagePtr);
     //frameBundleBuffer[imageWriteHead].setLocation(xloc, yloc);
     imageWriteHead = (imageWriteHead+1)%bufferSize;
 }

 void VideoImageBuffer::addImage(QImage *imagePtr, QPoint loc1, QPoint loc2) {

     if (frameBundleBuffer[imageWriteHead].imagePtr) {
         frameBundleBuffer[imageWriteHead].deleteFrame();
     }
     frameBundleBuffer[imageWriteHead].setImage(imagePtr);
     frameBundleBuffer[imageWriteHead].setLocation(loc1.x(),loc1.y(),loc2.x(),loc2.y());
     //frameBundleBuffer[imageWriteHead].setLocation(xloc, yloc);
     imageWriteHead = (imageWriteHead+1)%bufferSize;
 }


 void VideoImageBuffer::addTimestamp(quint32 timestamp) {

     frameBundleBuffer[timestampWriteHead].setTime(timestamp);
     timestampWriteHead = (timestampWriteHead+1)%bufferSize;
 }

 FrameBundle VideoImageBuffer::getNextFrame() {

     FrameBundle bundleOut;

     //Don't return a frame unless both time and the image were logged;
     if (frameBundleBuffer[frameReadHead].bothFieldsFilled()) {

        bundleOut = frameBundleBuffer[frameReadHead];

        lastFrameReadHead = frameReadHead;
        frameReadHead = (frameReadHead+1)%bufferSize;
     }

     return bundleOut;
 }

 void VideoImageBuffer::removeLastFrame() {
    frameBundleBuffer[lastFrameReadHead].deleteFrame();
 }

 //------------------------------------------------

 VideoImageProcessor::VideoImageProcessor(QObject *parent):
     streamActive(false),
     recording(false),
     frameHeight(0),
     frameWidth(0),
     fileCreated(false),
     clockRate(0),
     trackingOn(false),
     positionFile(NULL),
     positionFileOpen(false),
     lastFrame(NULL),
     skipPosCalc(false),
     fastPlayback(false),
     createFileAfterCameraLoad(false),
     currentVideoFormat(AbstractCamera::Fmt_Invalid)
 {
     logging = false;
     fpsTimer = new QElapsedTimer();
 }


 VideoImageProcessor::~VideoImageProcessor() {

 }

 void VideoImageProcessor::setVideoFormat(AbstractCamera::videoFmt format) {

     currentVideoFormat = format;
 }

 void VideoImageProcessor::initialize() {
     frameBuffer = new VideoImageBuffer(this);
 }

 void VideoImageProcessor::setFastPlayback(bool on) {
     fastPlayback = on;
 }

 void VideoImageProcessor::setIncludedPixels(QVector<bool> includedPixelsIn) {


     includedPixels.clear();
     includedPixels = includedPixelsIn;
     if (currentSettings.currentOperationMode & CAMERAMODULE_FILEPLAYBACKMODE) {

         replotLastFrame();
     }

 }

 //pixel threshold (dark or light)
 void VideoImageProcessor::newTrackingSettings(TrackingSettings t) {


     currentSettings = t;
     if (currentSettings.currentOperationMode & CAMERAMODULE_FILEPLAYBACKMODE) {

         replotLastFrame();
     }
 }

 void VideoImageProcessor::userInput1(QPoint loc) {

     if (trackingOn) {

         //Figure out which tracking algorithm to use
         if (!currentSettings.twoLEDs || currentSettings.trackDark) {
             xMedian = loc.x();
             yMedian = loc.y();
         } else {
             LED1Loc.setX(loc.x());
             LED1Loc.setY(loc.y());
             midPointLoc.setX((LED1Loc.x()+LED2Loc.x())/2);
             midPointLoc.setY((LED1Loc.y()+LED2Loc.y())/2);
         }

         skipPosCalc = true;

         replotLastFrame();
         skipPosCalc = false;
     }
 }

 void VideoImageProcessor::userInput2(QPoint loc) {

     if (trackingOn) {
         if (currentSettings.twoLEDs) {
             LED2Loc.setX(loc.x());
             LED2Loc.setY(loc.y());
             midPointLoc.setX((LED1Loc.x()+LED2Loc.x())/2);
             midPointLoc.setY((LED1Loc.y()+LED2Loc.y())/2);
         }
         skipPosCalc = true;

         replotLastFrame();
         skipPosCalc = false;
     }
 }

 /*
 void VideoImageProcessor::newThreshold(int value, bool trackDarkPix) {
     brightPixThreshold = value;
     trackDark = trackDarkPix;
     replotLastFrame();
 }

 void VideoImageProcessor::newRing(int newRingSize, bool newRingOn) {
     ringSize = newRingSize;
     ringOn = newRingOn;
     replotLastFrame();
 }*/

 //toggle position tracking
 void VideoImageProcessor::setTracking(bool track) {
     trackingOn = track;
     if (trackingOn && fileCreated && !positionFileOpen) {
         //live camera mode -- create a file
         createPositionFile(baseFileName+".videoPositionTracking");
     }
     if (currentSettings.currentOperationMode & CAMERAMODULE_FILEPLAYBACKMODE) {
         //offline tracking from file
         /*
         QFileInfo fi;
         fi.setFile(baseFileName+".videoPositionTracking");
         if (!fi.exists()) {
             qDebug() << "Creating new tracking file.";
             createPositionFile(baseFileName+".videoPositionTracking");
         } else {
             qDebug() << "Opening previous tracking file.";
         }*/


         replotLastFrame();
     }
 }


 void VideoImageProcessor::replotLastFrame() {


    if (lastFrame != NULL) {


        QImage* displayImage = new QImage(lastFrame->copy());




        if (trackingOn) {
            //Figure out which tracking algorithm to use
            if (!currentSettings.twoLEDs || currentSettings.trackDark) {
                processImage(displayImage);
            } else if (currentSettings.LEDColorPair == LED_COLOR_RED_GREEN){
                processImage_2LED_REDGREEN(displayImage);
            } else if (currentSettings.LEDColorPair == LED_COLOR_WHITE_WHITE) {
                processImage(displayImage); //will change
            }
        }

        emit newImage_signal(displayImage); //Signal to display the image. Not safe across threads!!


        //delete displayImage; //Not safe across threads!!


     }
 }

 void VideoImageProcessor::processImage(QImage *displayImage) {
     bool useIncludeFilter = false;
     if (includedPixels.length() > 0) {
         useIncludeFilter = true;
     }
     //If tracking is on, we find all the bright (or dark) pixels, based on the current threshold.
     //We count the number of pixels along the image columns and rows.
     int ycounts[displayImage->height()];
     int xcounts[displayImage->width()];
     for(int y = 0; y<displayImage->height(); y++){
         ycounts[y] = 0;
     }
     for(int x = 0; x<displayImage->width(); x++){
         xcounts[x] = 0;
     }

     int totalCount = 0;
     //QRgb * line;
     uint32_t currentPixNum = 0;

     int circMinX = 0;
     int circMaxX = 0;
     int circMinY = 0;
     int circMaxY = 0;

     //Calculate the rectangle around the exclude ring
     if (currentSettings.ringOn && currentSettings.onlyConsiderPixelsInsideRing) {
         circMinX = xMedian-currentSettings.currentRingSize;
         circMaxX = xMedian+currentSettings.currentRingSize;
         circMinY = yMedian-currentSettings.currentRingSize;
         circMaxY = yMedian+currentSettings.currentRingSize;
     }

     for(int y = 0; y<displayImage->height(); y++){
         //QRgb * line = (QRgb *)img->scanLine(y);



         for(int x = 0; x<displayImage->width(); x++){
             QRgb currentPixel = displayImage->pixel(x,y);
             int average = (qRed(currentPixel) + qGreen(currentPixel) + qRed(currentPixel))/3;
             //int average = (qRed(line[x]) + qGreen(line[x]) + qRed(line[x]))/3;
             //considerThisPix = true;
             if (useIncludeFilter) {
                 if (!includedPixels[currentPixNum]) {
                     currentPixNum++;

                     continue;
                     //considerThisPix = false;
                 }
                 if (currentSettings.ringOn && currentSettings.onlyConsiderPixelsInsideRing) {
                     if ((x < circMinX)||(x > circMaxX)||(y < circMinY)||(y > circMaxY)) {
                         currentPixNum++;
                         continue;
                     } else {


                         double dist = sqrt(pow((double) x-xMedian,2) + pow((double) y-yMedian,2));
                         if (dist > currentSettings.currentRingSize) {
                         currentPixNum++;
                         continue;
                         }
                     }
                 }
             }
             if ((!currentSettings.trackDark)&&(average > currentSettings.currentThresh)) {
                 ycounts[y]++;
                 xcounts[x]++;
                 totalCount++;
                 //If the pixel was bright, change it to red
                 displayImage->setPixel(x,y, qRgb(255, 0, 0));
             } else if ((currentSettings.trackDark)&&(average < currentSettings.currentThresh)) {

                 ycounts[y]++;
                 xcounts[x]++;
                 totalCount++;
                 //If the pixel was dark, change it to blue
                 displayImage->setPixel(x,y, qRgb(0, 0, 255));
             }
             currentPixNum++;
         }
     }

     if (!skipPosCalc) {
         //Now that we have the bright pixel counts, we can calculate the
         //median x and y location of all the bright pixels
         int halfCount = totalCount/2;
         totalCount = 0;
         for(int y = 0; y<displayImage->height(); y++){
             totalCount+=ycounts[y];
             if (totalCount > halfCount) {
                 yMedian = y;
                 break;
             }
         }

         totalCount = 0;
         for(int x = 0; x<displayImage->width(); x++){
             totalCount+=xcounts[x];
             if (totalCount > halfCount) {
                 xMedian = x;
                 break;
             }
         }
     }

     //Paint on a green circle where the median bright pixel location is
     //int circleRadius = displayImage->height()/100;
     /*
     QPainter p;
     p.begin(displayImage);
     QPen pen;
     pen.setColor(QColor(0,255,0));
     pen.setWidth(2);
     p.setPen(pen);
     p.drawEllipse(QPoint(xMedian,yMedian),circleRadius,circleRadius);




     if (currentSettings.ringOn) {
         p.drawEllipse(QPoint(xMedian,yMedian),currentSettings.currentRingSize,currentSettings.currentRingSize);
     }

     p.end();
     */

     emit newLocation(QPoint(xMedian,yMedian));
 }


 void VideoImageProcessor::processImage_2LED_REDGREEN(QImage *displayImage) {
     bool useIncludeFilter = false;
     if (includedPixels.length() > 0) {
         useIncludeFilter = true;
     }
     //If tracking is on, we find all the bright (or dark) pixels, based on the current threshold.
     //We count the number of pixels along the image columns and rows.
     int ycounts1[displayImage->height()];
     int xcounts1[displayImage->width()];

     int ycounts2[displayImage->height()];
     int xcounts2[displayImage->width()];

     for(int y = 0; y<displayImage->height(); y++){
         ycounts1[y] = 0;
         ycounts2[y] = 0;
     }
     for(int x = 0; x<displayImage->width(); x++){
         xcounts1[x] = 0;
         xcounts2[x] = 0;
     }

     int totalCount1 = 0;
     int totalCount2 = 0;
     //QRgb * line;
     uint32_t currentPixNum = 0;

     int circMinX = 0;
     int circMaxX = 0;
     int circMinY = 0;
     int circMaxY = 0;

     //Calculate the rectangle around the exclude ring
     if (currentSettings.ringOn && currentSettings.onlyConsiderPixelsInsideRing) {
         circMinX = midPointLoc.x()-currentSettings.currentRingSize;
         circMaxX = midPointLoc.x()+currentSettings.currentRingSize;
         circMinY = midPointLoc.y()-currentSettings.currentRingSize;
         circMaxY = midPointLoc.y()+currentSettings.currentRingSize;
     }


     for(int y = 0; y<displayImage->height(); y++){
         //QRgb * line = (QRgb *)img->scanLine(y);



         for(int x = 0; x<displayImage->width(); x++){
             QRgb currentPixel = displayImage->pixel(x,y);
             int average = (qRed(currentPixel) + qGreen(currentPixel) + qRed(currentPixel))/3;

             //int average = (qRed(line[x]) + qGreen(line[x]) + qRed(line[x]))/3;
             //considerThisPix = true;
             if (useIncludeFilter) {
                 if (!includedPixels[currentPixNum]) {
                     currentPixNum++;

                     continue;
                     //considerThisPix = false;
                 }
                 if (currentSettings.ringOn && currentSettings.onlyConsiderPixelsInsideRing) {
                     if ((x < circMinX)||(x > circMaxX)||(y < circMinY)||(y > circMaxY)) {
                         currentPixNum++;
                         continue;
                     } else {


                         double dist = sqrt(pow((double) x-midPointLoc.x(),2) + pow((double) y-midPointLoc.y(),2));
                         if (dist > currentSettings.currentRingSize) {
                            currentPixNum++;
                            continue;
                         }
                     }
                 }
             }


             if (currentSettings.LEDColorPair == LED_COLOR_RED_GREEN) {
                 int bluethresh = 255-currentSettings.currentThresh;
                 if ((qGreen(currentPixel) > currentSettings.currentThresh)&&(qRed(currentPixel) < currentSettings.currentThresh) && (qGreen(currentPixel) > qBlue(currentPixel)) ) {
                     //The pixel is more green than the other two colors
                     ycounts1[y]++;
                     xcounts1[x]++;
                     totalCount1++;
                     //Change pixel to pure green
                     displayImage->setPixel(x,y, qRgb(0, 255, 0));

                 } else if ((qRed(currentPixel) > currentSettings.currentThresh)&&(qGreen(currentPixel) < currentSettings.currentThresh) && (qRed(currentPixel) > qBlue(currentPixel)) ) {
                     //The pixel is more red than the other two colors
                     ycounts2[y]++;
                     xcounts2[x]++;
                     totalCount2++;
                     //Change pixel to pure red
                     displayImage->setPixel(x,y, qRgb(255, 0, 0));

                 }
             }

             currentPixNum++;
         }
     }

     if (!skipPosCalc) {
         //Now that we have the bright pixel counts, we can calculate the
         //median x and y location of the bright pixels
         int halfCount1 = totalCount1/2;
         int halfCount2 = totalCount2/2;

         //Calculate median location of LED1
         totalCount1 = 0;
         for(int y = 0; y<displayImage->height(); y++){
             totalCount1+=ycounts1[y];
             if (totalCount1 > halfCount1) {
                 LED1Loc.setY(y);
                 break;
             }
         }
         totalCount1 = 0;
         for(int x = 0; x<displayImage->width(); x++){
             totalCount1+=xcounts1[x];
             if (totalCount1 > halfCount1) {
                 LED1Loc.setX(x);
                 break;
             }
         }


         //Calculate median location of LED2
         totalCount2 = 0;
         for(int y = 0; y<displayImage->height(); y++){
             totalCount2+=ycounts2[y];
             if (totalCount2 > halfCount2) {
                 LED2Loc.setY(y);
                 break;
             }
         }
         totalCount2 = 0;
         for(int x = 0; x<displayImage->width(); x++){
             totalCount2+=xcounts2[x];
             if (totalCount2 > halfCount2) {
                 LED2Loc.setX(x);
                 break;
             }
         }

         midPointLoc.setX((LED1Loc.x()+LED2Loc.x())/2);
         midPointLoc.setY((LED1Loc.y()+LED2Loc.y())/2);



         //Now we determine if we need to ask the user for input
         double distBtwLEDs = sqrt(pow((double) LED1Loc.x()-LED2Loc.x(),2) + pow((double) LED1Loc.y()-LED2Loc.y(),2));
         if (distBtwLEDs > 300) {
             //The distance between the LEDs is too high

             //qDebug() << "Problem!" << distBtwLEDs;
             emit badLocation();
         } else if (totalCount1 == 0 || totalCount2 == 0) {
             //No pixels found for at least one LED
             emit badLocation();
         }
     }


     emit newLocation(LED1Loc,LED2Loc,midPointLoc);

 }


 void VideoImageProcessor::newImage(QImage *img, bool flip) {

     /*qDebug() << fpsTimer->elapsed();
     fpsTimer->restart();*/

     //qDebug() << "Processor got image, " << QThread::currentThreadId();
     if (flip) {
        *img = img->mirrored(false,true);
    }

    lastFrame = img;

    //The image to be displayed is not always the same as the image to save, so we need to make a deep copy.

    QImage* displayImage = new QImage(img->copy());

    //qint16 xMedian = 0;
    //qint16 yMedian = 0;


    if (trackingOn) {
        //Figure out which tracking algorithm to use
        if (!currentSettings.twoLEDs || currentSettings.trackDark) {
            processImage(displayImage);
        } else if (currentSettings.LEDColorPair == LED_COLOR_RED_GREEN){
            processImage_2LED_REDGREEN(displayImage);
        } else if (currentSettings.LEDColorPair == LED_COLOR_WHITE_WHITE) {
            processImage(displayImage); //will change
        }
    }

    if (!fastPlayback) {
        emit newImage_signal(displayImage); //Signal to display the image.
    } else {
        emit newImage_signal(displayImage); //Signal to display the image.
        //delete displayImage;
    }

    frameHeight = img->height();
    frameWidth = img->width();

    if (!streamActive) {
            streamActive = true;
            emit streamStarted(img->height(),img->width());
            if (createFileAfterCameraLoad) {
                //This means that we are waiting for the camera to initialize before open up a
                //file.  So we call createFile().
                createFileAfterCameraLoad = false;
                createFile(nextFileName);
            }
     }

     //Add the frame to the buffer (when all info for the frame is filled, it can be saved)
     if (trackingOn || logging) {
         if (!currentSettings.twoLEDs) {
            frameBuffer->addImage(img,QPoint(xMedian,yMedian),QPoint(0,0));
         } else {
             frameBuffer->addImage(img,LED1Loc,LED2Loc);
         }
     } else {
        frameBuffer->addImage(img);
     }



     //delete displayImage; //Not safe across threads!!

     //encoder->encodeImage(img);
 }

 void VideoImageProcessor::createFile(QString filename) {
    //Create all files (video, timestamps, position)

     if (fileCreated) {
         //If a file already exists, the user has switched cameras while a file was open.  This means that we need to close the current
         //file.  Then, we wait for the camera to get initialized, at which point this function gets called again by
         //newImage(...)

         qDebug() << "Setting up next file to open after camera initates.";
         closeFile();
         nextFileName = filename;
         createFileAfterCameraLoad = true;
         streamActive = false;
         return;
     }
     if (streamActive) {

        int fps = 25;
        videoEncoder = new X264VideoEncoder(NULL);
        connect(this,SIGNAL(encodeImage(QImage)),videoEncoder,SLOT(encodeImage(QImage)));
        connect(this,SIGNAL(writeTimeStamp(quint32)),videoEncoder,SLOT(writeTimestamp(quint32)));

        //Turning off this thread for now, it may not be needed.
        /*
        QThread* encoderThread = new QThread();
        //connect(encoderThread ,SIGNAL(started()),videoEncoder,SLOT(startAudio()));
        connect(videoEncoder, SIGNAL(finished()), encoderThread , SLOT(quit()));
        connect(videoEncoder, SIGNAL(finished()), videoEncoder, SLOT(deleteLater()));
        connect(encoderThread , SIGNAL(finished()), encoderThread , SLOT(deleteLater()));
        videoEncoder->moveToThread(encoderThread);
        encoderThread->start();*/

        bool ok = videoEncoder->createFile(filename+".h264",frameWidth,frameHeight,currentVideoFormat,fps);

        if (ok) {
            qDebug() << "Video file created";

        } else {
            qDebug() << "Error creating video file";            
            return;
        }

        ok = videoEncoder->createTimestampFile(filename+".videoTimeStamps", clockRate);
        if (ok) {
            qDebug() << "Timestamp file created";
            fileCreated = true;

        } else {
            qDebug() << "Error creating timestamp file";
            closeFile();
            return;
        }

        if (trackingOn) {
            if (!createPositionFile(filename+".videoPositionTracking")) {
                qDebug() << "Error creating tracking file";
                closeFile();
                return;
            }
        }

        baseFileName = filename;

        emit fileOpened();
        if (recording) {
            emit recordingStarted();
        }

     }
 }

 void VideoImageProcessor::createPlaybackLogFile(QString filename) {
     qDebug() << "Creating log file: " << filename;
     if (!createPositionFile(filename)) {
         //TODO: Code to open pop-up error window
         return;
     }
     logging = true;

 }

 void VideoImageProcessor::closePlaybackLogFile() {
     qDebug() << "Closing log file";
     closePositionFile();
 }

 bool VideoImageProcessor::isFileCreated() {
     return fileCreated;
 }

 /*
 void VideoImageProcessor::createFileAfterCameraLoaded(QString filename) {
     baseFileName = filename;
     createFileAfterCameraLoad = true;
     streamActive = false;
 }*/

 bool VideoImageProcessor::createPositionFile(QString filenameIn) {
     //Create a new position file

     if (!positionFileOpen) {

         qDebug() << "Creating position file: " << filenameIn;
         positionFile = new QFile;
         positionFile->setFileName(filenameIn);

         //Create file
         if (!positionFile->open(QIODevice::ReadWrite)) {
             return false;
         }

         positionFileOpen = true;

         //Write the current settings to file
         positionFile->write("<Start settings>\n");
         QString threshLine = QString("threshold: %1\n").arg(currentSettings.currentThresh);
         positionFile->write(threshLine.toLocal8Bit());
         QString trackDarkLine = QString("dark: %1\n").arg((int) currentSettings.trackDark);
         positionFile->write(trackDarkLine.toLocal8Bit());
         QString clockrateLine = QString("clockrate: %1\n").arg((int) clockRate);
         positionFile->write(clockrateLine.toLocal8Bit());
         QString fieldLine;
         fieldLine += "Fields: ";
         fieldLine += "<time uint32>";
         fieldLine += "<xloc uint16>";
         fieldLine += "<yloc uint16>";
         fieldLine += "<xloc2 uint16>";
         fieldLine += "<yloc2 uint16>";
         fieldLine += "\n";
         positionFile->write(fieldLine.toLocal8Bit());

         positionFile->write("<End settings>\n");
         positionfileStartOfData = positionFile->pos();
         return true;

     } else {
         return false;
     }

 }

 void VideoImageProcessor::closePositionFile() {
     if (positionFileOpen) {
        positionFile->close();
        delete positionFile;
        positionFileOpen = false;
        logging = false;
     }
 }

 void VideoImageProcessor::writePosition(const FrameBundle &fb) {
     if (positionFileOpen) {

         QDataStream outStream(positionFile); //link outStream to the file
         outStream.setByteOrder(QDataStream::LittleEndian);
         //TODO:  put the following line in an if statement,
         //depending on what info should be included in the file
         outStream << fb.timestamp << fb.xloc << fb.yloc << fb.xloc2 << fb.yloc2;
         positionFile->flush();
     }
 }

 void VideoImageProcessor::seekPositionFile(qint32 t) {
     //Seek to a specific position in the position log file, based on the timestamp.
     //Erase everything after that position
     if (positionFileOpen) {

        qint32 timestamp;
        qint16 xloc, yloc, xloc2, yloc2;
        timestamp = -1;
        positionFile->seek(positionfileStartOfData);
        QDataStream inStream(positionFile); //link inStream to the file
        inStream.setByteOrder(QDataStream::LittleEndian);
        qint32 lastPos;
        while ((timestamp < t)&&(!positionFile->atEnd())) {
            lastPos = positionFile->pos();
            inStream >> timestamp >> xloc >> yloc >> xloc2 >> yloc2; //Read in one record
        }
        positionFile->resize(lastPos); //Erases everything after
        positionFile->seek(lastPos);
     }
 }


 void VideoImageProcessor::startRecording() {

     qDebug() << "Got record signal" << streamActive << fileCreated;
     recording = true;

     if (streamActive && fileCreated) {
        emit recordingStarted();
     }
 }

 void VideoImageProcessor::stopRecording() {
     recording = false;
 }

 void VideoImageProcessor::closeFile() {
     videoEncoder->close();
     //videoEncoder->endThread(); //This is used if the encoder lived ina separate thread
     delete videoEncoder; //This is used if the encoder lives in this thread
     fileCreated = false;
     closePositionFile();


 }

 void VideoImageProcessor::toggleLogging(bool on) {

     logging = on;

 }

 void VideoImageProcessor::newTimestamp(quint32 t) {
    //The requested timestamp was received from the master module (ie., Trodes).
     //Now, we can save the image and the timestamp to file.

     //qDebug() << "Time stamp received: " << t;
     frameBuffer->addTimestamp(t);

     FrameBundle completedFrame = frameBuffer->getNextFrame();
     //qDebug() << "Next frame: " << completedFrame.timestamp;

     if (completedFrame.imagePtr != NULL) {
         emit newAnimalLocation(completedFrame.timestamp, completedFrame.xloc, completedFrame.yloc);

         if (recording) {
            //qDebug() << "Writing to file";
            emit encodeImage(*completedFrame.imagePtr);
            emit writeTimeStamp(completedFrame.timestamp);

            //videoEncoder->encodeImage(*completedFrame.imagePtr);
            //videoEncoder->writeTimestamp(completedFrame.timestamp);

            if (trackingOn) {
                if (positionFileOpen) {
                    writePosition(completedFrame);
                } else {
                    qDebug() << "No position file created!!";
                }

            }

            //encoder->encodeImage(*completedFrame.imagePtr);
            //encoder->writeTimestamp(completedFrame.timestamp);
         } else if ((positionFileOpen) && (logging)) {
             writePosition(completedFrame);
         }

         //if the frame existed, we remove it from the buffer
         //frameBuffer->removeLastFrame();
     }

 }

 void VideoImageProcessor::endProcessing() {
     emit finished();
 }

