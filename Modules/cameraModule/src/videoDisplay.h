/*
Trodes is a free, open-source neuroscience data collection and experimental control toolbox

Copyright (C) 2012 Mattias Karlsson

This program is free software: you can redistribute it and/or modify
                               it under the terms of the GNU General Public License as published by
                               the Free Software Foundation, either version 3 of the License, or
                               (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef VIDEODISPLAY_H
#define VIDEODISPLAY_H

#ifdef ARAVIS
#include "araviswrapper.h"
#endif

#ifdef AVT_GIGE //AVT GIGE
#include "avtWrapper.h"
#endif //

#include "webcamWrapper.h"


#include <QtGui>
#include <QtWidgets>
#include <QtMultimedia>
#include <QGLWidget>
#include <QCamera>
//#include <QCameraInfo>
#include "trodesSocket.h"
//#include "../Source/src-main/trodesSocket.h"
#include "videoDecoder.h"
#include "videoEncoder.h"
#include "abstractCamera.h"
#include "dialogs.h"
//#include "videoWriter.h"

//#include <QCameraViewfinder>

//#include <QtCore/QRect>
//#include <QtGui/QImage>
//#include <QtMultimedia/QAbstractVideoSurface>
//#include <QtMultimedia/QVideoFrame>

#define TIMESLIDERSTEPS 50000

#undef PixelFormat
Q_DECLARE_METATYPE(QVector<bool>);
Q_DECLARE_METATYPE(TrackingSettings);

//typedef QVector<bool> PixIncludeArray;

class VideoDisplayWindow;
class VideoImageProcessor;

//void new_frame_callback(ArvStream*, void*); 

/*
class VideoDisplaySurface : public QAbstractVideoSurface {

Q_OBJECT

public:
     VideoDisplaySurface(VideoDisplayWindow *displayWidget, QObject *parent = 0);

     QList<QVideoFrame::PixelFormat> supportedPixelFormats(
             QAbstractVideoBuffer::HandleType handleType = QAbstractVideoBuffer::NoHandle) const;
     bool isFormatSupported(const QVideoSurfaceFormat &format, QVideoSurfaceFormat *similar) const;

     bool start(const QVideoSurfaceFormat &format);
     void stop();

     bool present(const QVideoFrame &frame);

     QRect videoRect() const { return targetRect; }
     void updateVideoRect();

     void paint(QPainter *painter);
     //void sendImage(TrodesClient *tcpClient);
     int thresh;

 private:
     VideoDisplayWindow *widget;
     QImage::Format imageFormat;
     QRect targetRect;
     QSize imageSize;
     QRect sourceRect;
     QVideoFrame currentFrame;

signals:

     void newFrame(QImage *img, bool flip);
     void newFrame();
};

*/

class RubberBandPolygonNode : public QObject, public QGraphicsRectItem {
    Q_OBJECT

public:

    RubberBandPolygonNode(int nodeNum, QGraphicsItem *parent = 0);

private:
    int nodeNum;
    bool dragging;


protected:
    void mouseMoveEvent(QGraphicsSceneMouseEvent* event);
    void mouseReleaseEvent(QGraphicsSceneMouseEvent *event);
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget);

signals:
    void nodeMoved(int nodeNum);
    void nodeMoveFinished();


};

class RubberBandPolygon : public QObject, public QGraphicsPolygonItem {

Q_OBJECT

public:
    RubberBandPolygon(QGraphicsItem *parent = 0);
    ~RubberBandPolygon();
    void addPoint(QPointF);
    void moveLastPoint(QPointF);
    void removeLastPoint();
    bool isIncludeType();
    bool isExcludeType();
    bool isZoneType();
    void setIncludeType();
    void setExcludeType();
    void setZoneType();
    void calculateIncludedPoints(bool* inside, int imageWidth, int imageHeight);


private:
    QVector<RubberBandPolygonNode*> nodes;
    QVector<QPointF> points;
    QVector<QPointF> relativePoints; // points are mapped between 0 and 1 in x,y coordinates
    int type;
    bool dragging;
    //QGraphicsPolygonItem* poly;

protected:
    void mouseMoveEvent(QGraphicsSceneMouseEvent* event);
    void mousePressEvent(QGraphicsSceneMouseEvent *event);
    void mouseReleaseEvent(QGraphicsSceneMouseEvent *event);
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget);

private slots:
    void childNodeMoved(int nodeNum);
    void childNodeMoved();

public slots:
    void updateSize();
    void highlight();
    void removeHighlight();


signals:
    void hasHighlight();
    void shapeChanged();
};

class VideoDisplayWindow : public QWidget  {

Q_OBJECT


public:
    VideoDisplayWindow(QWidget *parent);
    //QAbstractVideoSurface *videoSurface() const { return surface; }
    //VideoDisplaySurface *surface;

    //QSize sizeHint() const;
    ~VideoDisplayWindow();
    QSize getResolution();

protected:
    void paintEvent(QPaintEvent *event);
    void resizeEvent(QResizeEvent *event);
    void enterEvent(QEvent *event);
    void leaveEvent(QEvent *event);

public slots:
    //void newImage(const QImage &image);
    void newImage(QImage *image);

private:

    QImage             currentImage;
    QSize              currentSize;
    //QCameraViewfinder *viewfinder;

signals:
    void resolutionChanged();

};

class CustomScene : public QGraphicsScene {

    Q_OBJECT

signals:
    void emptySpaceClicked();
protected:
    void mousePressEvent(QGraphicsSceneMouseEvent *event)
    {
        //check to see if the item pressed was a polygon or the background (a widget showing video)
        if(itemAt(event->scenePos(),QTransform())) {
            QGraphicsItem *clickedItem = itemAt(event->scenePos(),QTransform());
            if (!clickedItem->isWidget()) {
                //a polygon was clicked
                QGraphicsScene::mousePressEvent((event));
                return;
            }
        }
        emit emptySpaceClicked();
    }
};

class GraphicsWindow : public QGraphicsView {

    Q_OBJECT

public:
    GraphicsWindow(QWidget *parent);
    VideoDisplayWindow *dispWin; //shows the video frames
    void addIncludePolygon();
    void addExcludePolygon();
    void addZonePolygon();

protected:
    void resizeEvent(QResizeEvent *event);
    void mousePressEvent(QMouseEvent *event);
    void mouseDoubleClickEvent(QMouseEvent *event);
    void mouseMoveEvent(QMouseEvent *event);
    void keyPressEvent(QKeyEvent *event);

private:
    CustomScene *scene;
    QVector<RubberBandPolygon*> polygons;
    QGraphicsEllipseItem *medianLocMarker;
    QGraphicsEllipseItem *LED1Marker;
    QGraphicsEllipseItem *LED2Marker;
    QGraphicsEllipseItem *ringMarker;
    QGraphicsLineItem *directionMarker;

    bool isTrackingOn;
    bool showTwoLeds;

    TrackingSettings trackSettings;
    QSizeF ringSize;
    QPointF medianLoc;
    QPointF LED1Loc;
    QPointF LED2Loc;

    int currentIncludePolygon;
    bool currentlyDrawing;
    int currentlySelectedPolygon;
    int currentTool;


public slots:
    void polygonHighlighted();
    void spaceClicked();
    void setTool(int toolNum);
    void setNoHighlight();
    void newLocation(QPoint p);
    void newLocation(QPoint p1, QPoint p2, QPoint midPoint);
    void locMarkerOn(bool on);
    void newSettings(TrackingSettings s);

private slots:
    void calculateConsideredPixels();

signals:
    void newIncludeCalculation(QVector<bool>);
    void userInput1(QPoint loc);
    void userInput2(QPoint loc);
    //void newIncludeCalculation(PixIncludeArray);


};

#ifdef ARAVIS
class GigEVideoDisplayController : public QObject {

Q_OBJECT

public:
	GigEVideoDisplayController(QObject *parent, VideoDisplayWindow *displayWidget, TrodesClient *tcpClient, VideoEncoder *encoder);
	~GigEVideoDisplayController();
	void new_frame_callback(ArvStream*, void*); 

private:
    TrodesClient     *tcpClient;
    VideoImageProcessor *imageProcessor;
    QThread             *imageProcessorThread;

    void _init_aravis_gige();
    AravisWrapper *_arv;
	GigECameraInterface *_cur_camera;

	
public slots:
    //void setThresh(int thresh);
    //void endProcessor();
    //void startRecording();
    //void stopRecording();


signals:
	//void newFrame(const QImage& frame);
	void newFrame(QImage* frame);
	void dummy();
    //void videoStreamStart(int yResolution, int xResolution);
    //void signal_startRecording();
    //void signal_stopRecording();
//
};
#endif

class VideoDisplayController : public QObject {

Q_OBJECT

public:
    VideoDisplayController(QObject *parent, VideoDisplayWindow *displayWidget, TrodesClient *tcpClient);
    //VideoDisplayController(QObject *parent, VideoDisplayWindow *displayWidget, TrodesClient *tcpClient, VideoWriter *writer);
    ~VideoDisplayController();
    //VideoDisplayWindow *videoWindow;
    //QAbstractVideoSurface *videoSurface() const { return surface; }
    //VideoDisplaySurface *surface;
    VideoImageProcessor *imageProcessor;
    QStringList availableCameras();


private:
    QList<AbstractCamera*> cameraControllers;
    int currentCameraType;
    int currentCameraNum;
    bool liveCameraMode;
    quint32 sliderStart;
    quint32 sliderEnd;

    bool fastPlayback;

    void closeCurrentCamera();

    /*QCamera *cameraInput;
#ifdef AVT_GIGE
    AvtCameraController *avtCamera;
#endif
    */

    H264_Decoder *decoder;
    TrodesClient     *tcpClient;
    QThread             *imageProcessorThread;
    //void _init_qcamera(QCameraInfo*);
    QTimer *frameReadTimer;
    uint64_t currentFrameNum;

    QTimer displayFirstFrameTimer;
    QVector<quint32> playbackTimeStamps;

private slots:
    void readNextFrameFromFile();
    void displayFirstFrameFromFile();


public slots:
    //void setThresh(int thresh, bool trackDark);
    void endProcessor();
    void startRecording();
    void stopRecording();
    void newCameraSelected(int cameraID);
    bool inputFileSelected(QString fileName);
    void startPlayback();
    void startFastPlayback();
    void pausePlayback();
    void closePlaybackFile();
    void seekFrame(quint64);
    void stepForward();
    void stepBackward();
    void seekRelativeFrame(int position); //slider position input
    void seekToTime(quint32 t); //seek to the frame closest to the given time
    void newSliderRange(int start, int end);
    void newTimeRange(quint32 start, quint32 end);
    void startFirstCameraFound();
    void startController();
    void closeDown();
    void startFirstFrameTimer();
    void setLiveCameraMode(bool);
    void pauseForBadLoc();
    void userInput1(QPoint loc);
    void userInput2(QPoint loc);


signals:
    void videoStreamStart(int yResolution, int xResolution);
    void signal_startRecording();
    void signal_stopRecording();
    void toggleLogging(bool);
    void signal_createFile(QString filename);
    void signal_createPlaybackLogFile(QString);
    void signal_closePlaybackLogFile();
    void signal_closeFile();
    void signal_nextFileNeeded();
    void signal_newSettings(TrackingSettings t);

    void newFrame(QImage *img);
    //void newFrame(const QImage &img);
    void newAnimalPos(quint32 time, qint16 x, qint16 y);
    void filePlaybackReady();
    void filePlaybackClosed();
    void filePlaybackStopped();
    void videoFileTimeRate(quint32);
    void videoFileTimeRange(quint32, quint32);
    void newVideoTimeStamp(quint32);
    void finished();
    void sig_seekFrame(quint64);
    void signal_startFirstFrameTimer();
    void processorCreated();
    void getUserTrackingInput();
    void sig_UserInput1(QPoint loc);
    void sig_UserInput2(QPoint loc);
    void sig_newSliderRange(quint32 start, quint32 end);

};


class FrameBundle {

public:
    FrameBundle();
    void setImage(QImage *imagePtr);
    void setTime(quint32 timestamp);
    void setLocation(qint16 x1, qint16 y1, qint16 x2, qint16 y2);

    void deleteFrame();
    bool bothFieldsFilled();

    QImage      *imagePtr;
    quint32     timestamp;
    qint16      xloc;
    qint16      yloc;
    qint16      xloc2;
    qint16      yloc2;

private:
    bool        videoFieldFilled;
    bool        timeFieldFilled;
    bool        locationFieldFilled;

};

class VideoImageBuffer : public QObject {

    Q_OBJECT

public:

    VideoImageBuffer(QObject *parent);
    ~VideoImageBuffer();
    void addImage(QImage *imagePtr);
    void addImage(QImage *imagePtr,QPoint loc1,QPoint loc2);
    void addTimestamp(quint32 timestamp);
    FrameBundle getNextFrame();
    void removeLastFrame();

private:

    int bufferSize;
    FrameBundle *frameBundleBuffer;
    int imageWriteHead;
    int timestampWriteHead;
    int frameReadHead;
    int lastFrameReadHead;

};

class VideoImageProcessor : public QObject {

Q_OBJECT

public:
    VideoImageProcessor(QObject *parent);
    //VideoImageProcessor(QObject *parent,VideoWriter *writer);
    ~VideoImageProcessor();
    //VideoEncoder    *encoder;
    //VideoWriter     *writer;
    quint32 clockRate;
    bool isFileCreated();
    //void createFileAfterCameraLoaded(QString filename);


private:

    VideoImageBuffer    *frameBuffer;
    QImage              *lastFrame;
    bool                streamActive;
    bool                createFileAfterCameraLoad;

    bool                recording;
    bool                trackingOn;
    qint16              xMedian;
    qint16              yMedian;
    QPoint              LED1Loc;
    QPoint              LED2Loc;
    QPoint              midPointLoc;
    //int                 brightPixThreshold;
    //bool                trackDark;
    //int                 ringSize;
    //int                 ringOn;
    bool                fileCreated;
    QString             baseFileName;
    QString             nextFileName;
    TrackingSettings    currentSettings;
    AbstractCamera::videoFmt    currentVideoFormat;

    X264VideoEncoder*   videoEncoder;
    int                 frameHeight;
    int                 frameWidth;

    bool fastPlayback;

    bool createPositionFile(QString filename);
    void closePositionFile();
    void writePosition(const FrameBundle &fb);
    void processImage(QImage *image);
    void processImage_2LED_REDGREEN(QImage *image);

    QFile *positionFile;
    QString positionFileName;
    bool positionFileOpen;
    qint64 positionfileStartOfData;
    bool logging;  //for offline position logging

    QVector<bool> includedPixels;
    bool skipPosCalc;

    QElapsedTimer *fpsTimer;


public slots:

    void newImage(QImage *img, bool flip);
    void replotLastFrame();
    void newTimestamp(quint32 t);
    void endProcessing();
    void initialize();
    void createFile(QString filename);
    void createPlaybackLogFile(QString filename);
    void seekPositionFile(qint32 t);
    void closePlaybackLogFile();
    void toggleLogging(bool on);
    void startRecording();
    void stopRecording();
    void closeFile();
    void setFastPlayback(bool on);

    void newTrackingSettings(TrackingSettings t);
    //void newThreshold(int value, bool trackDark);
    //void newRing(int ringSize, bool ringOn);
    void setTracking(bool);
    void setIncludedPixels(QVector<bool> includedPixels);
    void setVideoFormat(AbstractCamera::videoFmt);

    void userInput1(QPoint loc);
    void userInput2(QPoint loc);
    //void setIncludedPixels(PixIncludeArray includedPixels);


signals:
    void finished();
    void streamStarted(int yResolution, int xResolution);
    void fileOpened();
    void recordingStarted();
    void recordingStopped();
    void newImage_signal(QImage *img);
    void newAnimalLocation(quint32 time, qint16 x, qint16 y);
    void encodeImage(const QImage &);
    void writeTimeStamp(quint32 t);
    void newLocation(QPoint p);
    void newLocation(QPoint p1, QPoint p2, QPoint midPoint);
    void badLocation();

};


#endif // VIDEODISPLAY_H
