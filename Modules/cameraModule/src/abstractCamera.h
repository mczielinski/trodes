#ifndef ABSTRACTCAMERA_H
#define ABSTRACTCAMERA_H

#include <QtWidgets>
#include <stdint.h>



class AbstractCamera: public QObject {

    Q_OBJECT

public:
    AbstractCamera();

    //Currently supported video formtats:
    enum videoFmt {
          Fmt_Invalid,
          Fmt_RGB24,
          Fmt_RGB32,
          Fmt_ARGB32
    };

    virtual QStringList availableCameras() = 0; //Get a list of available device names
    void setFormat(videoFmt format); //set the current video format
    videoFmt getFormat(); //get the current video format

public slots:

    //These are pure virtual public slots, so they need to be defined in your derived camera class.
    //The camera module uses these slots to operate the camera
    //---------------------------------------------
    virtual bool open(int cameraID) = 0; //opens a camera using the index of the list from availableCameras()
    virtual void close() = 0; //closes the camera
    virtual bool start() = 0; //starts acquistion
    virtual void stop() = 0; //stops acquisition   
    //---------------------------------------------


protected slots:

    void sendFrameSignals(QImage* img, bool flip);


protected:
    uint64_t numFramesRecieved;
    videoFmt fmt;

signals:

    void newFrame(QImage* img, bool flip); //signal picked up by the video processor
    void newFrame(); //used to send a message to Trodes forget the current time
    void formatSet(AbstractCamera::videoFmt format);

};


#endif // ABSTRACTCAMERA_H
