#ifndef WEBCAMWRAPPER_H
#define WEBCAMWRAPPER_H

#include "abstractCamera.h"
#include <QtMultimedia>
#include <QCamera>
//#include <QCameraInfo>




class WebcamDisplaySurface : public QAbstractVideoSurface {

Q_OBJECT

public:
     WebcamDisplaySurface(QObject *parent = 0);

     QList<QVideoFrame::PixelFormat> supportedPixelFormats(
             QAbstractVideoBuffer::HandleType handleType = QAbstractVideoBuffer::NoHandle) const;
     bool isFormatSupported(const QVideoSurfaceFormat &format, QVideoSurfaceFormat *similar) const;

     bool start(const QVideoSurfaceFormat &format);
     void stop();

     bool present(const QVideoFrame &frame);
     QImage::Format getFormat();

     QRect videoRect() const { return targetRect; }
     //void updateVideoRect();
     //void paint(QPainter *painter);


 private:

     QImage::Format imageFormat;
     QRect targetRect;
     QSize imageSize;
     QRect sourceRect;
     QVideoFrame currentFrame;
     QElapsedTimer frameTimer;

signals:

     void newFrame(QImage *img, bool flip);
     void newFrame();
};

class WebcamWrapper: public AbstractCamera {

    Q_OBJECT

public:
    WebcamWrapper();
    ~WebcamWrapper();
    QStringList availableCameras(); //Get a list of available device names

private:

    WebcamDisplaySurface *surface;
    QAbstractVideoSurface *videoSurface() const { return surface; }
    QCamera *cameraInput;

    bool isCameraOpen;
    bool isCameraAcquiring;
    uint64_t numFramesRecieved;

public slots:
    bool open(int cameraID); //opens a camera using the index of the list from availableCameras()
    void close(); //closes the camera
    bool start(); //starts acquistion
    void stop(); //stops acquisition


private slots:
    void cameraStateChanged(bool active);

signals:

    //void newFrame(QImage* img, bool flip);
    //void newFrame();

};






#endif // WEBCAMWRAPPER_H
