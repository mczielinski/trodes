/*
Trodes is a free, open-source neuroscience data collection and experimental control toolbox

Copyright (C) 2012 Mattias Karlsson

This program is free software: you can redistribute it and/or modify
                               it under the terms of the GNU General Public License as published by
                               the Free Software Foundation, either version 3 of the License, or
                               (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "dialogs.h"


TrackingSettings::TrackingSettings() {
    currentThresh = 0;
    trackDark = true;

    currentRingSize = 0;
    ringOn = false;
    onlyConsiderPixelsInsideRing = true;
}


//-----------------------------------------------------
ToolsDialog::ToolsDialog(int currentTool, QWidget *parent)
    :QFrame(parent)
{




    pointToolButton = new TrodesButton();
    pointToolButton->setText("Fix");
    pointToolButton->setFixedSize(40,40);
    pointToolButton->setProperty("ID",1);
    connect(pointToolButton,SIGNAL(clicked()),this,SLOT(toolButtonPressed()));

    excludeToolButton = new TrodesButton();
    excludeToolButton->setText("Excl");
    excludeToolButton->setFixedSize(40,40);
    excludeToolButton->setProperty("ID",2);
    connect(excludeToolButton,SIGNAL(clicked()),this,SLOT(toolButtonPressed()));

    includeToolButton = new TrodesButton();
    includeToolButton->setText("Incl");
    includeToolButton->setFixedSize(40,40);
    includeToolButton->setProperty("ID",3);
    connect(includeToolButton,SIGNAL(clicked()),this,SLOT(toolButtonPressed()));

    zoneToolButton = new TrodesButton();
    zoneToolButton->setText("Zone");
    zoneToolButton->setFixedSize(40,40);
    zoneToolButton->setProperty("ID",4);
    connect(zoneToolButton,SIGNAL(clicked()),this,SLOT(toolButtonPressed()));

    editToolButton = new TrodesButton();
    editToolButton->setText("Edit");
    editToolButton->setFixedSize(40,40);
    editToolButton->setProperty("ID",5);
    connect(editToolButton,SIGNAL(clicked()),this,SLOT(toolButtonPressed()));

    switch (currentTool) {
    case 1:
        pointToolButton->setDown(true);
        break;
    case 2:
        excludeToolButton->setDown(true);
        break;
    case 3:
        includeToolButton->setDown(true);
        break;
    case 4:
        zoneToolButton->setDown(true);
        break;
    case 5:
        editToolButton->setDown(true);
        break;
    }

    QGridLayout *mainLayout = new QGridLayout;
    mainLayout->addWidget(pointToolButton,0,0,Qt::AlignHCenter);
    mainLayout->addWidget(zoneToolButton,0,1,Qt::AlignHCenter);
    mainLayout->addWidget(excludeToolButton,1,0,Qt::AlignHCenter);
    mainLayout->addWidget(includeToolButton,1,1,Qt::AlignHCenter);
    mainLayout->addWidget(editToolButton,2,0,Qt::AlignHCenter);

    //mainLayout->setRowStretch(3,1);
    mainLayout->setMargin(0);
    mainLayout->setContentsMargins(2,2,2,2);
    mainLayout->setSpacing(0);

    setLayout(mainLayout);
    //setWindowTitle(tr("Sound"));

}

void ToolsDialog::toolButtonPressed() {
    TrodesButton* button = dynamic_cast<TrodesButton*>(sender());
    int buttonID = button->property("ID").toInt();

    if (buttonID != currentSelectedTool) {
        pointToolButton->setDown(false);
        excludeToolButton->setDown(false);
        includeToolButton->setDown(false);
        zoneToolButton->setDown(false);
        editToolButton->setDown(false);
        switch (buttonID) {
        case 1:
            pointToolButton->setDown(true);
            break;
        case 2:
            excludeToolButton->setDown(true);
            break;
        case 3:
            includeToolButton->setDown(true);
            break;
        case 4:
            zoneToolButton->setDown(true);
            break;
        case 5:
            editToolButton->setDown(true);
            break;
        }
        currentSelectedTool = buttonID;
        emit toolActivated(currentSelectedTool);
        this->close();
    }
}


void ToolsDialog::closeEvent(QCloseEvent *event) {

    emit windowOpenState(false);
    event->accept();
    emit windowClosed();
}

//-----------------------------------------------------------

//-----------------------------------------------------
SettingsDialog::SettingsDialog(TrackingSettings settings, QWidget *parent)
    :QFrame(parent) {

    currentSettings = settings;
    //trackDark = settings.trackDark;

    //currentThresh = settings.currentThresh;

    //ringOn = settings.ringOn;
    //currentRingSize = settings.currentRingSize;


    //------------------------------
    //Threshold settings
    threshBox = new QGroupBox(tr("Threshold"),this);
    QGridLayout *threshLayout = new QGridLayout(threshBox);
    threshLayout->setVerticalSpacing(5);
    threshLayout->setContentsMargins(5,5,5,5);
    threshBox->setCheckable(false);
    threshBox->setFixedHeight(90);

    threshSlider = new QSlider(Qt::Horizontal);
    threshSlider->setMaximum(255);
    threshSlider->setMinimum(0);
    threshSlider->setSingleStep(1);

    threshSlider->setValue(currentSettings.currentThresh);
    threshSlider->setFixedHeight(20);
    threshDisplay = new QLabel;
    threshDisplay->setNum(currentSettings.currentThresh);

    flipButton = new TrodesButton;
    flipButton->setFixedHeight(20);
    flipButton->setFixedWidth(70);
    if (currentSettings.trackDark) {
        flipButton->setText("Dark");
    } else {
        flipButton->setText("Bright");
    }
    connect(flipButton,SIGNAL(pressed()),this,SLOT(flipButtonPressed()));

    threshLayout->addWidget(threshDisplay,0,1,Qt::AlignHCenter);
    threshLayout->addWidget(threshSlider,1,1,Qt::AlignHCenter);
    threshLayout->addWidget(flipButton,2,1,Qt::AlignHCenter);
    //threshBox->setLayout(threshLayout);
    //----------------------


    //------------------------
    //Exclusion ring settings
    ringBox = new QGroupBox(tr("Ring"),this);
    QGridLayout *ringLayout = new QGridLayout(ringBox);
    ringLayout->setVerticalSpacing(2);
    ringLayout->setContentsMargins(5,5,5,5);
    ringBox->setCheckable(true);
    ringBox->setChecked(currentSettings.ringOn);
    ringBox->setFixedHeight(60);
    ringSizeSlider = new QSlider(Qt::Horizontal);
    ringSizeSlider->setMaximum(255);
    ringSizeSlider->setMinimum(0);
    ringSizeSlider->setSingleStep(1);

    ringSizeSlider->setValue(currentSettings.currentRingSize);    
    ringSizeSlider->setFixedHeight(20);
    ringSizeDisplay = new QLabel;
    ringSizeDisplay->setNum(currentSettings.currentRingSize);

    /*
    if (currentSettings.currentOperationMode & CAMERAMODULE_CAMERASTREAMINGMODE) {
        ringBox->setChecked(false);
        ringBox->setEnabled(false);
    }*/

    ringLayout->addWidget(ringSizeDisplay,0,1,Qt::AlignHCenter);
    ringLayout->addWidget(ringSizeSlider,1,1,Qt::AlignHCenter);

    QFont labelFont;
    labelFont.setPixelSize(10);
    LEDBox = new QGroupBox(tr("Two LEDs"),this);
    QGridLayout *numPointsLayout = new QGridLayout(LEDBox);
    numPointsLayout->setVerticalSpacing(5);
    numPointsLayout->setHorizontalSpacing(5);
    numPointsLayout->setContentsMargins(1,5,1,5);
    LEDBox->setCheckable(true);
    LEDBox->setChecked(currentSettings.twoLEDs);
    LEDBox->setFixedHeight(70);
    LEDBox->setFixedWidth(100);
    colorOneButton = new TrodesButton();
    colorOneButton->setText("White-White");

    numPointsLayout->addWidget(colorOneButton,1,1,Qt::AlignHCenter);

    numPointsLayout->setColumnStretch(0,1);
    numPointsLayout->setColumnStretch(3,1);





    QGridLayout *mainLayout = new QGridLayout;
    mainLayout->addWidget(threshBox,0,0,Qt::AlignHCenter);
    mainLayout->addWidget(ringBox,1,0,Qt::AlignHCenter);
    mainLayout->addWidget(LEDBox,2,0,Qt::AlignHCenter);

    mainLayout->setRowStretch(3,1);
    mainLayout->setMargin(10);
    //mainLayout->setContentsMargins(0,0,0,0);

    connect(threshSlider,SIGNAL(valueChanged(int)),this,SLOT(updateValues(int)));
    connect(ringSizeSlider,SIGNAL(valueChanged(int)),this,SLOT(updateRingSize(int)));
    connect(ringBox,SIGNAL(toggled(bool)),this,SLOT(toggleRing(bool)));

    connect(colorOneButton,SIGNAL(pressed()),this,SLOT(colorButton1Pressed()));
    connect(LEDBox,SIGNAL(toggled(bool)),this,SLOT(toggleTwoLEDs(bool)));


    setLayout(mainLayout);

    updateColorButtonLabels();

    //setWindowTitle(tr("Sound"));

}

void SettingsDialog::colorButton1Pressed() {
    currentSettings.LEDColorPair = (currentSettings.LEDColorPair+1)%2;
    updateColorButtonLabels();
    emit settingsChanged(currentSettings);
}


void SettingsDialog::updateColorButtonLabels() {
    if (currentSettings.trackDark) {
        LEDBox->setEnabled(false);
    } else {
        LEDBox->setEnabled(true);
    }

    switch (currentSettings.LEDColorPair) {
    case LED_COLOR_WHITE_WHITE:
        colorOneButton->setText("White-White");
        break;
    case LED_COLOR_RED_GREEN:
        colorOneButton->setText("Red-Green");
        break;
    }
}

void SettingsDialog::flipButtonPressed() {
    currentSettings.trackDark = !currentSettings.trackDark;
    if (currentSettings.trackDark) {
        flipButton->setText("Dark");
        LEDBox->setEnabled(false);
    } else {
        flipButton->setText("Bright");
        LEDBox->setEnabled(true);
    }

    emit settingsChanged(currentSettings);

    //updateValues(currentThresh);

}

void SettingsDialog::updateValues(int value) {
    threshDisplay->setNum(value);
    currentSettings.currentThresh = value;

    emit settingsChanged(currentSettings);
    //emit newThresh(value, trackDark);
}

void SettingsDialog::updateRingSize(int value) {
    ringSizeDisplay->setNum(value);
    currentSettings.currentRingSize = value;
    emit settingsChanged(currentSettings);
    //emit newRing(value,ringBox->isChecked());
}

void SettingsDialog::toggleRing(bool on) {
    currentSettings.ringOn = on;
    emit settingsChanged(currentSettings);
    //emit newRing(currentRingSize,on);
}

void SettingsDialog::toggleTwoLEDs(bool on) {
    currentSettings.twoLEDs = on;
    emit settingsChanged(currentSettings);
}

void SettingsDialog::closeEvent(QCloseEvent *event) {

    emit windowOpenState(false);
    event->accept();
    emit windowClosed();
}

//-----------------------------------------------------------

//-----------------------------------------------------
SourceDialog::SourceDialog(QStringList availableCameras, QWidget *parent)
    :QFrame(parent) {

    sourceListMenu = new QMenu();

    for (int i = 0; i < availableCameras.length(); i++) {
        cameraListActions.append(new QAction(this));
        cameraListActions.last()->setText(availableCameras[i]);
        cameraListActions.last()->setData(i);
        connect(cameraListActions.last(),SIGNAL(triggered()),this,SLOT(newCameraItemSelected()));
    }


    /*
    QList<QCameraInfo> availableCameras = QCameraInfo::availableCameras();
    for (int i = 0; i < availableCameras.length(); i++) {
        cameraListActions.append(new QAction(this));
        cameraListActions.last()->setText(availableCameras[i].description());
        cameraListActions.last()->setData(i);
        connect(cameraListActions.last(),SIGNAL(triggered()),this,SLOT(newCameraItemSelected()));
    }
    */
    sourceListMenu->addActions(cameraListActions);

    /*
    cameraMenuItem = new QMenu("Cameras",this);
    cameraMenuItem->addActions(cameraListActions);
    sourceListMenu->addMenu(cameraMenuItem);

    fileMenuAction = new QAction("File...",this);
    sourceListMenu->addAction(fileMenuAction);
    connect(fileMenuAction,SIGNAL(triggered()),this,SLOT(fileActionSelected()));
    */

    //QGridLayout *frameLayout = new QGridLayout;

    //QFrame windowFrame;
    //windowFrame.set
    //setStyleSheet("QFrame {border: 2px solid lightgray;"
    //                      "border-radius: 4px}");

    //frameLayout->addWidget(&windowFrame,0,0);

    //win
    QGridLayout *mainLayout = new QGridLayout;
    mainLayout->addWidget(sourceListMenu,0,0);



    //mainLayout->setRowStretch(3,1);
    mainLayout->setMargin(0);
    //mainLayout->setContentsMargins(0,0,0,0);

    //connect(threshSlider,SIGNAL(valueChanged(int)),this,SLOT(updateValues(int)));

    setLayout(mainLayout);
    //setLayout(frameLayout);
    //setWindowTitle(tr("Sound"));

}

void SourceDialog::fileActionSelected() {
    //Used the saved system settings from the last session as the default folder
    QSettings settings(QLatin1String("SpikeGadgets"), QLatin1String("cameraModule"));
    settings.beginGroup(QLatin1String("paths"));
    QString tempPath = settings.value(QLatin1String("inputFilePath")).toString();

    settings.endGroup();

    QString fileName = QFileDialog::getOpenFileName(0, QString("Select file to open"),tempPath,"h264 files (*.h264 *.mpg)");
    if (!fileName.isEmpty()) {

        //Save the folder in system setting for the next session
        QFileInfo fi(fileName);
        QSettings settings(QLatin1String("SpikeGadgets"), QLatin1String("cameraModule"));
        settings.beginGroup(QLatin1String("paths"));
        settings.setValue(QLatin1String("inputFilePath"), fi.absoluteFilePath());
        settings.endGroup();

        //Load the config file
        emit inputFileSelected(fi.absoluteFilePath());
        close();
    }
}

void SourceDialog::newCameraItemSelected() {
    QAction *senderAction = qobject_cast<QAction*>(sender());
    //QAction *senderAction = senderSignalIndex()

    emit cameraSelected(senderAction->data().toInt());
    close();
}


void SourceDialog::closeEvent(QCloseEvent *event) {

    //emit windowOpenState(false);
    event->accept();
    emit windowClosed();
}

//---------------------------------------------

TrodesButton::TrodesButton(QWidget *parent)
    :QPushButton(parent) {

    setStyleSheet("QPushButton {border: 2px solid lightgray;"
                          "border-radius: 4px;"
                          "padding: 2px;}"
                          "QPushButton::pressed {border: 2px solid gray;"
                          "border-radius: 4px;"
                          "padding: 2px;}"
                          "QPushButton::checked {border: 2px solid gray;"
                          "border-radius: 4px;"
                          "padding: 2px;}");

    QFont buttonFont;
    buttonFont.setPointSize(12);
    connect(&blinkTimer,SIGNAL(timeout()),this,SLOT(toggleBlink()));



}

void TrodesButton::setRedDown(bool yes) {
    if (yes) {
        setStyleSheet("QPushButton {border: 2px solid lightgrey;"
                              "border-radius: 4px;"
                              "padding: 2px;}"
                              "QPushButton::pressed {border: 2px solid red;"
                              "border-radius: 4px;"
                              "padding: 2px;}"
                              "QPushButton::checked {border: 2px solid red;"
                              "border-radius: 4px;"
                              "padding: 2px;}");
    } else {
        setStyleSheet("QPushButton {border: 2px solid lightgray;"
                              "border-radius: 4px;"
                              "padding: 2px;}"
                              "QPushButton::pressed {border: 2px solid gray;"
                              "border-radius: 4px;"
                              "padding: 2px;}"
                              "QPushButton::checked {border: 2px solid gray;"
                              "border-radius: 4px;"
                              "padding: 2px;}");
    }
}


void TrodesButton::setBlinkOn() {
    blinkTimer.start(500);
}

void TrodesButton::setBlinkOff() {
    blinkTimer.stop();
}

void TrodesButton::toggleBlink() {
    setChecked(!isChecked());
}


