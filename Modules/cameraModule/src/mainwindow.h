/*
Trodes is a free, open-source neuroscience data collection and experimental control toolbox

Copyright (C) 2012 Mattias Karlsson

This program is free software: you can redistribute it and/or modify
                               it under the terms of the GNU General Public License as published by
                               the Free Software Foundation, either version 3 of the License, or
                               (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include "videoDisplay.h"
#include <QMainWindow>
#include "videoEncoder.h"
#include "trodesSocket.h"
#include "configuration.h"
#include "dialogs.h"


extern NetworkConfiguration *networkConf;

class Style_tweaks : public QProxyStyle
{
    public:

        void drawPrimitive(PrimitiveElement element, const QStyleOption *option,
                           QPainter *painter, const QWidget *widget) const
        {
            /* do not draw focus rectangles - this permits modern styling */
            if (element == QStyle::PE_FrameFocusRect)
                return;

            QProxyStyle::drawPrimitive(element, option, painter, widget);
        }
};

class MySlider : public QSlider {
//A custom slider that inherits the QSlider.  It has two extra sliders (besides the main one
//that QSlider provides).  These are used to restrict the range of the slider to a region of interest.

Q_OBJECT

public:
    MySlider(QWidget *parent = 0);

private:
    int startBorder;
    int endBorder;
    bool draggingStart;
    bool draggingEnd;
    bool draggingKnob;
    bool logMarker[TIMESLIDERSTEPS];

public slots:
    void setStart(int newStartBorder); //set the start of the time range
    void setEnd(int newEndBorder); //set the end of the time range
    void markCurrentLocation();
    void clearLogMarks();
    void clearLogMarksAfterCurrentPos();


protected:
    void mousePressEvent (QMouseEvent * event);
    void mouseReleaseEvent(QMouseEvent *event);
    void paintEvent(QPaintEvent *event);
    void mouseMoveEvent(QMouseEvent *event);

signals:

    void newRange(int start,int end);
};

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QStringList options, QWidget *parent = 0);
    ~MainWindow();

protected:
    void closeEvent(QCloseEvent* event);
    void resizeEvent(QResizeEvent *);
    void moveEvent(QMoveEvent *);
    void keyPressEvent(QKeyEvent *);
    //bool eventFilter(QObject *obj, QEvent *ev);

private:
    QTimer                      closeTimer;
    bool                        programIsClosing;
    bool                        sourceDialogOpen;
    bool                        toolsDialogOpen;
    int                         currentTool;
    TrackingSettings            trackSettings;


    VideoDisplayController      *videoController;
    VideoDisplayWindow          *displayWindow;
    GraphicsWindow              *graphicsWindow;
    QGridLayout                 *mainLayout;
    TrodesModuleNetwork         *moduleNet;

    QStatusBar                  *statusbar;

    TrodesButton                 *settingsButton;
    TrodesButton                 *sourceButton;
    TrodesButton                 *pauseButton;
    TrodesButton                 *playButton;
    TrodesButton                 *trackButton;
    TrodesButton                 *toolsButton;
    //TrodesButton                 *offlineTrackButton;


    QLabel                      *timeLabel;

    QLineEdit                   *fileStartTimeLabel;
    QLineEdit                   *fileEndTimeLabel;
    MySlider                    *filePositionSlider;


    qint8 moduleID;
    uint32_t currentTime;
    quint8 cameraNum;

    uint32_t clockRate;

    int resX;
    int resY;
    QString serverAddress;
    QString serverPortValue;
    //int currentThresh;
    //bool trackDark;
    bool videoStreaming;
    bool fileInitiated;
    bool fileOpen;
    bool recording;
    bool justSeekedFlag;

    bool inputFileOpen;
    bool playbackMode;
    QString inputFileName;
    quint32 fileStartTime;
    quint32 fileEndTime;
    quint32 startSliderTime;
    quint32 endSliderTime;
    bool videoWasPlayingBeforeSliderPress;
    bool isVideoFilePlaying;
    bool isVideoSliderPressed;

    QString fileName;
    QString baseFileName;
    unsigned char currentOperationMode;
    bool trackingOn;
    bool loggingOn;

    int moduleInstance;

    bool convertTimeString(QString tString, quint32 &t);

private slots:
    void closeAfterDelay();
    void setTime(quint32);
    void setTimeRate(quint32);
    void setVideoTimeRange(quint32, quint32);
    void setSliderTimeRange(quint32 start, quint32 end);

    void newSettings(TrackingSettings t);
    //void setThresh(int newThresh, bool trackDark);
    //void setRing(int ringSize, bool ringOn);
    void settingsButtonPressed();
    void sourceButtonPressed();
    void toolsButtonPressed();
    void setSourceMenuClosed();
    void setToolsMenuClosed();

    void trackingButtonPressed();
    void trackingButtonReleased();
    void setTracking(bool);


    void videoStarted(int resolutionY, int resolutionX);
    void startRecording();
    void stopRecording();

    //void createFile();
    void closeFile();
    void sendNewAnimalPosition(quint32 time, qint16 x, qint16 y);
    void turnOnPlayback(QString);
    void turnOffPlayback();
    void activatePlaybackControls();
    void playbackSliderPositionChanged(int position);
    void playbackSliderPressed();
    void playbackSliderReleased();
    void setFilePlaybackStopped();


    void pauseButtonPressed();
    void playButtonPressed();
    void pauseButtonReleased();
    void playButtonReleased();

    void setRecordStatusOn();
    void setRecordStatusOff();
    void setFileOpenedStatus();
    void setFileClosedStatus();

    void setCurrentTool(int toolNum);
    void connectProcessor();

    void getUserInput();

    void newStartTimeEntered();
    void newEndTimeEntered();
    void setFullTimeRange();
    void rewindFileToStartRange();

    void setModuleInstance(int instanceNum);


//    void setTcpClientConnected();
//    void setTcpClientDisconnected();
//    void tcpConnect(QString addressIn,quint16 portIn);
//    void setModuleID(qint8 ID);


public slots:
    void setupFile(QString filename);
    void nextFileRequested();
    void setFileOpen();


signals:
    void signal_startRecording();
    void signal_stopRecording();
    void signal_createFile(QString file_name);
    void signal_closeFile();
    void signal_startPlayback();
    void signal_startFastPlayback();
    void signal_pausePlayback();
    void signal_stepFrameForward();
    void signal_stepFrameBackward();
    void signal_createPlaybackLogFile(QString file_name);
    void signal_closePlaybackLogFile();
    void signal_newSettings(TrackingSettings t);

    void closeDialogs();

};


#endif // MAINWINDOW_H
