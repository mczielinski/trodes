# fsSockets.py: Shared code for messaging between fsData and fsGUI / trodes
#
# Copyright 2015 Loren M. Frank
#
# This program is part of the trodes data acquisition package.
# trodes is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# trodes is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
#
import sys
sys.path.append('..')

from fsShared import *
import socket
import struct

class messagestruct:
    type = 0
    datalen = 0
    data = bytearray()

class posdatastruct:
    x = 0
    y = 0
    cameraID = 0

class digiodatastruct:
    port = 0
    input = False
    state = 0

class datastruct:
    timestamp = 0
    datatype = 0
    contdata = 0
    posdata = posdatastruct()
    digiodata = digiodatastruct()


class fssocket:
    'socket for connection to and from FSData and trodes'


    def __init__(self, hostname, port):
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        try:
            self.sock.connect((hostname, port))
        except:
            print('fsDataPy:Error connecting socket to', hostname, 'port', port)
        self.enabled = False
        self.dataclientinfo = []

    def sendmessage(self, messagetype, data):
        # pack the message type and length and then add the data, which must be a bytearray
        packeddata = struct.pack('=BI', messagetype, len(data))
        packeddata += data
        datalen = len(packeddata)
        totalsent = 0
        totalsent = self.sock.sendall(packeddata)
        return totalsent

    def getmessage(self):
        #print('socket reading message')
        mstruct = messagestruct() # this is the object that is returned when data are read
        bytes_recd = 0
        # get the message type
        data = self.sock.recv(1)
        mstruct.type = struct.unpack_from('B', data)[0]
        #print('socket got type ', mstruct.type)
        # get the message size (uint_32t)
        data = self.sock.recv(4)
        mstruct.datalen = int(struct.unpack_from('I', data)[0])
        #print('socket got type ', mstruct.type, ' datalen ', mstruct.datalen)
        toread = mstruct.datalen
        # clear out the data
        mstruct.data = bytearray()
        while toread > 0:
            chunk = self.sock.recv(toread)
            if chunk == '':
                raise RuntimeError("socket connection broken")
            mstruct.data.extend(chunk)
            toread -= len(chunk)
        return mstruct

    def getdata(self):
        dstruct = datastruct() # this is the object that is returned when data are read
        # get the message type
        data = self.sock.recv(1)
        dstruct.datatype = struct.unpack_from('B', data)[0]
        #print('socket got datatype ', dstruct.datatype)
        # get the message size (uint_32t)
        data = self.sock.recv(4)
        datalen = int(struct.unpack_from('I', data)[0])
        #print('socket got type ', mstruct.type, ' datalen ', mstruct.datalen)
        # we assume the data messages are relatively small
        data = self.sock.recv(datalen)
        if data == '':
            raise RuntimeError("socket connection broken")
        # parse the data
        # the first four bytes are always the timestamp
        dstruct.timestamp = struct.unpack_from('=I', data)[0]
        if (dstruct.datatype == TRODESDATATYPE_CONTINUOUS):
            # read in the continuous data sample
            dstruct.contdata = struct.unpack_from('=h', data, 4)[0]
        elif (dstruct.datatype == TRODESDATATYPE_POSITION):
            # read in the position data sample
            dstruct.posdata.x, dstruct.posdata.y, dstruct.posdata.cameraID = struct.unpack_from('=HHB',data, 4)
        elif (dstruct.datatype == TRODESDATATYPE_DIGITALIO):
            # read in the digital IO data: integer port number, a character input or output value and a character state value
            dstruct.digiodata.port, dstruct.digiodata.input, dstruct.digiodata.state = struct.unpack_from('=icc', data, 4)
        return dstruct

def SendMessage(sock, message, data):
    sock.sendmessage(message, data)

class hardwaresocket:
    'socket for connection to MCU hardware'

    def __init__(self, hostName, port):
        try:
            self.sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
            self.hostName = hostName
            self.port = port
            self.sock.bind((hostName, port))  #is this necessary?
        except:
            print('Error creating MCU/ECU socket')
            self.sock = -1

    def senddata(self, data):
        datalen = len(data)
        totalsent = 0
        if self.sock > 0:
            while totalsent < datalen:
                sent = self.sock.sendto(data[totalsent:], (self.hostName, self.port))
                if sent == 0:
                    raise RuntimeError("hardware socket connection broken")
                totalsent = totalsent + sent
        else:
            print('hardware socket not connected')

class fsudpsocket:
    'socket for connection to UDP data socketse'

    def __init__(self, hostName, port):
        try:
            self.sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
            self.hostName = hostName
            self.port = port
            self.sock.connect((hostName, port))
        except:
            print('Error creating UDP socket to', hostName, 'port', port)
            self.sock = -1

    def sendmessage(self, messagetype, data):
        # pack the message type and length and then add the data, which must be a bytearray
        packeddata = struct.pack('=BI', messagetype, len(data))
        packeddata += data
        datalen = len(packeddata)
        totalsent = 0
        totalsent = self.sock.sendall(packeddata)
        return totalsent

    def getdata(self):
        dstruct = datastruct() # this is the object that is returned when data are read
        # get the message
        data = self.sock.recv(1024)
        dstruct.datatype = struct.unpack_from('B', data)[0]
        #print('socket got datatype ', dstruct.datatype)
        # get the message size (uint_32t)
        datalen = int(struct.unpack_from('I', data, 1)[0])
        #print('socket got type ', mstruct.type, ' datalen ', mstruct.datalen)
        # get rid of the first five bytes
        data = data[5:]
        if data == '':
            raise RuntimeError("socket connection broken")
        # parse the data
        # the first four bytes are always the timestamp
        dstruct.timestamp = struct.unpack_from('=I', data)[0]
        if (dstruct.datatype == TRODESDATATYPE_CONTINUOUS):
            # read in the continuous data sample
            dstruct.contdata = struct.unpack_from('=h', data, 4)[0]
        elif (dstruct.datatype == TRODESDATATYPE_POSITION):
            # read in the position data sample
            dstruct.posdata.x, dstruct.posdata.y, dstruct.posdata.cameraID = struct.unpack_from('=HHB',data, 4)
        elif (dstruct.datatype == TRODESDATATYPE_DIGITALIO):
            # read in the digital IO data: integer port number, a character input or output value and a character state value
            dstruct.digiodata.port, dstruct.digiodata.input, dstruct.digiodata.state = struct.unpack_from('=icc', data, 4)
        return dstruct



