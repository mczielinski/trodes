import sys, getopt

sys.path.append('/home/loren/Src/Trodes/Modules/FSGui')

from fsShared import *
from fsFilters import *


def processdata(datasock, rtf):

    inLockout = False
    stim = True

    # parse the message
    data = datasock.getdata()

    rtf.timestamp = data.timestamp

    # update the lockout variables
    stimTimeDiff = rtf->timestamp - rtf->lastStimTime
    if rtf.rippleFilter.parm.enabled:
        rtf.rippleFilter.inLockout = (stimTimeDiff < rtf.rippleFilter.parm.lockoutTime)
        inLockout |= rtf.rippleFilter.inLockout
    if rtf.spatialFilter.parm.enabled:
        rtf.spatialFilter.inLockout = (stimTimeDiff < rtf.spatialFilter.parm.lockoutTime)
        inLockout |= rtf->spatialFilter.inLockout

    if data.datatype == TRODESDATATYPE_CONTINUOUS:
        if rtf.latencyTestEnabled:
            if rtf.latencyFilter.processContinuousData(rtf.timestamp):
                rtf.ecusocket.senddata(rtf.latencyFilter.parm.internal.stateScriptFnNum)
            if rtf.rippleFilter.parm.enabled:
                stim = stim and rtf.rippleFilter.ProcessRippleData(datasock.dataclientinfo.nTrodeId, data.contdata, rtf.stimEnabled, rtf.timestamp)
            if rtf.spatialFilter.parm.enabled:
                # the spatialFilter stim value is updated by position data
                if rtf.rippleFilter.parm.enabled == False:
                    # special condition: change stim on state change
                    stim = stim and rtf.spatialFilter.stimOn and rtf.spatialFilter.stimChanged
                else:
                    stim = stim and rtf.spatialFilter.stimOn
    elif (data.datatype == TRODESDATATYPE_POSITION:
        if rtf.rippleFilter.parm.enabled:
            stim = stim and rtf.rippleFilter.stimOn
        if rtf.spatialFilter.parm.enabled:
             if rtf.rippleFilter.parm.enabled == False:
                # special condition: change stim on state change
                lastStim = rtf.spatialFilter.stimOn
                stim = stim and rtf.spatialFilter.ProcessSpatialData(data.posdata.x, data.posdata.y, data.timestamp) and \
                        rtf.spatialFilter.stimChanged
            else:
                stim = stim and rtf.spatialFilter.ProcessSpatialData(data.posdata.x, data.posdata.y, data.timestamp)
    elif (data.datatype == TRODESDATATYPE_DIGITALIO)
        if rtf.rippleFilter.parm.enabled:
            stim = stim and rtf.rippleFilter.stimOn
        if rtf.spatialFilter.parm.enabled:
            if rtf.rippleFilter.parm.enabled == False:
                # special condition: change stim on state change
                stim = stim and rtf.spatialFilter.stimOn and rtf.spatialFilter.stimChanged
            else:
                stim = stim && rtf->spatialFilter.stimOn;
        if rtf.latencyFilter.internal.parm.enabled:
            if rtf.latencyFilter.processDigitalIOData(rtf.digOutState, rtf.timestamp, data.digiodata.port, \
                                                      data.digiodata.input, data.digiodata.state):
            `   #Trigger function again
                rtf.ecusocket.senddata(rtf.latencyFilter.parm.internal.stateScriptFnNum)
            #update the state vectors
           if data.digiodata.input:
               rtf.digInState[data.digiodata.port] = data.digiodata.states;
           else:
               rtf.digOutState[data.digiodata.port] = data.digiodata.state;

    # check to see if stimulation is enabled
    if rtf->stimEnabled:
        if stim and not inLockout:
            rtf.spatialFilter.stimChanged = False;
            rtf.lastStimTime = rtf.timestamp
            rtf.startStimulation()
            # immediately send the settle command
            rtf.sendSettleCommand()
        elif rtf.spatialFilter.stimChanged and not inLockout:
            rtf.spatialFilter.stimChanged = False # why is this false?
            # we had beens stimulating and now we need to stop
            rtf.stopStimulation()
