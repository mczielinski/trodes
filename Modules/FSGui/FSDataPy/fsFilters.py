
import struct
import numpy as np
import sys
sys.path.append('..')
from fsShared import *

NFILT = 19 #the number of points in the IIR ripple filter
NSPEED_FILT_POINTS = 15 #the number of points in the speed filter
NLAST_VALS = 20 # the number of values to average over for the gain of the ripple filter

fNumerator = np.array([2.435723358568172431e-02,
-1.229133831328424326e-01,
 2.832924715801946602e-01,
-4.629092463232863941e-01,
 6.834398182647745124e-01,
-8.526143367711925825e-01,
 8.137704425816699727e-01,
-6.516133270563613245e-01,
 4.138371933419512372e-01,
 2.165520280363200556e-14,
-4.138371933419890403e-01,
 6.516133270563868596e-01,
-8.137704425816841836e-01,
 8.526143367711996879e-01,
-6.834398182647782871e-01,
 4.629092463232882815e-01,
-2.832924715801954929e-01,
 1.229133831328426407e-01,
-2.435723358568174512e-02])


fDenominator = np.array([1.000000000000000000e+00,
-7.449887056735371438e+00,
 2.866742370538527496e+01,
-7.644272470167831557e+01,
 1.585893197862293391e+02,
-2.703338821178639932e+02,
 3.898186201116285474e+02,
-4.840217978093359079e+02,
 5.230782138295531922e+02,
-4.945387299274730140e+02,
 4.094389697124813665e+02,
-2.960738943482194827e+02,
 1.857150345772943751e+02,
-9.980204002570326338e+01,
 4.505294594295533273e+01,
-1.655156422615593215e+01,
 4.683913633549676270e+00,
-9.165841559639211766e-01,
 9.461443242601841330e-02])

speedFilterValues = np.array([0.0779,
0.0775,
0.0768,
0.0758,
0.0745,
0.0728,
0.0709,
0.0688,
0.0663,
0.0637,
0.0610,
0.0581,
0.0551,
0.0520,
0.0488])



class SingleChanRippleFilter:
    def __init__(self):
        self.posGain = 0.0
        self.nTrodeId = -1 # the user assigned number of this nTrode
        self.enabled = 0  # true if this Ntrode is enabled
        self.reInit()

    def reInit(self):
        global NFILT
        global NLAST_VALS
        self.rippleMean = 0.0
        self.rippleSd = 0.0
        self.fX = np.zeros(NFILT)
        self.fY = np.zeros(NFILT)
        self.filtind = 0
        self.lastVal = np.zeros(NLAST_VALS)
        self.lvind = 0
        self.currentVal = 0.0
        self.currentThresh = 0.0


class RippleFilter:
    def __init__(self, numNTrodes, fsguisock):
        self.numNTrodes = numNTrodes
        self.fsguisock = fsguisock
        self.filtNum = fNumerator
        self.filtDen = fDenominator
        self.chanRippleFilter = [SingleChanRippleFilter() for i in range(numNTrodes)]
        self.parm = RippleFilterParameters()
        self.parm.enabled = False
        self.reInit()

    def reInit(self):
        self.stimOn = False
        self.inLockout = False
        self.lastRipTime = 0
        self.ResetRippleData()
        self.ResetRippleCounters()

    def ResetRippleData(self):
        for c in self.chanRippleFilter:
            c.reInit()

    def ResetRippleCounters(self):
        self.counter = 0

    def UpdateRippleParms(self, data):
        #copy the parameters into the local parms structure. Is there a better way to do this?
        # int sampDivisor;
        # double ripCoeff1
        # double ripCoeff2;
        # double ripple_threshold;
        # int n_above_thresh;
        # int lockoutTime;
        # int detectNoRippleTime;
        # bool detectNoRipples;
        # bool enabled;
        ptmp = struct.unpack('idddiii??xx', data)  #NOTE: the pad bytes required trial and error
        self.parm.sampDivisor = ptmp[0]
        self.parm.ripCoeff1 = ptmp[1]
        self.parm.ripCoeff2 = ptmp[2]
        self.parm.ripple_threshold = ptmp[3]
        self.parm.n_above_thresh = ptmp[4]
        self.parm.lockoutTime = ptmp[5]
        self.parm.detectNoRippleTime = ptmp[6]
        self.parm.detectNoRipples = ptmp[7]
        self.parm.enabled = ptmp[8]
        print('ripple enabled', self.parm.enabled)

    def ProcessRippleData(self, nTrodeIndex, d, stimEnabled, currentTime, lastStimTime):
        crf = self.chanRippleFilter[nTrodeIndex]
        #print('in ProcessRippleData, ntindex', nTrodeIndex, 'inLo', self.inLockout, 'counter', self.counter)
        if (self.inLockout):
            # filter a scaled version of the data to ensure continuity
            rd = self.FilterChannel(crf, ((currentTime - lastStimTime) / self.parm.lockoutTime) * d)
            crf.currentVal = crf.rippleMean
            return False

        rd = self.FilterChannel(crf, d)
        # we need time to build up the estimate of the ripple power, so we wait at 10000 samples
        if (self.counter < 10000):
            self.counter += 1
            self.stimOn = False
            return self.stimOn
        # process the data if we get to this point
        y = abs(rd)

        #print('y =', y)
        # only update the values if we are not stimulating
        if not stimEnabled:
            crf.rippleMean = crf.rippleMean + (y - crf.rippleMean) / self.parm.sampDivisor
            crf.rippleSd = crf.rippleSd + (abs(y - crf.rippleMean) - crf.rippleSd) / self.parm.sampDivisor
            crf.currentThresh = crf.rippleMean + crf.rippleSd * self.parm.ripple_threshold
            #print('ntrode', crf.nTrodeId, 'mean', crf.rippleMean)

        # track the rising and falling of the signal
        df = y - crf.currentVal
        if (df > 0):
            gain = self.parm.ripCoeff1
            crf.posGain = self.updateLastVal(crf, gain)
            crf.currentVal += df * crf.posGain
        else:
            gain = self.parm.ripCoeff2
            crf.posGain = self.updateLastVal(crf,gain)
            crf.currentVal += df * gain

        # set stimOn based on the number of channels above their thresholds
        self.stimOn = (self.nAboveRippleThresh() >= self.parm.n_above_thresh)
        #print('ProcessRipData, stimOn: ', self.stimOn)
        if self.stimOn:
            # record the time of the last ripple
            self.lastRipTime = currentTime
        if not self.parm.detectNoRipples:
            return self.stimOn
        else:
            return ((currenttime - self.lastRipTime) > self.parm.detectNoRippleTime)


    def FilterChannel(self, crf, d):
        # return the results of filtering the current value and update the filter values
        val = 0
        crf.fX[crf.filtind] = d
        crf.fY[crf.filtind] = 0
        # apply the IIR filter this should be done with a dot product eventually
        for i in range(NFILT):
            jind = (crf.filtind + i) % NFILT
            val = val + crf.fX[jind] * self.filtNum[i] - crf.fY[jind] * self.filtDen[i]
        crf.fY[crf.filtind] = val
        crf.filtind -= 1
        if (crf.filtind < 0):
            crf.filtind += NFILT
        return val

    def updateLastVal(self, crf, d):
        # return the new gain for positive increments based on the gains from the last 20 points
        mn = np.mean(crf.lastVal)
        crf.lastVal[crf.lvind] = d
        crf.lvind = (crf.lvind + 1) % NLAST_VALS
        return mn

    def nAboveRippleThresh(self):
        nAbove = 0
        for i in range(self.numNTrodes):
            if (self.chanRippleFilter[i].enabled) and \
               (self.chanRippleFilter[i].currentVal > self.chanRippleFilter[i].currentThresh):
               nAbove += 1
        return nAbove

    def sendStatusUpdate(self, timeSinceLast):
        # the first byte is the status type
        s = bytearray(1)
        s[0] = ord(FS_RIPPLE_STATUS)
        # the rest of the array is the status string
        if self.parm.enabled:
            s.extend('Ripple Filter Enabled\n\n'.encode('utf-8'))
            for i in range(self.numNTrodes):
                crf = self.chanRippleFilter[i]
                if (crf.enabled):
                    s.extend('{}: Ripple mean (std): {:.2f} ({:.2f})\n'.format(crf.nTrodeId,
                                                             crf.rippleMean, crf.rippleSd).encode('utf-8'))
            s.extend('\nTimestamps since last {}\n'.format(timeSinceLast).encode('utf-8'))
        else:
                s.extend('Ripple Filter Disabled\n'.encode('utf-8'))
        self.fsguisock.sendmessage(FS_RT_STATUS, s)



class SpatialFilter:
    def __init__(self, fsguisock):
        self.fsguisock = fsguisock
        self.parm = SpatialFilterParameters()
        self.speedFilt = speedFilterValues
        self.parm.enabled = False
        self.reInit()

    def reInit(self):
        self.stimOn = False
        self.stimChanged = False
        self.lastChange = 0
        self.xpos = 0
        self.ypos = 0
        # reset the speed related variables
        self.speed = np.zeros(NSPEED_FILT_POINTS)
        self.ind = NSPEED_FILT_POINTS - 1
        self.lastx = 0
        self.lasty = 0

    def UpdateSpatialParms(self, data):
        #copy the parameters into the local parms structure. Is there a better way to do this?
        #    int lowerLeftX;
        #    int lowerLeftY;
        #    int upperRightX;
        #    int upperRightY;
        #    double minSpeed;
        #    double maxSpeed;
        #   double cmPerPix;
        #    int   lockoutTime;
        #     bool enabled;

        print('updating spatial parms, len = ', len(data))
        ptmp = struct.unpack('=iiiidddi?xxx', data) # note that the = prevents additional alignment bytes from being added
        self.parm.lowerLeftX = ptmp[0]
        self.parm.lowerLeftY = ptmp[1]
        self.parm.upperRightX = ptmp[2]
        self.parm.upperRightY = ptmp[3]
        self.parm.minSpeed = ptmp[4]
        self.parm.maxSpeed = ptmp[5]
        self.parm.cmPerPix = ptmp[6]
        self.parm.lockoutTime = ptmp[7]
        self.parm.enabled = ptmp[8]
        print('spatial filter enabled', self.parm.enabled)

    def ProcessSpatialData(xpos, ypos, timestamp):
        currentStim = self.stimOn
        self.xpos = xpos
        self.ypos = ypos

        # check to see if we are within the lockout time for changes in state
        if ((timestamp - self.lastChange) > self.parm.lockoutTime):
            return stimOn

        #calculate speed
        animalSpeed = filterPosSpeed(xpos, ypos)

        #check if speed is in window
        if ((animalSpeed < self.parm.minSpeed) or (animalSpeed > self.parm.maxSpeed)):
            self.stimOn = false
        else:
            # check to see if the location is in the specified box
            self.stimOn = ((self.xpos >= self.parm.xmin) and \
                      (self.xpos <= self.parm.xmax) and \
                      (self.ypos >= self.parm.ymin) and \
                      (self.ypos <= self.parm.ymax))
        # check to see if we are changing the stim state
        if (self.stimOn != currentStim):
            self.stimChanged = True
            self.lastChange = timestamp
        return self.stimOn

    def sendStatusUpdate(self):
        s = bytearray(1)
        s[0] = ord(FS_SPATIAL_STATUS)
        # the rest of the array is the status string
        if self.parm.enabled:
            s.extend('Spatial Filter Enabled\n'.encode('utf-8'))
        else:
            s.extend('Spatial Filter Disabled\n'.encode('utf-8'))
        s.extend('Animal Location: {}, {}\n'.format(self.xpos, self.ypos).encode('utf-8'))
        if self.parm.enabled:
            if self.stimOn:
                s.extend('Filter Triggered\n'.encode('utf-8'))
            else:
                s.extend('Filter not triggered\n'.encode('utf-8'))
        self.fsguisock.sendmessage(FS_RT_STATUS, s)



class ThetaFilter:
    def __init__(self, fsguisock):
        self.fsguisock = fsguisock
        self.stimOn = False
        parm = ThetaFilterParameters()

    def reInit(self):
        stimOn = False
        inLockout = False



class LatencyFilter:
    def __init__(self, fsguisock):
        self.fsguisock = fsguisock

        self.lastDIOState = False
        self.parm = LatencyParameters()
        self.parm.internal.enabled = False
        self.reInit()

    def reInit(self):
        self.counter = 0
        self.triggered = False
        self.lastTriggerTime = 0
        self.firstPulseDetected = False
        self.lastLatency = 0
        self.latencySum = 0
        self.nMeasurements = 0
        self.maxLatency = 0
        self.minLatency = 100000000

    def UpdateLatencyParms(self, data):
        print('updating latency parms, len  =', len(data))
        ptmp = struct.unpack('=?Hii?')
        self.parm.internal.enabled = ptmp[0]
        self.parm.internal.stateScriptFnNum = ptmp[1]
        self.parm.internal.outputDIOPort = ptmp[2]
        self.parm.internal.testInterval = ptmp[3]
        self.parm.hpc.enabled = ptmp[4]
        print('updating latency parms', self.parm.internal.enabled)

    def processContinuousData(timestamp):
        if (self.parm.internal.enabled):
            if ((timestamp - self.lastTriggerTime) > self.parm.internal.testInterval):
                if (not self.triggered):
                    print('fsProcessData: ProcessData() Trigger DIO, time', timestamp, '\n')
                    self.triggered = True
                    self.lastTriggerTime = timestamp
                    return True
        elif (self.parm.hpc.enabled):
            print( 'hpc enabled')
            # TODO: HPC Latency test
        return False

    def processDigitalIOData(digOutState, timestamp, port, input, state):
        if (port == self.port.internal.outputDIOPort):
            print( 'Latency Filter: ', self.maxLatency, self.minLatency, state)
            if (self.triggered and state):
                self.lastLatency = timstamp - lastTriggerTime
                latencySum += self.lastLatency
                self.maxLatency = max((self.maxLatency, self.lastLatency))
                self.minLatency = min((self.minLatency, self.lastLatency))
                self.nMeasurements += 1
                print( 'Hit ', timestamp)
                self.triggered = False
                return True
        return False

    def sendLatencyData(time):
        latencystr = '{}'.format(time)
        fsguisock.sendmessage(FS_LATENCY_DATA, latencystr)

    def sendStatusUpdate(self):
        # the first byte is the status type
        s = bytearray(1)
        s[0] = ord(FS_LATENCY_STATUS)
        # the rest of the array is the status string
        if self.parm.internal.enabled:
            self.meanLatency = self.latencySum / self.nMeasurements
            s.extend('Latency Test Enabled'.encode('utf-8'))
            s.extend('Last latency: {} samples = {} ms at 30 KHz\n'.format(self.lastLatency, self.lastLatency / 30.0).encode('utf-8'))
            s.extend('Mean latency: {} samples = {} ms at 30 KHz\n'.format(self.meanLatency, self.meanLatency / 30.0).encode('utf-8'))
            s.extend('Max latency: {} samples = {} ms at 30 KHz\n'.format(self.maxLatency, self.maxLatency / 30.0).encode('utf-8'))
            s.extend('Min latency: {} samples = {} ms at 30 KHz\n'.format(self.minLatency, self.minLatency / 30.0).encode('utf-8'))
        else:
            s.extend('Latency Test Disabled\n'.encode('utf-8'))

        self.fsguisock.sendmessage(FS_RT_STATUS, s)


class RTFilter:
    def __init__(self, numntrodes, fsguisock):
        self.fsguisock = fsguisock
        self.numNTrodes = numntrodes
        self.nDigInPorts = 0
        self.nDigOutPorts = 0
        self.digInState = []
        self.digOutState = []
        self.stimEnabled = False
        self.latencyTestEnabled = False
        self.contNTrodeEnabled = [False for i in range(numntrodes)]
        self.spikeNTrodeEnabled = [False for i in range(numntrodes)]
        self.lastStimTime = 0
        self.timestamp = 0
        self.rippleFilter = RippleFilter(self.numNTrodes, fsguisock)
        self.spatialFilter = SpatialFilter(self.fsguisock)
        self.thetaFilter = ThetaFilter(self.fsguisock)
        self.latencyFilter = LatencyFilter(self.fsguisock)
        self.validfunction = []
        self.ecusocket = []
        self.mcusocket = []

    def resetRealTimeProcessing(self):
        self.rippleFilter.reInit()
        self.spatialFilter.reInit()
        self.latencyFilter.reInit()
        self.lastStimTime = 0

    def sendStatusUpdate(self):
        # the first byte is the status type
        s = bytearray(1)
        s[0] = ord(FS_STIM_STATUS)
        # the rest of the array is the status string
        if (self.stimEnabled):
            if (self.timestamp - self.lastStimTime > 30000):
                s.extend('Feedback ENABLED\n'.encode('utf-8'))
            else:
                s.extend('Feedback TRIGGERED\n'.encode('utf-8'))
        else:
            s.extend('Feedback OFF\n'.encode('utf-8'))
        self.fsguisock.sendmessage(FS_RT_STATUS, s)

    def startStimulation(self):
        for i in self.validfunction:
            data = struct.pack('H', i) # pack as uint16_t
            self.ecusocket.senddata(data)

    def stopStimulation(self):
        for i in self.validfunction:
            # the stop function is the valid function number + 1
            data = struct.pack('=H', i+1) # pack as uint16_t
            self.ecusocket.senddata(data)

    def sendSettleCommand(self):
        settlecommand = struct.pack('=H', 0x6601)
        self.mcusocket.senddata(settlecommand)

    def processdata(self, datasock):
    
        inLockout = False
        stim = True
    
        # get the data and parse it
        data = datasock.getdata()
    
        self.timestamp = data.timestamp
    
        # update the lockout variables
        stimTimeDiff = self.timestamp - self.lastStimTime
        if self.rippleFilter.parm.enabled:
            self.rippleFilter.inLockout = (stimTimeDiff < self.rippleFilter.parm.lockoutTime)
            inLockout |= self.rippleFilter.inLockout
        if self.spatialFilter.parm.enabled:
            self.spatialFilter.inLockout = (stimTimeDiff < self.spatialFilter.parm.lockoutTime)
            inLockout |= self.spatialFilter.inLockout

        if data.datatype == TRODESDATATYPE_CONTINUOUS:
            if self.latencyTestEnabled:
                if self.latencyFilter.processContinuousData(self.timestamp):
                    self.ecusocket.senddata(self.latencyFilter.parm.internal.stateScriptFnNum)
            if self.rippleFilter.parm.enabled:
                stim = self.rippleFilter.ProcessRippleData(datasock.dataclientinfo.nTrodeIndex, data.contdata, self.stimEnabled, self.timestamp, self.lastStimTime) and stim
            if self.spatialFilter.parm.enabled:
                # the spatialFilter stim value is updated by position data
                if self.rippleFilter.parm.enabled == False:
                    # special condition: change stim on state change
                    stim = self.spatialFilter.stimOn and self.spatialFilter.stimChanged and stim
                else:
                    stim = self.spatialFilter.stimOn and stim
        elif data.datatype == TRODESDATATYPE_POSITION:
            self.spatialFilter.xpos = data.posdata.x;
            self.spatialFilter.ypos = data.posdata.y;
            if self.rippleFilter.parm.enabled:
                stim = self.rippleFilter.stimOn and stim
            if self.spatialFilter.parm.enabled:
                if self.rippleFilter.parm.enabled == False:
                    # special condition: change stim on state change
                    lastStim = self.spatialFilter.stimOn
                    stim = self.spatialFilter.ProcessSpatialData(data.posdata.x, data.posdata.y, data.timestamp) and \
                            self.spatialFilter.stimChanged and stim
                else:
                    stim = self.spatialFilter.ProcessSpatialData(data.posdata.x, data.posdata.y, data.timestamp) and stim
        elif data.datatype == TRODESDATATYPE_DIGITALIO:
            if self.rippleFilter.parm.enabled:
                stim = stim and self.rippleFilter.stimOn
            if self.spatialFilter.parm.enabled:
                if self.rippleFilter.parm.enabled == False:
                    # special condition: change stim on state change
                    stim = stim and self.spatialFilter.stimOn and self.spatialFilter.stimChanged
                else:
                    stim = stim and self.spatialFilter.stimOn;
            if self.latencyFilter.parm.internal.enabled:
                if self.latencyFilter.processDigitalIOData(self.digOutState, self.timestamp, data.digiodata.port, \
                                                          data.digiodata.input, data.digiodata.state):
                    #Trigger function again
                    self.ecusocket.senddata(self.latencyFilter.parm.internal.stateScriptFnNum)
                #update the state vectors
                if data.digiodata.input:
                    self.digInState[data.digiodata.port] = data.digiodata.states;
                else:
                    self.digOutState[data.digiodata.port] = data.digiodata.state;
              
        # check to see if stimulation is enabled
        if self.stimEnabled:
            if stim and not inLockout:
                self.spatialFilter.stimChanged = False;
                self.lastStimTime = self.timestamp
                self.startStimulation()
                # immediately send the settle command
                self.sendSettleCommand()
            elif self.spatialFilter.stimChanged and not inLockout:
                self.spatialFilter.stimChanged = False # why is this false?
                # we had beens stimulating and now we need to stop
                self.stopStimulation()
