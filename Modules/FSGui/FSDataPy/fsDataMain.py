
 #  fsdata.py: Program for getting data and processing data from trodes and delivering
 #  real-time feedback
 #
 #
 #
 # Copyright 2015 Loren M. Frank
 #
 # This program is part of the trodes data acquisition package.
 # trodes is free software; you can redistribute it and/or modify
 # it under the terms of the GNU General Public License as published by
 # the Free Software Foundation; either version 2 of the License, or
 # (at your option) any later version.
 #
 # trodes is distributed in the hope that it will be useful,
 # but WITHOUT ANY WARRANTY; without even the implied warranty of
 # MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 # GNU General Public License for more details.
 #

import sys, getopt

sys.path.append('..')

from fsShared import *
import select
from fsFilters import *
from fsSockets import *
import struct


def main(argv):

    #parse the command line arguments
    port = 0
    hostName = '';

    try:
        opts, args = getopt.getopt(argv,"",["port=","hostName="])
    except getopt.GetoptError:
        print('Usage: fsdata.py --port <fsGUIServerPort> --hostName <fsGUIServerHost>')
        sys.exit(2)

    for opt, arg in opts:
        if opt == '--port':
            port = int(arg)
        elif opt == "--hostName":
            hostName = arg

    if ((port == 0) or (hostName == '')):
        print('Usage: fsdata.py --port <fsGUIServerPort> --hostName <fsGUIServerHost>')
        sys.exit(2)

    #get the TCP/IP connection to the FSGui server
    print('FSData: getting TCPIP socket to FSGui server', hostName, 'port', port)

    fsguisocket = fssocket(hostName, port)
    # create the rtf object and pass in the socket so it can be passed to the subclasses
    print('FSData: got TCPIP socket to FSGui server', hostName, 'port', port)

    a = bytearray(10)
    fsguisocket.sendmessage(1,a)

    inputsocketlist = [fsguisocket.sock]
    datasocket = []
    datasocketfdlist = []
    rtf = []

    done = 0
    while not done:
        #check the sockets for messages or data
        readable, writeable, exception = select.select(inputsocketlist, [], [])
        for s in readable:
            # check the data sockets
            if (s != fsguisocket.sock):
                # get the index of this datasocket
                socketindex = datasocketfdlist.index(s)
                # read and process the data from this datasocket
                rtf.processdata(datasocket[socketindex])
            else:
                #this is a message from fsgui
                message = fsguisocket.getmessage();
                #print('fsData got message, type', message.type)
                #parse the message
                if (message.type == TRODESMESSAGE_ECUHARDWAREINFO):
                    # set the hardware address and open a UDP socket to it
                    hni = HardwareNetworkInfo()
                    hni.port, tmpaddress = struct.unpack('H80s', message.data)
                    hni.address = tmpaddress.partition(b'\0')[0].decode('utf-8')
                    print('trying to get hardware socket to ', hni.address, ' port ', hni.port)
                    rtf.ecusocket = hardwaresocket(hni.address, hni.port)
                elif (message.type == TRODESMESSAGE_MCUHARDWAREINFO):
                    # set the hardware address and open a UDP socket to it
                    hni = HardwareNetworkInfo()
                    hni.port, tmpaddress = struct.unpack('H80s', message.data)
                    hni.address = tmpaddress.partition(b'\0')[0].decode('utf-8')
                    print('trying to get hardware socket to ', hni.address, ' port ', hni.port)
                    rtf.mcusocket = hardwaresocket(hni.address, hni.port)
                elif (message.type == TRODESMESSAGE_NUMCONTNTRODES):
                    # the message contains the number of nTrodes
                    numnt = struct.unpack('i', message.data)
                    # create the filter object
                    rtf = RTFilter(numnt[0], fsguisocket)
                elif (message.type == TRODESMESSAGE_NUMSPIKENTRODES):
                    # the message contains the number of spiking data nTrodes; right now we don't use this.
                    numnt = struct.unpack('i', message.data)
                    # create the filter object
                elif (message.type == TRODESMESSAGE_STARTDATACLIENT):
                    #the message contains a DataClientInfo structure
                    # int  socketType;
                    # uint8_t dataType;
                    # uint16_t nTrodeId;
                    # int nTrodeIndex;
                    # uint16_t decimation;
                    # unsigned short port;
                    # char hostName[80];
                    #print('datasize', len(message.data), 'format size', struct.calcsize('BHiHH80s'))
                    tmpdc = struct.unpack('iBHiHH80s', message.data)
                    # move elements into a DataClient class for clarity
                    dc = DataClientInfo()
                    dc.socketType = int(tmpdc[0])
                    dc.dataType = int(tmpdc[1])
                    dc.nTrodeId = int(tmpdc[2])
                    dc.nTrodeIndex = int(tmpdc[3])
                    dc.decimation = int(tmpdc[4])
                    dc.port = int(tmpdc[5])
                    dc.hostName = tmpdc[6].partition(b'\0')[0].decode('utf-8') # partition splits at the /0 char
                    # start the socket
                    if (dc.socketType == TRODESSOCKETTYPE_TCPIP):
                        tmpdatasocket = fssocket(dc.hostName, dc.port)
                    elif (dc.socketType == TRODESSOCKETTYPE_UDP):
                        tmpdatasocket = fsudpsocket(dc.hostName, dc.port)
                    else:
                        print('Error: unknown socket type', dc.socketType, 'requested')
                        sys.exit(-1)

                    if (tmpdatasocket.sock == -1):
                        print('Error connecting to trodes data socket on host', dc.hostName, ' port ',dc.port, '\n')
                    else:
                        # set this to the appropriate datatype by sending a mssage with the datatype and the ntrode index if relevant
                        tmpdatasocket.dataclientinfo = dc
                        datasocket.append(tmpdatasocket)
                        # also add the socket to the list of sockets for the select call
                        inputsocketlist.append(tmpdatasocket.sock)
                        datasocketfdlist.append(tmpdatasocket.sock)
                        if (dc.dataType in [TRODESDATATYPE_CONTINUOUS, TRODESDATATYPE_SPIKES]):
                            c = struct.pack('=BH', dc.dataType, dc.nTrodeIndex)
                            datasocket[-1].enabled = False
                        else:
                            # set the nTrodeIndex element to 0
                            c = struct.pack('=BH', dc.dataType, 0)
                            # enabled by default
                            datasocket[-1].enabled = True
                        # send the datatype message to trodes
                        if (dc.socketType == TRODESSOCKETTYPE_TCPIP):
                            # we only need to set the datatype for TCPIP sockets, as there a server could be for multiple datatypes
                            datasocket[-1].sendmessage(TRODESMESSAGE_SETDATATYPE, c)
                        # now send the decimation
                        c = struct.pack('=H', dc.decimation)
                        datasocket[-1].sendmessage(TRODESMESSAGE_SETDECIMATION, c)
                elif (message.type == TRODESMESSAGE_ENABLECONTDATASOCKET):
                    ntrodeindex, enable = struct.unpack('=hB', message.data)
                    # find the data client for this ntrode and enable it
                    for i in range(len(datasocket)):
                        if (datasocket[i].dataclientinfo.dataType == TRODESDATATYPE_CONTINUOUS) and \
                            (datasocket[i].dataclientinfo.nTrodeIndex == ntrodeindex):
                            datasocket[i].enabled = enable
                            # set the id of this ntrode in the chanRippleFilter structure so we can print its status easily
                            rtf.rippleFilter.chanRippleFilter[ntrodeindex].nTrodeId = datasocket[i].dataclientinfo.nTrodeId
                            rtf.rippleFilter.chanRippleFilter[ntrodeindex].enabled = enable
                    rtf.contNTrodeEnabled[ntrodeindex] = enable
                    rtf.rippleFilter.chanRippleFilter[ntrodeindex].enable = enable
                elif (message.type == TRODESMESSAGE_ENABLESPIKEDATASOCKET):
                    ntrodeindex, enable = struct.unpack('hB', message.data)
                    # the ntrode index is a qint16, enable is a byte
                    for i in range(len(datasocket)):
                        if (datasocket[i].dataclientinfo.dataType == TRODESDATATYPE_SPIKES) and \
                            (datasocket[i].dataclientinfo.nTrodeIndex == ntrodeindex):
                            datasocket[i].enabled = enable
                elif (message.type == TRODESMESSAGE_SETSCRIPTFUNCTIONVALID):
                    # get the function number
                    fnum, valid = struct.unpack('=hB', message.data)
                    # add or remove this function depending on valid
                    if valid:
                        if fnum not in rtf.validfunction:
                            rtf.validfunction.append(fnum)
                    else:
                        if fnum in rtf.validfunction:
                            rtf.validfunction.pop(rtf.validfunction.index(fnum))
                elif (message.type == TRODESMESSAGE_NDIGITALPORTS):
                    inports, outports = struct.unpack('=ii', message.data) # two ints
                    rtf.digInState = np.zeros(inports, dtype=np.bool_)
                    rtf.digOutState = np.zeros(outports, dtype=np.bool_)
                elif (message.type == TRODESMESSAGE_TURNONDATASTREAM):
                    # go through the sockets and turn on the ones that are enabled
                    data = bytes()
                    for s in datasocket:
                        if (s.enabled):
                            s.sendmessage(TRODESMESSAGE_TURNONDATASTREAM, data)
                elif (message.type == TRODESMESSAGE_TURNOFFDATASTREAM):
                    # go through the sockets and turn on the ones that are enabled
                    data = bytes()
                    for s in datasocket:
                        if (s.enabled):
                            s.sendmessage(TRODESMESSAGE_TURNOFFDATASTREAM, data)
                elif (message.type == TRODESMESSAGE_QUIT):
                    done = 1
                # check for messages from FSGui
                elif (message.type == FS_SET_RIPPLE_STIM_PARAMS):
                    print('updating ripple filter parameters, len ', len(message.data))
                    # parse the ripple parameters structure
                    rtf.rippleFilter.UpdateRippleParms(message.data)
                elif (message.type == FS_SET_SPATIAL_STIM_PARAMS):
                    print('updating spatial parameters')
                    rtf.spatialFilter.UpdateSpatialParms(message.data)
                elif (message.type == FS_SET_LATENCY_TEST_PARAMS):
                    print('updating latency parameters')
                    rtf.latencyFilter.UpdateLatencyParams(message.data)
                elif (message.type ==  FS_QUERY_RT_FEEDBACK_STATUS):
                    rtf.sendStatusUpdate()
                    rtf.rippleFilter.sendStatusUpdate(rtf.timestamp - rtf.lastStimTime)
                    rtf.spatialFilter.sendStatusUpdate()
                    rtf.latencyFilter.sendStatusUpdate()
                elif (message.type == FS_RESET_RT_FEEDBACK):
                    rtf.resetRealTimeProcessing()
                    print('FSDataPy: Reseting realtime filters\n')
                elif (message.type == FS_START_RT_FEEDBACK):
                    rtf.stimEnabled = True
                    print('FSDataPy: Received realtime START command\n')
                elif (message.type == FS_STOP_RT_FEEDBACK):
                    # stop any ongoing stimulation
                    rtf.stopStimulation()
                    rtf.stimEnabled = False
                    print('FSDataPy: Received realtime STOP command\n')
                elif (message.type == FS_START_LATENCY_TEST):
                    rtf.latencyTestEnabled = true
                    print('FSDataPy: Received Latency Test START command\n')
                elif (message.type == FS_STOP_LATENCY_TEST):
                    rtf.latencyTestEnabled = false
                    print('FSDataPy: Received Latency Test STOP command\n')
                else:
                    print('FSDataPy: Error - unknown message type', message.type, ' received\n')
    print('FSDataPy: exiting')



if __name__ == "__main__":
    main(sys.argv[1:])


