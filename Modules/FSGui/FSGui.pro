QT += opengl widgets xml multimedia multimediawidgets

CONFIG += debug


cache()

TARGET = FSGui
TEMPLATE = app

CONFIG += c++11

INCLUDEPATH += ../../Source/src-config
INCLUDEPATH += ../../Source/src-main

SOURCES += main.cpp\
            fsgui.cpp \
            fsConfigureAOut.cpp \
            fsConfigureStimulators.cpp \
            fsStimForm.cpp \
            ../../Source/src-config/configuration.cpp \
            ../../Source/src-main/trodesSocket.cpp \
    fsFeedbackTab.cpp \
    fsLatencyTab.cpp

HEADERS  += fsGUI.h \
            fsConfigureStimulators.h \
            fsConfigureAOut.h \
            fsStimForm.h \
#            fsLatencyGui.h \
            laser.h \
            fsSharedStimControlDefines.h \
            ../../Source/src-config/configuration.h \
            ../../Source/src-main/trodesSocket.h \
            ../../Source/src-main/trodesSocketDefines.h \
    fsFeedbackTab.h \
    fsLatencyTab.h

