#include <fsDefines.h>
#include <unistd.h>
#include <stdio.h>
#include <time.h>

extern SocketInfo clientData[MAX_SOCKETS]; // the structure for sending data
extern SocketInfo ECUMessage; // structure for accessing ECU socket
extern SocketInfo MCUMessage; // structure for accessing ECU socket

//extern FILE *latencyTestFile;

u32 triggered_time = 0;
int skipped = 0;
bool last_state = 0;
u32 last_cont_time = 0;

u32 lastPacketTime = 0;
u32 lastPacketType = 0;


u32 numsamp = 0;
void ProcessData(int datatype, char *data, int datalen, int nTrodeIndex, RTFilter *rtf)
{

    char *dataptr;
    short *electnumptr;

    bool inLockout = false;
    u32 stim_timestamp, curr_timestamp;
    int16_t *posdataptr;
    bool stim = true;
    int i, j;

    static int flag = 1;

    int port;
    char input;
    char state;

    struct timespec lastReceiveTime;

    /* the first element of the data is always the timestamp*/
    rtf->timestamp = *((u32 *) data);

    // update the lockout variables
    int stimTimeDiff = ((int)rtf->timestamp - (int)rtf->lastStimTime);
    if (rtf->rippleFilter.parm.enabled) {
        rtf->rippleFilter.inLockout = (stimTimeDiff < (int) rtf->rippleFilter.parm.lockoutTime);
        inLockout |= rtf->rippleFilter.inLockout;
    }
    if (rtf->spatialFilter.parm.enabled) {
        rtf->spatialFilter.inLockout = (stimTimeDiff < (int) rtf->spatialFilter.parm.lockoutTime);
        inLockout |= rtf->spatialFilter.inLockout;
    }
//    if (rtf->timestamp == 0)
//           fprintf(stderr, "FSDATA: zero timestamp.\n");


    // move past the timestamp to the data
    dataptr = data + sizeof(u32);

//    if (rtf->timestamp - lastPacketTime < 0 || rtf->timestamp - lastPacketTime > 20) {
//        fprintf(stderr, "Packet Time Error: ts %u last %d diff %d %d\n", rtf->timestamp, lastPacketTime, rtf->timestamp - lastPacketTime, lastPacketType, datatype);
//    }
    lastPacketTime = rtf->timestamp;
    lastPacketType = datatype;

    switch (datatype) {
        case TRODESDATATYPE_CONTINUOUS: {
            struct timespec *sendTime;
            sendTime = (struct timespec*) (dataptr + sizeof(int16_t));
            struct timespec receiveTime;

#ifdef __linux__
            clock_gettime(CLOCK_MONOTONIC, &receiveTime);
#endif /** linux **/

            if((rtf->timestamp - last_cont_time) > 20) {
                fprintf(stderr, "Time difference error: %d\n",rtf->timestamp - last_cont_time);
            } else if((rtf->timestamp - last_cont_time) <0) {
                fprintf(stderr, "Packets out of order: %d\n",rtf->timestamp - last_cont_time);
            }
            last_cont_time = rtf->timestamp;

            if (rtf->latencyTestEnabled) {
                if (rtf->latencyFilter.processContinuousData(rtf->timestamp)) {
                    SendHardwareMessage(ECUMessage.fd, (char *)&rtf->latencyFilter.parm.internal.stateScriptFnNum,
                            sizeof(rtf->latencyFilter.parm.internal.stateScriptFnNum), ECUMessage.socketAddress);
                }
            }
            //fprintf(stderr, "FSData: Process Cont Data (%d, %lf)\n", (double) *((short *) dataptr));
            if (rtf->rippleFilter.parm.enabled) {
//                if (rtf->timestamp % 30000 == 0) {
//                    fprintf(stderr, "processing ripple data %d, timestamp = %u, laststimtime = %u, lockoutTime = %u\n", *((short *) dataptr), rtf->timestamp, rtf->lastStimTime, rtf->rippleFilter.parm.lockoutTime);
//                }
                // we need to send in the lockout status because the filter is set to the mean value during lockout
                stim = rtf->rippleFilter.ProcessRippleData(nTrodeIndex, (double) *((short *) dataptr), rtf->stimEnabled,
                                                           rtf->timestamp, sendTime, &receiveTime) && stim;
                //fprintf(stderr,"stim %d",stim);
            }
            if (rtf->thetaFilter.parm.enabled) {
                // process this data point through the theta filter
                //stim = rtf->rippleFilter.ProcessThetaData(nTrodeIndex, (double) *((short *) dataptr)) && stim;
            }
            if (rtf->spatialFilter.parm.enabled) {
                // the spatialFilter stim value will be updated by position data
                if (!rtf->rippleFilter.parm.enabled && !rtf->thetaFilter.parm.enabled) {
                    // Special condition if only spatial filter is active, you only want to stim on state change
                    stim = stim && rtf->spatialFilter.stimOn && rtf->spatialFilter.stimChanged;
                } else {
                     stim = stim && rtf->spatialFilter.stimOn;
                }
            }

        }
        break;
        case TRODESDATATYPE_POSITION: {
            /* position messages consist of three numbers,  the x position, the y position and the camera ID.
               We ignore the uint_8t camera ID for the moment. */
            posdataptr = (int16_t*)dataptr;
            //fprintf(stderr, "FSData got position %d %d\n", posdataptr[0], posdataptr[1]);
            rtf->spatialFilter.xpos = posdataptr[0];
            rtf->spatialFilter.ypos = posdataptr[1];
            if (rtf->rippleFilter.parm.enabled) {

                stim = stim && rtf->rippleFilter.stimOn && !inLockout;
            }
            if (rtf->thetaFilter.parm.enabled) {
                stim = stim && rtf->thetaFilter.stimOn;
            }
            if (rtf->spatialFilter.parm.enabled) {
                //fprintf(stderr, "about to process spatial data %d %d %d\n", posdataptr[0], posdataptr[1], rtf->timestamp);
                if (!rtf->rippleFilter.parm.enabled && !rtf->thetaFilter.parm.enabled) {
                    // Special condition if only spatial filter is active, you only want to stim on state change
                    bool lastStim = rtf->spatialFilter.stimOn;
                    stim = rtf->spatialFilter.ProcessSpatialData(posdataptr[0], posdataptr[1], rtf->timestamp) && stim &&
                            rtf->spatialFilter.stimChanged;
                } else {

                    stim = rtf->spatialFilter.ProcessSpatialData(posdataptr[0], posdataptr[1], rtf->timestamp) && stim;
                }

                //fprintf(stderr, "Processed spatial data\n");

            }
            break;
        }
        case TRODESDATATYPE_DIGITALIO: {

           // Digital IO information consists of an integer port number, a character input or output value and a character state value
           port = *((int *) dataptr);
           dataptr += sizeof(int);
           input = dataptr[0];
           state = dataptr[1];
           //fprintf(stderr, "%d %d %d %d\n",rtf->timestamp, port, input, state);
           if (rtf->rippleFilter.parm.enabled) {
               stim = stim && rtf->rippleFilter.stimOn;
           }
           if (rtf->thetaFilter.parm.enabled) {
               stim = stim && rtf->thetaFilter.stimOn;
           }
           if (rtf->spatialFilter.parm.enabled) {
               if (!rtf->rippleFilter.parm.enabled && !rtf->thetaFilter.parm.enabled) {
                   // Special condition if only spatial filter is active, you only want to stim on state change
                   stim = stim && rtf->spatialFilter.stimOn && rtf->spatialFilter.stimChanged;
               } else {
                    stim = stim && rtf->spatialFilter.stimOn;
               }
           }

           if (rtf->latencyTestEnabled) {
               if (rtf->latencyFilter.processDigitalIOData(rtf->digOutState, rtf->timestamp, port,
                                                           (bool) input, (bool) state)) {
                    // Trigger function again, used if the shortcut function is a flip, reset the state from one to zero
                    SendHardwareMessage(ECUMessage.fd, (char *)&rtf->latencyFilter.parm.internal.stateScriptFnNum,
                            sizeof(rtf->latencyFilter.parm.internal.stateScriptFnNum), ECUMessage.socketAddress);

               }
           }
           // update the state vectors
           if (input) {
               rtf->digInState[port] = state;
           }
           else {
               rtf->digOutState[port] = state;
           }
           break;
        }
    }

    // check to see if the digital port gate is set
    if (rtf->digOutGatePort != 0) {
        stim = stim && (rtf->digOutState[rtf->digOutGatePort] == 0);
    }

    // check to see if stimulation is enabled
    if (rtf->stimEnabled) {
        if (stim && !inLockout) {
            rtf->spatialFilter.stimChanged = false;
            rtf->lastStimTime = rtf->timestamp;
            // Ripple filter also needs last stim time for linear ramp filter during lockout
            rtf->rippleFilter.lastStimTime = rtf->timestamp;
            if (ECUMessage.fd != -1) {
                rtf->startStimulation(ECUMessage.fd, ECUMessage.socketAddress);
            }
            else {
                fprintf(stderr, "tried to start Stim - no ECU connection\n");
            }
            if (MCUMessage.fd != -1) {
                //immediately settle
                rtf->sendSettleCommand(MCUMessage.fd, MCUMessage.socketAddress);
            }
            else {
                fprintf(stderr, "tried to settle amplifiers - no MCU connection\n");
            }
        }
        else if ((rtf->spatialFilter.stimChanged && !inLockout)) {
            rtf->spatialFilter.stimChanged = false;
            // we had been stimulating but we need to stop
            if (ECUMessage.fd != -1) {
                rtf->stopStimulation(ECUMessage.fd, ECUMessage.socketAddress);
            }
            else {
                fprintf(stderr, "tried to stop Stim - no ECU connection\n");
            }
        }
    }
}

void InitPulseArray(void)
{

}





