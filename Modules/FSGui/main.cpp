#include "fsGUI.h"
#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    FSGui w(a.arguments());
    w.show();

    return a.exec();
}
