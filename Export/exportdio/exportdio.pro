#-------------------------------------------------
#
# Project created by QtCreator 2015-11-16T09:07:52
#
#-------------------------------------------------

QT       += core xml gui network
greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

#QT       -= gui

TARGET = exportdio
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app

INCLUDEPATH  += ../../Source/src-main
INCLUDEPATH  += ../../Source/src-config
INCLUDEPATH  += ../../Source/src-threads
INCLUDEPATH  += ../
INCLUDEPATH  += ../exportdio


#unix: QMAKE_CXXFLAGS += -D__STDC_CONSTANT_MACROS
QMAKE_CFLAGS += -g -O3
# Requied for some C99 defines
DEFINES += __STDC_CONSTANT_MACROS


SOURCES += main.cpp\
           ../../Source/src-config/configuration.cpp \
           ../../Source/src-main/iirFilter.cpp \
           ../abstractexporthandler.cpp \
           ../../Source/src-threads/spikeDetectorThread.cpp \
           ../../Source/src-main/trodesSocket.cpp\
            datexporthandler.cpp

HEADERS  += ../../Source/src-config/configuration.h \
            ../../Source/src-main/iirFilter.h \
            ../abstractexporthandler.h \
            ../../Source/src-threads/spikeDetectorThread.h \
            ../../Source/src-main/trodesSocket.h\
            datexporthandler.h

