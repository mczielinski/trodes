#include "datexporthandler.h"
#include <algorithm>
#include <iostream>

DATExportHandler::DATExportHandler(QStringList arguments):
    AbstractExportHandler(arguments)
{

    maxGapSizeInterpolation = 0;
    invertSpikes = true;

    parseArguments();

    /*
    Do custom argument checks here like this:
    if (ARGVAL != REQUIREMENT) {
        qDebug() << "Error: ....";
        argumentReadOk = false;
    }   
    */

    if ((maxGapSizeInterpolation > 0) || (maxGapSizeInterpolation < 0)) {
         qDebug() << "Error: interp must be 0.";
         argumentReadOk = false;
    }

    if (outputSamplingRate != -1) {
        qDebug() << "Error: outputrate must stay unchanged for spike processing.";
        qDebug() << outputSamplingRate;
        argumentReadOk = false;
    }

    //Parse custom arguments
    //parseCustomArguments(argumentsProcessed);

    if (argumentsProcessed != argumentList.length()-1) {
        _argumentsSupported = false;
        return;
    }
}

DATExportHandler::~DATExportHandler()
{

}

void DATExportHandler::printHelpMenu() {
    //printf("-outputrate <integer>  -- The sampling rate of the output file. \n"
    //       );

    printf("\nUsed to extract Auxilliary analog I/O channels from a raw rec file and save to individual files for each channel. \n");
    printf("Usage:  exportanalog -rec INPUTFILENAME OPTION1 VALUE1 OPTION2 VALUE2 ...  \n\n"
           "Input arguments \n");
    //printf("Defaults:\n -invert 1 -outputrate -1 (full sampling, can't be changed') \n -usespikefilters 1 \n -interp 0 \n -userefs 1 \n\n\n");
    //printf("-invert <1 or 0> -- Whether or not to invert spikes to go upward\n");


    //AbstractExportHandler::printHelpMenu();
}

void DATExportHandler::parseArguments() {
    //Parse extra arguments not handled by the base class


    int optionInd = 1;
    while (optionInd < argumentList.length()) {

        if ((argumentList.at(optionInd).compare("-h",Qt::CaseInsensitive)==0)) {
            //printCustomMenu();
            //return;
            printHelpMenu();
        }
        optionInd++;
    }



    AbstractExportHandler::parseArguments();
}


int DATExportHandler::processData() {


    qDebug() << "Exporting analog data...";

    //Calculate the packet positions for each channel that we are extracting, plus
    //other critical info (number of saved channels, reference info, etc).
    calculateChannelInfo();
    createFilters();

    if (!openInputFile()) {
        return -1;
    }


    //Create a directory for the output files located in the same place as the source file
    QFileInfo fi(recFileName);
    QString fileBaseName;
    if (outputFileName.isEmpty()) {
        fileBaseName = fi.baseName();
    } else {
        fileBaseName = outputFileName;
    }
    //QString saveLocation = fi.absolutePath()+QString(QDir::separator())+"spikes"+QString(QDir::separator());
    QString saveLocation = fi.absolutePath()+QString(QDir::separator())+fileBaseName+".analog"+QString(QDir::separator());
    QDir dir(saveLocation);
    if (!dir.exists()) {
        if (!dir.mkpath(".")) {
            qDebug() << "Error creating analog directory.";
            return -1;
        }
    }

    //Pointers to the neuro files
    /*QList<QFile*> neuroFilePtrs;
    QList<QDataStream*> neuroStreamPtrs;

    QList<QFile*> timeFilePtrs;
    QList<QDataStream*> timeStreamPtrs;*/



    QString infoLine;
    QString fieldLine;

    QList<QFile*> timeFilePtrs;
    QList<QDataStream*> timeStreamPtrs;

    //Create an output file for the timestamps
    //*****************************************
    timeFilePtrs.push_back(new QFile);
    timeFilePtrs.last()->setFileName(saveLocation+fileBaseName+QString(".timestamps.dat"));
    if (!timeFilePtrs.last()->open(QIODevice::WriteOnly)) {
        qDebug() << "Error creating output file.";
        return -1;
    }
    timeStreamPtrs.push_back(new QDataStream(timeFilePtrs.last()));
    timeStreamPtrs.last()->setByteOrder(QDataStream::LittleEndian);

    //Write the current settings to file

    timeFilePtrs.last()->write("<Start settings>\n");
    infoLine = QString("Description: Time stamps\n");
    timeFilePtrs.last()->write(infoLine.toLocal8Bit());
    infoLine = QString("Byte_order: little endian\n");
    timeFilePtrs.last()->write(infoLine.toLocal8Bit());
    infoLine = QString("Original_file: ") + fi.fileName() + "\n";
    timeFilePtrs.last()->write(infoLine.toLocal8Bit());
    infoLine = QString("Clock rate: %1\n").arg(hardwareConf->sourceSamplingRate);
    timeFilePtrs.last()->write(infoLine.toLocal8Bit());
    infoLine = QString("Time_offset: %1\n").arg(startOffsetTime);
    timeFilePtrs.last()->write(infoLine.toLocal8Bit());



    fieldLine.clear();
    fieldLine += "Fields: ";
    fieldLine += "<time uint32>";
    fieldLine += "\n";
    timeFilePtrs.last()->write(fieldLine.toLocal8Bit());
    timeFilePtrs.last()->write("<End settings>\n");
    timeFilePtrs.last()->flush();

    //Create an output file for the analog data
    //*****************************************
    QList<QFile*> analogFilePtrs;
    QList<QDataStream*> analogStreamPtrs;
    QList<int> analogChannelInds;
    //QList<uint8_t> DIOLastValue;



    for (int auxChInd = 0; auxChInd < headerConf->headerChannels.length(); auxChInd++) {

            if (headerConf->headerChannels[auxChInd].dataType == DeviceChannel::INT16TYPE) {
                //This is an analog channel
                analogChannelInds.push_back(auxChInd);
                analogFilePtrs.push_back(new QFile);

                analogFilePtrs.last()->setFileName(saveLocation+fileBaseName+QString(".analog_%1.dat").arg(headerConf->headerChannels[auxChInd].idString));
                if (!analogFilePtrs.last()->open(QIODevice::WriteOnly)) {
                    qDebug() << "Error creating output file.";
                    return -1;
                }
                analogStreamPtrs.push_back(new QDataStream(analogFilePtrs.last()));
                analogStreamPtrs.last()->setByteOrder(QDataStream::LittleEndian);

                //Write the current settings to file

                analogFilePtrs.last()->write("<Start settings>\n");
                infoLine = QString("Description: State change data for one digital channel. Display_order is 1-based\n");

                analogFilePtrs.last()->write(infoLine.toLocal8Bit());
                infoLine = QString("Byte_order: little endian\n");
                analogFilePtrs.last()->write(infoLine.toLocal8Bit());
                infoLine = QString("Original_file: ") + fi.fileName() + "\n";
                analogFilePtrs.last()->write(infoLine.toLocal8Bit());
                if (headerConf->headerChannels[auxChInd].input) {
                    infoLine = QString("Direction: input\n");
                } else {
                    infoLine = QString("Direction: output\n");
                }
                analogFilePtrs.last()->write(infoLine.toLocal8Bit());
                infoLine = QString("ID: %1\n").arg(headerConf->headerChannels[auxChInd].idString);
                analogFilePtrs.last()->write(infoLine.toLocal8Bit());
                infoLine = QString("Display_order: %1\n").arg(auxChInd+1);
                analogFilePtrs.last()->write(infoLine.toLocal8Bit());
                infoLine = QString("Clockrate: %1\n").arg(hardwareConf->sourceSamplingRate);
                analogFilePtrs.last()->write(infoLine.toLocal8Bit());

                fieldLine.clear();
                fieldLine += "Fields: ";
                //fieldLine += "<time uint32>";
                fieldLine += "<voltage int16>";


                fieldLine += "\n";
                analogFilePtrs.last()->write(fieldLine.toLocal8Bit());


                analogFilePtrs.last()->write("<End settings>\n");
                analogFilePtrs.last()->flush();

            }
    }




    //************************************************
    int inputFileInd = 0;

    while (inputFileInd < recFileNameList.length()) {
        if (inputFileInd > 0) {
            //There are multiple files that need to be stiched together. It is assumed that they all have
            //the exact same header section.
            recFileName = recFileNameList.at(inputFileInd);
            uint32_t lastFileTStamp = currentTimeStamp;


            qDebug() << "\nAppending from file: " << recFileName;
            QFileInfo fi(recFileName);

            if (!fi.exists()) {
                qDebug() << "File could not be found: " << recFileName;
                break;
            }
            if (!openInputFile()) {
                qDebug() << "Error: it appears that the file does not have an identical header to the last file. Cannot append to file.";
                return -1;
            }
            for (int i=0; i < channelFilters.length(); i++) {
                channelFilters[i]->resetHistory();
            }
            if (currentTimeStamp < lastFileTStamp) {
                qDebug() << "Error: timestamps do not begin with greater value than the end of the last file. Aborting.";
                return -1;
            }



        }

        int16_t tmpVal;
        //Process the data and stream results to output files
        while(!filePtr->atEnd()) {
            //Read in a packet of data to make sure everything looks good
            if (!(filePtr->read(buffer.data(),filePacketSize) == filePacketSize)) {
                //We have reached the end of the file
                break;
            }
            //Find the time stamp
            bufferPtr = buffer.data()+packetTimeLocation;
            tPtr = (uint32_t *)(bufferPtr);
            currentTimeStamp = *tPtr + startOffsetTime;
            *timeStreamPtrs.at(0) << currentTimeStamp;

            for (int chInd=0; chInd < analogChannelInds.length(); chInd++) {

                bufferPtr = buffer.data()+headerConf->headerChannels[analogChannelInds[chInd]].startByte;
                tmpVal = (int16_t)(*bufferPtr);
                *analogStreamPtrs.at(chInd) << tmpVal;

            }

            //Print the progress to stdout
            printProgress();

            lastTimeStamp = currentTimeStamp;
            //pointsSinceLastLog = (pointsSinceLastLog+numberOfPointsToProcess)%decimation;


        }
        filePtr->close();
        printf("\rDone\n");
        inputFileInd++;
    }



    for (int i=0; i < analogFilePtrs.length(); i++) {
        analogFilePtrs[i]->flush();
        analogFilePtrs[i]->close();
    }

    for (int i=0; i < timeFilePtrs.length(); i++) {
        timeFilePtrs[i]->flush();
        timeFilePtrs[i]->close();
    }

    return 1;
}

