-----------USB setup----------------

1)  sudo -s 
  or, if sudo is not available on your system: 
    su

Promotes you to super-user, with installation privileges.  If you're
already root, then step 3 (and step 7) is not necessary.

2)  cp ../../Source/Libraries/Linux/libftd2xx.* /usr/local/lib

Copies the libraries to a central location.

3)  chmod 0755 /usr/local/lib/libftd2xx.so.1.3.6

Allows non-root access to the shared object.

4)  ln -sf /usr/local/lib/libftd2xx.so.1.3.6 /usr/local/lib/libftd2xx.so

5) cp spikegadgets.rules /etc/udev/rules.d

6) restart udev

7) exit

