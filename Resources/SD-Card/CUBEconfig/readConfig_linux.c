
#include <string.h>
#include <stdio.h>
#include <stdint.h>
#include <unistd.h>


#define MAX_FNAME_LENGTH 1000
#define INPUT_BUFFER_SIZE 32768
/*-----------------------------------------------------------------------*/
/* Main                                                                  */


int main (int argc, char *argv[])
{
  size_t res;
  int pd, i, j, count;
  int last_packet_start;
  FILE *input_fp;
  FILE *output_fp;
  uint8_t Buff[INPUT_BUFFER_SIZE];			/* Working buffer */
  char input_fname[MAX_FNAME_LENGTH];
  char output_fname[MAX_FNAME_LENGTH];

  
  // turn off output buffering
  setvbuf(stdout, 0, _IONBF, 0);
  printf("\n*** readConfig 1.0 ***\n");

  if (argc < 1) {
       printf("\nUsage: pcheck <raw data file>\n");
       return 2;
  }

  if (argc > 1) {
    strncpy(input_fname, argv[1], MAX_FNAME_LENGTH);
    if (input_fname[MAX_FNAME_LENGTH-1] != '\0') {
      printf("\nMaximum input file name length exceeded.\n");
      return 2;
    }
    else if ( access(input_fname, F_OK ) == -1 ) {
       printf("\nUsage: pcheck <raw data file>\n");
       return 2;
    }
  }

  input_fp = fopen(input_fname, "r");
  res = fread(Buff, sizeof(uint8_t), INPUT_BUFFER_SIZE, input_fp);

  // read the configuration sector
  if (res != INPUT_BUFFER_SIZE) {
    printf("rc=%d\n", (int) res);
    return 2;
  }

  count = 0;
  for (i = 0; i < 32; i++) {
    printf("Group %02d: ", i);
    if (Buff[i]) {
      printf("Card(s) ");
      for (j = 0; j < 8; j++) {
        if ((Buff[i] >> j) & 0x01) {
          count++;
          if (j == 0)
            printf("%d",j);
          else
            printf(",%d",j);
        }
      }
      printf(" enabled\n");
    }
  }
  printf("\n%d channels enabled, packet size = %d\n", count, 2*count+6);

  i = 32;
  while (i < INPUT_BUFFER_SIZE) {
    printf("%d 0x",i);
    for (j = 0; j < 32; j++)
      printf("%02x ", Buff[i+j]);
    printf("\n");
    i = i+32;
  }


  return 0;

}
  


