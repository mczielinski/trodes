var searchData=
[
  ['nano',['nano',['../structlibusb__version.html#afbe4cf431bea706294667e333878508c',1,'libusb_version']]],
  ['ntrode',['NTrode',['../class_n_trode.html',1,'']]],
  ['ntrodedisplaywidget',['ntrodeDisplayWidget',['../classntrode_display_widget.html',1,'']]],
  ['ntrodestream',['nTrodeStream',['../classn_trode_stream.html',1,'']]],
  ['ntrodetable',['NTrodeTable',['../class_n_trode_table.html',1,'']]],
  ['num_5faltsetting',['num_altsetting',['../structlibusb__interface.html#afc930be16a60980424f64a88b23c10e9',1,'libusb_interface']]],
  ['num_5fiso_5fpackets',['num_iso_packets',['../structlibusb__transfer.html#a87d725a5521c26832fdc13611220014d',1,'libusb_transfer']]]
];
