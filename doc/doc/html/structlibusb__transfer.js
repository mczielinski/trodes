var structlibusb__transfer =
[
    [ "actual_length", "structlibusb__transfer.html#a7e858f07c48a271a62209d11376ae607", null ],
    [ "buffer", "structlibusb__transfer.html#a7fa594567e074191ce8f28b5fb4a3bea", null ],
    [ "callback", "structlibusb__transfer.html#a69c6df011ec23ff3e481cc98bfff0623", null ],
    [ "dev_handle", "structlibusb__transfer.html#adaaf06aeb5ab2a8819e75310ec253f7a", null ],
    [ "endpoint", "structlibusb__transfer.html#a0fcfd057f53b0e1bff6f1567761c231a", null ],
    [ "flags", "structlibusb__transfer.html#ae26c063df30c2e29835212aad98c6e06", null ],
    [ "iso_packet_desc", "structlibusb__transfer.html#a0de0b13dfae411fc36217d35eb89b138", null ],
    [ "length", "structlibusb__transfer.html#a68c023e1f40b50aa8604a2495b6a391e", null ],
    [ "num_iso_packets", "structlibusb__transfer.html#a87d725a5521c26832fdc13611220014d", null ],
    [ "status", "structlibusb__transfer.html#a64b2e70e76d52a7cd23daa3cd4fb397e", null ],
    [ "timeout", "structlibusb__transfer.html#a9a12af15ca5b482f5dcaebd26a848cbb", null ],
    [ "type", "structlibusb__transfer.html#a7c9fa575986fe9f23bbecb26b766dff1", null ],
    [ "user_data", "structlibusb__transfer.html#ab75ab3e7185f08e07a1ae858a35ebb7b", null ]
];