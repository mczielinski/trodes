var structlibusb__device__descriptor =
[
    [ "bcdDevice", "structlibusb__device__descriptor.html#afb5e0fc6f0cfe51de900d35506fb9317", null ],
    [ "bcdUSB", "structlibusb__device__descriptor.html#af0682f293291db942b5d253092d472e4", null ],
    [ "bDescriptorType", "structlibusb__device__descriptor.html#a63c4b109725ce829fc81c5c9cad8c87a", null ],
    [ "bDeviceClass", "structlibusb__device__descriptor.html#ab86c8f43e75d4d54fccbc199cfa9703b", null ],
    [ "bDeviceProtocol", "structlibusb__device__descriptor.html#a480a0b5345a2e59987e20fa7247c3f0e", null ],
    [ "bDeviceSubClass", "structlibusb__device__descriptor.html#a9c3a91102d3d53d9414d0dda0191c5ab", null ],
    [ "bLength", "structlibusb__device__descriptor.html#affda0be3fe1c37092ddc7cb120428f30", null ],
    [ "bMaxPacketSize0", "structlibusb__device__descriptor.html#a3b60170f077c9b26fc9f86e0cdb1d28a", null ],
    [ "bNumConfigurations", "structlibusb__device__descriptor.html#a0f3f80cd931628a0531a815b59d067dd", null ],
    [ "idProduct", "structlibusb__device__descriptor.html#a568d479fb0b76ea572a739c961fd3047", null ],
    [ "idVendor", "structlibusb__device__descriptor.html#ad26c5e2fbd16ce3ee143f4bac5d49bda", null ],
    [ "iManufacturer", "structlibusb__device__descriptor.html#ab9e9a064265f2833d1d3f216f15a487a", null ],
    [ "iProduct", "structlibusb__device__descriptor.html#a20082f9e03f1cbffef3d9472a8dcfd64", null ],
    [ "iSerialNumber", "structlibusb__device__descriptor.html#a327e8ede51268b38955ef035cf6de875", null ]
];